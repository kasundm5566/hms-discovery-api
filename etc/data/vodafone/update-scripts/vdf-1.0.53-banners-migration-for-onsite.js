conn = new Mongo("localhost:27017");
db = conn.getDB("appstore");

var id = 1;
var banners = [
    {display_location: "Popular-apps"},
    {display_location: "Categories"},
    {display_location: "Featured-apps"},
    {display_location: "Downloadable-apps"},
    {display_location: "My-apps"},
    {display_location: "Home"},
    {display_location: "Dev"},
    {display_location: "Subscription-apps"}
];

// Update the banners collection
db.banners.remove();

// Add banners included in db (app_banners) to the banners collection
banners.forEach(function (banner) {
    db.apps.find().forEach(function (app) {
        app.app_banners.forEach(function (appBanner) {
            var document = {"_id": NumberInt(id), "display_location": banner.display_location, "link": "", "description": "", "image_url": appBanner.url};
            db.banners.insert(document);
            id++;
        });
    });
});

print("> updated the banners collection.");
