conn = new Mongo("localhost:27017");
db = conn.getDB("appstore");

var result = [];
var id = 1;
var imagesBaseUrl = "/images/applications/icons/category/"
var categories = [
    {name: "Business", description: "Business Applications", image_url: imagesBaseUrl + "business-icon.png"},
    {name: "Education", description: "Education Applications", image_url: imagesBaseUrl + "education-icon.png"},
    {name: "Entertainment", description: "Entertainment Applications", image_url: imagesBaseUrl + "entertainment-icon.png"},
    {name: "Fun & Jokes", description: "Fun & Jokes Applications", image_url: imagesBaseUrl + "fun&jokes-icon.png"},
    {name: "Health", description: "Health Applications", image_url: imagesBaseUrl + "health-icon.png"},
    {name: "Information", description: "Information Applications", image_url: imagesBaseUrl + "information-icon.png"},
    {name: "News", description: "News Applications", image_url: imagesBaseUrl + "news-icon.png"},
    {name: "Other", description: "Other Applications", image_url: imagesBaseUrl + "other-icon.png"},
    {name: "Social", description: "Social Applications", image_url: imagesBaseUrl + "social-icon.png"},
    {name: "Sports", description: "Sports Applications", image_url: imagesBaseUrl + "sports-icon.png"},
    {name: "Utilities", description: "Utilities Applications", image_url: imagesBaseUrl + "utilities-icon.png"}
];

// Update the categories collection
db.categories.remove();
categories.forEach(function (category) {
//    var document = {"_id": NumberInt(id), "name": category.name, "description": category.description, "image_url": category.image_url, "appIds": []};
    var document = {"_id": category.name, "name": category.name, "description": category.description, "image_url": category.image_url, "appIds": []};
    db.categories.insert(document);
    id++;
});
print("> updated the categories collection.");

// Change the type of the category field (String -> Array)
db.eval(changeCategoryFieldType);
print("> changed the type of the apps.category field (String -> Array).");

db.apps.find().forEach(function (app) {
    app.category.forEach(function (appCategoryName) {
        categories.forEach(function (category) {
            if (appCategoryName == category.name) {
                db.categories.update({"name": category.name}, { $addToSet: { appIds: app._id } });
            }
        })
    });
});
print("> added appIds to the relevant categories.")


db.apps.update({}, {$set: {rate_count: 0}}, { multi: true });
print("> added field rate_count to apps.")

//db.eval(changeRatingFieldType);

function changeCategoryFieldType() {
    db.apps.find({ "category": { $type: 2 } }).snapshot().forEach(
        function (x) {
            if (!Array.isArray(x.category)) {
                x.category = [ x.category ];
                db.apps.save(x);
            }
        }
    );
}

/*function changeRatingFieldType() {
 db.apps.find({ 'rating': { $type: 18 } }).forEach(
 function (x) {
 x.rating = parseFloat(x.rating); // convert field to double
 db.apps.save(x);
 }
 );
 }*/
