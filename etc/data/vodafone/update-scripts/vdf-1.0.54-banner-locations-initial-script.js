conn = new Mongo("localhost:27017");
db = conn.getDB("appstore");

var id = 1;
var bannersLocations = [
    {display_location: "Popular-apps", name: "Popular apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Categories", name: "Categories", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Featured-apps", name: "Featured apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Downloadable-apps", name: "Downloadable apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "My-apps", name: "My apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},

    {display_location: "Home", name: "Home", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Dev", name: "Developer profile", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Subscription-apps", name: "Subscription apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Top-rated-apps", name: "Top rated apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Newly-added-apps", name: "Newly added apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},

    {display_location: "Most-used-apps", name: "Most used apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Free-apps", name: "Free apps", min_width: NumberInt(1210), max_width: NumberInt(1310), min_height: NumberInt(270), max_height: NumberInt(370), size: NumberInt(512)},
    {display_location: "Left-panel", name: "Left panel", min_width: NumberInt(110), max_width: NumberInt(210), min_height: NumberInt(650), max_height: NumberInt(750), size: NumberInt(512)}
];

// Create and update banner_locations collection
db.banner_locations.remove();

// Add data to the banners collection
bannersLocations.forEach(function (bannerLocation) {
    var document = {"_id": NumberInt(id), "display_location": bannerLocation.display_location, "name":bannerLocation.name, "min_width": bannerLocation.min_width,
        "max_width": bannerLocation.max_width, "min_height": bannerLocation.min_height, "max_height": bannerLocation.max_height,
        "size": bannerLocation.size
    };
    db.banner_locations.insert(document);
    id++;
});

print("> updated the banner_locations collection.");