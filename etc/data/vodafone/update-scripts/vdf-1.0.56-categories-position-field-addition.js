conn = new Mongo("localhost:27017");
db = conn.getDB("appstore");

var id = 1;
db.categories.find().forEach(function (category) {
    db.categories.update({"name": category.name}, {$set: {"position": NumberInt(id)}})
    id++;
});
print("> updated the categories collection by adding posision field.");
