conn = new Mongo("localhost:27017");
db = conn.getDB("appstore");

var id = 1;
var banners = [
    {display_location: "Popular-apps", image_url: "/images/applications/icons/banners-Popular-apps/Damodar1_banner.jpg"},
    {display_location: "Popular-apps", image_url: "/images/applications/icons/banners-Popular-apps/FijiliveM_banner.png"},
    {display_location: "Popular-apps", image_url: "/images/applications/icons/banners-Popular-apps/Rythm_banner.png"},
    {display_location: "Popular-apps", image_url: "/images/applications/icons/banners-Popular-apps/shopandsave_banner.jpg"},

    {display_location: "Categories", image_url: "/images/applications/icons/banners-Categories/Damodar1_banner.jpg"},
    {display_location: "Categories", image_url: "/images/applications/icons/banners-Categories/FijiliveM_banner.png"},
    {display_location: "Categories", image_url: "/images/applications/icons/banners-Categories/Rythm_banner.png"},
    {display_location: "Categories", image_url: "/images/applications/icons/banners-Categories/shopandsave_banner.jpg"},

    {display_location: "Featured-apps", image_url: "/images/applications/icons/banners-Featured-apps/Damodar1_banner.jpg"},
    {display_location: "Featured-apps", image_url: "/images/applications/icons/banners-Featured-apps/FijiliveM_banner.png"},
    {display_location: "Featured-apps", image_url: "/images/applications/icons/banners-Featured-apps/Rythm_banner.png"},
    {display_location: "Featured-apps", image_url: "/images/applications/icons/banners-Featured-apps/shopandsave_banner.jpg"},

    {display_location: "Downloadable-apps", image_url: "/images/applications/icons/banners-Downloadable-apps/Damodar1_banner.jpg"},
    {display_location: "Downloadable-apps", image_url: "/images/applications/icons/banners-Downloadable-apps/FijiliveM_banner.png"},
    {display_location: "Downloadable-apps", image_url: "/images/applications/icons/banners-Downloadable-apps/Rythm_banner.png"},
    {display_location: "Downloadable-apps", image_url: "/images/applications/icons/banners-Downloadable-apps/shopandsave_banner.jpg"},

    {display_location: "My-apps", image_url: "/images/applications/icons/banners-My-apps/Damodar1_banner.jpg"},
    {display_location: "My-apps", image_url: "/images/applications/icons/banners-My-apps/FijiliveM_banner.png"},
    {display_location: "My-apps", image_url: "/images/applications/icons/banners-My-apps/Rythm_banner.png"},
    {display_location: "My-apps", image_url: "/images/applications/icons/banners-My-apps/shopandsave_banner.jpg"},

    {display_location: "Home", image_url: "/images/applications/icons/banners-Home/Damodar1_banner.jpg"},
    {display_location: "Home", image_url: "/images/applications/icons/banners-Home/FijiliveM_banner.png"},
    {display_location: "Home", image_url: "/images/applications/icons/banners-Home/Rythm_banner.png"},
    {display_location: "Home", image_url: "/images/applications/icons/banners-Home/shopandsave_banner.jpg"},

    {display_location: "Dev", image_url: "/images/applications/icons/banners-Dev/Damodar1_banner.jpg"},
    {display_location: "Dev", image_url: "/images/applications/icons/banners-Dev/FijiliveM_banner.png"},
    {display_location: "Dev", image_url: "/images/applications/icons/banners-Dev/Rythm_banner.png"},
    {display_location: "Dev", image_url: "/images/applications/icons/banners-Dev/shopandsave_banner.jpg"},

    {display_location: "Subscription-apps", image_url: "/images/applications/icons/banners-Subscription-apps/Damodar1_banner.jpg"},
    {display_location: "Subscription-apps", image_url: "/images/applications/icons/banners-Subscription-apps/FijiliveM_banner.png"},
    {display_location: "Subscription-apps", image_url: "/images/applications/icons/banners-Subscription-apps/Rythm_banner.png"},
    {display_location: "Subscription-apps", image_url: "/images/applications/icons/banners-Subscription-apps/shopandsave_banner.jpg"},

    {display_location: "Top-rated-apps", image_url: "/images/applications/icons/banners-Top-rated-apps/Damodar1_banner.jpg"},
    {display_location: "Top-rated-apps", image_url: "/images/applications/icons/banners-Top-rated-apps/FijiliveM_banner.png"},
    {display_location: "Top-rated-apps", image_url: "/images/applications/icons/banners-Top-rated-apps/Rythm_banner.png"},
    {display_location: "Top-rated-apps", image_url: "/images/applications/icons/banners-Top-rated-apps/shopandsave_banner.jpg"},

    {display_location: "Newly-added-apps", image_url: "/images/applications/icons/banners-Newly-added-apps/Damodar1_banner.jpg"},
    {display_location: "Newly-added-apps", image_url: "/images/applications/icons/banners-Newly-added-apps/FijiliveM_banner.png"},
    {display_location: "Newly-added-apps", image_url: "/images/applications/icons/banners-Newly-added-apps/Rythm_banner.png"},
    {display_location: "Newly-added-apps", image_url: "/images/applications/icons/banners-Newly-added-apps/shopandsave_banner.jpg"},

    {display_location: "Most-used-apps", image_url: "/images/applications/icons/banners-Most-used-apps/Damodar1_banner.jpg"},
    {display_location: "Most-used-apps", image_url: "/images/applications/icons/banners-Most-used-apps/FijiliveM_banner.png"},
    {display_location: "Most-used-apps", image_url: "/images/applications/icons/banners-Most-used-apps/Rythm_banner.png"},
    {display_location: "Most-used-apps", image_url: "/images/applications/icons/banners-Most-used-apps/shopandsave_banner.jpg"},

    {display_location: "Free-apps", image_url: "/images/applications/icons/banners-Free-apps/Damodar1_banner.jpg"},
    {display_location: "Free-apps", image_url: "/images/applications/icons/banners-Free-apps/FijiliveM_banner.png"},
    {display_location: "Free-apps", image_url: "/images/applications/icons/banners-Free-apps/Rythm_banner.png"},
    {display_location: "Free-apps", image_url: "/images/applications/icons/banners-Free-apps/shopandsave_banner.jpg"},

    {display_location: "Left-panel", image_url: "/images/applications/icons/banners-Left-panel/left-banner.png"},
    {display_location: "Left-panel", image_url: "/images/applications/icons/banners-Left-panel/left-banner.png"},
    {display_location: "Left-panel", image_url: "/images/applications/icons/banners-Left-panel/left-banner.png"},
    {display_location: "Left-panel", image_url: "/images/applications/icons/banners-Left-panel/left-banner.png"}
];

// Update the banners collection
db.banners.remove();

// Add banners to the banners collection
banners.forEach(function (banner) {
    var document = {"_id": NumberInt(id), "display_location": banner.display_location, "link": "", "description": "", "image_url": banner.image_url};
    db.banners.insert(document);
    id++;
});

print("> updated the banners collection.");