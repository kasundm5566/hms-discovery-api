conn = new Mongo("localhost:27017");
db = conn.getDB("appstore");

// Change the type of the category field (String -> Array)
db.eval(changeCategoryFieldType);
print("> changed the type of the pending_approval_apps.category field (String -> Array).");

db.pending_approval_apps.update({}, {$set: {rate_count: 0}}, { multi: true });
print("> added field rate_count to pending_approval_apps.")

function changeCategoryFieldType() {
    db.pending_approval_apps.find({ "category": { $type: 2 } }).snapshot().forEach(
        function (x) {
            if (!Array.isArray(x.category)) {
                x.category = [ x.category ];
                db.pending_approval_apps.save(x);
            }
        }
    );
}