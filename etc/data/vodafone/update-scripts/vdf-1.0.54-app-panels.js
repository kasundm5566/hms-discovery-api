conn = new Mongo("localhost:27017");
db = conn.getDB("appstore");

var panels = [
    {"_id": "panelTop", "category":"mostlyUsedApps"},
    {"_id": "panelMiddle", "category":"newlyAddedApps"},
    {"_id": "panelBottom", "category":"freeApps"}
];

// Update the app_panels collection
db.app_panels.remove();
panels.forEach(function (panel) {
    var document = {"_id": panel._id, "category": panel.category};
    db.app_panels.insert(document);
});
