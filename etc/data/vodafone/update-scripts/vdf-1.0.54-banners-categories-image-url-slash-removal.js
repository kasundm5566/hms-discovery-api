conn = new Mongo("localhost:27017");
db = conn.getDB("appstore");

// Remove starting / from image urls
db.banners.find().forEach(function (banner) {
    var imageUrl = banner.image_url;
    var bannerId = banner._id;
    if (imageUrl.charAt(0) === "/") {
        imageUrl = imageUrl.substr(1);
    }
    db.banners.update({"_id": bannerId}, {$set: {"image_url": imageUrl}});
});
print("Removed starting / from image urls of the banners collection.")

db.categories.find().forEach(function (category) {
    var imageUrl = category.image_url;
    var categoryId = category._id;
    if (imageUrl.charAt(0) === "/") {
        imageUrl = imageUrl.substr(1);
    }
    db.categories.update({"_id": categoryId}, {$set: {"image_url": imageUrl}});
});
print("Removed starting / from image urls of the categories collection.")
