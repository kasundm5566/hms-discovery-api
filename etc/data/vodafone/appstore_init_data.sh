#!/bin/sh
DATA_DIR=`dirname $0`

mongoimport --db appstore --collection categories  --file $DATA_DIR/categories.json
mongoimport --db appstore --collection app_panels  --file $DATA_DIR/app_panels.json

