#!/bin/bash

SCRIPT_HOME=/hms/scripts/app_rating

source $SCRIPT_HOME/conf/ratings.conf

LOG_FILE=ratings.log.`date +"%Y-%m-%d"`

TEMP_DIR=$SCRIPT_HOME/temp
CONF_DIR=$SCRIPT_HOME/conf
LOGS_DIR=$SCRIPT_HOME/logs

run_mongo_query()
{
	MONGO_HOST=$1
	MONGO_PORT=$2
	MONGO_DB=$3
        script_file=$4

        mongo $MONGO_HOST:$MONGO_PORT/$MONGO_DB --quiet $script_file
}


export_mongo_query_out()
{
	DB=$1
	COLLECTION=$2
	FIELD_LIST=$3
	QUERY=$4
	IGNORE_PATTERN=$5
	OUT_FILE=$6
	echo "mongoexport -h$MONGO_HOST --port $MONGO_PORT -d $DB -c $COLLECTION -f $FIELD_LIST -q '$QUERY' --csv | grep -v $IGNORE_PATTERN > $TEMP_DIR/$OUT_FILE" > $TEMP_DIR/execure.sh
	sh $TEMP_DIR/execure.sh
}


ignore_configured_app_list()
{
        list_file_to_be_ignore=$1
        grep_pattern=""
        while read line
        do
                grep_pattern=`echo "grep -v $line | $grep_pattern"`
        done < $CONF_DIR/Rating_Ignore_App_List.conf

        grep_pattern=`echo $grep_pattern | sed '$s/.$//'`
        echo "cat $list_file_to_be_ignore | $grep_pattern > $TEMP_DIR/app_list.txt" > $TEMP_DIR/temp.sh
        sh $TEMP_DIR/temp.sh
}


get_rating_value()
{
        count=$1
        rating_weight=`cat $CONF_DIR/rating.conf | grep rating-weight | cut -f2 -d \=`
        rating_max=`cat $CONF_DIR/rating.conf | grep rating-max | cut -f2 -d \=`
        rating=$((count / rating_weight))
	if [ $rating -ge 10 ]; then
		rating=10
	fi
	echo $rating
}

#
# rating calculation for subscription app
# usage
#	get_rating_subscription <app-id> <subscription-count>
get_rating_subscription() {
	echo "$(date) - calculating rating for subscription app id [ $1 ]" >> $LOGS_DIR/$LOG_FILE
	
	if [ "$2" -le "$SUB_1_SLAB" ]; then
		rating=0
	elif [ "$2" -gt "$SUB_1_SLAB" ] && [ "$2" -le "$SUB_2_SLAB" ]; then
		rating=1
	elif [ "$2" -gt "$SUB_2_SLAB" ] && [ "$2" -le "$SUB_3_SLAB" ]; then
		rating=2
	elif [ "$2" -gt "$SUB_3_SLAB" ] && [ "$2" -le "$SUB_4_SLAB" ]; then
		rating=3
        elif [ "$2" -gt "$SUB_4_SLAB" ] && [ "$2" -le "$SUB_5_SLAB" ]; then
                rating=4
        elif [ "$2" -gt "$SUB_5_SLAB" ] && [ "$2" -le "$SUB_6_SLAB" ]; then
                rating=5
        elif [ "$2" -gt "$SUB_6_SLAB" ] && [ "$2" -le "$SUB_7_SLAB" ]; then
                rating=6
        elif [ "$2" -gt "$SUB_7_SLAB" ] && [ "$2" -le "$SUB_8_SLAB" ]; then
                rating=7
        elif [ "$2" -gt "$SUB_8_SLAB" ] && [ "$2" -le "$SUB_9_SLAB" ]; then
		rating=8
	elif [ "$2" -gt "$SUB_9_SLAB" ] && [ "$2" -le "$SUB_10_SLAB" ]; then
                rating=9
	elif [ "$2" -gt "$SUB_10_SLAB" ]; then
		rating=10
	else
		echo "$(date) - ERROR calculating rating for app id [ $1 ]" >> $LOGS_DIR/$LOG_FILE
	fi
	echo $rating
}

#
# rating calculation for downloadable app
# usage
#       get_rating_downloads <app-id> <download-count>
get_rating_downloads() {
        echo "$(date) - calculating rating for downloadable app id [ $1 ]" >> $LOGS_DIR/$LOG_FILE

        if [ "$2" -le "$DWN_1_SLAB" ]; then
                rating=0
        elif [ "$2" -gt "$DWN_1_SLAB" ] && [ "$2" -le "$DWN_2_SLAB" ]; then
                rating=1
        elif [ "$2" -gt "$DWN_2_SLAB" ] && [ "$2" -le "$DWN_3_SLAB" ]; then
                rating=2
        elif [ "$2" -gt "$DWN_3_SLAB" ] && [ "$2" -le "$DWN_4_SLAB" ]; then
                rating=3
        elif [ "$2" -gt "$DWN_4_SLAB" ] && [ "$2" -le "$DWN_5_SLAB" ]; then
                rating=4
        elif [ "$2" -gt "$DWN_5_SLAB" ] && [ "$2" -le "$DWN_6_SLAB" ]; then
                rating=5
        elif [ "$2" -gt "$DWN_6_SLAB" ] && [ "$2" -le "$DWN_7_SLAB" ]; then
                rating=6
        elif [ "$2" -gt "$DWN_7_SLAB" ] && [ "$2" -le "$DWN_8_SLAB" ]; then
                rating=7
        elif [ "$2" -gt "$DWN_8_SLAB" ] && [ "$2" -le "$DWN_9_SLAB" ]; then
		rating=8
	elif [ "$2" -gt "$DWN_9_SLAB" ] && [ "$2" -le "$DWN_10_SLAB" ]; then
                rating=9
        elif [ "$2" -gt "$DWN_10_SLAB" ]; then
                rating=10
        else
                echo "$(date) - ERROR calculating rating for app id [ $1 ]" >> $LOGS_DIR/$LOG_FILE
        fi
        echo $rating
}


get_subscription_list()
{
	echo "$(date) - get current subscription counts" >> $LOGS_DIR/$LOG_FILE
        #mongoexport -h$MONGO_HOST --port $MONGO_PORT -d subscription -c subscription -f app-id -q '{"current-status" : "REGISTERED"}' --csv | grep -v app-id> $TEMP_DIR/subscription_list.tmp            
        export_mongo_query_out subscription subscription app-id "{\"current-status\" : \"REGISTERED\"}" app-id subscription_list.tmp
	cat $TEMP_DIR/subscription_list.tmp | sort | uniq -c > $TEMP_DIR/subscription_list.txt
}

get_appstore_published_apps()
{
	echo "$(date) - get appstore published app list" >> $LOGS_DIR/$LOG_FILE
        export_mongo_query_out appstore apps _id  "{\"status\" : \"Publish\"}" id published_list.tmp 
        ignore_configured_app_list $TEMP_DIR/published_list.tmp
}

get_download_counts()
{
	echo "$(date) - get current download counts" >> $LOGS_DIR/$LOG_FILE
	export_mongo_query_out cms request applicationId  "{\"status\":{\$in: [\"downloading\", \"download-completed\", \"download-failed\", \"wap-push-delivered\", \"url-expired\"]}}" applicationId download_list.tmp
	cat  $TEMP_DIR/download_list.tmp | sort | uniq -c > $TEMP_DIR/download_list.txt
}


create_appstore_update_query()
{
        while read app_id 
        do
                subscription_count=`grep $app_id $TEMP_DIR/subscription_list.txt | awk '{print $1}' `
		download_count=`grep $app_id $TEMP_DIR/download_list.txt | awk '{print $1}'`

		# handle NULL values
		if [ "$subscription_count" == "" ]; then
		        subscription_count=0
		fi
		if [ "$download_count" == "" ]; then
		        download_count=0
		fi

                echo "$(date) - processing rating for app [ $app_id ]" >> $LOGS_DIR/$LOG_FILE
                echo "$(date) - app subscription count [ $subscription_count ]" >> $LOGS_DIR/$LOG_FILE
                echo "$(date) - app download count [ $download_count ]" >> $LOGS_DIR/$LOG_FILE

		PRIORITY1=`echo $RATING_PRIORITY | cut -d, -f1`
		PRIORITY2=`echo $RATING_PRIORITY | cut -d, -f2`

		if [ "$subscription_count" -gt 0 ] && [ "$download_count" -gt 0 ]; then

			echo "$(date) - Downlaodable/Subscription app found" >> $LOGS_DIR/$LOG_FILE
			echo "$(date) - Using priority $RATING_PRIORITY" >> $LOGS_DIR/$LOG_FILE

			if [ "$PRIORITY1" -eq 1 ] && [ "$PRIORITY2" -eq 2 ]; then
				echo "$(date) - rating priority given to subscription" >> $LOGS_DIR/$LOG_FILE
                                ratings=`get_rating_subscription $app_id $subscription_count`
			elif [ "$PRIORITY1" -eq 2 ] && [ "$PRIORITY2" -eq 1 ]; then
				echo "$(date) - rating priority given to downloads" >> $LOGS_DIR/$LOG_FILE
                                ratings=`get_rating_downloads $app_id $download_count`
  			else
                                echo "$(date) - ERROR invalid priority (please check configurations)" >> $LOGS_DIR/$LOG_FILE
				echo "$(date) - setting default rating" >> $LOGS_DIR/$LOG_FILE
				ratings=$DEFAULT_RATING
  			fi

		elif [ "$subscription_count" -gt 0 ] && [ "$download_count" -eq 0 ]; then

			echo "$(date) - Subscription app found" >> $LOGS_DIR/$LOG_FILE
			ratings=`get_rating_subscription $app_id $subscription_count`

		elif [ "$subscription_count" -eq 0 ] && [ "$download_count" -gt 0 ]; then

			echo "$(date) - Downlaodable app found" >> $LOGS_DIR/$LOG_FILE
			ratings=`get_rating_downloads $app_id $download_count`
		else
			echo "$(date) - app does not have any subscibers or downloads" >> $LOGS_DIR/$LOG_FILE
			echo "$(date) - setting default rating" >> $LOGS_DIR/$LOG_FILE
			ratings=$DEFAULT_RATING
		fi
	
		echo "$(date) - calculated rating value for app [ $ratings ]" >> $LOGS_DIR/$LOG_FILE
	
		if [ $ratings -gt 0 ]; then
			echo "db.apps.update({\"_id\" : $app_id}, {\$set : {\"rating\" : NumberLong($ratings)}});" >> $TEMP_DIR/rating.json	
		fi
	
        done < $TEMP_DIR/app_list.txt

}

update_appstore_ratings()
{
	BACKUP_DATE=`date +"%Y-%m-%d_%H-%M-%S"`
	echo "$(date) - updating appstore ratings on $BACKUP_DATE" >> $LOGS_DIR/$LOG_FILE
	run_mongo_query $APPSTORE_DB_HOST $APPSTORE_DB_PORT $APPSTORE_DB $TEMP_DIR/rating.json
	echo "$(date) - updating appstore ratings complete" >> $LOGS_DIR/$LOG_FILE
	echo "$(date) - cleaning temporary files" >> $LOGS_DIR/$LOG_FILE
	mv $TEMP_DIR/rating.json $SCRIPT_HOME/ratings/rating.json.$BACKUP_DATE
	rm $SCRIPT_HOME/temp/*
}

echo "$(date) - ###### script execution started #####" >> $LOGS_DIR/$LOG_FILE
get_subscription_list
get_appstore_published_apps
get_download_counts
create_appstore_update_query
update_appstore_ratings
echo "$(date) - ###### script execution complete #####" >> $LOGS_DIR/$LOG_FILE
