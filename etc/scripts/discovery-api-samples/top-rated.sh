#!/bin/bash
curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/parent-category/top-rated/start/2/limit/2

#  response
#	[{
#	  "id": "APP_000330",
#	  "name": "ffff",
#	  "developer": "oshancorp",
#	  "description": "dfdfd",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/ffff/ffff.jpg",
#	  "rating": 3.0,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/ffff/ffff_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/ffff/ffff_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/ffff/ffff_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["Alert", "", ""]
#	}, {
#	  "id": "APP_000232",
#	  "name": "FreeDownloadApp002",
#	  "developer": "testuser",
#	  "description": "FreeDownloadApp001",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/FreeDownloadApp002/FreeDownloadApp002.jpg",
#	  "rating": 2.5,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/FreeDownloadApp002/FreeDownloadApp002_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/FreeDownloadApp002/FreeDownloadApp002_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/FreeDownloadApp002/FreeDownloadApp002_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["Entertainment", "", ""]
#	}]
