#!/bin/bash
curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/related-applications/app-id/APP_000275/start/0/limit/2

#  response
#	[{
#	  "id": "APP_000169",
#	  "name": "JienjoySafari",
#	  "developer": "transcorp",
#	  "description": "<![CDATA[<p>Plan your safari adventure before you go. Use Jienjoy Safari application to reserve your safari. You can use web, wap, Android and Windows Mobile downloadable applications in order to make your life easy. 1. Download or log in. 2. Search Safari. 3. Select Safari.  4. Pay using virtual city Payment Gateway. No need to waste your time in limitless queues. USSD - dial #383#4,  <a href=https://sdp.hewani.co.ke/apps/web/selfcare/transport/JienjoySafari>Web URL</a> and  <a href=https://sdp.hewani.co.ke/apps/wap/transport/JienjoySafari/ui/>WAP URL</a></p>]]>",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/JienjoySafari/JienjoySafari.jpg",
#	  "rating": 3.0,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/JienjoySafari/JienjoySafari_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/JienjoySafari/JienjoySafari_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/JienjoySafari/JienjoySafari_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#	    "safaricom": "instructions"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["Downloads", "Downloads", "Downloads"]
#	}]
