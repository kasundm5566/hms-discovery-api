#!/bin/bash
curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/search/sms/start/0/limit/3

#  response
#	[{
#	  "id": "APP_000293",
#	  "name": "SMSKushan",
#	  "developer": "aroshacorp",
#	  "description": "test",
#	  "category": "Channel",
#	  "app-icon": "images/applications/icons/SMSKushan/SMSKushan.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [],
#	  "instructions": {
#	    "safaricom": "rest"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["", "", ""]
#	}, {
#	  "id": "APP_001141",
#	  "name": "SMSSubs",
#	  "developer": "oshancorp",
#	  "description": "test",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/SMSSubs/SMSSubs.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/SMSSubs/SMSSubs_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/SMSSubs/SMSSubs_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/SMSSubs/SMSSubs_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#	    "safaricom": "test"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["", "", ""]
#	}]
