#!/bin/bash
curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/parent-categories

#  response
#	[{
#	  "id": "featured.apps",
#	  "fragment": "create-featured-app-panel",
#	  "value": true
#	}, {
#	  "id": "most.used",
#	  "fragment": "create-most-used-app-panel",
#	  "value": true
#	}, {
#	  "id": "newly.added",
#	  "fragment": "create-newly-added-apps-panel",
#	  "value": true
#	}]
