#!/bin/bash
curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/parent-category/newly-added/category/Downloads/start/0/limit/2

#  response
#	[{
#	  "id": "APP_001160",
#	  "name": "SubHttp",
#	  "developer": "aroshacorp",
#	  "description": "subscription download without http subs",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/SubHttp/SubHttp.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/SubHttp/SubHttp_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/SubHttp/SubHttp_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/SubHttp/SubHttp_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#	    "safaricom": "eeee"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["", "", ""]
#	}, {
#	  "id": "APP_001159",
#	  "name": "Test_Dwn",
#	  "developer": "aroshacorp",
#	  "description": "download popup test",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/Test_Dwn/Test_Dwn.jpg",
#	  "rating": 4.0,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/Test_Dwn/Test_Dwn_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/Test_Dwn/Test_Dwn_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/Test_Dwn/Test_Dwn_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#	    "safaricom": "safaricom ins"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["", "", ""]
#	}]
