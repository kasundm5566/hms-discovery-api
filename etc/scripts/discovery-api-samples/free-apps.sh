#!/bin/bash
curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/free-apps/category/Downloads/start/1/limit/1

#  response
#	[{
#	  "id": "APP_000275",
#	  "name": "freeDownlaodableApp",
#	  "developer": "oshancorp",
#	  "description": "It's a free app",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/freeDownlaodableApp/freeDownlaodableApp.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/freeDownlaodableApp/freeDownlaodableApp_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/freeDownlaodableApp/freeDownlaodableApp_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/freeDownlaodableApp/freeDownlaodableApp_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#	    "airtel": "This is airtel description",
#	    "safaricom": "This is safaricom description"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["Other", "", ""]
#	}]


curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/free-apps/start/0/limit/2

#  response
#	[{
#	  "id": "APP_000096",
#	  "name": "Daily Bible",
#	  "developer": "hewani",
#	  "description": "Bible verses from the Old and New Testament",
#	  "category": "Channel",
#	  "app-icon": "images/applications/icons/Daily_Bible/Daily_Bible.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [],
#	  "instructions": {
#	    "safaricom": "SMS dailybible  to 383"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["", "", ""]
#	}, {
#	  "id": "APP_000114",
#	  "name": "Football Facts",
#	  "developer": "hewani",
#	  "description": "Facts about football. Get a daily update about football.",
#	  "category": "Channel",
#	  "app-icon": "images/applications/icons/Football_Facts/Football_Facts.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [],
#	  "instructions": {
#	    "safaricom": "SMS footballfacts to 383"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["", "", ""]
#	}]
