#!/bin/bash
curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/category/Alert/start/0/limit/1

#  response
#	[{
#	  "id": "APP_000348",
#	  "name": "FreshimaAlerts",
#	  "developer": "freshima",
#	  "description": "Get updates from Freshima Supermarkets. sms freshalerts to 383",
#	  "category": "Alert",
#	  "app-icon": "images/applications/icons/FreshimaAlerts/FreshimaAlerts.jpg",
#	  "rating": 1.0,
#	  "app-screenshots": [],
#	  "instructions": {
#	  "safaricom": "Users who will subscribe to this application have to send sms, type REG freshalerts and SEND to 383\nTo unsubscribe, type #		UNREG freshalerts and send to 383"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["", "", ""]
#	}]


curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/category/Downloads/start/1/limit/2

#  response
#	[{
#	  "id": "APP_000232",
#	  "name": "FreeDownloadApp002",
#	  "developer": "testuser",
#	  "description": "FreeDownloadApp001",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/FreeDownloadApp002/FreeDownloadApp002.jpg",
#	  "rating": 2.5,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/FreeDownloadApp002/FreeDownloadApp002_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/FreeDownloadApp002/FreeDownloadApp002_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/FreeDownloadApp002/FreeDownloadApp002_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["Entertainment", "", ""]
#	}, {
#	  "id": "APP_000272",
#	  "name": "oshtest",
#	  "developer": "oshancorp",
#	  "description": "testing app",
#	  "category": "Downloads",
#	  "app-icon": "images/applications/icons/oshtest/oshtest.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/oshtest/oshtest_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/oshtest/oshtest_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/oshtest/oshtest_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#	    "safaricom": "instructions instructions"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["Voting", "Games", "Sports"]
#	}]




