#!/bin/bash
curl -v -X GET http://192.168.0.125:6578/discovery-api/v2/featured-apps/start/0/limit/2

# response
#	[{
#	  "id": "APP_000096",
#	  "name": "Daily Bible",
#	  "developer": "hewani",
#	  "description": "Bible verses from the Old and New Testament",
#	  "category": "Channel",
#	  "app-icon": "images/applications/icons/Daily_Bible/Daily_Bible.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [],
#	  "instructions": {
#	    "safaricom": "SMS dailybible  to 383"
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["", "", ""]
#	}, {
#	  "id": "APP_000270",
#	  "name": "Huwavi",
#	  "developer": "testuser",
#	  "description": "Huwavi",
#	  "category": "Business",
#	  "app-icon": "images/applications/icons/Huwavi/Huwavi.jpg",
#	  "rating": 0.0,
#	  "app-screenshots": [{
#	    "caption": "Screen Shot 1",
#	    "url": "images/applications/icons/Huwavi/Huwavi_screenshot_1.jpg"
#	  }, {
#	    "caption": "Screen Shot 2",
#	    "url": "images/applications/icons/Huwavi/Huwavi_screenshot_2.jpg"
#	  }, {
#	    "caption": "Screen Shot 3",
#	    "url": "images/applications/icons/Huwavi/Huwavi_screenshot_3.jpg"
#	  }],
#	  "instructions": {
#
#	  },
#	  "charging-details": "",
#	  "usage": 0,
#	  "labels": ["Downloads", "Downloads", "Other"]
#	}]
