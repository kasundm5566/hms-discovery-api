package hms.appstore.api.client

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}

import hms.appstore.api.json._
import hms.appstore.api.client.util.{URLEncoder, ServiceFutureFilter}
import hms.scala.http.{DefaultHttpClient, HttpConnectorConfig}
import hms.scala.http.marshalling.HttpMarshalling

import concurrent.Future
import scala.tools.nsc.interpreter.Results

class DiscoveryInternalServiceImpl(implicit val bindingModule: BindingModule) extends DiscoveryInternalService
with ServiceFutureFilter
with DiscoveryConfig
with Injectable {

  import MsgJsonProtocol._
  import HttpMarshalling._


  private val connector = new DefaultHttpClient(
    HttpConnectorConfig(
      "discoveryApiConnector", "http",
      inject[String]("discovery.api.host"),
      inject[Int]("discovery.api.port")
    )
  )

  def parentCategories() = {
    val path = s"$apiBasePath/internal/parent-categories/find"
    connector.doGet[QueryResults[ParentCategory]](path)
  }

  def findApp(appId: String) = {
    val path = s"$apiBasePath/internal/app/app-id/$appId"
    connector.doGet[QueryResult[Application]](path)
  }

  override def findAppChargingDetails(appId: String) = {
    val path = s"$apiBasePath/internal/charging/app/app-id/$appId"
    connector.doGet[QueryResults[ChargingData]](path)
  }

  override def findChargingDescriptionMessageFormats(appId: String) = {
    val path = s"$apiBasePath/internal/charging-description-format/app/app-id/$appId"
    connector.doGet[QueryResults[ChargingDescriptionMessage]](path)
  }

  def findAllApps(start: Int, limit: Int) = {
    val path = s"$apiBasePath/internal/apps/find/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Application]](path)
  }

  def findAllDownloadableApps(start: Int, limit: Int) = {
    val path = s"$apiBasePath/internal/downloadable-apps/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Application]](path)
  }

  def findPendingApps(start: Int, limit: Int) = {
    val path = s"$apiBasePath/internal/pending-apps/find/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Application]](path)
  }

  def countPendingApps() = {
    val path = s"$apiBasePath/internal/pending-apps/count"
    connector.doGet[QueryResult[AppCount]](path)
  }

  def countAllApps() = {
    val path = s"$apiBasePath/internal/apps/count"
    connector.doGet[QueryResult[AppCount]](path)
  }

  def count(query: SearchQuery) = {
    val path = s"$apiBasePath/internal/apps/count"
    connector.doPost[SearchQuery, QueryResult[AppCount]](path, query)
  }

  def searchApps(query: SearchQuery) = {
    val path = s"$apiBasePath/internal/apps/search"
    connector.doPost[SearchQuery, QueryResults[Application]](path, query)
  }

  def requestApp(appId: String, remarks: String) = {
    val path = s"$apiBasePath/internal/app/$appId/request/remarks/${URLEncoder.encode(remarks)}"
    connector.doGet[StatusCode](path)
  }

  def requestApp(appId: String, remarks: String, userId: String): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/app/$appId/request/remarks/${URLEncoder.encode(remarks)}/userId/$userId"
    connector.doGet[StatusCode](path)
  }

  def approveApp(appId: String, publishName: String, remarks: String) = {
    val encodedName = URLEncoder.encode(publishName)
    val encodedRemarks = URLEncoder.encode(remarks)
    val path = s"$apiBasePath/internal/app/$appId/approve/publish-name/$encodedName/remarks/$encodedRemarks"
    connector.doGet[StatusCode](path)
  }

  def rejectApp(appId: String, remarks: String) = {
    val path = s"$apiBasePath/internal/app/$appId/reject/remarks/${URLEncoder.encode(remarks)}"
    connector.doGet[StatusCode](path)
  }

  def publishApp(appId: String, publishName: String, remarks: String) = {
    val encodedName = URLEncoder.encode(publishName)
    val encodedRemarks = URLEncoder.encode(remarks)
    val path = s"$apiBasePath/internal/app/$appId/publish/publish-name/$encodedName/remarks/$encodedRemarks"
    connector.doGet[StatusCode](path)
  }

  def unpublishApp(appId: String, remarks: String) = {
    val path = s"$apiBasePath/internal/app/$appId/un-publish/remarks/${URLEncoder.encode(remarks)}"
    connector.doGet[StatusCode](path)
  }

  @deprecated
  def updateApp(req: UpdateReq) = {
    val path = s"$apiBasePath/internal/app/update"
    connector.doPost[UpdateReq, StatusCode](path, req)
  }

  def updateAppAndCharging(req: UpdateAppAndChargingReq) = {
    val path = s"$apiBasePath/internal/app/update-new"
    connector.doPost[UpdateAppAndChargingReq, StatusCode](path, req)
  }

  def updateFeaturedApps(positions: Map[String, Int]) = {
    val path = s"$apiBasePath/internal/featured-apps/update"
    connector.doPost[Map[String, Int], StatusCode](path, positions)
  }

  def createOrUpdateParentCategories(pcs: List[ParentCategory]) = {
    val path = s"$apiBasePath/internal/parent-categories/update"
    connector.doPost[List[ParentCategory], StatusCode](path, pcs)
  }

  def findAdUrls: Future[QueryResults[AdUrl]] = {
    val path = s"$apiBasePath/internal/ad-urls/find"
    connector.doGet[QueryResults[AdUrl]](path)
  }

  def createOrUpdateAdUrls(urls: List[AdUrl]): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/ad-urls/update"
    connector.doPost[List[AdUrl], StatusCode](path, urls)
  }

  def changeMpin(req: MPinChangeReq): Future[MPinChangeResp] = {
    val path = s"$apiBasePath/internal/auth/change-mpin"
    connector.doPost[MPinChangeReq, MPinChangeResp](path, req)
  }

  def authenticateByUsername(userName: String): Future[AuthenticateResp] = {
    val path = s"$apiBasePath/internal/auth/username/$userName"
    connector.doGet[AuthenticateResp](path)
  }

  def authenticateByMsisdn(msisdn: String): Future[AuthenticateResp] = {
    val path = s"$apiBasePath/internal/auth/msisdn/$msisdn"
    connector.doGet[AuthenticateResp](path)
  }

  def findAppOrPendingApp(appId: String): Future[QueryResult[Application]] = {
    val path = s"$apiBasePath/internal/app/app-id/$appId"
    connector.doGet[QueryResult[Application]](path)
  }

  def userDetailsByMsisdn(msisdn: String): Future[UserDetailsResp] = {
    val path = s"$apiBasePath/internal/user-details/msisdn/$msisdn"
    connector.doGet[UserDetailsResp](path)
  }

  def removeComment(commentId: String): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/user-comment/remove/comment-id/$commentId"
    connector.doGet[StatusCode](path)
  }

  def clientPlatformVersion(platform: String, osVersion: String): Future[ClientVersion] = {
    val path = s"$apiBasePath/appstore/latest-version/platform/$platform/os-version/$osVersion"
    connector.doGet[ClientVersion](path)
  }

  def addCategory(req: CategoryAddReq): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/category/add"
    connector.doPost[CategoryAddReq, StatusCode](path, req)
  }

  def removeCategory(id: String): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/category/remove/$id"
    connector.doGet[StatusCode](path)
  }

  def editCategory(id: String, req: CategoryEditReq): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/category/edit/$id"
    connector.doPost[CategoryEditReq, StatusCode](path, req)
  }

  def addOrRemoveAppsOfCategory(id: String, req: AddOrRemoveAppsOfCategoryReq): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/category/update-apps/$id"
    connector.doPost[AddOrRemoveAppsOfCategoryReq, StatusCode](path, req)
  }

  override def addBanner(req: BannerAddReq): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/banner/add"
    connector.doPost[BannerAddReq, StatusCode](path, req)
  }

  override def editBanner(id: Int, req: BannerAddReq): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/banner/edit/$id"
    connector.doPost[BannerAddReq, StatusCode](path, req)
  }

  override def removeBanner(id: Int): Future[StatusCode] = {
    val path = s"$apiBasePath/internal/banner/remove/$id"
    connector.doGet[StatusCode](path)
  }

  override def getBanners(start: Int = 0, limit: Int = Int.MaxValue): Future[QueryResults[Banner]] = {
    val path = s"$apiBasePath/internal/banners/start/$start/limit/$limit"
    connector.doGet[QueryResults[Banner]](path)
  }

  override def getBannerLocations(): Future[QueryResults[BannerLocation]] = {
    val path = s"$apiBasePath/internal/banners/locations"
    connector.doGet[QueryResults[BannerLocation]](path)
  }

  override def getBannersCount(): Future[QueryResult[BannersCount]] = {
    val path = s"$apiBasePath/internal/banners/count"
    connector.doGet[QueryResult[BannersCount]](path)
  }

  override def getBannersByLocation(location: String, start: Int = 0, limit: Int = Int.MaxValue): Future[QueryResults[Banner]] = {
    val path = s"$apiBasePath/internal/banners/$location/start/$start/limit/$limit"
    connector.doGet[QueryResults[Banner]](path)
  }

  override def getBannersCountByLocation(location: String): Future[QueryResult[BannersCount]] = {
    val path = s"$apiBasePath/internal/banners/$location/count"
    connector.doGet[QueryResult[BannersCount]](path)
  }

  override def notifyAll(req: NotifyReq): Future[NotificationResp] = {
    val path = s"$apiBasePath/internal/notify/all"
    connector.doPost[NotifyReq, NotificationResp](path, req)
  }

  override def notifyAppSpecific(req: NotifyReq): Future[NotificationResp] = {
    val path = s"$apiBasePath/internal/notify/app-specific"
    connector.doPost[NotifyReq, NotificationResp](path, req)
  }

  override def notifyAppUpdate(req: AppUpdateNotificationReq): Future[NotificationResp] = {
    val path = s"$apiBasePath/internal/notify/app-update"
    connector.doPost[AppUpdateNotificationReq, NotificationResp](path, req)
  }
}
