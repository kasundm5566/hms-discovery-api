package hms.appstore.api.client.domain


import hms.appstore.api.json._
import scala.beans.BeanProperty

import java.util.{List => JList, Map => JMap, Set => JSet, Date, Collections}
import hms.appstore.api.client.util.DnPlatformUtils
import java.util

/**
 * Bean friendly scala class to bind an application to a view (JSP/Spring MVC)
 */
class ApplicationView  {
  @BeanProperty var id: String = ""

  @BeanProperty var name: String = ""

  @BeanProperty var category: String = ""

  @BeanProperty var appIcon: String = ""

  @BeanProperty var appBanner: JList[JMap[String, String]] = Collections.emptyList()

  @BeanProperty var usage: Long = 0

  @BeanProperty var currency: String = ""

  @BeanProperty var developer: String = ""

  @BeanProperty var rating: Double = 0

  @BeanProperty var rate_count: Int = 0

  @BeanProperty var labels: JList[String] = Collections.emptyList()

  @BeanProperty var description: String = ""

  @BeanProperty var shortDesc: String = ""

  @BeanProperty var appTypes: JList[String] = Collections.emptyList()

  @BeanProperty var screenShots: JList[JMap[String, String]] = Collections.emptyList()

  @BeanProperty var instructions: JMap[String, String] = Collections.emptyMap()

  @BeanProperty var ncs: JSet[String] = Collections.emptySet()

  @BeanProperty var subscription: Boolean = false

  @BeanProperty var downloadable: Boolean = false

  @BeanProperty var dnPlatforms: JMap[String, JSet[String]] = Collections.emptyMap()

  @BeanProperty var dnPlatformsAndContentIds: JMap[String, JSet[String]] = Collections.emptyMap()

  @BeanProperty var subscriptionStatus = SubscriptionStatus.NotAvailable

  @BeanProperty var downloadStatus = DownloadStatusView.emptyView

  @BeanProperty var canSubscribe: Boolean = false

  @BeanProperty var canUnSubscribe: Boolean = false

  @BeanProperty var canDownload: Boolean = false

  @BeanProperty var chargingLabel: String = ""

  @BeanProperty var chargingDetails: String = ""

  @BeanProperty var userComments: JList[UserCommentView] = Collections.emptyList()

  @BeanProperty var status: AppStatus.AppStatus = AppStatus.New

  @BeanProperty var requestedDate: Date = new Date

  @BeanProperty var modifiedDate: Date = new Date

  @BeanProperty var remarks: String = ""

  @BeanProperty var downloadsCount: Int = 0

  @BeanProperty var subscriptionsCount: Int = 0
}

object ApplicationView {

  def apply(app: Application): ApplicationView = {
    import scala.collection.convert.WrapAsJava._
    import SubscriptionStatus._

    val view = new ApplicationView
    view.id = app.id
    view.name = app.displayName
    view.category = app.category
    view.appIcon = app.appIcon
    view.appBanner = app.appBanners.get.map(m => {val jm: JMap[String, String] = m; jm})
    view.usage = app.usage
    view.currency = app.currency
    view.developer = app.developer
    view.rating = app.rating
    view.rate_count = app.rate_count
    view.labels = app.labels
    view.description = app.description
    view.shortDesc = app.shortDesc
    view.appTypes = app.appTypes
    view.screenShots = app.screenShots.map(m => {val jm: JMap[String, String] = m; jm})
    view.instructions = app.instructions
    view.ncs = app.ncs
    view.subscription = app.subscription
    view.downloadable = app.downloadable
    view.subscriptionStatus = app.subscriptionStatus
    view.downloadStatus = DownloadStatusView(app.downloadStatus)
    view.chargingLabel = app.chargingLabel
    view.chargingDetails = app.chargingDetails
    view.userComments = app.userComments.map(UserCommentView(_))
    view.status = app.status
    view.requestedDate = app.requestedDate.toDate
    view.modifiedDate = app.modifiedDate.toDate
    view.remarks = app.remarks

    view.dnPlatforms = DnPlatformUtils.dnPlatforms(app.downloadableBinaries)
    view.dnPlatformsAndContentIds = DnPlatformUtils.dnPlatformsAndContentIds(app.downloadableBinaries)

    view.downloadsCount = app.downloadsCount
    view.subscriptionsCount = app.subscriptionsCount

    view.canSubscribe = app.subscriptionStatus match {
      case NotAvailable | UnRegistered if app.subscription=> true
      case _ => false
    }

    view.canUnSubscribe = app.subscriptionStatus match {
      case Registered  if app.subscription=> true
      case _ => false
    }

    view.canDownload = app.downloadable

    view
  }
}