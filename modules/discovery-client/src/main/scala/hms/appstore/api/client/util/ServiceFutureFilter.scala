package hms.appstore.api.client.util

import hms.appstore.api.json.StatusCode
import concurrent.{ExecutionContext, Promise, Future}
import util.Success
import util.Failure
import org.slf4j.LoggerFactory


class ServiceException(message: String, cause: Option[Throwable] = None) extends Exception(message, cause.getOrElse(null))

trait ServiceFutureFilter {
  private lazy val logger = LoggerFactory.getLogger(classOf[ServiceFutureFilter])

  protected def filterSuccessFuture[S <: StatusCode](path: String, f: Future[S])(implicit exe: ExecutionContext): Future[S] = {
    val p = Promise[S]()

    f.onComplete{
      case Success(resp) if !resp.isSuccess => {
        logger.error(s"Error response [$resp] received from discovery-api call [$path]")
        p failure(new ServiceException(resp.description))
      }
      case Success(resp) => {
        logger.debug(s"Success response [$resp] received from discovery-api call [$path]")
        p success(resp)
      }
      case Failure(e) => {
        logger.error(s"Error occurred while calling discovery-api path [$path]")
        p.failure(new ServiceException(s"Internal error occurred", Some(e)))
      }
    }

    p.future
  }
}