package hms.appstore.api.client


trait DiscoveryConfig {

  lazy val apiVersion = "v2"

  lazy val apiBasePath = s"/discovery-api/$apiVersion"
}
