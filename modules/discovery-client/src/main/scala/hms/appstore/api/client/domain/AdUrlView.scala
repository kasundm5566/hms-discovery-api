package hms.appstore.api.client.domain

import beans.BeanProperty
import hms.appstore.api.json.AdUrl

/**
 * Bean friendly scala class to bind an application to a view (JSP/Spring MVC)
 */
class AdUrlView {
  @BeanProperty var id: String = ""
  @BeanProperty var name: String = ""
  @BeanProperty var imageUrl: String = ""
  @BeanProperty var targetUrl: String = ""
}

object AdUrlView {
  def apply(url: AdUrl): AdUrlView = {
    val view = new AdUrlView

    view.id = url.id
    view.name = url.name
    view.imageUrl = url.imageUrl
    view.targetUrl = url.targetUrl

    view
  }
}
