package hms.appstore.api.client.domain

import scala.beans.BeanProperty
import hms.appstore.api.json.ChargingDescriptionMessage

/**
 * Created by kasun on 6/4/18.
 */
class ChargingDescriptionMessageView {
  @BeanProperty var ncsType: String = ""
  @BeanProperty var messageFormat: String = ""
}

object ChargingDescriptionMessageView{
  def apply(chargingDescriptionMessage: ChargingDescriptionMessage): ChargingDescriptionMessageView = {
    val view= new ChargingDescriptionMessageView
    view.ncsType = chargingDescriptionMessage.ncsType
    view.messageFormat = chargingDescriptionMessage.messageFormat
    view
  }
}
