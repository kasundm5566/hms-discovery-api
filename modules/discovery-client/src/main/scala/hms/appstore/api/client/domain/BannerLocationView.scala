package hms.appstore.api.client.domain

import scala.beans.BeanProperty
import hms.appstore.api.json.BannerLocation

/**
 * Created by kasun on 6/12/18.
 */
class BannerLocationView {
  @BeanProperty var display_location: String = ""
  @BeanProperty var name: String = ""
  @BeanProperty var min_width: Int = 0
  @BeanProperty var max_width: Int = 0
  @BeanProperty var min_height: Int = 0
  @BeanProperty var max_height: Int = 0
  @BeanProperty var size: Int = 0
}

object BannerLocationView {
  def apply(bannerLocation: BannerLocation): BannerLocationView = {
    val view = new BannerLocationView
    view.display_location = bannerLocation.display_location
    view.name = bannerLocation.name
    view.min_width = bannerLocation.min_width
    view.max_width = bannerLocation.max_width
    view.min_height = bannerLocation.min_height
    view.max_height = bannerLocation.max_height
    view.size = bannerLocation.size
    view
  }
}
