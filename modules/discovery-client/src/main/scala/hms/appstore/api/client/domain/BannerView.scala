package hms.appstore.api.client.domain

import hms.appstore.api.json.{Banner}
import scala.beans.BeanProperty

/**
 * Bean friendly scala class to bind an application to a view (JSP/Spring MVC)
 */
class BannerView {
  @BeanProperty var id: Int = 0
  @BeanProperty var display_location: String = ""
  @BeanProperty var link: String = ""
  @BeanProperty var description: String = ""
  @BeanProperty var image_url: String = ""
}

object BannerView {
  def apply(banner: Banner): BannerView = {
    val view = new BannerView
    view.id = banner.id
    view.display_location = banner.display_location
    view.link = banner.link
    view.description = banner.description
    view.image_url = banner.image_url
    view
  }
}
