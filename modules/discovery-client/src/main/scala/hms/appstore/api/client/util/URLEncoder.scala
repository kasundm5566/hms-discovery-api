package hms.appstore.api.client.util

object URLEncoder {

  def encode(path: String): String  ={
    java.net.URLEncoder.encode(path, "UTF-8").replaceAll("\\+", "%20")
  }

}
