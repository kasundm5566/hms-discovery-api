package hms.appstore.api.client

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}

import hms.appstore.api.json._
import hms.appstore.api.client.util.{URLEncoder, ServiceFutureFilter}
import hms.scala.http.{DefaultHttpClient, HttpConnectorConfig}
import hms.scala.http.marshalling.HttpMarshalling

import scala.concurrent.Future
import spray.http.{MultipartFormData, HttpHeader}


class DiscoveryServiceImpl(implicit val bindingModule: BindingModule) extends DiscoveryService
with ServiceFutureFilter
with DiscoveryConfig
with Injectable {

  import MsgJsonProtocol._
  import HttpMarshalling._


  private val connector = new DefaultHttpClient(
    HttpConnectorConfig(
      "discoveryApiConnector", "http",
      inject[String]("discovery.api.host"),
      inject[Int]("discovery.api.port")
    )
  )


  def categories() = {
    val path = s"$apiBasePath/categories"
    connector.doGet[QueryResults[Category]](path)
  }

  def categoriesForPaging(start: Int, limit: Int) = {
    val path = s"$apiBasePath/categories/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Category]](path)
  }

  def parentCategories() = {
    val path = s"$apiBasePath/parent-categories"
    connector.doGet[QueryResults[ParentCategory]](path)
  }

  def mostlyUsedApps(start: Int, limit: Int) = {
    val path = s"$apiBasePath/parent-category/mostly-used/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Application]](path)
  }

  def mostlyUsedAppsByCategory(start: Int, limit: Int, category: String): Future[QueryResults[Application]] = {
    val path = s"$apiBasePath/parent-category/mostly-used/category/${URLEncoder.encode(category)}/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  def newlyAddedApps(start: Int, limit: Int) = {
    val path = s"$apiBasePath/parent-category/newly-added/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Application]](path)
  }

  def featuredApps(start: Int, limit: Int) = {
    val path = s"$apiBasePath/featured-apps/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Application]](path)
  }

  def topRatedApps(start: Int, limit: Int) = {
    val path = s"$apiBasePath/parent-category/top-rated/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Application]](path)
  }

  def freeApps(start: Int, limit: Int) = {
    val path = s"$apiBasePath/free-apps/start/$start/limit/$limit/"
    connector.doGet[QueryResults[Application]](path)
  }

  def appsByCategory(category: String, start: Int, limit: Int) = {
    val path = s"$apiBasePath/category/${URLEncoder.encode(category)}/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  def appsByCategoryWithoutSorting(category: String, start: Int, limit: Int) = {
    val path = s"$apiBasePath/category-without-sorting/${URLEncoder.encode(category)}/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  def appCount() = {
    val path = s"$apiBasePath/all-apps/count"
    connector.doGet[QueryResult[AppCount]](path)
  }

  def allAppIds(start: Int = 0, limit: Int = 8) = {
    val path = s"$apiBasePath/all-apps/app-ids/start/$start/limit/$limit"
    connector.doGet[QueryResults[String]](path)
  }

  def appDetails(appId: String) = {
    val path = s"$apiBasePath/app-details/app-id/$appId"
    connector.doGet[QueryResult[Application]](path)
  }

  def appDetails(sessionId: String, appId: String) = {
    val path = s"$apiBasePath/app-details/app-id/$appId/session-id/$sessionId"
    connector.doGet[QueryResult[Application]](path)
  }

  def searchApps(query: String, start: Int, limit: Int) = {
    val path = s"$apiBasePath/search/${URLEncoder.encode(query)}/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  def authProviders(platform: String, headers: List[HttpHeader]) = {
    val path = s"$apiBasePath/auth-provider/platform/$platform"
    connector.doGetWithHeader[AuthProviders](path, headers)
  }

  def authenticateByPassword(req: AuthenticateByPasswordReq) = {
    val path = s"$apiBasePath/login"
    connector.doPost[AuthenticateByPasswordReq, AuthenticateResp](path, req)
  }

  def authenticateByMsisdn(headers: List[HttpHeader]): Future[AuthenticateResp] = {
    val path = s"$apiBasePath/login/msisdn"
    connector.doGetWithHeader[AuthenticateResp](path, headers)
  }

  def authenticateByMpin(req: AuthenticateByMPinReq) = {
    val path = s"$apiBasePath/login/mpin"
    connector.doPost[AuthenticateByMPinReq, AuthenticateResp](path, req)
  }

  def validateSession(sessionId: String): Future[SessionValidationResp] = {
    val path = s"$apiBasePath/user/validate/session/$sessionId"
    connector.doGet[SessionValidationResp](path)
  }

  def mySubscriptions(sessionId: String) = {
    val path = s"$apiBasePath/my-apps/session-id/$sessionId"
    connector.doGet[QueryResults[Application]](path)
  }

  def myDownloads(sessionId: String) = {
    val path = s"$apiBasePath/my-downloads/session-id/$sessionId"
    connector.doGet[QueryResults[Application]](path)
  }

  def myDownloadsWithBinaries(sessionId: String) = {
    val path = s"$apiBasePath/my-downloads/session-id/$sessionId/with-binaries"
    connector.doGet[QueryResults[Application]](path)
  }

  def updateCount(sessionId: String) = {
    val path = s"$apiBasePath/my-downloads/update-count/session-id/$sessionId"
    connector.doGet[QueryResult[UpdateCountResp]](path)
  }

  def subscribe(sessionId: String, appId: String) = {
    val path = s"$apiBasePath/subscribe/session-id/$sessionId/app-id/$appId"
    connector.doGet[SubscriptionActionResp](path)
  }

  def unSubscribe(sessionId: String, appId: String) = {
    val path = s"$apiBasePath/unsubscribe/session-id/$sessionId/app-id/$appId"
    connector.doGet[SubscriptionActionResp](path)
  }

  def download(sessionId: String, appId: String, contentId: String) = {
    val path = s"$apiBasePath/download/session-id/$sessionId/app-id/$appId/content-id/$contentId"
    connector.doGet[DownloadResp](path)
  }

  def downloadWithChannel(sessionId: String, appId: String, contentId: String, channel: String) = {
    val path = s"$apiBasePath/download/session-id/$sessionId/app-id/$appId/content-id/$contentId/channel/$channel"
    connector.doGet[DownloadResp](path)
  }

  def downloadWithMobileAccount(sessionId: String, downloadRequestId: String, piName: String) = {
    val path = s"$apiBasePath/download/charge/session-id/$sessionId/request-id/$downloadRequestId/pi-name/${URLEncoder.encode(piName)}"
    connector.doGet[DownloadChargeResp](path)
  }

  def downloadWithMpaisa(sessionId: String, downloadRequestId: String, piName: String, pin: String) = {
    val path = s"$apiBasePath/download/charge/session-id/$sessionId/request-id/$downloadRequestId/pi-name/${URLEncoder.encode(piName)}/pin/$pin"
    connector.doGet[DownloadChargeResp](path)
  }

  def countByCategory(category: String): Future[QueryResult[AppCount]] = {
    val path = s"$apiBasePath/all-apps/count/category/${URLEncoder.encode(category)}"
    connector.doGet[QueryResult[AppCount]](path)
  }

  def searchRelated(appId: String, start: Int, limit: Int) = {
    val path = s"$apiBasePath/related-applications/app-id/$appId/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  def addComment(userComment: UserComment, sessionId: String): Future[StatusCode] = {
    val path = s"$apiBasePath/user-comment/session-id/$sessionId"
    connector.doPost[UserComment, StatusCode](path, userComment)
  }

  def reportAbuse(sessionId: String, appId: String, comment: String, channel: Option[String]): Future[StatusCode] = {
    val path = if (channel.isDefined) {
      s"$apiBasePath/report-abuse/session-id/$sessionId/app-id/$appId/comment/$comment"
    } else {
      s"$apiBasePath/report-abuse/session-id/$sessionId/app-id/$appId/comment/$comment/channel/${channel.get}"
    }
    connector.doGet[StatusCode](path)
  }

  def findAppsFromEachCategory(start: Int, limit: Int): Future[QueryResult[Map[String, List[Application]]]] = {
    val path = s"$apiBasePath/all-apps/find/all-categories/start/$start/limit/$limit"
    connector.doGet[QueryResult[Map[String, List[Application]]]](path)
  }

  def findPaymentInstruments(sessionId: String, appId: String) = {
    val path = s"$apiBasePath/user/pi-list/session-id/$sessionId/app-id/$appId"
    connector.doGet[PaymentInstrumentsResp](path)
  }

  def createUser(userCreateReq: UserRegistrationReq): Future[UserRegistrationResp] = {
    val path = s"$apiBasePath/create/user"
    connector.doPost[UserRegistrationReq, UserRegistrationResp](path, userCreateReq)
  }

  def verifyMsisdn(verificationReq: MsisdnVerificationReq): Future[MsisdnVerificationResp] = {
    val path = s"$apiBasePath/verify/msisdn"
    connector.doPost[MsisdnVerificationReq, MsisdnVerificationResp](path, verificationReq)
  }

  def requestNewVerificationCode(codeReq: MsisdnVerificationCodeReq): Future[MsisdnVerificationResp] = {
    val path = s"$apiBasePath/request/verification/code"
    connector.doPost[MsisdnVerificationCodeReq, MsisdnVerificationResp](path, codeReq)
  }

  def userDetailsByUserName(req: UserDetailsByUsernameReq): Future[UserDetailByUserNameResp] = {
    val path = s"$apiBasePath/user/retrieve"
    connector.doPost[UserDetailsByUsernameReq, UserDetailByUserNameResp](path, req)
  }

  def recoverUserAccount(req: UserAccountRecoveryReq): Future[UserAccountRecoveryResp] = {
    val path = s"$apiBasePath/user/account/recover"
    connector.doPost[UserAccountRecoveryReq, UserAccountRecoveryResp](path, req)
  }

  override def downloadableFeaturedApps(start: Int, limit: Int): Future[QueryResults[Application]] = {
    val path = s"$apiBasePath/featured-apps/downloadable/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  override def popularApps(start: Int, limit: Int): Future[QueryResults[Application]] = {
    val path = s"$apiBasePath/popular-apps/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  override def similarApps(appId: String, start: Int, limit: Int): Future[QueryResults[Application]] = {
    val path = s"$apiBasePath/similar-apps/app-id/$appId/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  override def subscriptionApps(start: Int, limit: Int): Future[QueryResults[Application]] = {
    val path = s"$apiBasePath/subscription-apps/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  override def downloadableApps(start: Int, limit: Int): Future[QueryResults[Application]] = {
    val path = s"$apiBasePath/downloadable-apps/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  override def bannersByLocation(location: String): Future[QueryResults[Banner]] = {
    val path = s"$apiBasePath/banners/location/$location"
    connector.doGet[QueryResults[Banner]](path)
  }

  override def appsByDeveloper(start: Int, limit: Int, developer: String): Future[QueryResults[Application]] = {
    val path = s"$apiBasePath/apps/developer/$developer/start/$start/limit/$limit"
    connector.doGet[QueryResults[Application]](path)
  }

  override def rateApp(req: AppRatingAddReq): Future[StatusCode] = {
    val path = s"$apiBasePath/app-rating"
    connector.doPost[AppRatingAddReq, StatusCode](path, req)
  }

  override def userProfileDetails(sessionId: String): Future[UserProfileDetailsResp] = {
    val path = s"$apiBasePath/user/profile/session-id/$sessionId"
    connector.doGet[UserProfileDetailsResp](path)
  }

  override def updateUserProfile(sessionId: String, req: MultipartFormData): Future[UserProfileUpdateResp] = {
    val path = s"$apiBasePath/user/profile/update/session-id/$sessionId"
    connector.doPost[MultipartFormData, UserProfileUpdateResp](path, req)
  }

  override def developerProfileDetails(req: UserDetailsByUsernameReq): Future[DeveloperProfileDetailsResp] = {
    val path = s"$apiBasePath/developer/profile"
    connector.doPost[UserDetailsByUsernameReq, DeveloperProfileDetailsResp](path, req)
  }
}
