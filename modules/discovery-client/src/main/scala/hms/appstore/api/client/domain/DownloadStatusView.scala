package hms.appstore.api.client.domain

import scala.beans.BeanProperty
import org.joda.time.DateTime
import hms.appstore.api.json.DownloadStatus


class DownloadStatusView {
  @BeanProperty var status: String = ""
  @BeanProperty var requestedDate: DateTime = _
  @BeanProperty var contentId: String = ""
}


object DownloadStatusView {
  lazy val emptyView = new DownloadStatusView
  
  def apply(dnStatusOpt: Option[DownloadStatus]): DownloadStatusView = {
    dnStatusOpt.fold(DownloadStatusView.emptyView) {
      dnStatus => {
        val view = new DownloadStatusView
        view.status = dnStatus.status
        view.requestedDate = dnStatus.requestedDate
        view.contentId = dnStatus.contentId
        view
      }
    }
  }
}

