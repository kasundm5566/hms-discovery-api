package hms.appstore.api.client

import concurrent.Future
import hms.appstore.api.json._
import hms.appstore.api.json.CategoryAddReq
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryReq
import hms.appstore.api.json.ClientVersion
import hms.appstore.api.json.CategoryEditReq
import hms.appstore.api.json.AdUrl
import hms.appstore.api.json.UpdateReq
import hms.appstore.api.json.SearchQuery
import hms.appstore.api.json.Banner
import hms.appstore.api.json.ParentCategory
import hms.appstore.api.json.AuthenticateResp
import hms.appstore.api.json.BannerAddReq
import hms.appstore.api.json.MPinChangeReq
import hms.appstore.api.json.MPinChangeResp
import hms.appstore.api.json.AppCount
import hms.appstore.api.json.UserDetailsResp

trait DiscoveryInternalService {
  def parentCategories(): Future[QueryResults[ParentCategory]]

  def findApp(appId: String): Future[QueryResult[Application]]

  def findAppChargingDetails(appId: String): Future[QueryResults[ChargingData]]

  def findChargingDescriptionMessageFormats(appId: String): Future[QueryResults[ChargingDescriptionMessage]]

  def findAllApps(start: Int = 0, limit: Int = 10): Future[QueryResults[Application]]

  def findAllDownloadableApps(start: Int = 0, limit: Int = 10): Future[QueryResults[Application]]

  def findPendingApps(start: Int = 0, limit: Int = 10): Future[QueryResults[Application]]

  def countPendingApps(): Future[QueryResult[AppCount]]

  def countAllApps(): Future[QueryResult[AppCount]]

  def count(query: SearchQuery): Future[QueryResult[AppCount]]

  def searchApps(query: SearchQuery): Future[QueryResults[Application]]

  def requestApp(appId: String, remarks: String): Future[StatusCode]

  def requestApp(appId: String, remarks: String, userId: String): Future[StatusCode]

  def approveApp(appId: String, publishName: String, remarks: String): Future[StatusCode]

  def rejectApp(appId: String, remarks: String): Future[StatusCode]

  def publishApp(appId: String, publishName: String, remarks: String): Future[StatusCode]

  def unpublishApp(appId: String, remarks: String): Future[StatusCode]

  @deprecated
  def updateApp(req: UpdateReq): Future[StatusCode]

  def updateAppAndCharging(req: UpdateAppAndChargingReq): Future[StatusCode]

  def updateFeaturedApps(positions: Map[String, Int]): Future[StatusCode]

  def createOrUpdateParentCategories(pcs: List[ParentCategory]): Future[StatusCode]

  def findAdUrls: Future[QueryResults[AdUrl]]

  def createOrUpdateAdUrls(urls: List[AdUrl]): Future[StatusCode]

  def changeMpin(req: MPinChangeReq): Future[MPinChangeResp]

  def authenticateByUsername(userName: String): Future[AuthenticateResp]

  def authenticateByMsisdn(msisdn: String): Future[AuthenticateResp]

  def findAppOrPendingApp(appId: String): Future[QueryResult[Application]]

  def userDetailsByMsisdn(msisdn: String): Future[UserDetailsResp]

  def removeComment(commentId: String): Future[StatusCode]

  def clientPlatformVersion(platform: String, osVersion: String): Future[ClientVersion]

  def addCategory(req: CategoryAddReq): Future[StatusCode]

  def removeCategory(id: String): Future[StatusCode]

  def editCategory(id: String, req: CategoryEditReq): Future[StatusCode]

  def addOrRemoveAppsOfCategory(id: String, req: AddOrRemoveAppsOfCategoryReq): Future[StatusCode]

  def addBanner(req: BannerAddReq): Future[StatusCode]

  def editBanner(id: Int, req: BannerAddReq): Future[StatusCode]

  def removeBanner(id: Int): Future[StatusCode]

  def getBanners(start: Int = 0, limit: Int = Int.MaxValue): Future[QueryResults[Banner]]

  def getBannerLocations(): Future[QueryResults[BannerLocation]]

  def getBannersCount(): Future[QueryResult[BannersCount]]

  def getBannersCountByLocation(location: String): Future[QueryResult[BannersCount]]

  def getBannersByLocation(location: String, start: Int = 0, limit: Int = Int.MaxValue): Future[QueryResults[Banner]]

  def notifyAll(req: NotifyReq): Future[NotificationResp]

  def notifyAppSpecific(req: NotifyReq): Future[NotificationResp]

  def notifyAppUpdate(req: AppUpdateNotificationReq): Future[NotificationResp]
}
