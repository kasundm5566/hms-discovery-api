package hms.appstore.api.client.domain

import hms.appstore.api.json.Category
import scala.beans.BeanProperty

/**
 * Bean friendly scala class to bind an application to a view (JSP/Spring MVC)
 */
class CategoryView {
  @BeanProperty var id: String = ""
  @BeanProperty var name: String = ""
  @BeanProperty var description: String = ""
  @BeanProperty var image_url: String = ""
  @BeanProperty var position: Int = 0
}

object CategoryView {
  def apply(category: Category): CategoryView = {
    val view = new CategoryView
    view.id = category.id
    view.name = category.name
    view.description = category.description
    view.image_url = category.image_url
    view.position = category.position
    view
  }
}
