package hms.appstore.api.client

import hms.appstore.api.json._
import concurrent.Future
import spray.http.{MultipartFormData, HttpHeader}

trait DiscoveryService {

  def categories(): Future[QueryResults[Category]]

  def categoriesForPaging(start: Int = 0, limit: Int = 7): Future[QueryResults[Category]]

  def parentCategories(): Future[QueryResults[ParentCategory]]

  def mostlyUsedApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def newlyAddedApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def featuredApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def topRatedApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def freeApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def appsByCategory(category: String, start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def appsByCategoryWithoutSorting(category: String, start: Int = 0, limit: Int = Int.MaxValue): Future[QueryResults[Application]]

  def searchApps(query: String, start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def allAppIds(start: Int = 0, limit: Int = 7): Future[QueryResults[String]]

  def appDetails(appId: String): Future[QueryResult[Application]]

  def appDetails(sessionId: String, appId: String): Future[QueryResult[Application]]

  def appCount(): Future[QueryResult[AppCount]]

  def authProviders(platform: String, headers: List[HttpHeader]): Future[AuthProviders]

  def authenticateByPassword(req: AuthenticateByPasswordReq): Future[AuthenticateResp]

  def authenticateByMsisdn(headers: List[HttpHeader]): Future[AuthenticateResp]

  def authenticateByMpin(req: AuthenticateByMPinReq): Future[AuthenticateResp]

  def validateSession(sessionId: String): Future[SessionValidationResp]

  def mySubscriptions(sessionId: String): Future[QueryResults[Application]]

  def myDownloads(sessionId: String): Future[QueryResults[Application]]

  def myDownloadsWithBinaries(sessionId: String): Future[QueryResults[Application]]

  def updateCount(sessionId: String): Future[QueryResult[UpdateCountResp]]

  def subscribe(sessionId: String, appId: String): Future[SubscriptionActionResp]

  def unSubscribe(sessionId: String, appId: String): Future[SubscriptionActionResp]

  def download(sessionId: String, appId: String, contentId: String): Future[DownloadResp]

  def downloadWithChannel(sessionId: String, appId: String, contentId: String, channel: String): Future[DownloadResp]

  def downloadWithMobileAccount(sessionId: String, downloadRequestId: String, piName: String): Future[DownloadChargeResp]

  def downloadWithMpaisa(sessionId: String, downloadRequestId: String, piName: String, pin: String): Future[DownloadChargeResp]

  def countByCategory(category: String): Future[QueryResult[AppCount]]

  def mostlyUsedAppsByCategory(start: Int = 0, limit: Int = 7, category: String): Future[QueryResults[Application]]

  def searchRelated(appId: String, start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def addComment(userComment: UserComment, sessionId: String): Future[StatusCode]

  def reportAbuse(sessionId: String, appId: String, comment: String, channel: Option[String]): Future[StatusCode]

  def findAppsFromEachCategory(start: Int = 0, limit: Int = 7): Future[QueryResult[Map[String, List[Application]]]]

  def findPaymentInstruments(sessionId : String, appId: String): Future[PaymentInstrumentsResp]

  def createUser(userCreateReq:UserRegistrationReq):Future[UserRegistrationResp]

  def verifyMsisdn(verificationReq:MsisdnVerificationReq):Future[MsisdnVerificationResp]

  def requestNewVerificationCode(codeReq:MsisdnVerificationCodeReq):Future[MsisdnVerificationResp]

  def userDetailsByUserName(req:UserDetailsByUsernameReq):Future[UserDetailByUserNameResp]

  def recoverUserAccount(req:UserAccountRecoveryReq):Future[UserAccountRecoveryResp]

  def downloadableFeaturedApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def popularApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def similarApps(appId: String, start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def subscriptionApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def downloadableApps(start: Int = 0, limit: Int = 7): Future[QueryResults[Application]]

  def bannersByLocation(location: String): Future[QueryResults[Banner]]

  def appsByDeveloper(start: Int = 0, limit: Int = 7, developer: String): Future[QueryResults[Application]]

  def rateApp(req: AppRatingAddReq): Future[StatusCode]

  def userProfileDetails(sessionId: String): Future[UserProfileDetailsResp]

  def updateUserProfile(sessionId: String, req: MultipartFormData): Future[UserProfileUpdateResp]

  def developerProfileDetails(req: UserDetailsByUsernameReq): Future[DeveloperProfileDetailsResp]
}
