package hms.appstore.api.client.domain

import scala.beans.BeanProperty
import hms.appstore.api.json.{ChargingData}

/**
 * Created by kasun on 5/30/18.
 */
class ChargingDataView {
  @BeanProperty var ncsType: String = ""
  @BeanProperty var chargingType: String = ""
  @BeanProperty var amount: String = ""
  @BeanProperty var direction: String = ""
  @BeanProperty var frequency: String = ""
}

object ChargingDataView {
  def apply(chargingData: ChargingData): ChargingDataView = {
    val view = new ChargingDataView
    view.ncsType = chargingData.ncsType
    view.chargingType = chargingData.chargingType
    view.amount = chargingData.amount.getOrElse("")
    view.direction = chargingData.direction.getOrElse("")
    view.frequency = chargingData.frequency.getOrElse("")
    view
  }
}
