package hms.appstore.api.client.domain

import hms.appstore.api.json.UserComment
import scala.beans.BeanProperty
import java.util.Date

class UserCommentView {

  @BeanProperty var commentId:  String  = ""
  @BeanProperty var date: Date  = new Date()
  @BeanProperty var appId:  String  = ""
  @BeanProperty var comments: String  = ""
  @BeanProperty var userName: String  = ""
  @BeanProperty var image: String  = ""
  @BeanProperty var abuse:  Boolean = false


}

object UserCommentView {
  def apply(userComment: UserComment): UserCommentView = {

    val commentView = new UserCommentView
    val date        = new Date(userComment.dateTime)

    commentView.commentId = userComment.commentId.getOrElse("comment")
    commentView.date      = date
    commentView.appId     = userComment.appId
    commentView.abuse     = userComment.abuse.getOrElse(false)
    commentView.comments  = userComment.comments
    commentView.userName  = userComment.username.getOrElse("user")
    commentView.image = userComment.image.getOrElse("")
    commentView
  }
}
