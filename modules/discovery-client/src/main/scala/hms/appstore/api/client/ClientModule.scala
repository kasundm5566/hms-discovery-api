package hms.appstore.api.client

import com.escalatesoft.subcut.inject.NewBindingModule
import com.typesafe.config.ConfigFactory

import concurrent.ExecutionContext
import java.util.concurrent.{SynchronousQueue, TimeUnit, ThreadPoolExecutor, Executors}
import hms.scala.http.util.ConfigUtils

object ClientModule extends NewBindingModule({
  implicit module =>
    val config = ConfigFactory.load(classOf[ConfigFactory].getClassLoader)
    val subConfig = ConfigUtils.prepareSubConfig(config, "appstore.discovery.api")

    module.bind[String] idBy "discovery.api.host" toSingle subConfig.getString("host")
    module.bind[Int] idBy "discovery.api.port" toSingle subConfig.getInt("port")

    module.bind[DiscoveryService] toSingle new DiscoveryServiceImpl
    module.bind[DiscoveryInternalService] toSingle new DiscoveryInternalServiceImpl
})
