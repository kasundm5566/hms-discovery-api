package hms.appstore.api.client.util

import scala.collection.JavaConverters._


import java.util.{List => JList, Map => JMap, Set => JSet}

object DnPlatformUtils {

  def dnPlatforms(in: Map[String, List[Map[String, List[Map[String, String]]]]]): JMap[String, JSet[String]] = {
    def combinePlatformAndVersion(platform: String, m: Map[String, List[Map[String, String]]]) = {
      (platform, m.keys.map {
        v => platform + "-" + v
      })
    }

    val result = in.map {
      case (platform, devicesAndContents) => {
        devicesAndContents.map(m => combinePlatformAndVersion(platform, m)).toMap
      }
    }.fold(Map.empty)((m1, m2) => m1 ++ m2)

    result.map {
      case (k, v) => (k, v.toSet.asJava)
    }.asJava
  }


  def dnPlatformsAndContentIds(in: Map[String, List[Map[String, List[Map[String, String]]]]]): JMap[String, JSet[String]] = {

    def combinePlatformVersionAndDnVersion(platform: String, devicesAndContents: List[Map[String, List[Map[String, String]]]]) = {
      (platform, devicesAndContents.map(m => m.map {
        case (k, v) => (platform + "-" + k, v)
      }))
    }

    val result = in.map {
      case (platform, devicesAndContents) => combinePlatformVersionAndDnVersion(platform, devicesAndContents)
    }.values.fold(List.empty)((m1, m2) => m1 ++ m2).map {
      m => m.map {
        case (platformVersion, dnVersions) => (platformVersion, dnVersions.map {m => m.getOrElse("content-id", "")})
      }
    }.fold(Map.empty)((m1, m2) => m1 ++ m2)

    result.map {
      case (k, v) => (k, v.toSet.asJava)
    }.asJava
  }

}
