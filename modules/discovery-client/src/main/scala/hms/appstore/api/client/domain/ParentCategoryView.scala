package hms.appstore.api.client.domain

import beans.BeanProperty
import hms.appstore.api.json.ParentCategory

/**
 * Bean friendly scala class to bind an application to a view (JSP/Spring MVC)
 */
class ParentCategoryView {
  @BeanProperty  var id : String = ""
  @BeanProperty  var category : String = ""
}


object ParentCategoryView {
  def apply(pc: ParentCategory): ParentCategoryView = {
    val view = new ParentCategoryView
    view.id = pc.id
    view.category = pc.category

    view
  }
}
