package hms.appstore.api.client


import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable
import hms.specs.matcher.FutureMatchers

class DiscoveryServiceImplSpecs extends Specification with Injectable with FutureMatchers {

  val bindingModule = ClientModule


  private val discoveryService = inject[DiscoveryService]

  "DiscoveryService" should {
    "list Categories" in {
      discoveryService.categories must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size must beGreaterThanOrEqualTo(1)
      }
    }

    "list mostly used apps" in {
      discoveryService.mostlyUsedApps(0,7) must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size must beLessThanOrEqualTo(8)
      }
    }

    "list featured apps" in {
      discoveryService.featuredApps(0,7) must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size must beLessThanOrEqualTo(8)
      }
    }

    "list newly added apps" in {
      discoveryService.newlyAddedApps(0,7) must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size must beLessThanOrEqualTo(25)
      }
    }

    "list applications by category" in {
      discoveryService.appsByCategory("Alert", 0, 24) must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size must beLessThanOrEqualTo(25)
      }
    }

    "application search results" in {
      discoveryService.searchApps("a", 0, 24) must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size must beLessThanOrEqualTo(25)
      }
    }

    "count all apps" in {
      discoveryService.appCount()   must beSuccessWithin(10000).like {
        case qr => qr.getResult.count must beGreaterThanOrEqualTo(1L)
      }
    }

    "find all app ids" in {
      discoveryService.allAppIds()   must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size must beGreaterThanOrEqualTo(1)
      }
    }

    "list mostly used Business apps" in {
      discoveryService.mostlyUsedAppsByCategory(0,7,"Business") must beSuccessWithin(1000).like{
        case qrs => qrs.getResults.size must beLessThanOrEqualTo(8)
      }
    }

    "count applications by category" in{
      discoveryService.countByCategory("Business") must beSuccessWithin(1000).like{
        case qrs => qrs.getResult.count must beGreaterThanOrEqualTo(0L)
      }
    }
  }
}
