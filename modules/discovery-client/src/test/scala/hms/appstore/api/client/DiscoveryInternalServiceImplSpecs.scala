package hms.appstore.api.client

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable
import hms.specs.matcher.FutureMatchers
import hms.appstore.api.json._
import hms.appstore.api.json.SearchQuery
import hms.appstore.api.json.UpdateReq

class DiscoveryInternalServiceImplSpecs extends Specification with Injectable with FutureMatchers {

  val bindingModule = ClientModule


  private val discoveryInternalService = inject[DiscoveryInternalService]

  "DiscoveryInternalService" should {
    "find all apps" in {
      discoveryInternalService.findAllApps() must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size must beGreaterThanOrEqualTo(1)
      }
    }

    "find pending apps" in {
      discoveryInternalService.findPendingApps() must beSuccessWithin(10000).like {
        case qrs => {
          qrs.getResults.size must beGreaterThanOrEqualTo(1)
        }
      }
    }

    "count pending apps" in {
      discoveryInternalService.countPendingApps() must beSuccessWithin(10000).like {
        case qrs => {
          qrs.getResult.count must beGreaterThanOrEqualTo(1L)
        }
      }
    }

    "search apps" in {
      discoveryInternalService.searchApps(SearchQuery()) must beSuccessWithin(10000).like {
        case qrs => {
          qrs.getResults.size must beGreaterThanOrEqualTo(1)
        }
      }
    }

    "update featured apps" in {
      discoveryInternalService.updateFeaturedApps(
        Map("APP_000678" -> 1, "APP_002041" -> 2, "APP_002078" -> 3, "APP_002084" -> 4)
      ) must beSuccessWithin(10000).like {
        case statusCode => {
          statusCode.status must beEqualTo("S1000")
        }
      }
    }

    "approve an app" in {
      discoveryInternalService.approveApp("APP_000678", "HelloWorld", "Approving...") must beSuccessWithin(10000).like {
        case statusCode => {
          statusCode.status must beEqualTo("S1000")
        }
      }
    }

    "reject an app" in {
      discoveryInternalService.approveApp("1234", "HelloWorld", "Rejected for everrr...") must beSuccessWithin(10000).like {
        case statusCode => {
          statusCode.status must beEqualTo("S1000")
        }
      }
    }

    "publish an app" in {
      discoveryInternalService.approveApp("1234", "HelloWorld", "Publish...") must beSuccessWithin(10000).like {
        case statusCode => {
          statusCode.status must beEqualTo("S1000")
        }
      }
    }

    "unpublish an app" in {
      discoveryInternalService.approveApp("APP_001907", "HelloWorld", "Unpublish...") must beSuccessWithin(10000).like {
        case statusCode => {
          statusCode.status must beEqualTo("S1000")
        }
      }
    }

    "update an non existing app" in {
      discoveryInternalService.updateApp(
        UpdateReq("123", None, None, None, None, None, List.empty, Map.empty, Map.empty)
      ) must beSuccessWithin(10000).like {
        case statusCode => {
          statusCode.status must beEqualTo("E5101")
        }
      }
    }


    "update an existing app" in {
      discoveryInternalService.updateApp(
        UpdateReq("APP_001907", None, None, None, None, None, List.empty, Map.empty, Map.empty)
      ) must beSuccessWithin(10000).like {
        case statusCode => {
          statusCode.status must beEqualTo("S1000")
        }
      }
    }
  }
}
