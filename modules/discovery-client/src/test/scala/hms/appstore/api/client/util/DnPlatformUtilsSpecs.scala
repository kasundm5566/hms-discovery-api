package hms.appstore.api.client.util

import org.specs2.mutable.Specification


import scala.collection.JavaConverters._

class DnPlatformUtilsSpecs extends Specification {

  "transform empty dn binaries" in {
    DnPlatformUtils.dnPlatforms(Map.empty)
    DnPlatformUtils.dnPlatformsAndContentIds(Map.empty)

    success
  }


  "transform dn binaries" in {
    val result = Map(
      "Android" -> List(
        Map("4.0" -> List(Map("content-id" -> "1313111813160007", "version" -> "1")))
      )
    )

    DnPlatformUtils.dnPlatforms(result).asScala must haveKey("Android")
    DnPlatformUtils.dnPlatforms(result).asScala must haveValue(Set("Android-4.0").asJava)

    DnPlatformUtils.dnPlatformsAndContentIds(result).asScala must haveKey("Android-4.0")
    DnPlatformUtils.dnPlatformsAndContentIds(result).asScala must haveValue(Set("1313111813160007").asJava)

    success
  }


  "transform large dn binaries" in {
    val result = Map(
      "Android" -> List(
        Map(
          "4.0" -> List(Map("content-id" -> "1313111813160007", "version" -> "1")),
          "4.1" -> List(Map("content-id" -> "1313111813160008", "version" -> "1"))
        )
      ),
      "J2ME" -> List(
        Map(
          "CLDC 1.0" -> List(Map("content-id" -> "1213111813160007", "version" -> "1")),
          "CLDC 1.1" -> List(Map("content-id" -> "1213111813160008", "version" -> "1"))
        )
      )

    )

    DnPlatformUtils.dnPlatforms(result).asScala must haveKeys("Android", "J2ME")
    DnPlatformUtils.dnPlatforms(result).asScala must haveValue(Set("Android-4.0", "Android-4.1").asJava)
    DnPlatformUtils.dnPlatforms(result).asScala must haveValue(Set("J2ME-CLDC 1.0", "J2ME-CLDC 1.1").asJava)

    DnPlatformUtils.dnPlatformsAndContentIds(result).asScala must haveKeys("Android-4.0", "Android-4.1", "J2ME-CLDC 1.0", "J2ME-CLDC 1.1")
    DnPlatformUtils.dnPlatformsAndContentIds(result).asScala must haveValue(Set("1313111813160007").asJava)
    DnPlatformUtils.dnPlatformsAndContentIds(result).asScala must haveValue(Set("1313111813160008").asJava)
    DnPlatformUtils.dnPlatformsAndContentIds(result).asScala must haveValue(Set("1213111813160007").asJava)
    DnPlatformUtils.dnPlatformsAndContentIds(result).asScala must haveValue(Set("1213111813160008").asJava)

    success
  }

}
