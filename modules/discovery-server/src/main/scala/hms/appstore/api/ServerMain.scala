/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.webapp.WebAppContext
import org.eclipse.jetty.server.nio.SelectChannelConnector
import com.typesafe.config.ConfigFactory
import hms.scala.http.util.ConfigUtils

/**
 * Main class which boots the Discovery API service in an embedded jetty.
 */
object ServerMain {

  def main(args: Array[String]) {


    val server = new Server()


    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.discovery.api")
    val connector = new SelectChannelConnector()
    connector.setHost(conf.getString("host"))
    connector.setPort(conf.getInt("port"))
    server.addConnector(connector)

    val context = new WebAppContext()

    context.setResourceBase("conf/jetty-base/")
    context.setDescriptor("conf/WEB-INF/web.xml")

    context.setContextPath("/")
    context.setParentLoaderPriority(true)

    server.setHandler(context)

    server.start()
    server.join()
  }
}
