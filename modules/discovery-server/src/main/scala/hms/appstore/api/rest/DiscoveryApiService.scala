/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.rest

import com.escalatesoft.subcut.inject.Injectable

import spray.routing._
import spray.http.StatusCodes._

import hms.appstore.api.json._

import hms.appstore.api.service._
import hms.appstore.api.service.query._
import hms.appstore.api.service.admin._
import hms.appstore.api.service.cms.DownloadService
import hms.appstore.api.service.auth.{UserRegService, AuthService}
import hms.appstore.api.service.governance.ReportAbuseService
import hms.appstore.api.service.subscription.SubscriptionService
import hms.appstore.api.service.caas.PaymentInstrumentService
import hms.scala.http.marshalling.HttpMarshalling

import spray.http.HttpHeaders.RawHeader
import spray.http.{MultipartFormData, HttpMethods, HttpEntity}

import scala.concurrent.{ExecutionContext, Future}
import java.net.URLDecoder
import shapeless.HList
import javax.mail.Multipart
import hms.appstore.api.service.notification.NotificationService

trait DiscoveryApiService extends HttpService with Injectable with EventLogging {

  import HttpMarshalling._

  val bindingModule = ServiceModule


  //common services
  private val authService = inject[AuthService]
  private val bannerService = inject[BannerService]
  private val userRegService = inject[UserRegService]
  private val categoryService = inject[CategoryService]
  private val appFindService = inject[AppFindService]
  private val downloadService = inject[DownloadService]
  private val appSearchService = inject[AppSearchService]
  private val appDetailsService = inject[AppDetailsService]
  private val featuredAppService = inject[FeaturedAppService]
  private val reportAbuseService = inject[ReportAbuseService]
  private val subscriptionService = inject[SubscriptionService]
  private val appCommentService = inject[AppCommentService]
  private val appRatingService = inject[AppRatingService]
  private val appCountService = inject[AppCountService]
  private val clientPlatformService = inject[ClientPlatformService]
  private val paymentInstrumentService = inject[PaymentInstrumentService]
  private val notificationService = inject[NotificationService]

  //admin services
  private val sysConfigService = inject[SysConfigService]
  private val appUpdateService = inject[AppDetailsModificationService]
  private val appStateMgmtService = inject[AppStateMgmtService]
  private val featuredAppMgmtService = inject[FeaturedAppMgmtService]
  private val categoryConfigService = inject[CategoryConfigService]
  private val bannerConfigService = inject[BannerConfigService]
  private val chargingConfigService = inject[ChargingConfigService]
  private val adUrlConfigService = inject[AdUrlConfigService]

  //additional http directives CROS handling
  override val options = method(HttpMethods.OPTIONS)

  val API_HEADERS = {
    List(RawHeader("Access-Control-Allow-Origin", sysConfigService.accessControlAllowedHosts.mkString("|")))
  }

  val API_PRE_FLIGHT_HEADERS = {
    List(
      RawHeader("Access-Control-Allow-Origin", sysConfigService.accessControlAllowedHosts.mkString("|")),
      RawHeader("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Content-Type"),
      RawHeader("Content-Type", "application/json")
    )
  }

  /**
   * [[hms.appstore.api.service.ServiceException]] handler for api routes
   * convert thrown exception into proper HTTP json response
   */
  def serviceExceptionHandler = {
    import ServiceJsonInternalProtocol._
    ExceptionHandler.apply {
      case se: ServiceException => ctx =>
        logger.error(s"Request [${ctx.request.message.uri}] could not be handled normally due to an error!", se)
        eventLog(se.getCorrelationId, se.getThrownFrom, se.getStatusCode)
        ctx.complete(OK, se.getStatusCode)
    }
  }

  /**
   * [[java.lang.Exception]] handler for api routes
   * convert thrown exception into proper HTTP json response
   */
  def generalExceptionHandler = {
    import ServiceJsonInternalProtocol._
    ExceptionHandler.apply {
      case e: Exception => ctx =>
        logger.error(s"Request [${ctx.request.message.uri}] could not be handled normally due to an unexpected error", e)
        ctx.complete(OK, E5000.asStatusCode)
    }
  }

  implicit def executionContext: ExecutionContext = actorRefFactory.dispatcher

  implicit def sc: ServiceContext = ServiceContext(executionContext)

  /**
   * Backward compatible path matcher
   */
  override def path[L <: HList](pm: PathMatcher[L]): Directive[L] = pathPrefix(pm ~ Slash.? ~ PathEnd)

  /**
   * Discovery API serving URL routes
   */
  val apiRoute = {
    pathPrefix("discovery-api" / "v2") {
      get {
        import ServiceJsonProtocol._
        path("categories") {
          complete {
            (OK, API_HEADERS, categoryService.findCategories())
          }
        } ~
          path("categories" / "start" / IntNumber / "limit" / IntNumber) {
            (start, limit) => complete {
              (OK, API_HEADERS, categoryService.findCategories(start, limit))
            }
          } ~
          path("category" / Segment / "start" / IntNumber / "limit" / IntNumber) {
            (category, start, limit) => complete {
              (OK, API_HEADERS, appFindService.findByCategory(start, limit, category))
            }
          } ~
          path("category-without-sorting" / Segment / "start" / IntNumber / "limit" / IntNumber) {
            (category, start, limit) => complete {
              (OK, API_HEADERS, appFindService.findCategoryApps(category, start, limit))
            }
          } ~
          path("parent-categories") {
            complete {
              (OK, API_HEADERS, categoryConfigService.findAllParentCategories)
            }
          } ~
          pathPrefix("parent-category") {
            path("newly-added" / "start" / IntNumber / "limit" / IntNumber) {
              (start, limit) => complete {
                (OK, API_HEADERS, appFindService.findNewlyAdded(start, limit))
              }
            } ~
              path("newly-added" / "category" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (category, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findNewlyAdded(start, limit, Some(category)))
                }
              } ~
              path("newly-added" / "platform" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (platform, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findNewlyAdded(start, limit, None, Some(platform)))
                }
              } ~
              path("newly-added" / "category" / Segment / "platform" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (category, platform, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findNewlyAdded(start, limit, Some(category), Some(platform)))
                }
              } ~
              path("mostly-used" / "start" / IntNumber / "limit" / IntNumber) {
                (start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findMostlyUsed(start, limit))
                }
              } ~
              path("mostly-used" / "category" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (category, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findMostlyUsed(start, limit, Some(category)))
                }
              } ~
              path("mostly-used" / "platform" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (platform, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findMostlyUsed(start, limit, None, Some(platform)))
                }
              } ~
              path("mostly-used" / "category" / Segment / "platform" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (category, platform, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findMostlyUsed(start, limit, Some(category), Some(platform)))
                }
              } ~
              path("top-rated" / "start" / IntNumber / "limit" / IntNumber) {
                (start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findTopRated(start, limit))
                }
              } ~
              path("top-rated" / "category" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (category, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findTopRated(start, limit, Some(category)))
                }
              } ~
              path("top-rated" / "platform" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (platform, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findTopRated(start, limit, None, Some(platform)))
                }
              } ~
              path("top-rated" / "category" / Segment / "platform" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (category, platform, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findTopRated(start, limit, Some(category), Some(platform)))
                }
              }
          } ~
          path("search" / Segment / "start" / IntNumber / "limit" / IntNumber) {
            (searchTxt, start, limit) => complete {
              (OK, API_HEADERS, appSearchService.search(SearchQuery(searchText = Some(URLDecoder.decode(searchTxt, "UTF-8")),
                status = Some(AppStatus.Published), start = start, limit = limit)))
            }
          } ~
          path("search" / Segment / "exclude" / Segment / "start" / IntNumber / "limit" / IntNumber) {
            (searchTxt, excludingCategory, start, limit) => complete {
              (OK, API_HEADERS, appSearchService.searchExcludingCategory(searchText = searchTxt, excludingCategory = excludingCategory, start = start, limit = limit))
            }
          } ~
          path("related-applications" / "app-id" / Segment / "start" / IntNumber / "limit" / IntNumber) {
            (appId, start, limit) => complete {
              (OK, API_HEADERS, appSearchService.searchRelated(appId, start, limit))
            }
          } ~
          path("featured-apps") {
            complete {
              (OK, API_HEADERS, featuredAppService.findApps(0, Int.MaxValue))
            }
          } ~
          path("featured-apps" / "start" / IntNumber / "limit" / IntNumber) {
            (start, limit) => complete {
              (OK, API_HEADERS, featuredAppService.findApps(start, limit))
            }
          } ~
          path("featured-apps" / "downloadable") {
            complete {
              (OK, API_HEADERS, featuredAppService.findDownloadableApps(0, Int.MaxValue))
            }
          } ~
          path("featured-apps" / "downloadable" / "start" / IntNumber / "limit" / IntNumber) {
            (start, limit) => complete {
              (OK, API_HEADERS, featuredAppService.findDownloadableApps(start, limit))
            }
          } ~
          path("popular-apps") {
            complete {
              (OK, API_HEADERS, appFindService.findTopRated(0, Int.MaxValue))
            }
          } ~
          path("popular-apps" / "start" / IntNumber / "limit" / IntNumber) {
            (start, limit) => complete {
              (OK, API_HEADERS, appFindService.findTopRated(start, limit))
            }
          } ~
          path("similar-apps" / "app-id" / Segment) {
            (appId) => complete {
              (OK, API_HEADERS, appFindService.findSimilarApps(appId, 0, Int.MaxValue))
            }
          } ~
          path("similar-apps" / "app-id" / Segment / "start" / IntNumber / "limit" / IntNumber) {
            (appId, start, limit) => complete {
              (OK, API_HEADERS, appFindService.findSimilarApps(appId, start, limit))
            }
          } ~
          path("subscription-apps") {
            complete {
              (OK, API_HEADERS, appFindService.findSubscriptionApps(0, Int.MaxValue))
            }
          } ~
          path("subscription-apps" / "start" / IntNumber / "limit" / IntNumber) {
            (start, limit) => complete {
              (OK, API_HEADERS, appFindService.findSubscriptionApps(start, limit))
            }
          } ~
          path("downloadable-apps") {
            complete {
              (OK, API_HEADERS, appFindService.findDownloadableApps(0, Int.MaxValue))
            }
          } ~
          path("downloadable-apps" / "start" / IntNumber / "limit" / IntNumber) {
            (start, limit) => complete {
              (OK, API_HEADERS, appFindService.findDownloadableApps(start, limit))
            }
          } ~
          path("banners" / "location" / Segment) {
            (location) => complete {
              (OK, API_HEADERS, bannerService.find(location))
            }
          } ~
          path("apps" / "developer" / Segment) {
            (developer) => complete {
              (OK, API_HEADERS, appFindService.findAppsByDeveloper(developer))
            }
          } ~
          path("apps" / "developer" / Segment / "start" / IntNumber / "limit" / IntNumber) {
            (developer, start, limit) => complete {
              (OK, API_HEADERS, appFindService.findAppsByDeveloper(developer, start, limit))
            }
          } ~
          path("push-notifications" / "session-id" / Segment / "firebase-token-id" / Segment /"start" / IntNumber / "limit" / IntNumber) {
            (sessionId, firebaseTokenId, start, limit) => complete {
              (OK, API_HEADERS, notificationService.findDispatchedMessages(sessionId, firebaseTokenId, start, limit))
            }
          } ~
          pathPrefix("free-apps") {
            path("start" / IntNumber / "limit" / IntNumber) {
              (start, limit) => complete {
                (OK, API_HEADERS, appFindService.findFreeApps(start, limit))
              }
            } ~
              path("category" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (category, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findFreeApps(start, limit, Some(category)))
                }
              } ~
              path("platform" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (platform, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findFreeApps(start, limit, None, Some(platform)))
                }
              } ~
              path("category" / Segment / "platform" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (category, platform, start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findFreeApps(start, limit, Some(category), Some(platform)))
                }
              } ~
              path("count") {
                complete {
                  (OK, API_HEADERS, appCountService.countFreeApps())
                }
              } ~
              path("count" / "category" / Segment) {
                category => complete {
                  (OK, API_HEADERS, appCountService.countFreeApps(Some(category)))
                }
              } ~
              path("count" / "downloadable") {
                complete {
                  (OK, API_HEADERS, appCountService.countDownloadableFreeApps())
                }
              } ~
              path("count" / "downloadable" / "category" / Segment) {
                category => complete {
                  (OK, API_HEADERS, appCountService.countDownloadableFreeApps(Some(category)))
                }
              }
          } ~
          pathPrefix("all-apps") {
            path("count") {
              complete {
                (OK, API_HEADERS, appCountService.countAllApps())
              }
            } ~
              path("count" / "category" / Segment) {
                category => complete {
                  (OK, API_HEADERS, appCountService.countByCategory(category))
                }
              } ~
              path("count" / "downloadable") {
                complete {
                  (OK, API_HEADERS, appCountService.countDownloadableApps())
                }
              } ~
              path("count" / "downloadable" / "category" / Segment) {
                category => complete {
                  (OK, API_HEADERS, appCountService.countDownloadableApps(Some(category)))
                }
              } ~
              path("find" / "all-categories" / "start" / IntNumber / "limit" / IntNumber) {
                (start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findAppsFromEachCategory(start, limit))
                }
              } ~
              path("app-ids" / "start" / IntNumber / "limit" / IntNumber) {
                (start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findAllAppIds(start, limit))
                }
              }
          } ~
          pathPrefix("my-apps") {
            path("session-id" / Segment) {
              (sessionId) => complete {
                (OK, API_HEADERS, subscriptionService.mySubscriptions(sessionId, 0, Int.MaxValue))
              }
            } ~
              path("session-id" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (sessionId, start, limit) => complete {
                  (OK, API_HEADERS, subscriptionService.mySubscriptions(sessionId, start, limit))
                }
              }
          } ~
          pathPrefix("my-downloads") {
            path("session-id" / Segment) {
              (sessionId) => complete {
                (OK, API_HEADERS, downloadService.myDownloads(sessionId, 0, Int.MaxValue))
              }
            } ~
              path("session-id" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (sessionId, start, limit) => complete {
                  (OK, API_HEADERS, downloadService.myDownloads(sessionId, start, limit))
                }
              } ~
              path("session-id" / Segment / "with-binaries") {
                (sessionId) => complete {
                  (OK, API_HEADERS, downloadService.myDownloadsWithBinaries(sessionId, 0, Int.MaxValue))
                }
              } ~
              path("session-id" / Segment / "start" / IntNumber / "limit" / IntNumber / "with-binaries") {
                (sessionId, start, limit) => complete {
                  (OK, API_HEADERS, downloadService.myDownloadsWithBinaries(sessionId, start, limit))
                }
              } ~
              path("update-count" / "session-id" / Segment) {
                (sessionId) => complete {
                  (OK, API_HEADERS, downloadService.updateCount(sessionId))
                }
              }
          } ~
          path("report-abuse" / "session-id" / Segment / "app-id" / Segment / "comment" / Segment) {
            (sessionId, appId, comment) => complete {
              (OK, API_HEADERS, reportAbuseService.reportAbuse(sessionId, appId, comment, None))
            }
          } ~
          path("report-abuse" / "session-id" / Segment / "app-id" / Segment / "comment" / Segment / "channel" / Segment) {
            (sessionId, appId, comment, channel) => complete {
              (OK, API_HEADERS, reportAbuseService.reportAbuse(sessionId, appId, comment, Some(channel)))
            }
          } ~
          pathPrefix("app-details") {
            path("app-id" / Segment / "session-id" / Segment) {
              (appId, sessionId) => complete {
                (OK, API_HEADERS, appDetailsService.appDetails(appId, Some(sessionId)))
              }
            } ~
              path("app-id" / Segment) {
                (appId) => complete {
                  (OK, API_HEADERS, appDetailsService.appDetails(appId, None))
                }
              }
          } ~
          path("download" / "session-id" / Segment / "app-id" / Segment / "content-id" / Segment / "channel" / Segment) {
            (sessionId, appId, contentId, channel) => complete {
              (OK, API_HEADERS, downloadService.download(sessionId, appId, contentId, Some(channel)))
            }
          } ~
          path("download" / "session-id" / Segment / "app-id" / Segment / "content-id" / Segment) {
            (sessionId, appId, contentId) => complete {
              (OK, API_HEADERS, downloadService.download(sessionId, appId, contentId, None))
            }
          } ~
          path("download" / "charge" / "session-id" / Segment / "request-id" / Segment / "pi-name" / Segment / "pin" / Segment) {
            (sessionId, requestId, piName, pin) => complete {
              (OK, API_HEADERS, downloadService.downloadWithCharge(sessionId, requestId, piName, Some(pin)))
            }
          } ~
          path("download" / "charge" / "session-id" / Segment / "request-id" / Segment / "pi-name" / Segment) {
            (sessionId, requestId, piName) => complete {
              (OK, API_HEADERS, downloadService.downloadWithCharge(sessionId, requestId, piName, None))
            }
          } ~
          path("subscribe" / "session-id" / Segment / "app-id" / Segment) {
            (sessionId, appId) => complete {
              (OK, API_HEADERS, subscriptionService.subscribe(sessionId, appId))
            }
          } ~
          path("subscription-count" / "session-id" / Segment / "app-id" / Segment) {
            (sessionId, appId) => complete {
              (OK, API_HEADERS, subscriptionService.subscriptionCount(appId))
            }
          } ~
          path("unsubscribe" / "session-id" / Segment / "app-id" / Segment) {
            (sessionId, appId) => complete {
              (OK, API_HEADERS, subscriptionService.unSubscribe(sessionId, appId))
            }
          } ~
          path("user-comment" / "app-id" / Segment / "start" / IntNumber / "limit" / IntNumber) {
            (appId, start, limit) => complete {
              (OK, API_HEADERS, appCommentService.findWithUserImage(appId, start, limit))
            }
          } ~
          path("auth-provider" / "platform" / Segment) {
            (platform) =>
              optionalHeaderValueByName("X-Forwarded-For") {
                host =>
                  optionalHeaderValueByName("msisdn") {
                    (msisdn) => complete {
                      (OK, API_HEADERS, authService.authProviders(platform, msisdn, host))
                    }
                  }
              }
          } ~
          path("login" / "msisdn") {
            optionalHeaderValueByName("X-Forwarded-For") {
              host =>
                optionalHeaderValueByName("msisdn") {
                  msisdn => complete {
                    (OK, API_HEADERS, authService.authenticate(msisdn, host))
                  }
                }
            }
          } ~
          path("appstore" / "latest-version" / "platform" / Segment / "os-version" / Segment) {
            (platform, osVersion) => complete {
              (OK, API_HEADERS, clientPlatformService.findLatestVersion(platform, osVersion))
            }
          } ~
          path("user" / "profile" / "session-id" / Segment) {
            (sessionId) => complete {
              (OK, API_HEADERS, authService.userProfileDetails(sessionId))
            }
          } ~
          pathPrefix("user") {
            path("pi-list" / "session-id" / Segment / "app-id" / Segment) {
              (sessionId, appId) => complete {
                (OK, API_HEADERS, paymentInstrumentService.findPaymentInstruments(sessionId, appId))
              }
            } ~
              path("validate" / "session-id" / Segment) {
                (sessionToken) => complete {
                  (OK, API_HEADERS, authService.validate(SessionValidationReq(sessionToken)))
                }
              }
          }
      } ~
        post {
          import ServiceJsonProtocol._
          path("login") {
            entity(as[AuthenticateByPasswordReq]) {
              authReq => complete {
                (OK, API_HEADERS, authService.authenticate(authReq))
              }
            }
          } ~
            path("login" / "mpin") {
              entity(as[AuthenticateByMPinReq]) {
                authReq => complete {
                  (OK, API_HEADERS, authService.authenticate(authReq))
                }
              }
            } ~
            path("user-comment" / "session-id" / Segment) {
              (sessionId) =>
                entity(as[UserComment]) {
                  userComment => complete {
                    (OK, API_HEADERS, appCommentService.update(sessionId, userComment.appId, userComment.comments, userComment.abuse.getOrElse(false)))
                  }
                }
            } ~
            path("app-rating") {
              entity(as[AppRatingAddReq]) {
                appRating => complete {
                  (OK, API_HEADERS, appRatingService.create(appRating))
                }
              }
            } ~
            path("image-upload") {
              entity(as[ImageUploadReq]) {
                uploadReq => complete {
                  (OK, API_PRE_FLIGHT_HEADERS, appUpdateService.cacheImageUpload(uploadReq))
                }
              }
            } ~
            path("user" / "retrieve") {
              entity(as[UserDetailsByUsernameReq]) {
                userDetailReq => complete {
                  (OK, API_HEADERS, authService.userDetailsByName(userDetailReq))
                }
              }
            } ~
            path("user" / "profile" / "update" / "session-id" / Segment) {
              (sessionId) => entity(as[MultipartFormData]) {
                userDetailReq => complete {
                  (OK, API_HEADERS, authService.userProfileUpdate(sessionId, userDetailReq))
                }
              }
            } ~
            path("developer" / "profile") {
              entity(as[UserDetailsByUsernameReq]) {
                userDetailReq => complete {
                  (OK, API_HEADERS, authService.developerProfileDetails(userDetailReq))
                }
              }
            } ~
            path("device" / "register" / "session-id" / Segment) {
              (sessionId) => entity(as[DeviceRegistrationReq]) {
                deviceRegReq => complete {
                  (OK, API_HEADERS, notificationService.registerDevice(sessionId, deviceRegReq))
                }
              }
            } ~
            path("create" / "user") {
              entity(as[UserRegistrationReq]) {
                userCreateReq => complete {
                  (OK, API_HEADERS, userRegService.createUser(userCreateReq))
                }
              }
            } ~
            path("verify" / "msisdn") {
              entity(as[MsisdnVerificationReq]) {
                verificationReq => complete {
                  (OK, API_HEADERS, userRegService.verifyMsisdn(verificationReq))
                }
              }
            } ~
            path("request" / "verification" / "code") {
              entity(as[MsisdnVerificationCodeReq]) {
                verificationCodeReq => complete {
                  (OK, API_HEADERS, userRegService.requestNewVerificationCode(verificationCodeReq))
                }
              }
            } ~
            path("user" / "account" / "recover") {
              entity(as[UserAccountRecoveryReq]) {
                accountRecoverReq => complete {
                  (OK, API_HEADERS, userRegService.recoveryUserAccount(accountRecoverReq))
                }
              }
            }
        } ~
        get {
          import ServiceJsonInternalProtocol._
          pathPrefix("internal") {
            path("parent-categories" / "find") {
              complete {
                (OK, API_HEADERS, categoryConfigService.findAllParentCategories)
              }
            } ~
              path("banners") {
                complete {
                  (OK, API_HEADERS, bannerConfigService.getBanners())
                }
              } ~
              path("banners" / "start" / IntNumber / "limit" / IntNumber) {
                (start, limit) => complete {
                  (OK, API_HEADERS, bannerConfigService.getBanners(start, limit))
                }
              } ~
              path("banners" / "locations") {
                complete {
                  (OK, API_HEADERS, bannerConfigService.getBannerLocations())
                }
              } ~
              path("banners" / "count") {
                complete {
                  (OK, API_HEADERS, bannerConfigService.getBannersCount())
                }
              } ~
              path("banners" / Segment / "count") {
                (location) => complete {
                  (OK, API_HEADERS, bannerConfigService.getBannersCountByLocation(location))
                }
              } ~
              path("banners" / Segment) {
                (banners) => complete {
                  (OK, API_HEADERS, bannerConfigService.getBannersByLocation(banners))
                }
              } ~
              path("banners" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (banners, start, limit) => complete {
                  (OK, API_HEADERS, bannerConfigService.getBannersByLocation(banners, start, limit))
                }
              } ~
              path("banner" / "remove" / IntNumber) {
                (id) => complete {
                  (OK, API_HEADERS, bannerConfigService.removeBanner(id))
                }
              } ~
              path("category" / "remove" / Segment) {
                (id) => complete {
                  (OK, API_HEADERS, categoryConfigService.removeCategory(id))
                }
              } ~
              path("apps" / "find" / "start" / IntNumber / "limit" / IntNumber) {
                (start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findAll(None, None, start, limit))
                }
              } ~
              path("apps" / "count") {
                complete {
                  (OK, API_HEADERS, appCountService.countAllApps())
                }
              } ~
              path("downloadable-apps" / "start" / IntNumber / "limit" / IntNumber) {
                (start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findDownloadableApps(start, limit))
                }
              } ~
              path("app" / "app-id" / Segment) {
                (appId) => complete {
                  (OK, API_HEADERS, appFindService.findAppOrPendingApp(appId))
                }
              } ~
              path("charging" / "app" / "app-id" / Segment) {
                (appId) => complete {
                  (OK, API_HEADERS, chargingConfigService.findChargingDetails(appId))
                }
              } ~
              path("charging-description-format" / "app" / "app-id" / Segment) {
                (appId) => complete {
                  (OK, API_HEADERS, chargingConfigService.findChargingDescriptionMessageFormat(appId))
                }
              } ~
              path("pending-apps" / "find" / "start" / IntNumber / "limit" / IntNumber) {
                (start, limit) => complete {
                  (OK, API_HEADERS, appFindService.findPending(start, limit))
                }
              } ~
              path("apps" / "search" / Segment / "category" / Segment / "start" / IntNumber / "limit" / IntNumber) {
                (searchText, category, start, limit) => complete {
                  val query = SearchQuery(
                    searchText = Some(searchText),
                    category = Some(category),
                    appId = None,
                    spName = None,
                    appType = None,
                    status = None,
                    start = start,
                    limit = limit
                  )
                  (OK, API_HEADERS, appSearchService.search(query))

                }
              } ~
              path("pending-apps" / "count") {
                complete {
                  (OK, API_HEADERS, appCountService.countByStatus(status = AppStatus.New))
                }
              } ~
              path("app" / Segment / "request" / "remarks" / Segment) {
                (appId, remarks) => complete {
                  (OK, API_HEADERS, appStateMgmtService.request(appId, remarks))
                }
              } ~
              path("app" / Segment / "approve" / "publish-name" / Segment / "remarks" / Segment) {
                (appId, publishName, remarks) => complete {
                  (OK, API_HEADERS, appStateMgmtService.approve(appId, publishName, URLDecoder.decode(remarks, "UTF-8")))
                }
              } ~
              path("app" / Segment / "reject" / "remarks" / Segment) {
                (appId, remarks) => complete {
                  (OK, API_HEADERS, appStateMgmtService.reject(appId, URLDecoder.decode(remarks, "UTF-8")))
                }
              } ~
              path("app" / Segment / "publish" / "publish-name" / Segment / "remarks" / Segment) {
                (appId, publishName, remarks) => complete {
                  (OK, API_HEADERS, appStateMgmtService.publish(appId, publishName, URLDecoder.decode(remarks, "UTF-8")))
                }
              } ~
              path("app" / Segment / "un-publish" / "remarks" / Segment) {
                (appId, remarks) => complete {
                  (OK, API_HEADERS, appStateMgmtService.unpublish(appId, URLDecoder.decode(remarks, "UTF-8")))
                }
              } ~
              path("user-comment" / "remove" / "comment-id" / Segment) {
                (commentId) => complete {
                  (OK, API_HEADERS, appCommentService.remove(commentId))
                }
              } ~
              path("ad-urls" / "find") {
                complete {
                  (OK, API_HEADERS, adUrlConfigService.findAll)
                }
              } ~
              path("auth" / "username" / Segment) {
                (username) => complete {
                  (OK, API_HEADERS, authService.authenticate(AuthenticateByUsernameReq(username)))
                }
              } ~
              path("auth" / "msisdn" / Segment) {
                (msisdn) => complete {
                  (OK, API_HEADERS, authService.authenticate(AuthenticateByMsisdnReq(msisdn)))
                }
              } ~
              path("user-details" / "msisdn" / Segment) {
                (msisdn) => complete {
                  val details: Future[UserDetailsResp] = authService.userDetails(UserDetailsByMsisdnReq(msisdn))
                  (OK, API_HEADERS, details)
                }
              }
          }
        } ~
        post {
          import ServiceJsonInternalProtocol._
          pathPrefix("internal") {
            path("apps" / "count") {
              entity(as[SearchQuery]) {
                searchQuery => complete {
                  (OK, API_HEADERS, appCountService.countBy(searchQuery))
                }
              }
            } ~
              path("apps" / "search") {
                entity(as[SearchQuery]) {
                  searchQuery => complete {
                    (OK, API_HEADERS, appSearchService.searchByName(searchQuery))
                  }
                }
              } ~
              path("app" / "update") {
                entity(as[UpdateReq]) {
                  updateReq => complete {
                    (OK, API_HEADERS, appUpdateService.updateDetails(updateReq))
                  }
                }
              } ~
              path("app" / "update-new") {
                entity(as[UpdateAppAndChargingReq]) {
                  updateReq => complete {
                    (OK, API_HEADERS, appUpdateService.updateDetailsAndCharging(updateReq))
                  }
                }
              } ~
              path("featured-apps" / "update") {
                entity(as[Map[String, Int]]) {
                  positions => complete {
                    (OK, API_HEADERS, featuredAppMgmtService.update(positions))
                  }
                }
              } ~
              path("parent-categories" / "update") {
                entity(as[List[ParentCategory]]) {
                  parentCategories => complete {
                    (OK, API_HEADERS, categoryConfigService.createOrUpdateParentCategories(parentCategories))
                  }
                }
              } ~
              path("category" / "add") {
                entity(as[CategoryAddReq]) {
                  category => complete {
                    (OK, API_HEADERS, categoryConfigService.createCategory(category))
                  }
                }
              } ~
              path("category" / "edit" / Segment) {
                (id) => entity(as[CategoryEditReq]) {
                  category => complete {
                    (OK, API_HEADERS, categoryConfigService.editCategory(id, category))
                  }
                }
              } ~
              path("category" / "update-apps" / Segment) {
                (id) => entity(as[AddOrRemoveAppsOfCategoryReq]) {
                  apps => complete {
                    (OK, API_HEADERS, categoryConfigService.addOrRemoveAppsOfCategory(id, apps))
                  }
                }
              } ~
              path("banner" / "add") {
                entity(as[BannerAddReq]) {
                  banner => complete {
                    (OK, API_HEADERS, bannerConfigService.addBanner(banner))
                  }
                }
              } ~
              path("banner" / "edit" / IntNumber) {
                (id) => entity(as[BannerAddReq]) {
                  banner => complete {
                    (OK, API_HEADERS, bannerConfigService.editBanner(id, banner))
                  }
                }
              } ~
              path("ad-urls" / "update") {
                entity(as[List[AdUrl]]) {
                  adUrls => complete {
                    (OK, API_HEADERS, adUrlConfigService.createOrUpdate(adUrls))
                  }
                }
              } ~
              path("auth" / "change-mpin") {
                entity(as[MPinChangeReq]) {
                  request => complete {
                    (OK, API_HEADERS, authService.changeMPin(request))
                  }
                }
              } ~
              path("notify" / "all") {
                entity(as[NotifyReq]) {
                  request => complete {
                    (OK, API_HEADERS, notificationService.notifyAll(request))
                  }
                }
              } ~
              path("notify" / "app-specific") {
                entity(as[NotifyReq]) {
                  request => complete {
                    (OK, API_HEADERS, notificationService.notifyAppSpecific(request))
                  }
                }
              } ~
              path("notify" / "app-update") {
                entity(as[AppUpdateNotificationReq]) {
                  request => complete {
                    (OK, API_HEADERS, notificationService.notifyAppUpdate(request))
                  }
                }
              } ~
              path("user" / "password" / "reset") {
                entity(as[UserPasswordResetReq]) {
                  userPasswordResetReq => complete {
                    (OK, API_HEADERS, authService.resetUserPassword(userPasswordResetReq))
                  }
                }
              } ~
              path("charging" / "edit") {
                entity(as[ChargingEditReq]) {
                  request => complete {
                    (OK, API_HEADERS, chargingConfigService.editCharging(request))
                  }
                }
              }
          }
        } ~
        options {
          path("image-upload") {
            complete {
              (OK, API_PRE_FLIGHT_HEADERS, HttpEntity.Empty)
            }
          }
        }

    }
  }
}
