/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api

import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.api.service.{SysConfigService, EventLogging, ServiceModule}
import hms.appstore.api.rest.DiscoveryApiServiceActor
import hms.commons.SnmpLogUtil

import spray.servlet.WebBoot
import akka.actor.{Props, ActorSystem}


class DiscoveryServiceBoot extends WebBoot with Injectable with EventLogging {


  val bindingModule = ServiceModule
  val sysConfigService = inject[SysConfigService]

  val system = ActorSystem("discoveryService", sysConfigService.config)

  // the service actor replies to incoming HttpRequests
  val serviceActor = system.actorOf(Props[DiscoveryApiServiceActor])


  system.log.info("discovery-server is started")
  SnmpLogUtil.log(sysConfigService.snmpStartupTrap)

  system.registerOnTermination {
    SnmpLogUtil.log(sysConfigService.snmpShutdownTrap)
    system.log.info("discovery-server is shutdown")
  }

  println(
    """
      | ____                                                                      ____
      |/\  _`\    __                                                             /\  _`\
      |\ \ \/\ \ /\_\     ____    ___     ___    __  __     __   _ __   __  __   \ \,\L\_\      __   _ __   __  __     __   _ __
      | \ \ \ \ \\/\ \   /',__\  /'___\  / __`\ /\ \/\ \  /'__`\/\`'__\/\ \/\ \   \/_\__ \    /'__`\/\`'__\/\ \/\ \  /'__`\/\`'__\
      |  \ \ \_\ \\ \ \ /\__, `\/\ \__/ /\ \L\ \\ \ \_/ |/\  __/\ \ \/ \ \ \_\ \    /\ \L\ \ /\  __/\ \ \/ \ \ \_/ |/\  __/\ \ \/
      |   \ \____/ \ \_\\/\____/\ \____\\ \____/ \ \___/ \ \____\\ \_\  \/`____ \   \ `\____\\ \____\\ \_\  \ \___/ \ \____\\ \_\
      |    \/___/   \/_/ \/___/  \/____/ \/___/   \/__/   \/____/ \/_/   `/___/> \   \/_____/ \/____/ \/_/   \/__/   \/____/ \/_/
      |                                                                     /\___/
      |                                                                     \/__/
    """.stripMargin)
}
