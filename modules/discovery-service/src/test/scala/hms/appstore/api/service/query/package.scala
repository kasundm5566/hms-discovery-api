package hms.appstore.api.service

import concurrent.ExecutionContext


package object query {
  implicit def mockServiceContext = new ServiceContext(ExecutionContext.global)
}
