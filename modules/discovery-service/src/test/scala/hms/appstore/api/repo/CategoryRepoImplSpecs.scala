package hms.appstore.api.repo

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable


class CategoryRepoImplSpecs extends Specification with Injectable  {
  val bindingModule = RepoModule

  val repo = inject[CategoryRepo]

  "category repo" should {


    "find categories" in {
      val categories = repo.findCategories(start = 0, limit = 2)
      categories.size must beEqualTo(2)
      success
    }

    "find super categories" in {
      val featuredCategories = repo.findParentCategories(start = 0, limit = 2)
      featuredCategories.size must beEqualTo(2)
      success
    }

  }

}
