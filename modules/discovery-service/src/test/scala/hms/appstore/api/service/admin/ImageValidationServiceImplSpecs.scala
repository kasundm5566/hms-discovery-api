package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification
import org.parboiled.common.FileUtils
import hms.appstore.api.json._
import hms.appstore.api.service.{E5103, S1000}


class ImageValidationServiceImplSpecs extends Specification with Injectable {
  val bindingModule = AdminServiceModule

  val imageValidationService = inject[ImageValidationService]

  "validate a valid icon image" in {
    val imageUploadReq = ImageUploadReq(
      "icon_1",
      FileType.JPG,
      Icon,
      Base64String(FileUtils.readAllBytesFromResource("icon-sample.jpg"))
    )

    imageValidationService.validate(imageUploadReq) must beEqualTo(S1000)
  }

  "validate an invalid icon image" in {
    val imageUploadReq = ImageUploadReq(
      "icon_2",
      FileType.PNG,
      ScreenShot,
      Base64String(FileUtils.readAllBytesFromResource("icon-sample-invalid.png"))
    )

    imageValidationService.validate(imageUploadReq) must beEqualTo(E5103)
  }
}
