package hms.appstore.api.service

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification

import hms.appstore.api.domain.AppUserComment
import hms.specs.matcher.FutureMatchers
import hms.appstore.api.json.ClientVersion

class ClientPlatformServiceImplSpecs extends Specification with Injectable with FutureMatchers {

  val bindingModule = ServiceModule
  val clientPlatformService = inject[ClientPlatformService]

  "Client Platform Service" should {

    "Find the new version for the android appStore" in {
      val version = clientPlatformService.findLatestVersion("android", "level-7-15")
      version must beSuccessWithin(10000).like {
        case results => results.platform must not beEmpty
      }
      success
    }

    "Find the new version for the android appStore" in {
      val version = clientPlatformService.findLatestVersion("android", "level-7-10")
      version must beSuccessWithin(10000).like {
        case results => results.newClientVersion must beEmpty
      }
      success
    }
  }

}
