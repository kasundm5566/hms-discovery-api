package hms.appstore.api.service

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable

class StatusCodeMapperSpecs extends Specification with Injectable {

  val bindingModule = ServiceModule
  val statusCodeMapper = inject[StatusCodeMapper]

  "StatusCodeMapper" should {

    "sdp status code S1000 should be map to S1000" in {
      val statusCode = statusCodeMapper.mapToApiStatusCode(EventSource.COMMON_REG.AUTHENTICATE_BY_PASSWORD, ServiceStatusCode("S1000", "Success"))
      statusCode.isSuccess must beTrue
      statusCode.status must beEqualTo("S1000")
      statusCode.description must beEqualTo("You have successfully logged in to the system.")
    }


    "sdp status code P1500 should be map to S1000" in {
      val statusCode = statusCodeMapper.mapToApiStatusCode(EventSource.CMS.CMS_SERVICE_DOWNLOAD, ServiceStatusCode("P1500", "Success"))
      statusCode.isSuccess must beTrue
      statusCode.status must beEqualTo("S1000")
      statusCode.description must beEqualTo("Your request has been processed successfully, you will receive a download link via sms shortly.")
    }


    "sdp status code E1500 should be map to E1500. This status code is not defined in discovery api status code." in {
      val statusCode = statusCodeMapper.mapToApiStatusCode(EventSource.CMS.CMS_SERVICE_DOWNLOAD, ServiceStatusCode("E1500", "Success"))
      statusCode.isSuccess must beFalse
      statusCode.status must beEqualTo("E1500")
      statusCode.description must beEqualTo("Success")
    }

  }
}
