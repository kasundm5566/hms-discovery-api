package hms.appstore.api.service.auth

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification
import scala.concurrent.{ExecutionContext, Await}
import scala.concurrent.duration._


class AuthServiceImplSpecs extends Specification with Injectable {

  val bindingModule = AuthServiceModule

  val authService = inject[AuthService]
  private implicit val executionContext = inject[ExecutionContext]

  "Auto login with allowed host validation : registered user" in {
    val resp = authService.authenticate(Some("6799123456"), Some("192.168.1.96"))
    val r =  Await.result(resp, 1000 milli)
    r.statusCode must beEqualTo("S1000")
  }

  "Auto login with allowed host validation : non-registered user" in {
    val resp = authService.authenticate(Some("6799654789"), Some("192.168.1.96"))
    val r =  Await.result(resp, 1000 milli)
    r.statusCode must beEqualTo("S1003")
  }

  "Auto login with allowed host validation : missing msisdn " in {
    val resp = authService.authenticate(None, Some("192.168.2.96"))
    val r =  Await.result(resp, 1000 milli)
    r.statusCode must beEqualTo("E5200")
  }

  "Auto login with allowed host validation : out of range host " in {
    val resp = authService.authenticate(Some("771234567"), Some("192.168.4.96"))
    val r =  Await.result(resp, 1000 milli)
    r.statusCode must beEqualTo("E5200")
  }

  "Auto login with allowed host validation : missing msisdn & out of range msisdn " in {
    val resp = authService.authenticate(None, Some("192.168.4.96"))
    val r =  Await.result(resp, 1000 milli)
    r.statusCode must beEqualTo("E5200")
  }

  "Auto login with allowed host validation : missing msisdn & out of range msisdn " in {
    val resp = authService.authenticate(Some("771234567"), None)
    val r =  Await.result(resp, 1000 milli)
    r.statusCode must beEqualTo("E5200")
  }


}
