/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.api.domain.AppUserComment

class AppCommentRepoSpecs extends Specification with Injectable with BaseSalatMapper {

  val bindingModule = RepoModule
  val repo = inject[AppCommentRepo]

  sequential

  "review and rating" should {

    "save review" in {
      val comment = AppUserComment(username = Some("hsenid"), appId = "APP_00012", dateTime = System.currentTimeMillis(), comments = "Interesting app")
      repo.create(asDBObject(comment))
      success
    }

    "return review" in {
      val comment = AppUserComment(username = Some("user1"), appId = "APP_00012", dateTime = System.currentTimeMillis(), comments = "Nice App!")
      repo.create(asDBObject(comment))
      repo.find("APP_00012", 0, 5) must not beEmpty
    }
  }

}
