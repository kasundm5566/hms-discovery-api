/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.api.repo.{SalatMapper, AppRepo}

class AppStoreAppSpecs extends Specification with Injectable with SalatMapper {

  val bindingModule = ServiceModule

  val appRepo = inject[AppRepo]

  "AppStore app" should {

    "list app parameters" in {
      val appResult = appRepo.findOne().map(asAppObject(_))

      appResult must beSome

      val app = appResult.get

      "app id" in {
        app.id must not beEmpty
      }

      "app name" in {
        app.displayName must not beEmpty
      }

      "app category" in {
        app.category must not beEmpty
      }

      "app rating" in {
        app.rating must not beNull
      }

      "app ncs" in {
        app.ncs must not beEmpty
      }

      "app chargingLabel" in {
        app.chargingLabel must not beEmpty
      }

      "app chargingDetails" in {
        app.chargingDetails must not beEmpty
      }
      success
    }
  }


}
