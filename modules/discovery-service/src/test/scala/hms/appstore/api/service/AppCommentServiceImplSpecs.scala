/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification

import hms.appstore.api.domain.AppUserComment
import hms.specs.matcher.FutureMatchers

class AppCommentServiceImplSpecs extends Specification with Injectable with FutureMatchers {
  val bindingModule = ServiceModule
  val appCommentService = inject[AppCommentService]
  "AppCommentService" should {

    "find comments for an application" in {
      val appComments = appCommentService.find("APP_00012", 0, 1)

      appComments must beSuccessWithin(10000).like {
        case list: List[AppUserComment] => list must not beEmpty
      }
    }
  }

}
