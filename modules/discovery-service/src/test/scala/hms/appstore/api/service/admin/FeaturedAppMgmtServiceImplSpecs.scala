package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.api.json.StatusCode
import hms.appstore.api.service.ServiceModule
import hms.appstore.api.service.query.FeaturedAppService

import hms.specs.matcher.FutureMatchers
import org.specs2.mutable.Specification
import hms.appstore.api.domain.AppStoreApp


class FeaturedAppMgmtServiceImplSpecs extends Specification with Injectable with FutureMatchers {
  val bindingModule = ServiceModule

  val featuredAppService = inject[FeaturedAppService]

  val featuredAppMgmtService = inject[FeaturedAppMgmtService]

  "update featured apps as it is" in {

    featuredAppService.findApps(0, 10) must beSuccessWithin(10000).like {
      case qrs => {

        qrs.getResults must not beEmpty

        val update = featuredAppMgmtService.update(
          Map("APP_000678" -> 1, "APP_002041" -> 2, "APP_002078" -> 3, "APP_002084" -> 4)
        )

        update must beSuccessWithin(10000).like {
          case sc: StatusCode => {
            sc.status must beEqualTo("S1000")
          }
        }

        featuredAppService.findApps(0, 10) must beSuccessWithin(10000).like {
          case newQrs => {
            newQrs.getResults.map(_.id) must haveTheSameElementsAs(qrs.getResults.map(_.id))
          }
        }
      }
    }
  }
}
