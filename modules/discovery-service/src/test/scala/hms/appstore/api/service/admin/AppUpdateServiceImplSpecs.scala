package hms.appstore.api.service.admin

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import hms.appstore.api.service.{S1000, ServiceModule}
import hms.appstore.api.json._
import org.parboiled.common.FileUtils
import hms.appstore.api.json.ImageUploadReq
import hms.specs.matcher.FutureMatchers


class AppUpdateServiceImplSpecs extends Specification with Injectable with FutureMatchers{
  val bindingModule = ServiceModule

  val appUpdateService = inject[AppDetailsModificationService]

  "upload an image and then update app details" in {

    val imageUploadReq = ImageUploadReq(
      "screenshot_1",
      FileType.JPG,
      Icon,
      Base64String(FileUtils.readAllBytesFromResource("icon-sample.jpg"))
    )

    val cacheResp = appUpdateService.cacheImageUpload(imageUploadReq)


    val updateReq = UpdateReq(id = "APP_001907", category = Some("Dialog Apps"), appIcon = Some(cacheResp.getResult.uid))
    val updateResp = appUpdateService.updateDetails(updateReq)
    updateResp must beSuccessWithin(10000).like {
      case resp => resp must beEqualTo(S1000)
    }
  }
}
