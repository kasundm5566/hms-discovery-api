/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable

import com.mongodb.casbah.query.Imports._

class AppRepoImplSpecs extends Specification with Injectable {
  val bindingModule = RepoModule

  val repo = inject[AppRepo]

  "app repository " should {

    "find one app" in {
      val app = repo.findOne(None)
      app must beSome
      app must beLike {
        case Some(a) => a.getAs[String]("name") must beSome[String]
      }
      success
    }

    "find apps by their ids" in {
      val appIds = repo.find(AppQueryCmd(start = 0, limit = 10)).map(_.getAsOrElse[String]("_id", ""))
      val apps = repo.findByIds(appIds)

      apps.size must_!= 0
      appIds.forall(id => apps.exists(a => a.getAs[String]("name") must beSome[String])) must beTrue
      success
    }

    "find all and count" in {
      repo.countWithQuery(AppQueryCmd()) must beGreaterThan(0L)
    }
  }
}

