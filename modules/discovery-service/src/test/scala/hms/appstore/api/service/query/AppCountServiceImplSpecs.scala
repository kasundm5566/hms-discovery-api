package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification

import hms.appstore.api.json.{QueryResult, AppCount}
import hms.specs.matcher.FutureMatchers

class AppCountServiceImplSpecs extends Specification with Injectable with FutureMatchers {
  val bindingModule = QueryServiceModule

  val appCountService = inject[AppCountService]


  "App count service"  should {

    "count free apps" in {
      val results = appCountService.countFreeApps()
      results must beSuccessWithin(10000).like {
        case QueryResult(_, _, Some(AppCount(count))) => count mustNotEqual(0)
      }
    }

    "count free apps with category" in {
      val results = appCountService.countFreeApps(Some("Dialog Apps"))
      results must beSuccessWithin(10000).like {
        case QueryResult(_, _, Some(AppCount(count))) => count mustNotEqual(0)
      }
    }

    "count apps by category" in {
      val results = appCountService.countByCategory("Business")
      results must beSuccessWithin(10000).like {
        case QueryResult(_, _, Some(AppCount(count))) => count mustNotEqual(0)
      }
    }

    "count downloadable free apps" in {
      val results = appCountService.countDownloadableFreeApps()
      results must beSuccessWithin(10000).like {
        case QueryResult(_, _, Some(AppCount(count))) => count mustNotEqual(0)
      }
    }

    "count downloadable free apps by category" in {
      val results = appCountService.countDownloadableFreeApps(Some("Dialog Apps"))
      results must beSuccessWithin(10000).like {
        case QueryResult(_, _, Some(AppCount(count))) => count mustNotEqual(0)
      }
    }

    "count downloadable apps" in {
      val results = appCountService.countDownloadableApps()
      results must beSuccessWithin(10000).like {
        case QueryResult(_, _, Some(AppCount(count))) => count mustNotEqual(0)
      }
    }

    "count downloadable apps by category" in {
      val results = appCountService.countDownloadableApps(Some("Dialog Apps"))
      results must beSuccessWithin(10000).like {
        case QueryResult(_, _, Some(AppCount(count))) => count mustNotEqual(0)
      }
    }

    "find and count all apps" in {
      val results = appCountService.countAllApps()
      results must beSuccessWithin(10000).like {
        case QueryResult(_, _, Some(AppCount(count))) => count mustNotEqual(0)
      }
    }


  }




}
