/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification

import hms.appstore.api.repo.AppRepo
import hms.appstore.api.json.{QueryResult, AppCount}
import hms.specs.matcher.FutureMatchers

class AppFindServiceImplSpecs extends Specification with Injectable with FutureMatchers {
  val bindingModule = QueryServiceModule

  val appRepo = inject[AppRepo]
  val appFindService = inject[AppFindService]


  "AppFindingService " should {

    "find mostly used apps" in {
      val mostlyApp = appFindService.findMostlyUsed(0, 10)
      mostlyApp must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
      success
    }

    "find mostly used apps for Information" in {
      val mostlyApp = appFindService.findMostlyUsed(0, 10, Some("Information"))
      mostlyApp must beSuccessWithin(10000).like{
        case qrs => qrs.getResults must not beEmpty
      }
    }


    "find top rated apps" in {
      val topRated = appFindService.findTopRated(0, 10)
      topRated must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
    }

    "find top rated apps for Sports " in {
      val topRated = appFindService.findTopRated(0, 10, Some("Sports"))
      topRated must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
    }

    "find newly added apps for Sports" in {
      val newlyAdded = appFindService.findNewlyAdded(0, 10, Some("Sports"))
      newlyAdded must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
    }

    "list newly added apps" in {
      val newlyAdded = appFindService.findNewlyAdded(0, 10)
      newlyAdded must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
    }

    "find free apps" in {
      val freeApps = appFindService.findFreeApps(0, 5)
      freeApps must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
    }

    "find pending apps" in {
      val freeApps = appFindService.findPending(0, 5)
      freeApps must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
    }

    "find free apps for Sports category" in {
      val freeApps = appFindService.findFreeApps(0, 5, Some("Sports"))
      freeApps must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
    }


    "find all app ids" in {
      val results = appFindService.findAllAppIds(0, 100)
      results must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.size mustNotEqual(0)
      }
    }

    "find all apps" in {
      val mostlyApp = appFindService.findAll()
      mostlyApp must beSuccessWithin(10000).like {
        case qrs => qrs.getResults must not beEmpty
      }
      success
    }
  }


}
