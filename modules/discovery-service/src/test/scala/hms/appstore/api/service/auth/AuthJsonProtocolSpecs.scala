package hms.appstore.api.service.auth

import org.specs2.mutable.Specification
import hms.appstore.api.json.UserDetailsResp


class AuthJsonProtocolSpecs extends Specification {
  import spray.json._
  import AuthJsonProtocol._

  "read user details by msisdn response" in {
    val source=
      """
        |{
        |
        |   "status-code": "SUCCESS",
        |
        |   "additional-parameters": {
        |
        |      "birthday": "1980-01-01 00:00:00.0",
        |
        |      "mpin": "saapin",
        |
        |      "corporate-user-name": "hsenid2",
        |
        |      "username": "hsenid2",
        |
        |      "email": "anjulaw@hsenidmobile.com",
        |
        |      "last-login-time": "2013-10-15 10:05:46.0",
        |
        |      "msisdn": "94770808373",
        |
        |      "gender": "",
        |
        |      "corporate-user-email": "anjulaw@hsenidmobile.com"
        |
        |   },
        |
        |   "status": "ACTIVE",
        |
        |   "user-type": "CORPORATE",
        |
        |   "roles": [],
        |
        |   "user-id": "20120306160013183",
        |
        |   "corporate-user-id": "20120306160013183",
        |
        |   "groups": [
        |
        |      []
        |
        |   ]
        |
        |}
      """.stripMargin

    val resp = source.asJson.convertTo[UserDetailsResp]

    resp.isSuccess must beTrue
    resp.userStatus must beSome("ACTIVE")
    resp.userType must beSome("CORPORATE")
  }

}
