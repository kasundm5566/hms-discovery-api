/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification

import query.AppFindService
import hms.specs.matcher.FutureMatchers
import hms.appstore.api.json.QueryResults

class AppDetailsServiceImplSpecs extends Specification with Injectable with FutureMatchers {
  val bindingModule = ServiceModule
  val appFindService = inject[AppFindService]
  val appDetailService = inject[AppDetailsService]

  "App detail service" should {

    "app detail without session id" in {
      appFindService.findMostlyUsed(0, 1) must beSuccessWithin(10000).like {
        case QueryResults(_, _, Some(head :: Nil)) =>  {
          appDetailService.appDetails(head.id, None) must beSuccessWithin(10000).like{case qr => qr.getResult.id must beEqualTo(head.id)}
        }
      }
    }
  }

}
