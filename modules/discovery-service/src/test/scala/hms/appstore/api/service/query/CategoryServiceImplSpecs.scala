/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable

import hms.specs.matcher.FutureMatchers

class CategoryServiceImplSpecs extends Specification with FutureMatchers with Injectable {
  val bindingModule = QueryServiceModule

  val categoryService = inject[CategoryService]

  "Category listing Service" should {

    "find categories" in {
      val categories = categoryService.findCategories(0, 2)

      categories must  beSuccessWithin(10000).like{
        case qrs => qrs.getResults must not beEmpty
      }
      success
    }

    "find super categories" in {
      val superCategories = categoryService.findParentCategories(0, 2)

      superCategories must  beSuccessWithin(10000).like{
        case qrs => qrs.getResults must not beEmpty
      }
      success
    }

  }
}
