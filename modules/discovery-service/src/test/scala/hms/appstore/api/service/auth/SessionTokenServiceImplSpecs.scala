package hms.appstore.api.service.auth

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}


class SessionTokenServiceImplSpecs extends Specification with Injectable {

  val bindingModule = AuthServiceModule
  
  val sessionTokenService = inject[SessionTokenService]

  "create and validate a session" in {
     val token = sessionTokenService.create("94771234567", "user1")
     SessionToken.isExternal(token.id) must beFalse

     sessionTokenService.validate(token.id) must beLike {
       case Some(newToken) => newToken.id must beEqualTo(token.id)
     }
  }

  "create and validate a session" in {
    val token = sessionTokenService.create("94771234567", "user1")
    SessionToken.isExternal(token.id) must beFalse

    val s = sessionTokenService


  }

}
