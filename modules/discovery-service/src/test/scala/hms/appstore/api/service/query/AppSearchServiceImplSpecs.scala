/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification

import hms.appstore.api.json.{SearchQuery, QueryResults}
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.repo.AppRepo
import hms.specs.matcher.FutureMatchers

import scala.concurrent.duration._
import scala.concurrent.Await

class AppSearchServiceImplSpecs extends Specification with FutureMatchers with Injectable {
  val bindingModule = QueryServiceModule

  val appRepo = inject[AppRepo]
  val appSearchService = inject[AppSearchService]

  "AppSearchService" should {
    "search app" in {
      val searchQuery=SearchQuery(searchText = Some("Cric"),start=0,limit=1)
      val result = appSearchService.search(searchQuery)

      result must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.forall(_.displayName.toLowerCase.contains("cric")) must beTrue
      }
      success
    }

    "search app should work with invalid characters" in {
      val searchQuery=SearchQuery(searchText = Some("\\{}Cric"),start=0,limit=1)
      val result = appSearchService.search(searchQuery)

      result must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.forall(_.displayName.toLowerCase.contains("cric")) must beTrue
      }
      success
    }

    "search related apps" in {
      val searchQuery=SearchQuery(searchText = Some("sms"),start=0,limit=1)
      val searchResults: QueryResults[AppStoreApp] = Await.result(appSearchService.search(searchQuery), Duration(10000, MILLISECONDS))

      val app = searchResults.getResults.head
      val appId = app.id
      val developer = app.developer
      val category = app.category

      val results = appSearchService.searchRelated(appId, 0, 1)

      results must beSuccessWithin(10000).like {
        case qrs => qrs.getResults.forall(_.category == category) must beTrue
      }
      success
    }

    "search apps based on appId | serviceProvider | appType" in {
      val searchQuery=SearchQuery(appType = Some("soltura"),limit = 1000)
      val searchResult = Await.result(appSearchService.search(searchQuery),Duration(1000, MILLISECONDS))

      val appCount = searchResult.getResults.size
      println("Query result count " +appCount)
      success
    }
  }

}
