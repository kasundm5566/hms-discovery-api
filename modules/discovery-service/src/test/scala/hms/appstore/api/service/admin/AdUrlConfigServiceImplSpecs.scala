package hms.appstore.api.service.admin

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.api.service.ServiceModule
import hms.specs.matcher.FutureMatchers


class AdUrlConfigServiceImplSpecs extends Specification with Injectable with FutureMatchers {
  val bindingModule = ServiceModule

  val adUrlConfigService = inject[AdUrlConfigService]

  "find all ad urls" in {
    adUrlConfigService.findAll must beSuccessWithin(10000).like {
      case qrs => qrs.getResults must not beEmpty
    }
  }


  "update all ad url" in {
    adUrlConfigService.findAll must beSuccessWithin(10000).like {
      case qrs => {
        qrs.getResults must not beEmpty

        adUrlConfigService.createOrUpdate(qrs.getResults.zip(Stream.from(1, 1)).map{case (url, pos) => url.copy(name = pos.toString)})  must beSuccessWithin(10000).like {
          case statusCode => statusCode.isSuccess must beTrue
        }
      }
    }
  }

}
