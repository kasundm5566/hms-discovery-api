package hms.appstore.api.service.admin

import org.specs2.mutable.Specification
import java.io.File


class IOUtilSpecs extends Specification {

  "create a new file" in {
    val basePath = System.getProperty("java.io.tmpdir")
    val dirName = System.currentTimeMillis().toString
    val fileName = System.currentTimeMillis().toString

    IOUtil.createFile(basePath, dirName, fileName, "Hello".getBytes)

    canRead(basePath + File.separator + dirName + File.separator + fileName) must beTrue
  }


  "create a new file with existing dir" in {
    val basePath = System.getProperty("java.io.tmpdir")
    val dirName = System.currentTimeMillis().toString
    val fileName = System.currentTimeMillis().toString

    new File(basePath + File.separator + dirName).mkdir()

    IOUtil.createFile(basePath, dirName, fileName, "Hello".getBytes)

    canRead(basePath + File.separator + dirName + File.separator + fileName) must beTrue
  }


  "recreate the same file" in {


    val basePath = System.getProperty("java.io.tmpdir")
    val dirName = System.currentTimeMillis().toString
    val fileName = System.currentTimeMillis().toString
    val filePath = basePath + File.separator + dirName + File.separator + fileName


    new File(basePath + File.separator + dirName).mkdir()

    IOUtil.createFile(basePath, dirName, fileName, "Hello".getBytes)

    canRead(filePath) must beTrue
    val content1 = readFile(filePath)



    IOUtil.createFile(basePath, dirName, fileName, "Hello".getBytes)

    canRead(filePath) must beTrue


    IOUtil.createFile(basePath, dirName, fileName, "Hello there ....".getBytes)

    canRead(filePath) must beTrue

    val content2 = readFile(filePath)

    content2 must_!=(content1)
  }

}
