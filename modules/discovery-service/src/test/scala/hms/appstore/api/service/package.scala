package hms.appstore.api

import concurrent.ExecutionContext

package object service {
  implicit def mockServiceContext = new ServiceContext(ExecutionContext.global)
}
