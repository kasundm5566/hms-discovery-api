package hms.appstore.api.domain

import org.specs2.mutable.Specification

import com.mongodb.casbah.commons.Imports._
import com.mongodb.util.JSON
import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.api.repo.{RepoModule, I18nResource}


class NcsSlaSpecs extends Specification with Injectable {
  val bindingModule = RepoModule

  val i18nResource = inject[I18nResource]

  "NcsSla" should {
    "generate charging details" in {


      val jsonStr =
        """
          | {
          |	"ncses" : [
          |		{
          |			"ncs-type" : "subscription",
          |			"status" : "ncs-configured"
          |		},
          |		{
          |			"operator" : "dialog",
          |			"ncs-type" : "sms",
          |			"status" : "ncs-configured"
          |		}
          |	]
          | }
        """.stripMargin

      val dbObject = JSON.parse(jsonStr).asInstanceOf[DBObject]
      success
    }
  }

}
