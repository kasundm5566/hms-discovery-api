package hms.appstore.api.service

import com.escalatesoft.subcut.inject.Injectable
import org.specs2.mutable.Specification


class SysConfigServiceSpecs extends Specification with Injectable {
  val bindingModule = ServiceModule
  val sysConfigService = inject[SysConfigService]

  "load operators" in {
    sysConfigService.operators must not beEmpty
  }

  "load ncs types" in {
    sysConfigService.ncsTypes must not beEmpty
  }

  "load discovery-api status codes" in {
    sysConfigService.statusCodeConfig.getString("common-reg-connector.authenticate-by-password.S1000.status-code") must beEqualTo("S1000")
  }

  "load allowed hosts regex and validate allowed host" in {
    sysConfigService.allowedHosts must not beEmpty

    sysConfigService.allowedHosts.collectFirst{case r if r.findFirstIn("192.168.1.12").nonEmpty => true}.getOrElse(false) must beTrue

  }

  "load allowed hosts regex and  validate not allowed host" in {
    sysConfigService.allowedHosts must not beEmpty

    sysConfigService.allowedHosts.collectFirst{case r if r.findFirstIn("192.168.4.12").nonEmpty => true}.getOrElse(false) must beFalse

  }

}
