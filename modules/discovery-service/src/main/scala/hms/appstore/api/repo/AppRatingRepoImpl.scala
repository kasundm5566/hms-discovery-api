package hms.appstore.api.repo

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.json.AppRatingAddReq
import com.mongodb.casbah.query.Imports._
import hms.appstore.api.json.AppRatingAddReq
import scala.math.BigDecimal.RoundingMode

/**
 * Created by kasun on 4/27/18.
 */
class AppRatingRepoImpl(implicit val bindingModule: BindingModule) extends AppRatingRepo with Injectable {

  private val db: Db = inject[Db]

  override def create(req: AppRatingAddReq){
    val queryToCheckAppAvailability = MongoDBObject("_id" -> req.appId)
    if (db.apps.find(queryToCheckAppAvailability).count > 0) {
      val currentRating = BigDecimal(db.apps.findOne(queryToCheckAppAvailability).get.get("rating").toString)
      val rateCount = BigDecimal(db.apps.findOne(queryToCheckAppAvailability).get.get("rate_count").toString)
      val rating = calculateRating(currentRating, BigDecimal(req.rating), rateCount)

      val updateRatingOfApp = MongoDBObject(
//        "$set" -> MongoDBObject("rating" -> rating.longValue)
        "$set" -> MongoDBObject("rating" -> rating.toDouble)
      )
      val updateRateCountOfApp = MongoDBObject(
        "$set" -> MongoDBObject("rate_count" -> (rateCount.longValue + 1))
      )
      db.apps.findAndModify(queryToCheckAppAvailability, updateRatingOfApp)
      db.apps.findAndModify(queryToCheckAppAvailability, updateRateCountOfApp)
    }
  }

  private def calculateRating(currentRating: BigDecimal, addedRating: BigDecimal, rateCount: BigDecimal): BigDecimal = {
    val rating = (addedRating + (currentRating * rateCount)) / (rateCount + 1)
//    rating.setScale(0, RoundingMode.HALF_UP)
    rating
  }
}
