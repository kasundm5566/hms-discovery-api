/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import com.escalatesoft.subcut.inject.NewBindingModule
import com.typesafe.config.ConfigFactory
import hms.scala.http.util.ConfigUtils

/**
 * Dependencies provided by [[hms.appstore.api.repo]] package to others.
 */
object RepoModule extends NewBindingModule({
  implicit module =>

    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.mongo")

    module.bind[String] idBy "mongo.host" toSingle conf.getString("host")
    module.bind[Int] idBy "mongo.port" toSingle conf.getInt("port")
    module.bind[String] idBy "mongo.dbname" toSingle conf.getString("dbname")


    module.bind[I18nResource] toSingle new DefaultI18nResource

    module.bind[Db] toSingle new DefaultDb

    module.bind[AppRepo] toSingle new AppRepoImpl

    module.bind[CategoryRepo] toSingle new CategoryRepoImpl

    module.bind[BannerRepo] toSingle new BannerRepoImpl

    module.bind[AppCommentRepo] toSingle new AppCommentRepoImpl

    module.bind[AppRatingRepo] toSingle new AppRatingRepoImpl

    module.bind[FeaturedAppRepo] toSingle new FeaturedAppRepoImpl

    module.bind[AdUrlRepo] toSingle new AdUrlRepoImpl

    module.bind[DownloadReqRepo] toSingle new DownloadReqRepoImpl

    module.bind[ChargingRepo] toSingle new ChargingRepoImpl
}
)