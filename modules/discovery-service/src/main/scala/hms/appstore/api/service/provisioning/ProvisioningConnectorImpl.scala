package hms.appstore.api.service.provisioning

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.json.MsgJsonProtocol
import hms.appstore.api.service.{EventSource, ServiceContext, ServiceFutureFilter}
import hms.scala.http.{HttpConnectorConfig, DefaultHttpClient}
import hms.scala.http.marshalling.HttpMarshalling
import scala.concurrent.Future


class ProvisioningConnectorImpl(implicit val bindingModule: BindingModule) extends ProvisioningConnector
  with ServiceFutureFilter
  with Injectable {

  import MsgJsonProtocol._
  import HttpMarshalling._

  private val connector = new DefaultHttpClient(
    HttpConnectorConfig("provConnector", "http", inject[String]("provisioning.host"), inject[Int]("provisioning.port"))
  )

  def findAppDetails(appId: String)(implicit sc: ServiceContext): Future[String] = {
    connector.doPost[Map[String, String], String]("/prov-api/app/find", Map("app-id" -> appId))
  }
}
