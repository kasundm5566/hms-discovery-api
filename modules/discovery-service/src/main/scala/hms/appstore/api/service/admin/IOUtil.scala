package hms.appstore.api.service.admin

import java.io.{FileOutputStream, BufferedOutputStream, File}


object IOUtil {

  def createFile(baseDirectoryPath: String, dirName: String, fileName: String, data: Array[Byte]) {
    val dirPath = baseDirectoryPath + File.separator + dirName

    val filePath = dirPath + File.separator + fileName

    var dir = new File(dirPath)

    if (dir.exists()) {
      if (!dir.isDirectory) {
        dir.delete() // is this ok ???
        dir = new File(dirPath)
        dir.mkdir()
      }
    } else dir.mkdir()

    var file = new File(filePath)

    if (file.exists()) {
      assert(file.delete())
      file = new File(filePath)
      file.createNewFile()
    } else {
      file.createNewFile()
    }

    val fos = new BufferedOutputStream(new FileOutputStream(file))

    try {
      fos.write(data)
      fos.flush()
      file.setLastModified(System.currentTimeMillis())
    } finally {
      if (fos != null) fos.close()
    }
  }
}
