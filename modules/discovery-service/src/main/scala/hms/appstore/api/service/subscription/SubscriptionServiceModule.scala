package hms.appstore.api.service.subscription

import com.escalatesoft.subcut.inject.NewBindingModule
import hms.appstore.api.service.auth.AuthServiceModule
import hms.scala.http.util.ConfigUtils
import com.typesafe.config.ConfigFactory
import hms.appstore.api.repo.RepoModule
import hms.appstore.api.service.query.QueryServiceModule

object SubscriptionServiceModule extends NewBindingModule({
  implicit module =>
    module <~ RepoModule
    module <~ AuthServiceModule
    module <~ QueryServiceModule

    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "nbl.connector.subscription")

    module.bind [String] idBy "subscription.host" toSingle conf.getString("host")
    module.bind [Int] idBy "subscription.port" toSingle conf.getInt("port")


    module.bind [SubscriptionConnector] toSingle(new SubscriptionConnectorImpl)

    module.bind[SubscriptionService] toSingle new SubscriptionServiceImpl
})
