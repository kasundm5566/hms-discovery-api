/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.auth

import scala.concurrent.Future
import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext
import spray.http.MultipartFormData


/**
 * SDP NBL Common Registration connector
 */
trait CommonRegConnector {

  def authenticateByPassword(req: AuthenticateByPasswordReq)(implicit sc: ServiceContext): Future[AuthenticateResp]

  def authenticateByMPin(req: AuthenticateByMPinReq)(implicit sc: ServiceContext): Future[AuthenticateResp]

  def validate(req: SessionValidationReq)(implicit sc: ServiceContext): Future[SessionValidationResp]

  def changeMPin(req: MPinChangeReq)(implicit sc:ServiceContext): Future[MPinChangeResp]

  def userDetailsByUsername(req: UserDetailsByUsernameReq)(implicit sc:ServiceContext): Future[UserDetailByUserNameResp]

  def userProfileDetails(req: UserDetailsByUsernameReq)(implicit sc:ServiceContext): Future[UserProfileDetailsResp]

  def updateUserProfile(req: UserProfileUpdateReq)(implicit sc:ServiceContext): Future[UserProfileUpdateResp]

  def userDetailsByMsisdn(req: UserDetailsByMsisdnReq)(implicit sc:ServiceContext):Future[UserDetailsResp]

  def createConsumerUser(req:UserRegistrationReq)(implicit sc:ServiceContext):Future[UserRegistrationResp]

  def verifyMsisdn(req:MsisdnVerificationReq)(implicit sc:ServiceContext):Future[MsisdnVerificationResp]

  def requestNewVerificationCode(req:MsisdnVerificationCodeReq)(implicit sc:ServiceContext):Future[MsisdnVerificationResp]

  def recoverUserAccount(req:UserAccountRecoveryReq)(implicit sc:ServiceContext):Future[UserAccountRecoveryResp]

  def createAutoLoginConsumerUser(req:AutoLoginUserRegistrationReq)(implicit sc:ServiceContext):Future[AutoLoginUserRegistrationResp]

  def resetUserPassword(req:UserPasswordResetReq)(implicit sc:ServiceContext):Future[UserPasswordResetResp]
}