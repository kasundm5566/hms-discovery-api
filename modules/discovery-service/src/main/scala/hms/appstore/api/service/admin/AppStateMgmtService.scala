package hms.appstore.api.service.admin

import hms.appstore.api.service.ServiceContext
import concurrent.Future
import hms.appstore.api.json.StatusCode

/**
 * Backward compatible application state management service,
 * The state names/transitions are confusing, but we have to live with it until appstore-web redesigned
 */
trait AppStateMgmtService {


  /**
  * Create the initial app and move the status to pending status
  * @param appId  appId
  * @param remarks  notes
  * @return
  */
  def request(appId: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode]

  /**
   * Move the application from pending status to publish status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def approve(appId: String, publishName: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode]


  /**
   * Move the the application from pending status to rejected status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def reject(appId: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode]


  /**
   * Move the application from unpublish status to publish status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def publish(appId: String, publishName: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode]

  /**
   * Move the application from publish status to un-publish status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def unpublish(appId: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode]
}
