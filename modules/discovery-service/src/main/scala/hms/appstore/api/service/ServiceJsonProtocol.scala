/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import hms.appstore.api.json.{UserComment, Application, MsgJsonProtocol}

import spray.json._
import hms.appstore.api.domain.{AppUserCommentWithUserImage, AppUserComment, AppStoreApp}


/**
 * Service Message serialization format, this delegates to discovery-json format
 */
trait ServiceJsonProtocol extends MsgJsonProtocol {

  implicit val internalAppFmt = new RootJsonFormat[AppStoreApp] {
    def write(obj: AppStoreApp) = obj.asInstanceOf[Application].toJson

    def read(json: JsValue) = ??? //Not used
  }

  implicit val internalAppCommentFmt = new RootJsonFormat[AppUserComment] {
    def write(obj: AppUserComment): JsValue = obj.asInstanceOf[UserComment].toJson

    def read(json: JsValue): AppUserComment = ???
  }

  implicit val internalAppCommentWithUserImageFmt = new RootJsonFormat[AppUserCommentWithUserImage] {
    def write(obj: AppUserCommentWithUserImage): JsValue = obj.asInstanceOf[UserComment].toJson

    def read(json: JsValue): AppUserCommentWithUserImage = ???
  }

  implicit val unit2S1000ConversionFmt = new RootJsonFormat[Unit] {
    def write(obj: Unit): JsValue = S1000.asStatusCode.toJson
    def read(json: JsValue): Unit = ??? //Not used
  }
}

object ServiceJsonInternalProtocol extends ServiceJsonProtocol

object ServiceJsonProtocol extends ServiceJsonProtocol {
  lazy val formatter = ServiceModule.inject[UsernameDisplayFormatter](None)

  override def formatUsername(username: String) = {
    formatter.formatUsername(username)
  }
}





