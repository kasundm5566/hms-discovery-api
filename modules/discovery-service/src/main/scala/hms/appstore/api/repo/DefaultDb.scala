/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import com.mongodb.casbah.Imports._
import com.mongodb.casbah.MongoClient
import com.escalatesoft.subcut.inject.{Injectable, BindingModule}

class DefaultDb(implicit val bindingModule: BindingModule) extends Db with Injectable {

  private val mongoClient = MongoClient(inject[String]("mongo.host"), inject[Int]("mongo.port"))

  def apps: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("apps")

  def pendingApps: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("pending_approval_apps")

  def backupApps: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("previous_state_apps")

  def categories: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("categories")

  def superCategories: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("app_panels")

  def appUserReviews: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("application_comments")

  def featuredApps: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("featured_apps")

  def addUrls: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("ad_urls")

  def downloadRequests: MongoCollection = mongoClient("cms")("request")

  def sysConfig: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("system_properties")

  def banners: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("banners")

  def bannerLocations: MongoCollection = mongoClient(inject[String]("mongo.dbname"))("banner_locations")
}
