/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.domain

import com.mongodb.casbah.query.Imports._
import org.joda.time.DateTime
import hms.appstore.api.json._
import hms.appstore.api.repo.{BaseSalatMapper, NcsSlaExtractor}
import hms.appstore.api.service.ServiceJsonProtocol

import spray.json._
import scala.concurrent.Future
import scala.math.BigDecimal.RoundingMode

case class AppStoreApp(extractorFactory: (DBObject) => NcsSlaExtractor,
                       source: DBObject = DBObject.empty,
                       downloadableBinaries: Application#DnBinaries = Map.empty,
                       subscriptionStatus: SubscriptionStatus.SubscriptionStatus = SubscriptionStatus.NotAvailable,
                       userComments: List[UserComment] = List.empty,
                       downloadStatus: Option[DownloadStatus] = None,
                       downloadsCount: Int = 0,
                       var subscriptionsCount: Int = 0) extends Application with BaseSalatMapper {
  import ServiceJsonProtocol._

  private lazy val appFields = asObject[AppFields](source)

  private val ncsSlaExtractor = extractorFactory(source)

  def id: String                                 = appFields._id
  def name: String                               = appFields.name
  def displayName: String                        = appFields.displayName
  def category: String                           = appFields.category.mkString(", ")
  def appIcon: String                            = appFields.app_icon
  def appBanners: Option[List[Map[String,String]]] = Some(appFields.app_banners.map(_.asMap))
  def usage: Long                                = appFields.usage
  def currency: String                           = appFields.currency.replace(".", "")
  def developer: String                          = appFields.developer
  def rating: Double                             = BigDecimal(appFields.rating.toString).setScale(0, RoundingMode.HALF_UP).toDouble
  def rate_count: Int                            = appFields.rate_count.intValue
  def labels: List[String]                       = appFields.labels

  def description: String                        = appFields.description
  def shortDesc: String                          = appFields.short_description

  def appTypes: List[String]                     = appFields.app_type
  def screenShots :List[Map[String,String]]      = appFields.app_screenshots.sortBy(_.url.getOrElse("")).map(_.asMap)
  def instructions: Map[String, String]          = appFields.instructions.map { case(k, v) => (k.capitalize, v)}
  def remarks: String                            = appFields.remarks

  def ncs: Set[String]                           = ncsSlaExtractor.ncs

  def subscription: Boolean                      = ncsSlaExtractor.subscription
  def downloadable: Boolean                      = ncsSlaExtractor.downloadable
  def chargingLabel: String                      = ncsSlaExtractor.chargingLabel(currency)
  def chargingDetails: String                    = ncsSlaExtractor.chargingDetails(currency)

  def requestedDate: DateTime                    = new DateTime(appFields.date_added)
  def modifiedDate: DateTime                     = new DateTime(appFields.modified_date)

  def status :AppStatus.AppStatus = {
    if (AppStatus.valuesAsSet.contains(appFields.status)) AppStatus.withName(appFields.status) else AppStatus.New
  }

  def password :String                           = appFields.password

  override def toString = this.toJson.prettyPrint
}

