package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}

import hms.appstore.api.json.StatusCode
import hms.appstore.api.json.ImageUploadReq
import hms.appstore.api.service._

import java.io.ByteArrayInputStream
import javax.imageio.ImageIO


class ImageValidationServiceImpl(implicit val bindingModule: BindingModule) extends ImageValidationService
  with Injectable
  with EventLogging {

  private val sysConfigService = inject[SysConfigService]

  /**
   * Validate the image upload in following way
   * Icon should be min 120x120, aspect ratio is = 1 and max size 50*1024
   * ScreenShot should be min 240x360, aspect ratio is = 24/36 and max size 50*1024
   * MobileScreenShot should be min 360x600, aspect ratio is = 360/600 and max size 40*1024
   *
   * @param upload request
   * @return
   */
  def validate(upload: ImageUploadReq)(implicit sc: ServiceContext): StatusCode = {

    val bytes = upload.data.toBytes

    val iis = ImageIO.createImageInputStream(new ByteArrayInputStream(bytes))
    val readers = ImageIO.getImageReadersByFormatName(upload.fileType.toString.toLowerCase)

    if (readers.hasNext) {
      val reader = readers.next()
      reader.setInput(iis, false)

      val images = reader.getNumImages(true)

      if (images > 1) {
        E5102
      } else {

        val config = sysConfigService.imageValidationConfig(upload.uploadType)

        val width = reader.getWidth(0)
        val height = reader.getHeight(0)
        val aspectRatio = reader.getAspectRatio(0)

        if (width < config.minWidth || height < config.minHeight || aspectRatio != config.aspectRatio) {
          E5103
        } else {
          if (bytes.length > config.maxSizeInBytes) E5104  else S1000
        }
      }
    } else {
      E5000
    }
  }
}
