package hms.appstore.api.repo

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.casbah.commons.MongoDBObject
import hms.appstore.api.service.{EventSource, ServiceContext, EventLogging}
import com.mongodb.casbah.query.Imports._
import hms.appstore.api.json._
import com.mongodb.casbah.MongoCollection
import hms.appstore.api.json.AppStatus._
import hms.appstore.api.json.ChargingData
import hms.appstore.api.json.ChargingEditReq
import hms.appstore.api.json.ChargingDescriptionMessage
import hms.appstore.api.json.AppStatus.AppStatus
import hms.appstore.api.service.query.AppFindService

/**
 * Created by kasun on 5/18/18.
 */
class ChargingRepoImpl(implicit val bindingModule: BindingModule) extends ChargingRepo with Injectable with EventLogging {

  private val db: Db = inject[Db]

  override def editChargingAmount(appStatus: Option[AppStatus], req: ChargingEditReq) {

    val queryToFindApp = MongoDBObject("_id" -> req.appId)

    var database: MongoCollection = null

    if (!appStatus.exists(_ == AppStatus.New)) {
      database = db.apps
    } else {
      database = db.pendingApps
    }

    if (database.find(queryToFindApp).count == 1) {

      var ncsSlas: BasicDBObject = null
      database.findOne(queryToFindApp).foreach(x => ncsSlas = x.as[BasicDBObject]("ncs-slas"))

      val chargingDataList = req.chargingData

      chargingDataList.foreach(chargingData => {
        chargingData.ncsType match {
          case "vodafone-ussd" => updateUssdChargingData(ncsSlas, chargingData.chargingType, req.appId, chargingData.amount.getOrElse(""), chargingData.direction.getOrElse(""), database)
          case "vodafone-sms" => updateSmsChargingData(ncsSlas, chargingData.chargingType, req.appId, chargingData.amount.getOrElse(""), chargingData.direction.getOrElse(""), database)
          case "subscription" => updateSubscriptionChargingData(ncsSlas, chargingData.chargingType, req.appId, chargingData.amount.getOrElse(""), chargingData.frequency.getOrElse(""), database)
          case "downloadable" => updateDownloadableChargingData(ncsSlas, chargingData.chargingType, req.appId, chargingData.amount.getOrElse(""), database)
          case _ =>
        }
      })

      /*      var ncses: MongoDBList = MongoDBList.empty
            db.apps.findOne(queryToFindApp).foreach(x => ncses = x.as[MongoDBList]("ncses"))

            eventLog(correlationId, EventSource.CORE.EDIT_CHARGING_AMOUNT, "====>>>" + ncses)

            var ncsTypes: Set[String] = Set.empty
            ncses.foreach(ncs => {
              ncsTypes += ncs.asInstanceOf[BasicDBObject].get("ncs-type").toString
            })

            eventLog(correlationId, EventSource.CORE.EDIT_CHARGING_AMOUNT, "====>>>" + ncsTypes)*/

    }
  }

  private def updateSmsChargingData(ncsSlas: BasicDBObject, chargingType: String, appId: String, amount: String, direction: String, collection: MongoCollection) {

    val chargingMoObj: BasicDBObject = ncsSlas.get("vodafone-sms").asInstanceOf[BasicDBObject].get("mo").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]
    val chargingMtObj: BasicDBObject = ncsSlas.get("vodafone-sms").asInstanceOf[BasicDBObject].get("mt").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]

    val queryToFindApp = MongoDBObject("_id" -> appId)

    val updateSmsMtChargingAmount = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-sms.mt.charging.amount" -> amount)
    )

    val updateSmsMoChargingAmount = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-sms.mo.charging.amount" -> amount)
    )

    val updateMtChargingTypeToFree = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-sms.mt.charging.type" -> "free")
    )

    val updateMtChargingTypeToFlat = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-sms.mt.charging.type" -> "flat")
    )

    val updateMoChargingTypeToFree = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-sms.mo.charging.type" -> "free")
    )

    val updateMoChargingTypeToFlat = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-sms.mo.charging.type" -> "flat")
    )

    val removeMtFields = MongoDBObject(
      "$unset" -> MongoDBObject("ncs-slas.vodafone-sms.mt.charging.allowed-payment-instruments" -> "",
        "ncs-slas.vodafone-sms.mt.charging.amount" -> "",
        "ncs-slas.vodafone-sms.mt.charging.frequency" -> "",
        "ncs-slas.vodafone-sms.mt.charging.party" -> "")
    )

    val addMtFields = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-sms.mt.charging.allowed-payment-instruments" -> List("Mobile Account"),
        "ncs-slas.vodafone-sms.mt.charging.party" -> "subscriber")
    )

    val removeMoFields = MongoDBObject(
      "$unset" -> MongoDBObject("ncs-slas.vodafone-sms.mo.charging.allowed-payment-instruments" -> "",
        "ncs-slas.vodafone-sms.mo.charging.amount" -> "",
        "ncs-slas.vodafone-sms.mo.charging.frequency" -> "",
        "ncs-slas.vodafone-sms.mo.charging.party" -> "")
    )

    val addMoFields = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-sms.mo.charging.allowed-payment-instruments" -> List("Mobile Account"),
        "ncs-slas.vodafone-sms.mo.charging.party" -> "subscriber")
    )

    if (direction == "mt") {
      if (chargingMtObj.get("type").toString != "free") {
        if (chargingType == "free") {
          collection.findAndModify(queryToFindApp, updateMtChargingTypeToFree)
          collection.update(queryToFindApp, removeMtFields, multi = true)
        } else {
          if (collection.find(queryToFindApp).count == 1) {
            collection.findAndModify(queryToFindApp, updateSmsMtChargingAmount)
          }
        }
      } else {
        if (chargingType == "flat") {
          collection.findAndModify(queryToFindApp, updateSmsMtChargingAmount)
          collection.findAndModify(queryToFindApp, updateMtChargingTypeToFlat)
          collection.update(queryToFindApp, addMtFields, multi = true)
        }
      }
    } else if (direction == "mo") {
      if (chargingMoObj.get("type").toString != "free") {
        if (chargingType == "free") {
          //TODO: Clarifiy whether it can hold an amount
          collection.findAndModify(queryToFindApp, updateMoChargingTypeToFree)
          collection.update(queryToFindApp, removeMoFields, multi = true)
        } else if (chargingType == "flat") {
          if (collection.find(queryToFindApp).count == 1) {
            collection.findAndModify(queryToFindApp, updateSmsMoChargingAmount)
          }
        }
      } else {
        if (chargingType == "flat") {
          collection.findAndModify(queryToFindApp, updateSmsMoChargingAmount)
          collection.findAndModify(queryToFindApp, updateMoChargingTypeToFlat)
          collection.update(queryToFindApp, addMoFields, multi = true)
        }
      }
    }
  }

  private def updateUssdChargingData(ncsSlas: BasicDBObject, chargingType: String, appId: String, amount: String, direction: String, collection: MongoCollection) {

    val chargingMoObj: BasicDBObject = ncsSlas.get("vodafone-ussd").asInstanceOf[BasicDBObject].get("mo").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]
    val chargingMtObj: BasicDBObject = ncsSlas.get("vodafone-ussd").asInstanceOf[BasicDBObject].get("mt").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]

    val queryToFindApp = MongoDBObject("_id" -> appId)

    val updateUssdMtChargingAmount = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-ussd.mt.charging.amount" -> amount)
    )

    val updateUssdMoChargingAmount = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-ussd.mo.charging.amount" -> amount)
    )

    val updateMtChargingTypeToFree = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-ussd.mt.charging.type" -> "free")
    )

    val updateMtChargingTypeToFlat = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-ussd.mt.charging.type" -> "flat")
    )

    val updateMoChargingTypeToFree = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-ussd.mo.charging.type" -> "free")
    )

    val updateMoChargingTypeToFlat = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-ussd.mo.charging.type" -> "flat")
    )

    val removeMtFields = MongoDBObject(
      "$unset" -> MongoDBObject("ncs-slas.vodafone-ussd.mt.charging.allowed-payment-instruments" -> "",
        "ncs-slas.vodafone-ussd.mt.charging.amount" -> "",
        "ncs-slas.vodafone-ussd.mt.charging.frequency" -> "",
        "ncs-slas.vodafone-ussd.mt.charging.party" -> "")
    )

    val addMtFields = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-ussd.mt.charging.allowed-payment-instruments" -> List("Mobile Account"),
        "ncs-slas.vodafone-ussd.mt.charging.party" -> "subscriber")
    )

    val removeMoFields = MongoDBObject(
      "$unset" -> MongoDBObject("ncs-slas.vodafone-ussd.mo.charging.allowed-payment-instruments" -> "",
        "ncs-slas.vodafone-ussd.mo.charging.amount" -> "",
        "ncs-slas.vodafone-ussd.mo.charging.frequency" -> "",
        "ncs-slas.vodafone-ussd.mo.charging.party" -> "")
    )

    val addMoFields = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.vodafone-ussd.mo.charging.allowed-payment-instruments" -> List("Mobile Account"),
        "ncs-slas.vodafone-ussd.mo.charging.party" -> "subscriber")
    )

    if (direction == "mo") {
      if (chargingMoObj.get("type").toString != "free") {
        if (chargingType == "free") {
          collection.findAndModify(queryToFindApp, updateMoChargingTypeToFree)
          collection.update(queryToFindApp, removeMoFields, multi = true)
        } else {
          if (collection.find(queryToFindApp).count == 1) {
            collection.findAndModify(queryToFindApp, updateUssdMoChargingAmount)
          }
        }
      } else {
        if (chargingType == "flat") {
          collection.findAndModify(queryToFindApp, updateUssdMoChargingAmount)
          collection.findAndModify(queryToFindApp, updateMoChargingTypeToFlat)
          collection.update(queryToFindApp, addMoFields, multi = true)
        }
      }
    } else if (direction == "mt") {
      if (chargingMtObj.get("type").toString != "free") {
        if (chargingType == "free") {
          //TODO: Clarifiy whether it can hold an amount
          collection.findAndModify(queryToFindApp, updateMtChargingTypeToFree)
          collection.update(queryToFindApp, removeMtFields, multi = true)
        } else {
          if (collection.find(queryToFindApp).count == 1) {
            collection.findAndModify(queryToFindApp, updateUssdMtChargingAmount)
          }
        }
      } else {
        if (chargingType == "flat") {
          collection.findAndModify(queryToFindApp, updateUssdMtChargingAmount)
          collection.findAndModify(queryToFindApp, updateMtChargingTypeToFlat)
          collection.update(queryToFindApp, addMtFields, multi = true)
        }
      }
    }
  }

  private def updateSubscriptionChargingData(ncsSlas: BasicDBObject, ncsType: String, appId: String, amount: String, frequency: String, collection: MongoCollection) {

    val chargingObj: BasicDBObject = ncsSlas.get("subscription").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]

    val queryToFindApp = MongoDBObject("_id" -> appId)
    val updateChargingAmount = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.subscription.charging.amount" -> amount)
    )

    val updateChargingFrequency = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.subscription.charging.frequency" -> frequency)
    )

    val updateChargingTypeToFree = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.subscription.charging.type" -> "free")
    )

    val updateChargingTypeToFlat = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.subscription.charging.type" -> "flat")
    )

    val removeFields = MongoDBObject(
      "$unset" -> MongoDBObject("ncs-slas.subscription.charging.allowed-payment-instruments" -> "",
        "ncs-slas.subscription.charging.amount" -> "",
        "ncs-slas.subscription.charging.frequency" -> "",
        "ncs-slas.subscription.charging.party" -> "")
    )

    val addFields = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.subscription.charging.allowed-payment-instruments" -> List("Mobile Account"),
        "ncs-slas.subscription.charging.party" -> "subscriber")
    )

    if (chargingObj.get("type").toString != "free") {
      if (ncsType == "free") {
        if (collection.find(queryToFindApp).count == 1) {
          collection.findAndModify(queryToFindApp, updateChargingTypeToFree)
          collection.update(queryToFindApp, removeFields, multi = true)
        }
      } else if (ncsType == "flat") {
        if (collection.find(queryToFindApp).count == 1) {
          collection.findAndModify(queryToFindApp, updateChargingAmount)
          collection.findAndModify(queryToFindApp, updateChargingFrequency)
        }
      }
    } else {
      if (ncsType == "flat") {
        collection.findAndModify(queryToFindApp, updateChargingAmount)
        collection.findAndModify(queryToFindApp, updateChargingFrequency)
        collection.findAndModify(queryToFindApp, updateChargingTypeToFlat)
        collection.update(queryToFindApp, addFields, multi = true)
      }
    }
  }

  private def updateDownloadableChargingData(ncsSlas: BasicDBObject, ncsType: String, appId: String, amount: String, collection: MongoCollection) {

    val chargingObj: BasicDBObject = ncsSlas.get("downloadable").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]

    val queryToFindApp = MongoDBObject("_id" -> appId)
    val updateChargingAmount = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.downloadable.charging.amount" -> amount)
    )

    val updateChargingTypeToFree = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.downloadable.charging.type" -> "free")
    )

    val updateChargingTypeToFlat = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.downloadable.charging.type" -> "flat")
    )

    val removeFields = MongoDBObject(
      "$unset" -> MongoDBObject("ncs-slas.downloadable.charging.allowed-payment-instruments" -> "",
        "ncs-slas.downloadable.charging.amount" -> "",
        "ncs-slas.downloadable.charging.frequency" -> "",
        "ncs-slas.downloadable.charging.party" -> "")
    )

    val addFields = MongoDBObject(
      "$set" -> MongoDBObject("ncs-slas.downloadable.charging.allowed-payment-instruments" -> List("Mobile Account"),
        "ncs-slas.downloadable.charging.party" -> "subscriber")
    )

    if (chargingObj.get("type").toString != "free") {
      if (ncsType == "free") {
        collection.findAndModify(queryToFindApp, updateChargingTypeToFree)
        collection.update(queryToFindApp, removeFields, multi = true)
      } else if (ncsType == "flat") {
        if (collection.find(queryToFindApp).count == 1) {
          collection.findAndModify(queryToFindApp, updateChargingAmount)
        }
      }
    } else {
      if (ncsType == "flat") {
        collection.findAndModify(queryToFindApp, updateChargingAmount)
        collection.findAndModify(queryToFindApp, updateChargingTypeToFlat)
        collection.update(queryToFindApp, addFields, multi = true)
      }
    }
  }

  override def findChargingDetails(appId: String): Set[ChargingData] = {

    val queryToFindApp = MongoDBObject("_id" -> appId)
    var chargingDataSet: Set[ChargingData] = Set.empty

    var database: MongoCollection = null
    if (db.apps.find(queryToFindApp).count == 1) {
      database = db.apps
    } else {
      database = db.pendingApps
    }

    if (database.find(queryToFindApp).count == 1) {

      var ncsSlas: BasicDBObject = null
      database.findOne(queryToFindApp).foreach(x => ncsSlas = x.as[BasicDBObject]("ncs-slas"))

      var smsChargingMoObj: ChargingData = null
      var smsChargingMtObj: ChargingData = null
      var ussdChargingMoObj: ChargingData = null
      var ussdChargingMtObj: ChargingData = null
      var subscriptionChargingObj: ChargingData = null
      var downloadableChargingObj: ChargingData = null

      if (ncsSlas.contains("vodafone-sms")) {
        val chargingSmsMoObj: BasicDBObject = ncsSlas.get("vodafone-sms").asInstanceOf[BasicDBObject].get("mo").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]
        val chargingSmsMtObj: BasicDBObject = ncsSlas.get("vodafone-sms").asInstanceOf[BasicDBObject].get("mt").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]
        if (chargingSmsMoObj.get("type").toString == "free") {
          smsChargingMoObj = ChargingData(ncsType = "vodafone-sms", chargingType = chargingSmsMoObj.get("type").toString, amount = Option(""), direction = Option("mo"), frequency = Option(""))
          chargingDataSet += smsChargingMoObj
        } else {
          smsChargingMoObj = ChargingData(ncsType = "vodafone-sms", chargingType = chargingSmsMoObj.get("type").toString, amount = Option(chargingSmsMoObj.get("amount").toString), direction = Option("mo"), frequency = Option(""))
          chargingDataSet += smsChargingMoObj
        }
        if (chargingSmsMtObj.get("type").toString == "free") {
          smsChargingMtObj = ChargingData(ncsType = "vodafone-sms", chargingType = chargingSmsMtObj.get("type").toString, amount = Option(""), direction = Option("mt"), frequency = Option(""))
          chargingDataSet += smsChargingMtObj
        } else {
          smsChargingMtObj = ChargingData(ncsType = "vodafone-sms", chargingType = chargingSmsMtObj.get("type").toString, amount = Option(chargingSmsMtObj.get("amount").toString), direction = Option("mt"), frequency = Option(""))
          chargingDataSet += smsChargingMtObj
        }
      }

      if (ncsSlas.contains("vodafone-ussd")) {
        val chargingUssdMoObj: BasicDBObject = ncsSlas.get("vodafone-ussd").asInstanceOf[BasicDBObject].get("mo").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]
        val chargingUssdMtObj: BasicDBObject = ncsSlas.get("vodafone-ussd").asInstanceOf[BasicDBObject].get("mt").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]
        if (chargingUssdMoObj.get("type").toString == "free") {
          ussdChargingMoObj = ChargingData(ncsType = "vodafone-ussd", chargingType = chargingUssdMoObj.get("type").toString, amount = Option(""), direction = Option("mo"), frequency = Option(""))
          chargingDataSet += ussdChargingMoObj
        } else {
          ussdChargingMoObj = ChargingData(ncsType = "vodafone-ussd", chargingType = chargingUssdMoObj.get("type").toString, amount = Option(chargingUssdMoObj.get("amount").toString), direction = Option("mo"), frequency = Option(""))
          chargingDataSet += ussdChargingMoObj
        }
        if (chargingUssdMtObj.get("type").toString == "free") {
          ussdChargingMtObj = ChargingData(ncsType = "vodafone-ussd", chargingType = chargingUssdMtObj.get("type").toString, amount = Option(""), direction = Option("mt"), frequency = Option(""))
          chargingDataSet += ussdChargingMtObj
        } else {
          ussdChargingMtObj = ChargingData(ncsType = "vodafone-ussd", chargingType = chargingUssdMtObj.get("type").toString, amount = Option(chargingUssdMtObj.get("amount").toString), direction = Option("mt"), frequency = Option(""))
          chargingDataSet += ussdChargingMtObj
        }
      }

      if (ncsSlas.contains("subscription")) {
        val chargingSubscriptionObj: BasicDBObject = ncsSlas.get("subscription").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]
        if (chargingSubscriptionObj.get("type").toString == "free") {
          subscriptionChargingObj = ChargingData(ncsType = "subscription", chargingType = chargingSubscriptionObj.get("type").toString, amount = Option(""), direction = Option(""), frequency = Option(""))
          chargingDataSet += subscriptionChargingObj
        } else {
          subscriptionChargingObj = ChargingData(ncsType = "subscription", chargingType = chargingSubscriptionObj.get("type").toString, amount = Option(chargingSubscriptionObj.get("amount").toString), direction = Option(""), frequency = Option(chargingSubscriptionObj.get("frequency").toString))
          chargingDataSet += subscriptionChargingObj
        }
      }

      if (ncsSlas.contains("downloadable")) {
        val chargingDownloadableObj: BasicDBObject = ncsSlas.get("downloadable").asInstanceOf[BasicDBObject].get("charging").asInstanceOf[BasicDBObject]
        if (chargingDownloadableObj.get("type").toString == "free") {
          downloadableChargingObj = ChargingData(ncsType = "downloadable", chargingType = chargingDownloadableObj.get("type").toString, amount = Option(""), direction = Option(""), frequency = Option(""))
          chargingDataSet += downloadableChargingObj
        } else {
          downloadableChargingObj = ChargingData(ncsType = "downloadable", chargingType = chargingDownloadableObj.get("type").toString, amount = Option(chargingDownloadableObj.get("amount").toString), direction = Option(""), frequency = Option(""))
          chargingDataSet += downloadableChargingObj
        }
      }
    }
    chargingDataSet
  }

  override def findChargingDescriptionMessageFormat(appId: String): Set[ChargingDescriptionMessage] = {
    val queryToFindApp = MongoDBObject("_id" -> appId)
    var messageFormats: Set[ChargingDescriptionMessage] = Set.empty
    val i18nResource = inject[I18nResource]
    var database: MongoCollection = null
    if (db.apps.find(queryToFindApp).count == 1) {
      database = db.apps
    } else {
      database = db.pendingApps
    }

    if (database.find(queryToFindApp).count == 1) {
      var ncses: MongoDBList = MongoDBList.empty
      database.findOne(queryToFindApp).foreach(x => ncses = x.as[MongoDBList]("ncses"))

      var ncsTypes: Set[String] = Set.empty
      ncses.foreach(ncs => {
        ncsTypes += ncs.asInstanceOf[BasicDBObject].get("ncs-type").toString
      })

      ncsTypes.foreach(ncs => {
        ncs match {
          case "subscription" => {
            messageFormats += ChargingDescriptionMessage("subscription", i18nResource.getString("charging.details.subscription"))
          }
          case "downloadable" => {
            messageFormats += ChargingDescriptionMessage("downloadable", i18nResource.getString("charging.details.download"))
          }
          case "sms" => {
            messageFormats += ChargingDescriptionMessage("sms-mo", i18nResource.getString("charging.details.sms.mo"))
            messageFormats += ChargingDescriptionMessage("sms-mt", i18nResource.getString("charging.details.sms.mt"))
          }
          case "ussd" => {
            messageFormats += ChargingDescriptionMessage("ussd", i18nResource.getString("charging.details.ussd.menu"))
          }
        }
      })
      messageFormats += ChargingDescriptionMessage("free", i18nResource.getString("charging.details.free"))
    }
    messageFormats
  }
}
