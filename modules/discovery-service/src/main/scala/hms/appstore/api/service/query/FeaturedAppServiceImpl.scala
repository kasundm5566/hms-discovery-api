/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.casbah.Imports._

import hms.appstore.api.json.QueryResults
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.repo.{SalatMapper, FeaturedAppRepo, AppRepo}
import hms.appstore.api.service.{EventSource, EventLogging, ServiceContext}
import hms.appstore.api.service.Futures._

import scala.concurrent.Future


class FeaturedAppServiceImpl(implicit val bindingModule: BindingModule) extends FeaturedAppService
with Injectable
with SalatMapper
with EventLogging {

  private val appRepo = inject[AppRepo]
  private val featuredRepo = inject[FeaturedAppRepo]
  protected val eventSource = EventSource.CORE

  def findApps(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_FEATURED_APPS, s"start=$start,limit=$limit")

      //      val keyPairs = featuredRepo.findIds(start, limit).map(m => (m.keys.find(_.startsWith("featured")).head, m.getAs[String]("_id").get))
      //
      //      val featuredMap = keyPairs.toMap

      //the featured apps schema does not support sorting, so... we sort the appIds by featured rank
      //      val ids = for {
      //        key <- featuredMap.keys.toList.sorted
      //        value <- featuredMap.get(key)
      //      } yield (value)

      val ids = findAndSortFeaturedAppIds(start, limit)

      val apps = appRepo.findByIdInOrder(ids).map(asAppObject(_))
      val results = QueryResults(apps)

      eventLog(correlationId, eventSource.FIND_FEATURED_APPS, "Success")
      results
    }
  }

  override def findDownloadableApps(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._
    future {
      val ids = findAndSortFeaturedAppIds(start, limit)

      val apps = appRepo.findDownloadableByIdInOrder(ids).map(asAppObject(_))
      val results = QueryResults(apps)

      eventLog(correlationId, eventSource.FIND_FEATURED_APPS, "Success")
      results
    }
  }

  private def findAndSortFeaturedAppIds(start: Int, limit: Int): List[String] = {
    val keyPairs = featuredRepo.findIds(start, limit).map(m => (m.keys.find(_.startsWith("featured")).head, m.getAs[String]("_id").get))

    val featuredMap = keyPairs.toMap

    //the featured apps schema does not support sorting, so... we sort the appIds by featured rank
    val ids = for {
      key <- featuredMap.keys.toList.sorted
      value <- featuredMap.get(key)
    } yield (value)

    ids
  }
}
