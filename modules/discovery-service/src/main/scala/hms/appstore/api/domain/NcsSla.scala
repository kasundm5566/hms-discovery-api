package hms.appstore.api.domain

import com.novus.salat.annotations._

trait NcsSla {
  def ncses: Set[String]
  def chargingSlas: List[ChargingSla]
  def messagingSlas: List[MessagingSla]
}

case class CommonNcsSla(subscription: Option[ChargingSla],
                        downloadable: Option[ChargingSla]) extends NcsSla {
  def ncses: Set[String] = chargingSlas.map(_.ncsType).toSet

  def chargingSlas = (subscription ++ downloadable).toList

  def messagingSlas = List.empty
}

case class DialogNcsSla(@Key("dialog-sms") sms: Option[MessagingSla],
                        @Key("dialog-wap-push") wapPush: Option[MessagingSla],
                        @Key("dialog-ussd") ussd: Option[MessagingSla]) extends NcsSla {
  def ncses: Set[String] = messagingSlas.map("dialog-" + _.ncsType).toSet

  def chargingSlas =  List.empty

  def messagingSlas = (sms ++ wapPush ++ ussd).toList
}

case class VodafoneNcsSla(@Key("vodafone-sms") sms: Option[MessagingSla],
                          @Key("vodafone-ussd") ussd: Option[MessagingSla]) extends NcsSla {
  def ncses: Set[String] = messagingSlas.map("vodafone-" + _.ncsType).toSet

  def chargingSlas =  List.empty

  def messagingSlas = (sms ++ ussd).toList
}

// SMS, WAP-PUSH and USSD
case class MessagingSla(@Key("ncs-type") ncsType: String,
                        status: String,
                        mo: Option[Mo],
                        mt: Option[Mt],
                        operator: Option[String])

case class Mo(charging: Charging)

case class Mt(charging: Charging)

// Subscription, Downloadable
case class ChargingSla(@Key("ncs-type") ncsType: String,
                       status: String,
                       charging: Charging,
                       operator: Option[String])

case class Charging(@Key("type") chargingType: String, party: Option[String], amount: Option[String], frequency: Option[String])