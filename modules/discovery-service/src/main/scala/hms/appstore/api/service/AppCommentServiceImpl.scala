/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}

import hms.appstore.api.json._
import hms.appstore.api.repo.{SalatMapper, AppCommentRepo}
import hms.appstore.api.domain.{AppUserCommentWithUserImage, AppUserComment}
import hms.appstore.api.service.auth.{CommonRegConnector, AuthService}
import hms.appstore.api.service.governance.GovernanceConnector
import hms.appstore.api.service.Futures._

import query.AppFindService
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

class AppCommentServiceImpl(implicit val bindingModule: BindingModule) extends AppCommentService
with Injectable
with SalatMapper
with EventLogging {

  private val authService = inject[AuthService]
  private val governanceConnector = inject[GovernanceConnector]
  private val appCommentRepo = inject[AppCommentRepo]
  private val commonRegConnector = inject[CommonRegConnector]
  protected val eventSource = EventSource.CORE
  private val appFindService = inject[AppFindService]

  def find(appId: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[List[AppUserComment]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_COMMENTS, s"appId = $appId")
      val comments = appCommentRepo.find(appId, start, limit).map(asObject[AppUserComment])
      eventLog(correlationId, eventSource.FIND_COMMENTS, "Success")
      comments
    }
  }


  override def findWithUserImage(appId: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[List[AppUserCommentWithUserImage]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_COMMENTS_WITH_USER_IMAGE, s"appId = $appId")
      var lst: Set[AppUserCommentWithUserImage] = Set.empty
      val comments = appCommentRepo.find(appId, start, limit).map(asObject[AppUserComment])
      comments.foreach(comment => {
        val userDetailsFuture = Await.result(commonRegConnector.userProfileDetails(UserDetailsByUsernameReq(comment.username.getOrElse(""))), Duration(2000, TimeUnit.MILLISECONDS))
        var userImage: String = ""
        if (!userDetailsFuture.image.isEmpty) {
          userImage = userDetailsFuture.image
        }
        lst += AppUserCommentWithUserImage(comment.id, comment.username, Some(userImage), comment.appId, comment.dateTime, comment.comments, comment.abuse)
      })
      eventLog(correlationId, eventSource.FIND_COMMENTS_WITH_USER_IMAGE, "Success")
      lst.toList.sortWith(_.dateTime > _.dateTime)
    }
  }

  def update(sessionId: String, appId: String, comments: String, reportAbuse: Boolean)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    def handleReportAbuse(name: String, msisdn: String): Future[StatusCode] = {
      governanceConnector.report(AbuseReport(appId, msisdn, comments, "web")).map(
        rs => ServiceStatusCode(rs.statusCode, rs.statusDescription.getOrElse("")).asStatusCode
      )
    }

    def handleAppComment(username: String): Future[StatusCode] = {
      future {
        val appUserComments = AppUserComment(
          username = Some(username),
          appId = appId,
          dateTime = System.currentTimeMillis(),
          comments = comments
        )
        appCommentRepo.create(asDBObject(appUserComments))
        S1000
      }
    }

    authService.validate(SessionValidationReq(sessionId)).flatMap {
      case SessionValidationResp(_, _, _, Some(msisdn), Some(name)) => {
        eventLog(correlationId, EventSource.CORE.UPDATE_COMMENTS, s"appId = $appId")
        val result = appFindService.findOne(appId).flatMap {
          _ => {
            if (reportAbuse) {
              handleReportAbuse(name, msisdn)
            } else {
              handleAppComment(name)
            }
          }
        }
        eventLog(correlationId, EventSource.CORE.UPDATE_COMMENTS, "Asynchronusly computed!")
        result
      }
    }
  }

  def remove(commentId: String)(implicit sc: ServiceContext): Future[StatusCode] = {
    future {
      appCommentRepo.remove(commentId)
      S1000
    }
  }
}
