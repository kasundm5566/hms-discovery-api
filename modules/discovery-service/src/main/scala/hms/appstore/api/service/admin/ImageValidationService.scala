package hms.appstore.api.service.admin

import hms.appstore.api.json.{StatusCode, ImageUploadReq}
import hms.appstore.api.service.ServiceContext


trait ImageValidationService {
  /**
   * Validate the image upload in following way
   * Icon should be min 120x120, aspect ratio is = 1 and max size 50*1024
   * ScreenShot should be min 240x360, aspect ratio is = 24/36 and max size 50*1024
   * MobileScreenShot should be min 360x600, aspect ratio is = 360/600 and max size 40*1024
   *
    * @param upload request
   * @return
   */
 def validate(upload: ImageUploadReq)(implicit sc: ServiceContext): StatusCode
}
