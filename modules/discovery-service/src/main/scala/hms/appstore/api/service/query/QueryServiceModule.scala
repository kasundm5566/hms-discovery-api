package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject.NewBindingModule

import hms.appstore.api.repo.RepoModule
import hms.appstore.api.service._
import cms.CmsServiceModule

object QueryServiceModule extends NewBindingModule({
  implicit module =>
    module <~ BaseServiceModule
    module <~ RepoModule
    module <~ CmsServiceModule

    module.bind[AppSearchService] toSingle new AppSearchServiceImpl

    module.bind[CategoryService] toSingle new CategoryServiceImpl

    module.bind[AppFindService] toSingle new AppFindServiceImpl

    module.bind[FeaturedAppService] toSingle new FeaturedAppServiceImpl

    module.bind[AppCountService] toSingle new AppCountServiceImpl
})
