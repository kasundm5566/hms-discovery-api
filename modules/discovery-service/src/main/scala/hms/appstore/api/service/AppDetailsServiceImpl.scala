/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import java.util.concurrent.TimeUnit

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import hms.appstore.api.domain.{AppDnStatus, AppStoreApp, AppUserComment}
import hms.appstore.api.json._
import hms.appstore.api.service.auth.{AuthService, CommonRegConnector}
import hms.appstore.api.service.cms.CmsConnector
import hms.appstore.api.service.query.AppFindService
import hms.appstore.api.service.subscription.SubscriptionConnector

import scala.concurrent.{Await, Future}
import hms.appstore.api.repo.{DownloadReqRepo, SalatMapper}
import hms.appstore.api.domain.AppUserComment

import scala.concurrent.duration.Duration

class AppDetailsServiceImpl(implicit val bindingModule: BindingModule) extends AppDetailsService
with SalatMapper
with Injectable
with EventLogging {

  private val subscriptionConnector = inject[SubscriptionConnector]

  private val authService = inject[AuthService]

  private val cmsConnector = inject[CmsConnector]

  private val appFindService = inject[AppFindService]

  private val appCommentService = inject[AppCommentService]

  private val downloadReqRepo = inject[DownloadReqRepo]

  def appDetails(appId: String, sessionId: Option[String])(implicit sc: ServiceContext): Future[QueryResult[AppStoreApp]] = {
    import sc._

    def fillSubscriptionDetail(app: AppStoreApp, mobileNo: Option[String]): Future[AppStoreApp] = {
      logger.debug("adding subscription-status detail for app[{}]", app.id)



      mobileNo match {
        case Some(m) => {
          val subscriptioncountFuture = subscriptionConnector.subscriptionCount(app.id)
          val subcount = Await.result(subscriptioncountFuture, Duration(1000L, TimeUnit.MILLISECONDS)).baseSize
          app.subscriptionsCount = subcount.toInt

          subscriptionConnector.subscriptionStatus(m, app.id)
            .map {
              resp => {
                app.copy(subscriptionStatus = resp.getSubscriptionStatus)
              }
            }

        }
        case None => {
          subscriptionConnector.subscriptionCount(app.id)
            .map{ resp => app.copy( subscriptionsCount = resp.baseSize.toInt ) }
        }

      }

    }

    def fillDownloadableBinaryDetail(app: AppStoreApp): Future[AppStoreApp#DnBinaries] = {
      cmsConnector.findPlatforms(PlatformQuery(List(app.id))).map {
        resp => {
          logger.debug("adding downloadable binaries detail for app[{}]", appId)
          resp.platformsByAppId(app.id)
        }
      }
    }

    def fillDownloadableRequestDetail(app: AppStoreApp, mobileNo: Option[String]): Option[AppDnStatus] = {
      mobileNo match {
        case Some(m) => {
          logger.debug("getting downloadable request details of [{}] for app[{}] ", m, app.id)
          downloadReqRepo.findByApplicationId(app.id, m).map(dbObj => asObject[AppDnStatus](dbObj)).lastOption
        }
        case None => None
      }
    }


    def fillDownloadsCountDetail(appId: String): Future[AppDownloadsCountResp] = {
      cmsConnector.appDownloadsCount(AppDownloadsCountReq(appId)).map {
        resp => {
          logger.debug("app downloads count response {}", resp)
          resp
        }
      }
    }

    def doFillAppDetails(app: AppStoreApp, mobileNo: Option[String] = None): Future[AppStoreApp] = {
      logger.debug("returning app details for [{}] with session id", appId)

      //      val appReviewsFuture = appCommentService.find(appId)
      // Above method commented because it is required to add user profile image to the comments response objects.
      val appReviewsFuture = appCommentService.findWithUserImage(appId)
      val downloadsCountFuture = fillDownloadsCountDetail(appId)

      if (app.downloadable && app.subscription) {
        val cmsDetailFuture = fillDownloadableBinaryDetail(app)
        val subscriptionDetailFuture = fillSubscriptionDetail(app, mobileNo)
        val downloadReqDetail = fillDownloadableRequestDetail(app, mobileNo)

        for {
          reviews <- appReviewsFuture
          binaries <- cmsDetailFuture
          appInfoNew <- subscriptionDetailFuture
          appDownloadsCount <- downloadsCountFuture
        } yield appInfoNew.copy(downloadableBinaries = binaries, userComments = reviews, downloadStatus = downloadReqDetail, downloadsCount = appDownloadsCount.downloadsCount, subscriptionsCount = 0)
      } else if (app.subscription) {
        val subscriptionDetailFuture = fillSubscriptionDetail(app, mobileNo)

        for {
          reviews <- appReviewsFuture
          appInfoNew <- subscriptionDetailFuture
        } yield appInfoNew.copy(userComments = reviews)
      } else if (app.downloadable) {
        val cmsDetailFuture = fillDownloadableBinaryDetail(app)
        val downloadReqDetail = fillDownloadableRequestDetail(app, mobileNo)

        for {
          reviews <- appReviewsFuture
          binaries <- cmsDetailFuture
          appDownloadsCount <- downloadsCountFuture
        } yield app.copy(downloadableBinaries = binaries, userComments = reviews, downloadStatus = downloadReqDetail, downloadsCount = appDownloadsCount.downloadsCount, subscriptionsCount = 0)
      } else {
        appReviewsFuture.map {
          reviews => app.copy(userComments = reviews)
        }
      }
    }


    eventLog(correlationId, EventSource.CORE.APP_DETAILS, s"appId = $appId, sessionId = ${sessionId.getOrElse("N/A")}")

    val appResultFuture = appFindService.findOne(appId)

    val results = sessionId match {
      case Some(id) =>
        val sessionResultFuture = authService.validate(SessionValidationReq(id))

        for {
          appResult <- appResultFuture
          mobileNo <- sessionResultFuture.collect {
            case SessionValidationResp(_, _, _, mobileNo, _) => mobileNo
          }
          result <- doFillAppDetails(appResult.getResult, mobileNo)
        } yield QueryResult(result)

      case _ => {
        logger.debug("returning app details for [{}]", appId)
        appResultFuture.flatMap(appResult => doFillAppDetails(appResult.getResult)).map(QueryResult(_))
      }
    }

    eventLog(correlationId, EventSource.CORE.APP_DETAILS, "Asynchronously computed!")

    results
  }
}
