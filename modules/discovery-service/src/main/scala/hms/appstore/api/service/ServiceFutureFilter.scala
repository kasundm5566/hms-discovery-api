package hms.appstore.api.service

import scala.concurrent.{Promise, Future}
import scala.util.{Failure, Success}
import hms.appstore.api.json.ServiceResp
import com.escalatesoft.subcut.inject.Injectable

trait ServiceFutureFilter extends EventLogging with Injectable{

  protected val statusCodeMapper = inject[StatusCodeMapper]

  protected def filterWithHttpSuccess[S <: ServiceResp[S]](ec: EventSource, path: String, f: Future[S])(implicit sc: ServiceContext): Future[S] = {
    import sc._
    val p = Promise[S]()

    f.onComplete{
      case Success(resp) => {
        val sc = statusCodeMapper.mapToApiStatusCode(ec , resp.asStatusCode)
        val finalResp = resp.copyFrom(status = sc.status, description = sc.description)
        eventLog(correlationId, ec, finalResp)
        p success finalResp
      }
      case Failure(e) => {
        logger.error(s"NBL API request [$path] failed, for correlationId [$correlationId], action [$path]", e)
        eventLog(correlationId, ec, "ERROR")
        p failure new ServiceException(correlationId, E5000, ec)
      }
    }
    p.future
  }

  protected def filterWithServiceSuccess[S <: ServiceResp[S]](ec: EventSource, path: String, f: Future[S])(implicit sc: ServiceContext): Future[S] = {
    import sc._
    val p = Promise[S]()

    f.onComplete{
      case Success(resp) if !resp.isSuccess => {
        val sc = statusCodeMapper.mapToApiStatusCode(ec , resp.asStatusCode)
        val finalResp = resp.copyFrom(status = sc.status, description = sc.description)
        eventLog(correlationId, ec, finalResp)
        p failure new ServiceException(correlationId, finalResp, ec)
      }
      case Success(resp) => {
        val sc = statusCodeMapper.mapToApiStatusCode(ec , resp.asStatusCode)
        val finalResp = resp.copyFrom(status = sc.status, description = sc.description)
        eventLog(correlationId, ec, finalResp)
        p success finalResp
      }
      case Failure(e) => {
        logger.error(s"NBL API request [$path] failed, for correlationId [$correlationId], action [$path]", e)
        eventLog(correlationId, ec, "ERROR")
        p failure new ServiceException(correlationId, E5000, ec)
      }
    }
    p.future
  }

  protected def filterWithIgnoreErrors[S <: ServiceResp[S]](ec: EventSource, path: String, f: Future[S], default: S)(implicit sc: ServiceContext): Future[S] = {
    import sc._
    val p = Promise[S]()

    f.onComplete{
      case Success(resp) => {
        val sc = statusCodeMapper.mapToApiStatusCode(ec , resp.asStatusCode)
        val finalResp = resp.copyFrom(status = sc.status, description = sc.description)
        eventLog(correlationId, ec, finalResp)
        p success finalResp
      }
      case Failure(e) => {
        logger.error(s"NBL API request [$path] failed, for correlationId [$correlationId], action [$path], recovering with empty response", e)
        eventLog(correlationId, ec, "ERROR")
        p success default
      }
    }
    p.future
  }
}


