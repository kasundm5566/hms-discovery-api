package hms.appstore.api.service.caas

import hms.appstore.api.service.{EventSource, StatusCodeMapper, ServiceContext, ServiceFutureFilter}
import hms.appstore.api.json.{MsgJsonProtocol, PaymentInstrumentsResp, PaymentInstrumentsReq}

import hms.scala.http.{HttpConnectorConfig, DefaultHttpClient}
import hms.scala.http.marshalling.HttpMarshalling
import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import scala.concurrent.Future


class CaasConnectorImpl (implicit val bindingModule: BindingModule) extends CaasConnector
with ServiceFutureFilter
with Injectable{

  import MsgJsonProtocol._
  import HttpMarshalling._

  protected val eventSource = EventSource.CAAS

  private val connector = new DefaultHttpClient(
    HttpConnectorConfig("caasConnector", "http", inject[String]("caas.host"), inject[Int]("caas.port"))
  )

  def findPaymentInstruments(req: PaymentInstrumentsReq)(implicit sc: ServiceContext): Future[PaymentInstrumentsResp] = {
    val path = "/caas/list/pi"
    val result = connector.doPost[PaymentInstrumentsReq, PaymentInstrumentsResp](path, req.copy(subscriberId = "tel:" + req.subscriberId))
    filterWithHttpSuccess(eventSource.PAYMENT_INSTRUMENTS, path, result)
  }
}
