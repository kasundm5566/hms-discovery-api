package hms.appstore.api.service

import com.escalatesoft.subcut.inject.NewBindingModule

/**
 * Created by kasun on 3/20/18.
 */
object BannerServiceModule extends NewBindingModule({
  implicit module =>
    module <~ BaseServiceModule

    module.bind[BannerService] toSingle new BannerServiceImpl
})
