package hms.appstore.api.domain

/**
 * Created by kasun on 5/15/18.
 */
import com.novus.salat.annotations._
import hms.appstore.api.json.UserComment
import org.bson.types.ObjectId

case class AppUserCommentWithUserImage(@Key("_id") id: ObjectId = new ObjectId,
                                       username: Option[String],
                                       image: Option[String],
                                       @Key("app_id") appId: String,
                                       @Key("time_in_mili") dateTime: Long,
                                       comments: String,
                                       @Ignore abuse: Option[Boolean] = Some(false)) extends UserComment {
  def commentId: Option[String] = Some(id.toString)
}
