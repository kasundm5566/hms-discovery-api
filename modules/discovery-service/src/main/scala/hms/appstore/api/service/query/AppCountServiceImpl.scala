package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.casbah.query.Imports._

import hms.appstore.api.json.{SearchQuery, AppStatus, QueryResult, AppCount}
import hms.appstore.api.repo.{NoSorting, AppQueryCmd, AppRepo, SalatMapper}
import hms.appstore.api.service._
import hms.appstore.api.service.Futures._

import scala.concurrent.Future
import util.matching.Regex
import hms.appstore.api.service.ServiceContext
import scala.Some
import hms.appstore.api.json.SearchQuery
import hms.appstore.api.json.AppCount
import hms.appstore.api.repo.AppQueryCmd

class AppCountServiceImpl(implicit val bindingModule: BindingModule) extends AppCountService
 with Injectable
 with SalatMapper
 with EventLogging {

  private val appRepo = inject[AppRepo]
  private val sysConfigService = inject[SysConfigService]

  private val regexChars = List('?', '(', ')', '*', '+', '\\', '`', '{', '}', '[', ']', '/')

  private def searchTxtAsRegex(txt: String) = {
    val filteredTxt = txt.filter(!regexChars.contains(_))
    s".*$filteredTxt.*(?i)".r
  }

  private def downloadableQuery = {
    MongoDBObject("ncs-slas.downloadable" -> MongoDBObject("$exists" -> true))
  }

  private def freeAppQuery = {
    def genQuery(key: String) = $or(MongoDBObject(key -> MongoDBObject("$exists" -> false)), MongoDBObject(key -> "free"))
    sysConfigService.ncsTypes.map(ncs => s"ncs-slas.$ncs.charging.type").toList.map(genQuery)
  }

  protected def doCount(query: AppQueryCmd)(implicit sc: ServiceContext): QueryResult[AppCount] = {
    QueryResult(AppCount(appRepo.countWithQuery(query)))
  }


  def countBy(searchQuery: SearchQuery)(implicit sc: ServiceContext): Future[QueryResult[AppCount]] = {
    import sc._
    future {
      eventLog(correlationId, EventSource.CORE.COUNT_APPS_BY_QUERY, s"query = $searchQuery")

      val queryArgsOpt = searchQuery.searchText.map {
        txt => Map("publish-name" -> searchTxtAsRegex(txt))
      }

      val queryArgs = queryArgsOpt.getOrElse(Map.empty[String, List[Map[String, Regex]]])

      val query = AppQueryCmd(
        queryArgs = queryArgs,
        includeOnly = Map.empty[String, String],
        sortOrder = NoSorting,
        appStatus = searchQuery.status,
        appCategory = searchQuery.category,
        appId = searchQuery.appId,
        appType = searchQuery.appType,
        sp = searchQuery.spName,
        start = searchQuery.start,
        limit = searchQuery.limit
      )

      val results = doCount(query)

      eventLog(correlationId, EventSource.CORE.COUNT_APPS_BY_QUERY, s"Success, count = ${results.getResult.count}")
      results
    }
  }

  def countByCategory(category: String)(implicit sc: ServiceContext): Future[QueryResult[AppCount]] = {
    import sc._
    future {
      eventLog(correlationId, EventSource.CORE.COUNT_APPS_BY_CATEGORY, s"category = $category")

      val queryMap = Map("$or" -> List(Map("category" -> category), Map("labels" -> Map("$in" -> List(category)))))
      val results = doCount(AppQueryCmd(queryArgs = queryMap))

      eventLog(correlationId, EventSource.CORE.COUNT_APPS_BY_CATEGORY, s"Success, count = ${results.getResult.count}")
      results
    }
  }

  def countFreeApps(category: Option[String])(implicit sc: ServiceContext): Future[QueryResult[AppCount]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.COUNT_FREE_APPS, s"category = ${category.getOrElse("N/A")}")

      val queryMap = Map("category" -> category).collect {
        case (k, Some(v)) => (k, v)
      }

      val results = doCount(AppQueryCmd(queryArgs = queryMap ++ $and(freeAppQuery)))

      eventLog(correlationId, EventSource.CORE.COUNT_FREE_APPS, s"Success, count = ${results.getResult.count}")
      results
    }
  }

  def countDownloadableFreeApps(category: Option[String])(implicit sc: ServiceContext): Future[QueryResult[AppCount]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.COUNT_DOWNLOADABLE_FREE_APPS, s"category = ${category.getOrElse("N/A")}")

      val queryMap = Map("category" -> category).collect {
        case (k, Some(v)) => (k, v)
      }

      val queryPart: List[DBObject] = freeAppQuery ::: List( downloadableQuery)

      val results = doCount(AppQueryCmd(queryArgs = queryMap ++ Map("$and" -> queryPart)))

      eventLog(correlationId, EventSource.CORE.COUNT_DOWNLOADABLE_FREE_APPS, s"Success, count = ${results.getResult.count}")
      results
    }
  }

  def countDownloadableApps(category: Option[String])(implicit sc: ServiceContext): Future[QueryResult[AppCount]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.COUNT_DOWNLOADABLE_APPS, s"category = ${category.getOrElse("N/A")}")

      val queryMap = Map("category" -> category).collect {
        case (k, Some(v)) => (k, v)
      }

      val results = doCount(AppQueryCmd(queryArgs = queryMap ++ $and(downloadableQuery)))

      eventLog(correlationId, EventSource.CORE.COUNT_DOWNLOADABLE_APPS, s"Success, count = ${results.getResult.count}")
      results
    }

  }

  def countAllApps()(implicit sc: ServiceContext): Future[QueryResult[AppCount]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.COUNT_PUBLISHED_APPS, "")

      val count = appRepo.countWithQuery(AppQueryCmd())
      val result = QueryResult(AppCount(count))

      eventLog(correlationId, EventSource.CORE.COUNT_PUBLISHED_APPS, s"Success, count = $count")
      result
    }
  }

  def countByStatus(status: AppStatus.AppStatus)(implicit sc: ServiceContext): Future[QueryResult[AppCount]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.COUNT_APPS_BY_STATUS, s"status = $status")

      val count = appRepo.countWithQuery(AppQueryCmd(appStatus = Some(status)))

      val result = QueryResult(AppCount(count))

      eventLog(correlationId, EventSource.CORE.COUNT_APPS_BY_STATUS,s"Success, count = $count")
      result
    }
  }
}
