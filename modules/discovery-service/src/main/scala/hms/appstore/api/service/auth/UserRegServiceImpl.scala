package hms.appstore.api.service.auth

import hms.appstore.api.json._
import hms.appstore.api.service.{EventLogging, ServiceContext}
import scala.concurrent.Future
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import hms.appstore.api.json.UserRegistrationResp
import hms.appstore.api.json.MsisdnVerificationResp
import hms.appstore.api.json.MsisdnVerificationReq
import hms.appstore.api.json.UserRegistrationReq
import hms.appstore.api.service.ServiceContext

class UserRegServiceImpl(implicit val bindingModule: BindingModule) extends UserRegService with Injectable with EventLogging {

  private val commonRegConnector = inject[CommonRegConnector]

  def createUser(createReq: UserRegistrationReq)
                        (implicit sc: ServiceContext): Future[UserRegistrationResp] = {
    commonRegConnector.createConsumerUser(createReq)
  }

  def verifyMsisdn(verificationReq: MsisdnVerificationReq)
                  (implicit sc: ServiceContext): Future[MsisdnVerificationResp] = {
    commonRegConnector.verifyMsisdn(verificationReq)
  }

  def requestNewVerificationCode(codeReq: MsisdnVerificationCodeReq)
                                (implicit sc: ServiceContext): Future[MsisdnVerificationResp] = {
    commonRegConnector.requestNewVerificationCode(codeReq)
  }

  def recoveryUserAccount(recoverReq: UserAccountRecoveryReq)
                         (implicit sc: ServiceContext): Future[UserAccountRecoveryResp] = {
    commonRegConnector.recoverUserAccount(recoverReq)
  }
}
