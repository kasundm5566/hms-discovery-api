package hms.appstore.api.domain

import org.joda.time.DateTime
import hms.appstore.api.json.{Application, DownloadStatus}

case class AppDnStatus(applicationId: String, status: String, requestedDate: DateTime, contentId: String) extends DownloadStatus