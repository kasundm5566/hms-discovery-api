/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.subscription

import scala.concurrent.Future
import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext

/**
 * SDP NBL Subscription module connector
 */
trait SubscriptionConnector {
  def subscribedApps(msisdn: String)(implicit sc: ServiceContext): Future[SubscribedApps]

  def subscribe(msisdn: String, appId: String, password: String)(implicit sc: ServiceContext): Future[SubscriptionActionResp]

  def subscriptionStatus(msisdn: String, appId: String)(implicit sc: ServiceContext): Future[SubscriptionStatusResp]

  def unSubscribe(msisdn: String, appId: String, password: String)(implicit sc: ServiceContext): Future[SubscriptionActionResp]

  def subscriptionCount(appId: String)(implicit sc: ServiceContext) : Future[SubscriptionCountResp]
}
