package hms.appstore.api.service.notification

import com.escalatesoft.subcut.inject.NewBindingModule
import hms.appstore.api.service.BaseServiceModule
import hms.scala.http.util.ConfigUtils
import com.typesafe.config.ConfigFactory

/**
 * Created by kasun on 6/19/18.
 */
object NotificationServiceModule extends NewBindingModule({
  implicit module =>
    module <~ BaseServiceModule

    val confServer = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "nbl.connector.notification.server")
    val confAdmin = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "nbl.connector.notification.admin")

    module.bind[String] idBy "notification.server.host" toSingle confServer.getString("host")
    module.bind[Int] idBy "notification.server.port" toSingle confServer.getInt("port")

    module.bind[String] idBy "notification.admin.host" toSingle confAdmin.getString("host")
    module.bind[Int] idBy "notification.admin.port" toSingle confAdmin.getInt("port")

    module.bind[NotificationServerConnector] toSingle new NotificationServerConnectorImpl
    module.bind[NotificationAdminConnector] toSingle new NotificationAdminConnectorImpl

})
