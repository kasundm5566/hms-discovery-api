/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.subscription

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.typesafe.scalalogging.slf4j.Logging

import hms.appstore.api.json._
import hms.appstore.api.service.query.AppFindService
import hms.appstore.api.service.auth.AuthService
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.json.SessionValidationReq
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.SubscriptionActionResp

import scala.concurrent.Future

class SubscriptionServiceImpl(implicit val bindingModule: BindingModule) extends SubscriptionService
  with Injectable
  with Logging {

  private val appFindService = inject[AppFindService]
  private val authService = inject[AuthService]
  private val subscriptionConnector = inject[SubscriptionConnector]


  /**
   * mySubscriptions
   *
   * list all app subscriptions for the current user
   *
   * @param sessionId (String)
   * @param start number of elements to skip
   * @param limit number of elements to return
   * @return QueryResult[AppStoreApp]
   */
  def mySubscriptions(sessionId: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    val req = SessionValidationReq(sessionId)

    authService.validate(req).flatMap {
      case SessionValidationResp(_, _, _, Some(msisdn), _) =>
        subscriptionConnector.subscribedApps(msisdn).map(_.appIds).flatMap {
          appIds => { appFindService.findApps(appIds).map(_.getResults).map(
              _.map(app => subscriptionConnector.subscriptionStatus(msisdn, app.id).map(
                  resp => app.copy(subscriptionStatus = resp.getSubscriptionStatus)
                )
              )
            ).flatMap(Future.fold(_)(List.empty[AppStoreApp])((app, apps) => apps :: app)).map(apps => QueryResults(apps))
          }
        }
    }
  }


  def subscribe(sessionId: String, appId: String)(implicit sc: ServiceContext): Future[SubscriptionActionResp] = {
    import sc._
    val appResult = appFindService.findOne(appId)
    val sessionResult = authService.validate(SessionValidationReq(sessionId))

    for {
      app <- appResult
      session <- sessionResult
      subResult <- subscriptionConnector.subscribe(session.mobileNo.get, appId, app.getResult.password)
    } yield subResult
  }

  def subscriptionCount(appId: String)(implicit sc: ServiceContext): Future[SubscriptionCountResp] = {
    import sc._

    for {
      subCountResult <- subscriptionConnector.subscriptionCount(appId)
    } yield subCountResult
  }

  def unSubscribe(sessionId: String, appId: String)(implicit sc: ServiceContext): Future[SubscriptionActionResp] = {
    import sc._
    val appResult = appFindService.findOne(appId)
    val sessionResult = authService.validate(SessionValidationReq(sessionId))

    for {
      app <- appResult
      session <- sessionResult
      subResult <- subscriptionConnector.unSubscribe(session.mobileNo.get, appId, app.getResult.password)
    } yield subResult
  }
}
