/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import hms.appstore.api.json.StatusCode


/**
 * Success
 */
case object S1000 extends StatusCode {
  val status = "S1000"

  val description = "Success"
}

//TODO: add status code for S1003, S1005

/**
 * Internal Error
 */
case object E5000 extends StatusCode {
  val status: String = "E5000"
  val description: String = "Internal Error occurred in Discovery API Service!"
}

/**
 * Application does not exist/active
 */
case object E5101 extends StatusCode {
  val status: String = "E5101"
  val description: String = "The given application is not active or does not exist in the appstore"
}


/**
 * Invalid Request
 */
case object E5102 extends StatusCode {
  val status: String = "E5102"
  val description: String = "The input given/referred in the request is not valid or does not exist"
}


/**
 * Image Upload Failed due to resolution/aspect-ratio validation
 */
case object E5103 extends StatusCode {
  val status: String = "E5103"
  val description: String = "Image resolution or aspect ratio validation failed"
}


/**
 * Image Upload Failed due to file size validation
 */
case object E5104 extends StatusCode {
  val status: String = "E5104"
  val description: String = "Image size validation failed"
}


/**
 * Session Validation failure
 */
case object E5105 extends StatusCode {
  val status: String = "E5105"
  val description: String = "Invalid session, Please logout and login again!"
}

/**
 * Application already published
 */
case object E5106 extends StatusCode {
  val status: String = "E5106"
  val description: String = "Application already published!"
}

/**
 * Application developer & requested user mismatch
 */
case object E5107 extends  StatusCode{
  def status: String = "E5107"
  def description: String = "Application developer id & requesting user id does not match"
}

/**
 * Other constructable service status.
 * @param status  code
 * @param description status description
 */
case class ServiceStatusCode(override val status: String, override val description: String) extends StatusCode
