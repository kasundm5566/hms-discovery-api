package hms.appstore.api.service.admin

import hms.appstore.api.json.Application
import akka.actor.Actor
import hms.appstore.api.service.EventLogging


/**
 * Application LifeCycle event processor,
 * here we can implement the business logic to handle specific events like sending an email
 */
class AppStateEventObserver extends Actor with EventLogging{
  def receive = {
    case  AppRejected(eventId: Long, userId: String, app: Application) =>
    case  AppPublished(eventId: Long, userId: String, app: Application) =>
    case  AppUnPublished(eventId: Long, userId: String, app: Application) =>
    case  _ =>
  }
}
