/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import com.mongodb.casbah.Imports._

/**
 * Holder trait of all appstore db collections
 */
trait Db {
  def apps: MongoCollection

  def pendingApps: MongoCollection

  def backupApps: MongoCollection

  def categories: MongoCollection

  def superCategories: MongoCollection

  def appUserReviews: MongoCollection

  def featuredApps : MongoCollection

  def addUrls : MongoCollection

  def downloadRequests : MongoCollection

  def sysConfig : MongoCollection

  def banners: MongoCollection

  def bannerLocations: MongoCollection
}
