package hms.appstore.api.service.subscription

import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.service.Futures._

import collection.mutable.{ListMap => MutableMap}
import concurrent.Future

class MockSubscriptionConnectorImpl extends SubscriptionConnector {
  private val subscriptions = MutableMap.empty[String, Set[String]]

  def subscribedApps(msisdn: String)(implicit sc: ServiceContext): Future[SubscribedApps] = {
    future {
      val appIds = subscriptions.getOrElse(msisdn, Set.empty)
      SubscribedApps("S1000", Some("Success"), appIds.toList)
    }
  }

  def subscribe(msisdn: String, appId: String, password: String)(implicit sc: ServiceContext) = {
    future {
      val appIds = subscriptions.getOrElse(msisdn, Set.empty)

      if (!appIds.contains(appId)) {
        subscriptions.put(msisdn, appIds + appId)
        SubscriptionActionResp("S1000", Some("Subscribed"))
      } else {
        SubscriptionActionResp("E1000", Some("Already subscribged"))
      }
    }
  }

  def subscriptionStatus(msisdn: String, appId: String)(implicit sc: ServiceContext) = {
    future {
      val appIds = subscriptions.getOrElse(msisdn, Set.empty)

      if (appIds.contains(appId)) {
        SubscriptionStatusResp("S1000", Some("Success"), Some(SubscriptionStatus.Registered))
      } else {
        SubscriptionStatusResp("S1000", Some("Success"), Some(SubscriptionStatus.UnRegistered))
      }
    }
  }

  def unSubscribe(msisdn: String, appId: String, password: String)(implicit sc: ServiceContext) = {
    future {
      val appIds = subscriptions.getOrElse(msisdn, Set.empty)

      if (appIds.contains(appId)) {
        subscriptions.put(msisdn, appIds - appId)
        SubscriptionActionResp("S1000", Some("Unsubscribed"))
      } else {
        SubscriptionActionResp("E1000", Some("Not subscribed"))
      }
    }
  }

  def subscriptionCount(appId: String)(implicit sc: ServiceContext) = {
    future {
      SubscriptionCountResp("S1000", Some("Success"), "10")
    }
  }

}
