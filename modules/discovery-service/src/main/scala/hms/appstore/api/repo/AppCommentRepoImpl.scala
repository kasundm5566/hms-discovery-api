/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import com.escalatesoft.subcut.inject._
import com.mongodb.casbah.query.Imports._
import com.mongodb.casbah.commons.conversions.scala._

import org.joda.time.DateTime
import hms.appstore.api.domain.AppUserComment

class AppCommentRepoImpl(implicit val bindingModule: BindingModule) extends AppCommentRepo with Injectable {

  private val db: Db = inject[Db]

  def create(dbObject: DBObject) {
    db.appUserReviews.insert(dbObject)
  }


  def find(appId: String, start: Int, limit: Int): List[DBObject] = {
    db.appUserReviews.find(MongoDBObject("app_id" -> appId)).
      sort(MongoDBObject("time_in_mili" -> -1)).
      skip(start).
      limit(limit).toList
  }

  def remove(commentId: String){
    db.appUserReviews.remove(MongoDBObject("_id" -> new ObjectId(commentId)))
  }

}
