/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.auth

import hms.appstore.api.json._
import hms.appstore.api.service._
import hms.scala.http.{HttpConnectorConfig, DefaultHttpClient}
import hms.scala.http.marshalling.HttpMarshalling

import concurrent.Future
import com.escalatesoft.subcut.inject._
import org.apache.commons.codec.digest.DigestUtils
import spray.http.{HttpHeader, MultipartFormData}
import spray.http.parser.HttpParser
import spray.http.HttpHeaders.RawHeader


class CommonRegConnectorImpl(implicit val bindingModule: BindingModule) extends CommonRegConnector
with ServiceFutureFilter
with Injectable {

  import AuthJsonProtocol._
  import HttpMarshalling._

  private val sysConfig = inject[SysConfigService]
  protected val ec = EventSource.COMMON_REG
  private val connector = new DefaultHttpClient(
    HttpConnectorConfig("commonRegConnector", "http", inject[String]("common.reg.host"), inject[Int]("common.reg.port"))
  )

  def authenticateByPassword(req: AuthenticateByPasswordReq)(implicit sc: ServiceContext): Future[AuthenticateResp] = {
    val (path, message) = sysConfig.authProvider match {
      case "dialog-connect" => {
        ("/registration/registrationservice/authenticate/using-provider", Map("username" -> req.username, "password" -> req.password))
      }
      case _ => {
        ("/registration/registrationservice/discovery/authentication", Map("user-name" -> req.username, "password" -> DigestUtils.md5Hex(req.password)))
      }
    }
    val result = connector.doPost[Map[String, String], AuthenticateResp](path, message)
    filterWithHttpSuccess(ec.AUTHENTICATE_BY_PASSWORD, path, result)
  }

  def authenticateByMPin(req: AuthenticateByMPinReq)(implicit sc: ServiceContext): Future[AuthenticateResp] = {
    val path = "/registration/registrationservice/user/mpin/authenticate"
    val result = connector.doPost[AuthenticateByMPinReq, AuthenticateResp](path, req.copy(mpin = DigestUtils.md5Hex(req.mpin)))
    filterWithServiceSuccess(ec.AUTHENTICATE_BY_MPIN, path, result)
  }

  def validate(req: SessionValidationReq)(implicit sc: ServiceContext) = {
    val path = "/registration/registrationservice/user/session-validate"
    val result = connector.doPost[SessionValidationReq, SessionValidationResp](path, req)
    filterWithServiceSuccess(ec.USER_SESSION_VALIDATE, path, result)
  }

  def userDetailsByUsername(req: UserDetailsByUsernameReq)(implicit sc: ServiceContext): Future[UserDetailByUserNameResp] = {
    val path = "/registration/registrationservice/user/retrieve"
    val result = connector.doPost[UserDetailsByUsernameReq, UserDetailByUserNameResp](path, req)
    logger.debug("userDetails resp [{}]", result)
    filterWithHttpSuccess(ec.USER_DETAILS_BY_USERNAME, path, result)
  }

  override def userProfileDetails(req: UserDetailsByUsernameReq)(implicit sc: ServiceContext): Future[UserProfileDetailsResp] = {
    val path = "/registration/registrationservice/user/retrieve"
    val result = connector.doPost[UserDetailsByUsernameReq, UserProfileDetailsResp](path, req)
    logger.debug("userDetails resp [{}]", result)
    filterWithHttpSuccess(ec.USER_DETAILS_BY_USERNAME, path, result)
  }

  override def updateUserProfile(req: UserProfileUpdateReq)(implicit sc: ServiceContext): Future[UserProfileUpdateResp] = {

    val path = "/registration/registrationservice/user/update"
    //    val result = connector.doPostWithHeader[MultipartFormData, UserProfileUpdateResp](path, req, API_HEADERS)
    val result = connector.doPost[UserProfileUpdateReq, UserProfileUpdateResp](path, req)
    logger.debug("user profile update resp [{}]", result)
    //Change
    filterWithHttpSuccess(ec.USER_PROFILE_UPDATE, path, result)
  }

  def changeMPin(req: MPinChangeReq)(implicit sc: ServiceContext): Future[MPinChangeResp] = {
    val path = "/registration/registrationservice/user/mpin/change-by-msisdn"
    val result = connector.doPost[MPinChangeReq, MPinChangeResp](path, req)
    filterWithServiceSuccess(ec.CHANGE_MPIN_BY_MSISDN, path, result)
  }

  def userDetailsByMsisdn(req: UserDetailsByMsisdnReq)(implicit sc: ServiceContext): Future[UserDetailsResp] = {
    val path = "/registration/registrationservice/user/detail/msisdn"
    val result = connector.doPost[UserDetailsByMsisdnReq, UserDetailsResp](path, req)
    filterWithHttpSuccess(ec.USER_DETAILS_BY_MSISDN, path, result)
  }

  def createConsumerUser(req: UserRegistrationReq)(implicit sc: ServiceContext): Future[UserRegistrationResp] = {
    val path = "/registration/registrationservice/user/create"
    val result = connector.doPost[UserRegistrationReq, UserRegistrationResp](path, req)
    filterWithHttpSuccess(ec.CREATE_CONSUMER_USER, path, result)
  }

  def verifyMsisdn(req: MsisdnVerificationReq)(implicit sc: ServiceContext): Future[MsisdnVerificationResp] = {
    val path = "/registration/registrationservice/user/verify"
    val result = connector.doPost[MsisdnVerificationReq, MsisdnVerificationResp](path, req)
    filterWithHttpSuccess(ec.VERIFY_MSISDN, path, result)
  }

  def requestNewVerificationCode(req: MsisdnVerificationCodeReq)(implicit sc: ServiceContext): Future[MsisdnVerificationResp] = {
    val path = "/registration/registrationservice/user/verify/code/new"
    val result = connector.doPost[MsisdnVerificationCodeReq, MsisdnVerificationResp](path, req)
    filterWithHttpSuccess(ec.REQUEST_NEW_VERIFICATION_CODE, path, result)
  }

  def recoverUserAccount(req: UserAccountRecoveryReq)(implicit sc: ServiceContext): Future[UserAccountRecoveryResp] = {
    val path = "/registration/registrationservice/user/account/recover"
    val result = connector.doPost[UserAccountRecoveryReq, UserAccountRecoveryResp](path, req)
    filterWithHttpSuccess(ec.RECOVER_USER_ACCOUNT, path, result)
  }

  def createAutoLoginConsumerUser(req: AutoLoginUserRegistrationReq)(implicit sc: ServiceContext): Future[AutoLoginUserRegistrationResp] = {
    val path = "/registration/registrationservice/autologin/user/create"
    val result = connector.doPost[AutoLoginUserRegistrationReq, AutoLoginUserRegistrationResp](path, req)
    filterWithHttpSuccess(ec.CREATE_AUTO_LOGIN_CONSUMER_USER, path, result)
  }

  override def resetUserPassword(req: UserPasswordResetReq)(implicit sc: ServiceContext): Future[UserPasswordResetResp] = {
    val path = "/registration/registrationservice/user/password/reset"
    val result = connector.doPost[UserPasswordResetReq, UserPasswordResetResp](path, req)
    filterWithHttpSuccess(ec.RESET_USER_PASSWORD, path, result)
  }
}
