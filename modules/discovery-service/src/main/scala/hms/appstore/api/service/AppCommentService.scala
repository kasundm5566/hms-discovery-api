/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import scala.concurrent.Future
import hms.appstore.api.json.StatusCode
import hms.appstore.api.domain.{AppUserCommentWithUserImage, AppUserComment}

trait AppCommentService {

  def find(appId: String, start: Int = 0, limit: Int = 10)(implicit sc: ServiceContext): Future[List[AppUserComment]]

  def findWithUserImage(appId: String, start: Int = 0, limit: Int = 10)(implicit sc: ServiceContext): Future[List[AppUserCommentWithUserImage]]

  def update(sessionId: String, appId: String, comments: String, reportAbuse: Boolean)(implicit sc: ServiceContext): Future[StatusCode]

  def remove(commentId: String)(implicit sc: ServiceContext) :Future[StatusCode]
}
