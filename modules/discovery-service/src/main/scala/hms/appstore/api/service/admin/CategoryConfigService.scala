package hms.appstore.api.service.admin

import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext
import concurrent.Future
import hms.appstore.api.json.Category
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.ParentCategory

trait CategoryConfigService {

  def findAllParentCategories(implicit sc: ServiceContext): Future[QueryResults[ParentCategory]]

  def createOrUpdateCategories(cs: List[Category])(implicit sc: ServiceContext): Future[StatusCode]

  def createCategory(cs: CategoryAddReq)(implicit sc: ServiceContext): Future[StatusCode]

  def editCategory(id: String, req: CategoryEditReq)(implicit sc: ServiceContext): Future[StatusCode]

  def removeCategory(id: String)(implicit sc: ServiceContext): Future[StatusCode]

  def addOrRemoveAppsOfCategory(id: String, req: AddOrRemoveAppsOfCategoryReq)(implicit sc: ServiceContext): Future[StatusCode]

  def createOrUpdateParentCategories(pcs: List[ParentCategory])(implicit sc: ServiceContext): Future[StatusCode]
}
