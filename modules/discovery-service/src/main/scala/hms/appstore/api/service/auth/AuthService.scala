/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.auth

import scala.concurrent.Future
import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.AuthenticateByPasswordReq
import hms.appstore.api.json.AuthenticateByMPinReq
import hms.appstore.api.json.AuthenticateResp
import hms.appstore.api.json.SessionValidationReq
import hms.appstore.api.service.ServiceContext
import spray.http.MultipartFormData

/**
* Discovery API Services are stateless and authentication/authorization is done
* using session token found in the service URL.
*
* This service
* 1. Create session id for users coming from trusted clients
*
* 2. Authenticate a user and create session id for users coming un-trusted/trusted clients
*
* 2. Checks the given session token is valid and return the associated user details
*
*/
trait AuthService {

  def authProviders(platform: String, msisdn: Option[String], host: Option[String])(implicit sc:ServiceContext): Future[AuthProviders]

  def authenticate(msisdn: Option[String], host: Option[String])(implicit sc:ServiceContext):Future[AuthenticateResp]

  def authenticate(req: AuthenticateByUsernameReq)(implicit sc: ServiceContext): Future[AuthenticateResp]

  def authenticate(req: AuthenticateByMsisdnReq)(implicit sc: ServiceContext): Future[AuthenticateResp]

  def authenticate(req: AuthenticateByMPinReq)(implicit sc: ServiceContext): Future[AuthenticateResp]

  def authenticate(req: AuthenticateByPasswordReq)(implicit sc: ServiceContext): Future[AuthenticateResp]

  def validate(req: SessionValidationReq)(implicit sc: ServiceContext): Future[SessionValidationResp]

  def changeMPin(req: MPinChangeReq)(implicit sc: ServiceContext): Future[MPinChangeResp]

  def userDetails(req: UserDetailsByMsisdnReq)(implicit sc: ServiceContext): Future[UserDetailsResp]

  def userDetailsByName(req: UserDetailsByUsernameReq)(implicit sc: ServiceContext):Future[UserDetailByUserNameResp]

  def userProfileDetails(sessionId: String)(implicit sc: ServiceContext):Future[UserProfileDetailsResp]

  def developerProfileDetails(req: UserDetailsByUsernameReq)(implicit sc: ServiceContext):Future[DeveloperProfileDetailsResp]

  def userProfileUpdate(sessionId: String, req: MultipartFormData)(implicit sc: ServiceContext):Future[UserProfileUpdateResp]

  def resetUserPassword(req: UserPasswordResetReq)(implicit sc: ServiceContext): Future[UserPasswordResetResp]
}
