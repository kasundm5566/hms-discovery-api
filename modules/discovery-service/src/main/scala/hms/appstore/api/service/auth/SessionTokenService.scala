/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.auth

import hms.appstore.api.service.ServiceContext

class SessionToken(val id: String, val msisdn: String, val username: String)

object SessionToken {
  val DsPrefix = "ds-"

  def isExternal(s: String): Boolean = !s.startsWith(DsPrefix)
}


trait SessionTokenService {

  def create(msisdn: String, username: String)(implicit sc: ServiceContext): SessionToken

  def validate(id: String)(implicit sc: ServiceContext): Option[SessionToken]
}
