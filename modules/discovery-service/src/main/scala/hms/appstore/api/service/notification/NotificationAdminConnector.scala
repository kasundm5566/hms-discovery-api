package hms.appstore.api.service.notification

import hms.appstore.api.json.{AppUpdateNotificationResp, AppUpdateNotificationReq, NotifyResp, NotifyReq}
import hms.appstore.api.service.ServiceContext
import scala.concurrent.Future

/**
 * Created by kasun on 6/20/18.
 */
trait NotificationAdminConnector {

  def notifyAll(req: NotifyReq)(implicit sc: ServiceContext): Future[NotifyResp]

  def notifyAppSpecific(req: NotifyReq)(implicit sc: ServiceContext): Future[NotifyResp]

  def notifyAppUpdate(req: AppUpdateNotificationReq)(implicit sc: ServiceContext): Future[AppUpdateNotificationResp]
}
