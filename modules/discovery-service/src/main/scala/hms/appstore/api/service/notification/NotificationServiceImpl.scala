package hms.appstore.api.service.notification

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.service.{EventSource, ServiceContext, EventLogging}
import hms.appstore.api.json._
import scala.concurrent.{Await, Future}
import hms.appstore.api.service.auth.{AuthService}
import hms.appstore.api.service.Futures._
import javax.mail.Session
import hms.appstore.api.repo.SalatMapper
import hms.appstore.api.json.DeviceRegistrationReq
import hms.appstore.api.json.SessionValidationReq
import scala.Some
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import spray.http.HttpEntity

/**
 * Created by kasun on 6/19/18.
 */
class NotificationServiceImpl(implicit val bindingModule: BindingModule) extends NotificationService with Injectable
with SalatMapper
with EventLogging {

  private val notificationConnector = inject[NotificationServerConnector]
  private val notificationAdminConnector = inject[NotificationAdminConnector]
  private val authService = inject[AuthService]

  override def registerDevice(sessionId: String, req: DeviceRegistrationReq)(implicit sc: ServiceContext): Future[NotificationResp] = {
    import sc._
    authService.validate(SessionValidationReq(sessionId)).flatMap {
      case SessionValidationResp(_, _, _, Some(msisdn), Some(name)) => {
        val deviceRegReq = DeviceRegistrationReq(deviceId = req.deviceId, userId = Some(msisdn), platform = req.platform,
          platformVersion = req.platformVersion, notificationToken = req.notificationToken)
        eventLog(correlationId, EventSource.NOTIFICATION_SERVER.DEVICE_REGISTER, req)
        val resp = Await.result(notificationConnector.registerDevice(deviceRegReq), Duration(5000, TimeUnit.MILLISECONDS))
        eventLog(correlationId, EventSource.NOTIFICATION_SERVER.DEVICE_REGISTER, resp)
        var additionalParams: Map[String, String] = Map()
        resp.getClass.getDeclaredFields.foreach(field => {
          field.setAccessible(true)
          additionalParams += (field.getName -> field.get(resp).toString)
        })
        future {
          NotificationResp("S1000", Some("Success"), Some(additionalParams))
        }
      }
    }
  }

  override def notifyAll(req: NotifyReq)(implicit sc: ServiceContext): Future[NotificationResp] = {
    import sc._
    eventLog(correlationId, EventSource.NOTIFICATION_ADMIN.NOTIFY_ALL, req)
    val resp = Await.result(notificationAdminConnector.notifyAll(req), Duration(5000, TimeUnit.MILLISECONDS))
    eventLog(correlationId, EventSource.NOTIFICATION_ADMIN.NOTIFY_ALL, resp)
    var additionalParams: Map[String, String] = Map()
    resp.getClass.getDeclaredFields.foreach(field => {
      field.setAccessible(true)
      additionalParams += (field.getName -> field.get(resp).toString)
    })
    future {
      NotificationResp("S1000", Some("Success"), Some(additionalParams))
    }
  }

  override def notifyAppSpecific(req: NotifyReq)(implicit sc: ServiceContext): Future[NotificationResp] = {
    import sc._
    eventLog(correlationId, EventSource.NOTIFICATION_ADMIN.NOTIFY_APP_SPECIFIC, req)
    val resp = Await.result(notificationAdminConnector.notifyAppSpecific(req), Duration(5000, TimeUnit.MILLISECONDS))
    eventLog(correlationId, EventSource.NOTIFICATION_ADMIN.NOTIFY_APP_SPECIFIC, resp)
    var additionalParams: Map[String, String] = Map()
    resp.getClass.getDeclaredFields.foreach(field => {
      field.setAccessible(true)
      additionalParams += (field.getName -> field.get(resp).toString)
    })
    future {
      NotificationResp("S1000", Some("Success"), Some(additionalParams))
    }
  }

  override def notifyAppUpdate(req: AppUpdateNotificationReq)(implicit sc: ServiceContext): Future[NotificationResp] = {
    import sc._
    eventLog(correlationId, EventSource.NOTIFICATION_ADMIN.NOTIFY_APP_UPDATE, req)
    val resp = Await.result(notificationAdminConnector.notifyAppUpdate(req), Duration(5000, TimeUnit.MILLISECONDS))
    eventLog(correlationId, EventSource.NOTIFICATION_ADMIN.NOTIFY_APP_UPDATE, resp)
    var additionalParams: Map[String, String] = Map()
    resp.getClass.getDeclaredFields.foreach(field => {
      field.setAccessible(true)
      additionalParams += (field.getName -> field.get(resp).toString)
    })
    future {
      NotificationResp("S1000", Some("Success"), Some(additionalParams))
    }
  }

  override def findDispatchedMessages(sessionId: String, firebaseToken: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[PushNotificationsResp] = {
    import sc._
    val req = SessionValidationReq(sessionId)
    authService.validate(req).flatMap {
      case SessionValidationResp(_, _, _, Some(msisdn), Some(name)) => {
        eventLog(correlationId, EventSource.NOTIFICATION_SERVER.FIND_MESSAGES, sessionId + " " + msisdn + " " + firebaseToken)
        val resp = Await.result(notificationConnector.findDispatchedMessages(msisdn, firebaseToken, start, limit), Duration(5000, TimeUnit.MILLISECONDS))
        eventLog(correlationId, EventSource.NOTIFICATION_SERVER.FIND_MESSAGES, resp)
        future {
          resp
        }
      }
    }
  }
}
