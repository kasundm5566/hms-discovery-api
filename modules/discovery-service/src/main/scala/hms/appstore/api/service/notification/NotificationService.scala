package hms.appstore.api.service.notification

import hms.appstore.api.service.ServiceContext
import scala.concurrent.Future
import hms.appstore.api.json._
import hms.appstore.api.json.DeviceRegistrationReq
import hms.appstore.api.json.NotificationResp
import hms.appstore.api.json.NotifyReq
import hms.appstore.api.service.ServiceContext

/**
 * Created by kasun on 6/19/18.
 */
trait NotificationService {

  def registerDevice(sessionId: String, req: DeviceRegistrationReq)(implicit sc: ServiceContext): Future[NotificationResp]

  def notifyAll(req: NotifyReq)(implicit sc: ServiceContext): Future[NotificationResp]

  def notifyAppSpecific(req: NotifyReq)(implicit sc: ServiceContext): Future[NotificationResp]

  def notifyAppUpdate(req: AppUpdateNotificationReq)(implicit sc: ServiceContext): Future[NotificationResp]

  def findDispatchedMessages(sessionId: String, firebaseToken: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[PushNotificationsResp]
}
