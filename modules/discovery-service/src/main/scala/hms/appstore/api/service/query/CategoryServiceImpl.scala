/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}

import hms.appstore.api.json._
import hms.appstore.api.repo.{SalatMapper, CategoryRepo}
import hms.appstore.api.service.{EventSource, EventLogging, ServiceContext}
import hms.appstore.api.service.Futures._

import scala.concurrent.Future

class CategoryServiceImpl(implicit val bindingModule: BindingModule) extends CategoryService
  with Injectable
  with SalatMapper
  with EventLogging {

  private val categoryRepo = inject[CategoryRepo]
  protected val eventSource = EventSource.CORE

  /**
   * findCategories
   *
   * list application categories with paging.
   * @param start (Int) number of elements to skip
   * @param limit (Int) number of elements to return
   * @return QueryResult[Category]
   */
  def findCategories(start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext) : Future[QueryResults[Category]] = {
    import sc._

    future{
      eventLog(correlationId, eventSource.FIND_CATEGORIES, s"start = $start, limit = $limit")

      val categories = categoryRepo.findCategories(start = start, limit = limit).map(asObject[Category](_))
      val results = QueryResults(categories)

      eventLog(correlationId, eventSource.FIND_CATEGORIES, "Success")
      results
    }
  }

  /**
   * findSuperCategories
   *
   * list super set of application categories with paging.
   * @param start (Int) number of elements to skip
   * @param limit (Int) number of elements to return
   * @return QueryResult[ParentCategory]
   */
  def findParentCategories(start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext) : Future[QueryResults[ParentCategory]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_PARENT_CATEGORIES, s"start = $start, limit = $limit")

      val parentCategories = categoryRepo.findParentCategories(start = start, limit = limit).map(asObject[ParentCategory](_))
      val results = QueryResults(parentCategories)

      eventLog(correlationId, eventSource.FIND_PARENT_CATEGORIES, "Success")
      results
    }
  }
}
