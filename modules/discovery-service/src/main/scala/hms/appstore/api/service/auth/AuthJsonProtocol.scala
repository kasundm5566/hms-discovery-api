package hms.appstore.api.service.auth

import hms.scala.http.json.JsObjectReader
import hms.appstore.api.json._
import spray.json._
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime

/**
 * Common Registration json spec is very inconsistent (crap !!!), so we hide it from our api users
 */
trait AuthJsonProtocol extends DefaultJsonProtocol with JsObjectReader {

  private val dateTimeFormat = DateTimeFormat.forPattern("yyyyMMdd HH:mm:ss")


  private def readStatusCode(value: JsValue): String = {
    fromJsValue[String](value, "status-code") match {
      case "Success" | "SUCCESS" => "S1000"
      case _@any => any
    }
  }

  private def readDescription(value: JsValue): Option[String] = {
    Some(fromJsValue[Option[List[String]]](value, "common-errors").getOrElse(List("Success")).mkString(","))
  }

  private def readAdditionalParams(value: JsValue): Option[Map[String, String]] = {
    fromJsValue[Option[Map[String, String]]](value, "additional-parameters")
  }


  implicit val sessionReqFmt = jsonFormat1(SessionValidationReq)
  implicit val sessionRespFmt = jsonFormat5(SessionValidationResp.apply)

  implicit object AuthenticateByMPinReqFormat extends RootJsonFormat[AuthenticateByMPinReq] {
    def write(obj: AuthenticateByMPinReq) = JsObject(
      "msisdn" -> JsString(obj.msisdn),
      "m-pin" -> JsString(obj.mpin)
    )

    def read(value: JsValue) = ???
  }

  implicit object AuthenticateByPasswordReqFormat extends RootJsonFormat[AuthenticateByPasswordReq] {
    def write(obj: AuthenticateByPasswordReq) = JsObject(
      "user-name" -> JsString(obj.username),
      "password" -> JsString(obj.password)
    )

    def read(value: JsValue) = ???
  }


  implicit val authenticateResp = jsonFormat5(AuthenticateResp)


  implicit object UserDetailsByMsisdnReqFormat extends RootJsonFormat[UserDetailsByMsisdnReq] {

    def write(obj: UserDetailsByMsisdnReq) = JsObject(
      "msisdn" -> JsString(obj.msisdn),
      "module-name" -> JsString("appstore"),
      "request-type" -> JsString("USER_BASIC_DETAILS"),
      "requested-timestamp" -> JsString(dateTimeFormat.print(DateTime.now()))
    )

    def read(value: JsValue) = ???
  }

  implicit object UserDetailsByUsernameReqFormat extends RootJsonFormat[UserDetailsByUsernameReq] {

    def write(obj: UserDetailsByUsernameReq) = JsObject(
      "username" -> JsString(obj.username),
      "module-name" -> JsString("appstore"),
      "request-type" -> JsString("USER_BASIC_DETAILS"),
      "requested-timestamp" -> JsString(dateTimeFormat.print(DateTime.now()))
    )

    def read(value: JsValue) = ???
  }


  implicit object UserDetailsRespFormat extends RootJsonFormat[UserDetailsResp] {
    def write(obj: UserDetailsResp) = ???

    def read(value: JsValue) = {
      UserDetailsResp(
        statusCode = readStatusCode(value),
        statusDescription = readDescription(value),
        userStatus = fromJsValue[Option[String]](value, "status"),
        userType = fromJsValue[Option[String]](value, "user-type"),
        userRoles = fromJsValue[Option[Set[String]]](value, "user-roles"),
        additionalParams = readAdditionalParams(value)
      )
    }

  }

  implicit object UserDetailByUserNameRespFormat extends RootJsonFormat[UserDetailByUserNameResp] {
    def write(obj: UserDetailByUserNameResp): JsValue = ???

    def read(json: JsValue): UserDetailByUserNameResp = UserDetailByUserNameResp(
      displayName = fromJsValue[Option[String]](json, "display-name"),
      statusCode = fromJsValue[String](json, "status-code"),
      firstName = fromJsValue[Option[String]](json, "first-name"),
      birthDate = fromJsValue[Option[String]](json, "birth-date"),
      lastName = fromJsValue[Option[String]](json, "last-name"),
      username = fromJsValue[Option[String]](json, "username"),
      msisdn = fromJsValue[String](json, "msisdn"),
      statusDescription = None
    )
  }

  implicit object UserProfileDetailsRespFormat extends RootJsonFormat[UserProfileDetailsResp] {
    def write(obj: UserProfileDetailsResp): JsValue = ???

    def read(json: JsValue): UserProfileDetailsResp = UserProfileDetailsResp(
      displayName = fromJsValue[Option[String]](json, "display-name"),
      statusCode = fromJsValue[String](json, "status-code"),
      firstName = fromJsValue[Option[String]](json, "first-name"),
      birthDate = fromJsValue[Option[String]](json, "birth-date"),
      email = fromJsValue[Option[String]](json, "email"),
      userType = fromJsValue[String](json,"userType"),
      image = fromJsValue[String](json, "image"),
      lastName = fromJsValue[Option[String]](json, "last-name"),
      username = fromJsValue[Option[String]](json, "username"),
      msisdn = fromJsValue[String](json, "msisdn"),
      statusDescription = None
    )
  }

  implicit object DeveloperProfileDetailsRespFormat extends RootJsonFormat[DeveloperProfileDetailsResp] {
    def write(obj: DeveloperProfileDetailsResp): JsValue = ???

    def read(json: JsValue): DeveloperProfileDetailsResp = DeveloperProfileDetailsResp(
      statusCode = fromJsValue[String](json, "status-code"),
      firstName = fromJsValue[String](json, "first-name"),
      email = fromJsValue[String](json, "email"),
      image = fromJsValue[String](json, "image"),
      lastName = fromJsValue[String](json, "last-name"),
      username = fromJsValue[String](json, "username"),
      statusDescription = None
    )
  }

  implicit object MPinChangeReqFormat extends RootJsonFormat[MPinChangeReq] {
    def write(obj: MPinChangeReq) = JsObject(
      "msisdn" -> JsString(obj.msisdn),
      "new-mpin" -> JsString(obj.newPin),
      "old-mpin" -> obj.oldPin.toJson
    )

    def read(value: JsValue) = ???
  }

  implicit object MPinChangeRespFormat extends RootJsonFormat[MPinChangeResp] {
    def write(obj: MPinChangeResp) = ???

    def read(value: JsValue) = {
      MPinChangeResp(
        statusCode = readStatusCode(value),
        statusDescription = readDescription(value)
      )
    }
  }

  implicit object CreateUserReqFormat extends RootJsonFormat[UserRegistrationReq] {

    def write(obj: UserRegistrationReq) = JsObject(
      "domain" -> JsString(obj.domain),
      "msisdn" -> JsString(obj.msisdn),
      "operator" -> JsString(obj.operator),
      "username" -> JsString(obj.userName),
      "password" -> JsString(obj.password),
      "last-name" -> JsString(obj.lastName.getOrElse("")),
      "first-name" -> JsString(obj.firstName.getOrElse("")),
      "birth-date" -> JsString(obj.birthDate),
      "verify-msisdn" -> JsBoolean(obj.verifyMsisdn),
      "email" -> JsString(obj.email.getOrElse("")),
      "mpin" -> JsString(obj.mpin.getOrElse(""))
    )

    def read(json: JsValue): UserRegistrationReq = ???
  }

  implicit object CreateConsumerUserResFormat extends RootJsonFormat[UserRegistrationResp] {
    def write(obj: UserRegistrationResp): JsValue = ???

    def read(json: JsValue): UserRegistrationResp = {
      UserRegistrationResp(
        statusCode = readStatusCode(json),
        statusDescription = readDescription(json)
      )
    }
  }

  implicit object MsisdnVerificationReqFormat extends RootJsonFormat[MsisdnVerificationReq] {
    def write(obj: MsisdnVerificationReq): JsObject = JsObject(
      "username" -> JsString(obj.username),
      "msisdn-verification-code" -> JsString(obj.verificationCode)
    )

    def read(json: JsValue): MsisdnVerificationReq = ???
  }

  implicit object MsisdnVerificationRespFormat extends RootJsonFormat[MsisdnVerificationResp] {
    def write(obj: MsisdnVerificationResp): JsValue = ???

    def read(json: JsValue): MsisdnVerificationResp = MsisdnVerificationResp(
      statusCode = readStatusCode(json),
      statusDescription = readDescription(json)
    )
  }

  implicit object MsisdnVerificationCodeReqFormat extends RootJsonFormat[MsisdnVerificationCodeReq] {
    def write(obj: MsisdnVerificationCodeReq): JsObject = JsObject(
      "username" -> JsString(obj.username),
      "msisdn" -> JsString(obj.msisdn)
    )

    def read(json: JsValue): MsisdnVerificationCodeReq = ???
  }

  implicit object recoverUserAccountRespFormat extends RootJsonFormat[UserAccountRecoveryResp] {
    def write(obj: UserAccountRecoveryResp): JsValue = ???

    def read(json: JsValue): UserAccountRecoveryResp = UserAccountRecoveryResp(
      statusCode = readStatusCode(json),
      statusDescription = readDescription(json)
    )
  }

  implicit object recoverUserAccountReqFormat extends RootJsonFormat[UserAccountRecoveryReq] {
    def write(obj: UserAccountRecoveryReq): JsObject = JsObject(
      "recover-text" -> JsString(obj.recoverText)
    )

    def read(json: JsValue): UserAccountRecoveryReq = ???
  }

  implicit object UserProfileUpdateReqFormat extends RootJsonFormat[UserProfileUpdateReq] {
    def write(obj: UserProfileUpdateReq): JsObject = JsObject(
      "firstName" -> JsString(obj.firstName),
      "lastName" -> JsString(obj.lastName),
      "username" -> JsString(obj.username),
      "email" -> JsString(obj.email),
      "image" -> JsString(obj.image),
      "fileName" -> JsString(obj.fileName)
    )

    def read(json: JsValue): UserProfileUpdateReq = ???
  }

  implicit object UserProfileUpdateRespFormat extends RootJsonFormat[UserProfileUpdateResp] {
    def write(obj: UserProfileUpdateResp) = ???

    def read(value: JsValue) = {
      UserProfileUpdateResp(
        statusCode = readStatusCode(value),
        statusDescription = readDescription(value)
      )
    }
  }

  implicit object CreateAutoLoginUserReqFormat extends RootJsonFormat[AutoLoginUserRegistrationReq] {

    def write(obj: AutoLoginUserRegistrationReq) = JsObject(
      "msisdn" -> JsString(obj.msisdn)
    )

    def read(json: JsValue): AutoLoginUserRegistrationReq = ???
  }

  implicit object CreateAutoLoginUserResFormat extends RootJsonFormat[AutoLoginUserRegistrationResp] {
    def write(obj: AutoLoginUserRegistrationResp): JsValue = ???

    def read(json: JsValue): AutoLoginUserRegistrationResp = {
      AutoLoginUserRegistrationResp(
        statusCode = readStatusCode(json),
        statusDescription = readDescription(json)
      )
    }
  }

  implicit object UserPasswordResetReqFormat extends RootJsonFormat[UserPasswordResetReq] {

    def write(obj: UserPasswordResetReq) = JsObject(
      "msisdn" -> JsString(obj.msisdn)
    )

    def read(json: JsValue): UserPasswordResetReq = ???
  }

  implicit object UserPasswordResetRespFormat extends RootJsonFormat[UserPasswordResetResp] {
    def write(obj: UserPasswordResetResp): JsValue = ???

    def read(json: JsValue): UserPasswordResetResp = {
      UserPasswordResetResp(
        statusCode = readStatusCode(json),
        statusDescription = readDescription(json)
      )
    }
  }
}

object AuthJsonProtocol extends AuthJsonProtocol
