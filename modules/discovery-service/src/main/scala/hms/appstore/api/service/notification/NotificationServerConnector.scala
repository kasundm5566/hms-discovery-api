package hms.appstore.api.service.notification

import hms.appstore.api.service.ServiceContext
import scala.concurrent.Future
import hms.appstore.api.json._
import hms.appstore.api.json.DeviceRegistrationReq
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.DeviceRegistrationServerResp

/**
 * Created by kasun on 6/19/18.
 */
trait NotificationServerConnector {

  def registerDevice(req: DeviceRegistrationReq)(implicit sc: ServiceContext): Future[DeviceRegistrationServerResp]

  def findDispatchedMessages(msisdn: String, firebaseToken: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[PushNotificationsResp]
}
