/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import hms.appstore.api.json.{SearchQuery, AppStatus, QueryResults}
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.service.ServiceContext

import scala.concurrent.Future

import AppStatus._

trait AppSearchService {

  def search(searchQuery: SearchQuery)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def searchExcludingCategory(searchText: String, excludingCategory: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def searchByName(searchQuery: SearchQuery)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def searchRelated(appId: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]
}
