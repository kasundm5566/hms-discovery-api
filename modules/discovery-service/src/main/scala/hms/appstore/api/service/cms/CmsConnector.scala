/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.cms

import scala.concurrent.Future
import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext

/**
 * SDP NBL Content Management Service connector
 */
trait CmsConnector {

  def findPlatforms(query: PlatformQuery)(implicit sc: ServiceContext): Future[PlatformResults]

  def downloadApp(req: DownloadReq)(implicit sc: ServiceContext): Future[DownloadResp]

  def downloadAppWithCharge(req: DownloadChargeReq)(implicit sc: ServiceContext): Future[DownloadChargeResp]

  def appDownloadsCount(req: AppDownloadsCountReq)(implicit sc: ServiceContext): Future[AppDownloadsCountResp]
}