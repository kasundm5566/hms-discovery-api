package hms.appstore.api.service

import hms.appstore.api.json.StatusCode
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}

trait StatusCodeMapper {

  def mapToApiStatusCode(source: EventSource, statusCode: StatusCode): StatusCode
}

class StatusCodeMapperImpl(implicit val bindingModule: BindingModule) extends StatusCodeMapper
  with Injectable
  with EventLogging{

  val sysConfigService = inject[SysConfigService]

  def mapToApiStatusCode(source: EventSource, statusCode: StatusCode)= {

    val status = s"${source.name}.${source.action}.${statusCode.status}"
    val key = sysConfigService.statusCodeConfig.hasPath(status)

    if(key.equals(true)){
      val mappedStatusCode = sysConfigService.statusCodeConfig.getString(s"$status.status-code")
      val mappedStatusDescription = sysConfigService.statusCodeConfig.getString(s"$status.description")

      ServiceStatusCode(status = mappedStatusCode, description = mappedStatusDescription)
    }
    else ServiceStatusCode(status = statusCode.status, description = statusCode.description)

  }
}
