/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.governance

import hms.appstore.api.json.AbuseReportResp
import concurrent.Future
import hms.appstore.api.service.ServiceContext

trait ReportAbuseService {

  def reportAbuse(sessionId: String, appId: String, comment: String, channel: Option[String])(implicit sc: ServiceContext): Future[AbuseReportResp]

}
