package hms.appstore.api.service

import scala.concurrent.Future
import hms.appstore.api.json.{Banner, QueryResults, AppBannersResp}

/**
 * Created by kasun on 3/20/18.
 */
trait BannerService {
  def find(bannerLocation: String)(implicit sc: ServiceContext):  Future[QueryResults[Banner]]
}
