package hms.appstore.api.service.auth

import hms.appstore.api.json._
import scala.concurrent.Future
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.MsisdnVerificationReq
import hms.appstore.api.json.UserRegistrationResp
import hms.appstore.api.json.UserRegistrationReq
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.MsisdnVerificationResp

/**
 * create a new consumer user
 */
trait UserRegService {
  def createUser(createReq:UserRegistrationReq)(implicit sc: ServiceContext):Future[UserRegistrationResp]

  def verifyMsisdn(verificationReq:MsisdnVerificationReq)(implicit sc:ServiceContext):Future[MsisdnVerificationResp]

  def requestNewVerificationCode(codeReq:MsisdnVerificationCodeReq)(implicit sc:ServiceContext):Future[MsisdnVerificationResp]

  def recoveryUserAccount(recoverReq:UserAccountRecoveryReq)(implicit sc:ServiceContext):Future[UserAccountRecoveryResp]
}
