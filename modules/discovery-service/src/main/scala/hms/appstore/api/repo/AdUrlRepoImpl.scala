package hms.appstore.api.repo

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.casbah.query.Imports._


class AdUrlRepoImpl(implicit val bindingModule: BindingModule) extends AdUrlRepo with Injectable {
  val db = inject[Db]

  def create(dbo: DBObject) {
    db.addUrls.insert(dbo)
  }

  def createOrUpdate(id: String, dbo: DBObject) {
    db.addUrls.update(
      MongoDBObject("_id" -> id),
      dbo,
      upsert = true,
      multi = false
    )
  }

  def remove(id: String) {
    db.addUrls.remove(MongoDBObject("_id" -> id))
  }

  def findAll(): List[DBObject] = {
    db.addUrls.find().toList
  }
}
