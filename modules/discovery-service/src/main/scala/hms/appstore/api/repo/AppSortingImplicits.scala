package hms.appstore.api.repo

import com.mongodb.casbah.commons.MongoDBObject

object AppSortingImplicits {
   implicit def sortingOrder2Query(order: AppSorting) = order match {
     case DescDateAdded => MongoDBObject("date_added" -> -1)
     case DescUsage => MongoDBObject("usage" -> -1)
     case DescRating => MongoDBObject("rating" -> -1)
     case DescRequestedDate => MongoDBObject("date_added" -> -1)
     case DescModifiedDated => MongoDBObject("modified_date" -> -1)
     case _ => MongoDBObject()
   }
}
