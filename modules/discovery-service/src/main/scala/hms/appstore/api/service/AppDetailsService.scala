/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import scala.concurrent.Future
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.json.QueryResult

trait AppDetailsService {
  def appDetails(appId: String, sessionId: Option[String])(implicit exec: ServiceContext): Future[QueryResult[AppStoreApp]]
}
