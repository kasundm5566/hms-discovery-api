package hms.appstore.api.service

import hms.appstore.api.json.ClientVersion
import scala.concurrent.Future


trait ClientPlatformService {

  def findLatestVersion(platform: String, osVersion: String)(implicit sc:ServiceContext): Future[ClientVersion]

}
