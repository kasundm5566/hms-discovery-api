/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.cms

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.typesafe.scalalogging.slf4j.Logging

import hms.appstore.api.json._
import hms.appstore.api.service.auth.AuthService
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.repo.{SalatMapper, AppRepo, DownloadReqRepo}

import hms.appstore.api.service.Futures._

import scala.concurrent.Future
import util.Success
import util.Failure
import hms.appstore.api.domain.{AppStoreApp, AppDnStatus}
import scala.util.Try

class DownloadServiceImpl(implicit val bindingModule: BindingModule) extends DownloadService
 with SalatMapper
 with Injectable
 with Logging {

  private val appRepo = inject[AppRepo]
  private val reqRepo = inject[DownloadReqRepo]
  private val authService = inject[AuthService]
  private val cmsConnector = inject[CmsConnector]


  def myDownloads(sessionId: String, start: Int, limit: Int)(implicit sc: ServiceContext):Future[QueryResults[Application]] = {
    import sc._

    authService.validate(SessionValidationReq(sessionId)).collect {
      case SessionValidationResp(_, _, _, Some(mobileNo), _) => mobileNo
    }.map {
      mobileNo => {
        val infs = reqRepo.findByMsisdn(mobileNo, start, limit).map(dbObj => asObject[AppDnStatus](dbObj))
        val apps = appRepo.findByIds(infs.map(_.applicationId)).map(asAppObject(_))

        //this is inefficient, refine this later
        val results = apps.map {
          app => app.copy(downloadStatus = infs.collect{case dn if dn.applicationId == app.id => dn}.lastOption)
        }

        QueryResults(results)
      }
    }
  }

  def myDownloadsWithBinaries(sessionId: String, start: Int, limit: Int)(implicit sc: ServiceContext):Future[QueryResults[Application]] = {
    import sc._

    def fillDownloadableBinaryDetail(apps: List[AppStoreApp]): Future[List[AppStoreApp]] = {
      val futureTryList = apps.map(app => {
        cmsConnector.findPlatforms(PlatformQuery(List(app.id))).map {
          resp => {
            logger.debug("adding downloadable binaries detail for app[{}]", app.id)
            app.copy(downloadableBinaries = resp.platformsByAppId(app.id))
          }
        }
      }).map(futureToFutureTry(_))

      Future.sequence(futureTryList).map(_.collect{ case Success(x) => x})
    }

    def fillDownloadReqDetails(appDnStatusList: List[AppDnStatus], appId: String): Option[DownloadStatus] = {
      appDnStatusList.collect{case dn if dn.applicationId == appId => dn}.lastOption
    }

    def appListFuture(mobileNo: String): Future[List[AppStoreApp]] = {
      val infs = reqRepo.findByMsisdn(mobileNo, start, limit).map(dbObj => asObject[AppDnStatus](dbObj))
      future {
        appRepo.findByIds(infs.map(_.applicationId)).map(asAppObject(_)).map(app => {
          app.copy(downloadStatus = fillDownloadReqDetails(infs, app.id))
        })
      }
    }

    val sessionResultFuture = authService.validate(SessionValidationReq(sessionId))

    for {
      mobileNo <- sessionResultFuture.collect { case SessionValidationResp(_, _, _, Some(mobileNo), _) => mobileNo}
      appList <- appListFuture(mobileNo)
      results <- fillDownloadableBinaryDetail(appList)
    } yield QueryResults(results)
  }

  def download(sessionId: String, appId: String, contentId: String, channel: Option[String])(implicit sc: ServiceContext): Future[DownloadResp] = {
    import sc._
    authService.validate(SessionValidationReq(sessionId)).collect {
      case SessionValidationResp(_, _, _, Some(mobileNo), _) => mobileNo
    }.flatMap(m => cmsConnector.downloadApp(DownloadReq(appId, contentId, m, channel)))
  }

  def downloadWithCharge(sessionId: String,
                         downloadRequestId: String,
                         piName: String,
                         pin: Option[String])(implicit sc: ServiceContext): Future[DownloadChargeResp] = {
    import sc._
    val sessionResult = authService.validate(SessionValidationReq(sessionId))
    val additionalParam = if(pin.isDefined) Some(Map("pin"->pin.getOrElse(""))) else None
    val downloadResult = cmsConnector.downloadAppWithCharge(DownloadChargeReq(downloadRequestId, piName, additionalParam))
      for {
        session <- sessionResult
        downloadApp <- downloadResult
      } yield downloadApp
    }

  def updateCount(sessionId: String)(implicit sc: ServiceContext): Future[QueryResult[UpdateCountResp]] = {
    import sc._

    def dnReqListFuture(mobileNo: String): Future[List[AppDnStatus]] = {
      val downloadReqList = reqRepo.findByMsisdn(mobileNo).map(dbObj => asObject[AppDnStatus](dbObj))

      future {
        downloadReqList.map(req => req.applicationId).distinct.map(
          appId => downloadReqList.filter(_.applicationId == appId).last
        )
      }
    }

    def containsUpdate(lastContentId: String, binaries: Application#DnBinaries): Boolean = {
      val osDetailsList = binaries.flatMap(_._2).toList.flatMap(mapItem => mapItem.flatMap(_._2).toList)

      osDetailsList.map(m => {
        if(m.contains("content-id")) { m.get("content-id").get.toLong > lastContentId.toLong } else false
      }).contains(true)
    }

    def appsWithUpdateStatus(reqList: List[AppDnStatus]): Future[List[(String, Boolean)]] = {
      val futureTryList = reqList.map(app => {
        cmsConnector.findPlatforms(PlatformQuery(List(app.applicationId))).map {
          resp => {
            logger.debug("adding downloadable binaries detail for app[{}]", app.applicationId)
            (app.applicationId, containsUpdate(app.contentId, resp.platformsByAppId(app.applicationId)))
          }
        }
      }).map(futureToFutureTry(_))

      Future.sequence(futureTryList).map(_.collect{ case Success(x) => x})
    }

    val sessionResultFuture = authService.validate(SessionValidationReq(sessionId))

    for {
      mobileNo <- sessionResultFuture.collect { case SessionValidationResp(_, _, _, Some(mobileNo), _) => mobileNo}
      reqList <- dnReqListFuture(mobileNo)
      updateStatus <- appsWithUpdateStatus(reqList)
    } yield QueryResult(UpdateCountResp(updateStatus.filter(_._2).size))

  }

}
