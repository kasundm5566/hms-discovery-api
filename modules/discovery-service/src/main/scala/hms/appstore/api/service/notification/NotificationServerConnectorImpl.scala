package hms.appstore.api.service.notification

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.service._
import hms.appstore.api.service.auth.AuthJsonProtocol
import hms.scala.http.marshalling.HttpMarshalling
import hms.scala.http.{HttpConnectorConfig, DefaultHttpClient}
import hms.appstore.api.json._
import scala.concurrent.Future
import hms.scala.http.HttpConnectorConfig
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.DeviceRegistrationReq
import hms.scala.http.HttpConnectorConfig
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.DeviceRegistrationServerResp

/**
 * Created by kasun on 6/19/18.
 */
class NotificationServerConnectorImpl(implicit val bindingModule: BindingModule) extends NotificationServerConnector
with ServiceFutureFilter
with Injectable {

  import MsgJsonProtocol._
  import HttpMarshalling._

  private val sysConfig = inject[SysConfigService]
  protected val ec = EventSource.NOTIFICATION_SERVER
  private val connector = new DefaultHttpClient(
    HttpConnectorConfig("notificationServerConnector", "http", inject[String]("notification.server.host"), inject[Int]("notification.server.port"))
  )

  override def registerDevice(req: DeviceRegistrationReq)(implicit sc: ServiceContext): Future[DeviceRegistrationServerResp] = {
    val path = "/appnotification/v1/device/register"
    val result = connector.doPost[DeviceRegistrationReq, DeviceRegistrationServerResp](path, req)
    result
  }

  override def findDispatchedMessages(msisdn: String, firebaseToken: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[PushNotificationsResp] = {
    val path = "/appnotification/v1/messages/" + msisdn + "/" + firebaseToken + "/start/" + start + "/limit/" + limit
    val result = connector.doGet[PushNotificationsResp](path)
    result
  }
}
