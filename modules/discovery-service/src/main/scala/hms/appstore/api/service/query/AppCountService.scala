package hms.appstore.api.service.query

import concurrent.Future
import hms.appstore.api.json.{SearchQuery, AppStatus, QueryResult, AppCount}
import hms.appstore.api.service.ServiceContext

trait AppCountService {

  def countBy(query: SearchQuery)(implicit sc: ServiceContext): Future[QueryResult[AppCount]]

  def countByStatus(status: AppStatus.AppStatus)(implicit sc: ServiceContext): Future[QueryResult[AppCount]]

  def countAllApps()(implicit sc: ServiceContext): Future[QueryResult[AppCount]]

  def countByCategory(category : String)(implicit sc: ServiceContext): Future[QueryResult[AppCount]]

  def countFreeApps(category : Option[String] = None)(implicit sc: ServiceContext): Future[QueryResult[AppCount]]

  def countDownloadableApps(category : Option[String] = None)(implicit sc: ServiceContext): Future[QueryResult[AppCount]]

  def countDownloadableFreeApps(category : Option[String] = None)(implicit sc: ServiceContext): Future[QueryResult[AppCount]]
}
