package hms.appstore.api.service.caas

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.typesafe.scalalogging.slf4j.Logging
import scala.concurrent.Future

import hms.appstore.api.repo.SalatMapper
import hms.appstore.api.json._
import hms.appstore.api.service.{SysConfigService, ServiceContext}
import hms.appstore.api.service.auth.AuthService



class PaymentInstrumentServiceImpl(implicit val bindingModule: BindingModule) extends PaymentInstrumentService
  with SalatMapper
  with Injectable
  with Logging {

  private val authService = inject[AuthService]
  private val sysConfig = inject[SysConfigService]
  private val caasConnector = inject[CaasConnector]

  def findPaymentInstruments(sessionId: String, applicationId: String)(implicit sc: ServiceContext): Future[PaymentInstrumentsResp] = {
    import sc._

    val resultFuture = authService.validate(SessionValidationReq(sessionId)).collect {
      case SessionValidationResp(_, _, _, Some(mobileNo), _) => mobileNo
    }.flatMap(mobileNo => caasConnector.findPaymentInstruments(PaymentInstrumentsReq(mobileNo, applicationId, sysConfig.paymentInstrumentType)))
    //TODO hack to hide async payment instruments this need to be fixed from NBL-caas api side
    resultFuture.map(res =>
      PaymentInstrumentsResp(
        statusCode = res.statusCode
        , statusDescription = res.statusDescription
        , paymentInstrumentList = Some(res.paymentInstrumentList.fold(List.empty[Map[String, String]])(
          items => items.filterNot(pi => pi.get("type") == Some("ASYNC"))))
      ))
  }
}
