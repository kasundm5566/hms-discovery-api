package hms.appstore.api.service

import hms.scala.util.ReqIdGenerator
import concurrent.ExecutionContext

case class CorrelationId(value: String) {
  override def toString = value
}

object CorrelationId {
  def apply(): CorrelationId = CorrelationId(ReqIdGenerator.newId)
}

case class ServiceContext(sourceExec: ExecutionContext,
                          correlationId: CorrelationId = CorrelationId()) {
  implicit val executionContext = sourceExec
}
