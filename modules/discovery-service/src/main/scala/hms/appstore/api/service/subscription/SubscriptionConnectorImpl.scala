/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.subscription

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.json._
import hms.appstore.api.service._
import hms.scala.http.{HttpConnectorConfig, DefaultHttpClient}
import hms.scala.http.marshalling.HttpMarshalling

class SubscriptionConnectorImpl(implicit val bindingModule: BindingModule) extends SubscriptionConnector
  with ServiceFutureFilter
  with Injectable {

  import MsgJsonProtocol._
  import HttpMarshalling._

  private val sysConfig = inject[SysConfigService]
  protected val eventSource = EventSource.SUBSCRIPTION

  private val connector = new DefaultHttpClient(
    HttpConnectorConfig("subscriptionConnector", "http", inject[String]("subscription.host"), inject[Int]("subscription.port"))
  )

  def subscribedApps(msisdn: String)(implicit sc: ServiceContext) = {
    val path = "/subscription/applist"
    val result = connector.doPost[Map[String, String], SubscribedApps](path, Map("senderAddress" -> msisdn))
    filterWithServiceSuccess(eventSource.APP_LIST, path, result)
  }

  def subscribe(msisdn: String, appId: String, password: String)(implicit sc: ServiceContext) = {
    val path = "/subscription/reg"
    val res = connector.doPost[SubscriptionAction, SubscriptionActionResp](path, SubscriptionAction(s"tel:$msisdn", appId, password, 1))
    filterWithHttpSuccess(eventSource.REG, path, res)
  }

  def subscriptionStatus(msisdn: String, appId: String)(implicit sc: ServiceContext) = {
    val path = "/subscription/subscriptionstatus"
    val subscriberId = s"tel:$msisdn"
    val result = connector.doPost[Map[String, String], SubscriptionStatusResp](path, Map("subscriberId" -> subscriberId, "applicationId" -> appId))
    filterWithServiceSuccess(eventSource.SUBSCRIPTION_STATUS, path, result)
  }

  def unSubscribe(msisdn: String, appId: String, password: String)(implicit sc: ServiceContext) = {
    val path = "/subscription/reg"
    val result = connector.doPost[SubscriptionAction, SubscriptionActionResp](path, SubscriptionAction(s"tel:$msisdn", appId, password, 0))
    filterWithHttpSuccess(eventSource.UN_REG, path, result)
  }

  def subscriptionCount(appId: String)(implicit sc: ServiceContext) = {
    val path = "/subscription/basesize"
    val result = connector.doPost[Map[String, String], SubscriptionCountResp](path, Map("applicationId" -> appId) )
    filterWithServiceSuccess(eventSource.SUBSCRIPTION_COUNT, path, result)
  }

}