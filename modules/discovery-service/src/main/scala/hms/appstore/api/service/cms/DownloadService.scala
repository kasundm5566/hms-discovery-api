package hms.appstore.api.service.cms

import concurrent.Future
import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.DownloadResp
import hms.appstore.api.json.DownloadChargeResp
import hms.appstore.api.service.ServiceContext


trait DownloadService {
  def myDownloads(sessionId: String, start: Int, limit: Int)(implicit sc: ServiceContext):Future[QueryResults[Application]]

  def myDownloadsWithBinaries(sessionId: String, start: Int, limit: Int)(implicit sc: ServiceContext):Future[QueryResults[Application]]

  def download(sessionId: String, appId: String, contentId: String, channel: Option[String])(implicit sc: ServiceContext):Future[DownloadResp]

  def downloadWithCharge(sessionId: String, downloadRequestId: String, piName: String, pin: Option[String])(implicit sc: ServiceContext): Future[DownloadChargeResp]

  def updateCount(sessionId: String)(implicit sc: ServiceContext): Future[QueryResult[UpdateCountResp]]
}
