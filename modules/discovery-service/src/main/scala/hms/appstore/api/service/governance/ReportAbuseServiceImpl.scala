/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.governance

import com.typesafe.scalalogging.slf4j.Logging
import com.escalatesoft.subcut.inject.{Injectable, BindingModule}

import hms.appstore.api.json._
import hms.appstore.api.service.auth.AuthService
import hms.appstore.api.service.ServiceContext

import scala.concurrent.Future

class ReportAbuseServiceImpl(implicit val bindingModule: BindingModule) extends ReportAbuseService
  with Injectable
  with Logging {

  private val authService = inject[AuthService]
  private val governanceConnector = inject[GovernanceConnector]

  def reportAbuse(sessionId: String, appId: String, comment: String, channel: Option[String])(implicit sc: ServiceContext): Future[AbuseReportResp] = {
    import sc._
    authService.validate(SessionValidationReq(sessionId)).collect{
      case SessionValidationResp(_, _, _, Some(mobileNo), _) => mobileNo
    }.flatMap(mobileNo => governanceConnector.report(AbuseReport(appId, mobileNo, comment, channel.getOrElse("web"))))
  }
}
