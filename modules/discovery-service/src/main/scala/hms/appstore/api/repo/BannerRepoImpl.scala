package hms.appstore.api.repo

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.casbah.query.Imports
import com.mongodb.DBObject
import com.mongodb.casbah.query.Imports._
import hms.appstore.api.json.{BannerLocation, BannersCount, BannerLocations, BannerAddReq}
import scala.collection.mutable.ListBuffer
import hms.appstore.api.service.{EventSource, EventLogging, ServiceContext}

/**
 * Created by kasun on 4/2/18.
 */
class BannerRepoImpl(implicit val bindingModule: BindingModule) extends BannerRepo with Injectable with EventLogging {
  private val db: Db = inject[Db]
  private val imagePath: String = "images/applications/icons/banners-"

  override def addBanner(b: DBObject) {
    val nextID = findNextIdForBanners()
    b.put("_id", nextID)
    b.put("image_url", (imagePath + b.get("display_location") + "/" + b.get("image_url").toString))
    db.banners.insert(b)
  }

  private def findNextIdForBanners(): Int = {
    val query = MongoDBObject() // All documents
    val fields = MongoDBObject("_id" -> 1) // Only return `_id`
    val orderBy = MongoDBObject("_id" -> -1) // Order by _id descending

    val ids = db.banners.find(query, fields).sort(orderBy).limit(1).toVector

    var nextId = 1
    if (ids.size > 0) {
      ids.foreach(i => nextId = i.get("_id").asInstanceOf[Int])
      nextId + 1
    } else {
      nextId
    }
  }

  override def removeBanner(id: Int) {
    val queryToCheckBannerAvailability = MongoDBObject("_id" -> id)
    if (db.banners.find(queryToCheckBannerAvailability).count > 0) {
      db.banners.remove(queryToCheckBannerAvailability)
    }
  }

  override def editBanner(id: Int, req: BannerAddReq) {
    val queryToCheckBannerAvailability = MongoDBObject("_id" -> id)
    if (db.banners.find(queryToCheckBannerAvailability).count > 0) {
      val updatedImageUrl: String = imagePath + req.display_location + "/" + req.image_url
      val update = MongoDBObject(
        "$set" -> MongoDBObject("display_location" -> req.display_location, "description" -> req.description, "image_url" -> updatedImageUrl, "link" -> req.link)
      )
      db.banners.findAndModify(queryToCheckBannerAvailability, update)
    }
  }

  override def getBanners(start: Int = 0, limit: Int = 10): List[DBObject] = {
    db.banners.find().sort(MongoDBObject("display_location" -> 1)).
      skip(start).
      limit(limit).
      toList
  }

  override def getBannerLocations(): Set[BannerLocation] = {
    var bannerLocationsSet: Set[BannerLocation] = Set.empty
    db.bannerLocations.find().foreach(u => {
      val obj = u.asInstanceOf[BasicDBObject]
      val bannerLocation = BannerLocation(obj.get("display_location").toString, obj.get("name").toString, obj.get("min_width").toString.toInt, obj.get("max_width").toString.toInt,
        obj.get("min_height").toString.toInt, obj.get("max_height").toString.toInt, obj.get("size").toString.toInt)
      bannerLocationsSet += bannerLocation
    })
    bannerLocationsSet
  }

  override def getBannersCount(): BannersCount = {
    BannersCount(db.banners.count())
  }

  override def getBannersByLocation(location: String, start: Int = 0, limit: Int = 10): List[Imports.DBObject] = {
    val queryToFilterByBannerLocation = MongoDBObject("display_location" -> location)
    db.banners.find(queryToFilterByBannerLocation).
      skip(start).
      limit(limit).
      toList
  }

  override def getBannersCountByLocation(location: String): BannersCount = {
    val queryToFilterByBannerLocation = MongoDBObject("display_location" -> location)
    BannersCount(db.banners.find(queryToFilterByBannerLocation).count)
  }
}
