/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject._
import com.mongodb.casbah.query.Imports._


import hms.appstore.api.json._
import hms.appstore.api.repo._
import hms.appstore.api.service._
import hms.appstore.api.service.cms.CmsConnector
import hms.appstore.api.service.Futures._

import scala.concurrent.Future
import hms.appstore.api.json.AppStatus._
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.service.ServiceContext
import scala.Some
import hms.appstore.api.json.PlatformQuery
import hms.appstore.api.repo.AppQueryCmd
import scala.concurrent.duration.Duration

class AppFindServiceImpl(implicit val bindingModule: BindingModule) extends AppFindService
with Injectable
with SalatMapper
with EventLogging {

  private val appRepo = inject[AppRepo]
  private val cmsConnector = inject[CmsConnector]
  private val sysConfigService = inject[SysConfigService]
  protected val eventSource = EventSource.CORE

  def findOne(appId: String, appStatus: Option[AppStatus] = Some(AppStatus.Published))(implicit sc: ServiceContext): Future[QueryResult[AppStoreApp]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_APP_BY_ID_QUERY, s"appId = $appId")
      val app = appRepo.findOne(Some(appId)).map(asAppObject(_))

      val result = app match {
        case Some(a) => QueryResult(a)
        case None => throw new ServiceException(correlationId, E5101, EventSource.CORE.ERROR_HANDLING)
      }

      eventLog(correlationId, eventSource.FIND_APP_BY_ID_QUERY, "Success")
      result
    }
  }

  def findAppOrPendingApp(appId: String)(implicit sc: ServiceContext): Future[QueryResult[AppStoreApp]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_APP_ONE_PENDING_APPS, s"appId = $appId")
      val apps = appRepo.findAppOrPendingApp(
        AppQueryCmd(
          queryArgs = Map("_id" -> appId),
          appStatus = None,
          start = 0,
          limit = 1
        )
      ).map(asAppObject(_))

      val result = apps match {
        case app :: tail => QueryResult(app)
        case Nil => throw new ServiceException(correlationId, E5101, EventSource.CORE.ERROR_HANDLING)
      }

      eventLog(correlationId, eventSource.FIND_APP_ONE_PENDING_APPS, "Success")
      result
    }
  }

  def findApps(appIds: List[String])(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_APPS_BY_IDS, s"appIds = $appIds")

      val apps = appRepo.findByIds(appIds).map(asAppObject(_))
      val results = QueryResults(apps)

      eventLog(correlationId, eventSource.FIND_APPS_BY_IDS, "Success")
      results
    }
  }

  def findAll(appStatus: Option[AppStatus] = Some(Published), appCategory: Option[String] = None, start: Int = 0, limit: Int = Int.MaxValue)
             (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_ALL_APPS, "")

      val apps = appRepo.find(
        AppQueryCmd(
          appStatus = appStatus,
          sortOrder = DescRequestedDate,
          appCategory = appCategory,
          start = start,
          limit = limit)
      ).map(asAppObject(_))

      val results = QueryResults(apps)

      eventLog(correlationId, eventSource.FIND_ALL_APPS, "Success")
      results
    }
  }

  def findPending(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_PENDING_APPS, "")

      val apps = appRepo.find(
        AppQueryCmd(
          appStatus = Some(AppStatus.New),
          sortOrder = DescRequestedDate,
          start = start,
          limit = limit)
      ).map(asAppObject(_))

      val results = QueryResults(apps)

      eventLog(correlationId, eventSource.FIND_PENDING_APPS, "Success")
      results
    }
  }

  def findByCategory(start: Int, limit: Int, category: String, platform: Option[String] = None)
                    (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    val eventMsg = s"start = $start, limit = $limit, category = $category, platform = ${platform.getOrElse("N/A")}"
    eventLog(correlationId, eventSource.FIND_APPS_BY_CATEGORY, eventMsg)

    val queryMap = Map("$or" -> List(Map("category" -> category), Map("labels" -> Map("$in" -> List(category)))))
    val results = doFind(start, limit, queryMap, DescDateAdded, platform)

    eventLog(correlationId, eventSource.FIND_APPS_BY_CATEGORY, "Asynchronusly computed!")
    results
  }

  /**
   * findMostlyUsed
   *
   * calls the find repo method if category is available in the query
   * then find by category other wise list the mostly used apps
   */
  def findMostlyUsed(start: Int, limit: Int, category: Option[String] = None, platform: Option[String] = None)
                    (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    val eventMsg = s"start = $start, limit = $limit, category = ${category.getOrElse("N/A")}, platform = ${platform.getOrElse("N/A")}"
    eventLog(correlationId, eventSource.FIND_MOSTLY_USED_APPS, eventMsg)

    val queryMap = Map("category" -> category).collect {
      case (k, Some(v)) => (k, v)
    }
    val results = doFind(start, limit, queryMap, DescUsage, platform)

    eventLog(correlationId, eventSource.FIND_MOSTLY_USED_APPS, "Asynchronusly computed!")
    results
  }


  /**
   * findNewlyAdded
   *
   * calls the find repo method if category is available in the query
   * then find by category other wise list the newly added apps
   */
  def findNewlyAdded(start: Int, limit: Int, category: Option[String] = None, platform: Option[String] = None)
                    (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    val eventMsg = s"start = $start, limit = $limit, category = ${category.getOrElse("N/A")}, platform = ${platform.getOrElse("N/A")}"
    eventLog(correlationId, eventSource.FIND_NEWLY_ADD_APPS, eventMsg)

    val queryMap = Map("category" -> category).collect {
      case (k, Some(v)) => (k, v)
    }
    val results = doFind(start, limit, queryMap, DescDateAdded, platform)

    eventLog(correlationId, eventSource.FIND_NEWLY_ADD_APPS, "Asynchronusly computed!")
    results
  }


  /**
   * findTopRated
   *
   * calls the find repo method if category is available in the query
   * then find by category other wise list the top rated apps
   */
  def findTopRated(start: Int, limit: Int, category: Option[String] = None, platform: Option[String] = None)
                  (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    val eventMsg = s"start = $start, limit = $limit, category = ${category.getOrElse("N/A")}, platform = ${platform.getOrElse("N/A")}"
    eventLog(correlationId, eventSource.FIND_TOP_RATED_APPS, eventMsg)

    val queryMap = Map("category" -> category).collect {
      case (k, Some(v)) => (k, v)
    }
    val results = doFind(start, limit, queryMap, DescRating, platform)

    eventLog(correlationId, eventSource.FIND_TOP_RATED_APPS, "Asynchronusly computed!")
    results
  }

  /**
   * findFreeApps
   *
   * find the free apps by checking the default_cost
   * if category is available then return category based free apps
   */
  def findFreeApps(start: Int, limit: Int, category: Option[String], platform: Option[String] = None)
                  (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    val eventMsg = s"start = $start, limit = $limit, category = ${category.getOrElse("N/A")}, platform = ${platform.getOrElse("N/A")}"
    eventLog(correlationId, eventSource.FIND_FREE_APPS, eventMsg)

    val queryMap = Map("category" -> category).collect {
      case (k, Some(v)) => (k, v)
    }
    val slaKeys = sysConfigService.ncsTypes.map(ncs => s"ncs-slas.$ncs.charging.type").toList

    def genQuery(key: String) = $or(MongoDBObject(key -> MongoDBObject("$exists" -> false)), MongoDBObject(key -> "free"))

    val results = doFind(
      start,
      limit,
      queryMap ++ $and(slaKeys.map(genQuery)),
      DescDateAdded,
      platform
    )

    eventLog(correlationId, eventSource.FIND_FREE_APPS, "Asynchronusly computed!")
    results
  }

  protected def doFind(start: Int, limit: Int,
                       queryMap: DBObject = DBObject.empty,
                       sortOrder: AppSorting = DescDateAdded,
                       platform: Option[String] = None)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {

    import sc._

    val appQuery: AppQueryCmd = AppQueryCmd(
      queryArgs = queryMap,
      sortOrder = sortOrder,
      start = start,
      limit = limit
    )


    def find: Future[QueryResults[AppStoreApp]] = {
      future {
        val apps = appRepo.find(appQuery).map(asAppObject(_))
        QueryResults(apps)
      }
    }

    def findWithPlatform(platformName: String): Future[QueryResults[AppStoreApp]] = {
      logger.debug("find app by query for platform [{}]", platformName)

      appRepo.findAppIds(
        appQuery.copy(
          queryArgs = appQuery.queryDbObj ++ MongoDBObject("ncs-slas.downloadable" -> MongoDBObject("$exists" -> true))
        )
      ) match {
        case Nil => future(QueryResults.empty)
        case appIds => cmsConnector.findPlatforms(PlatformQuery(appIds, platform)).map {
          ps => QueryResults(appRepo.findByIds(ps.getResult.keys.toList).map(asAppObject(_)))
        }
      }
    }

    platform.fold(find)(findWithPlatform)
  }


  def findAllAppIds(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[String]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.FIND_ALL_APP_IDS, s"start = $start, limit = $limit")

      val ids = appRepo.findAppIds(AppQueryCmd(includeOnly = Map("_id" -> true), start = start, limit = limit))
      val results = QueryResults(ids)

      eventLog(correlationId, eventSource.FIND_ALL_APP_IDS, "Success")
      results
    }
  }

  def findAppsFromEachCategory(start: Int = 0, limit: Int = Int.MaxValue)
                              (implicit sc: ServiceContext): Future[QueryResult[Map[String, List[AppStoreApp]]]] = {
    import sc._

    val featuredAppService = inject[FeaturedAppService]

    val newlyAddedApps = findNewlyAdded(start, limit)
    val mostlyUsedApps = findMostlyUsed(start, limit)
    val topRatedApps = findTopRated(start, limit)
    val freeApps = findFreeApps(start, limit)
    val featuredApps = featuredAppService.findApps(start, limit)

    val result = for {
      newly <- newlyAddedApps
      mostly <- mostlyUsedApps
      top <- topRatedApps
      free <- freeApps
      featured <- featuredApps
    } yield {
      Map("newly-added" -> newly.getResults,
        "mostly-used" -> mostly.getResults,
        "top-rated" -> top.getResults,
        "free" -> free.getResults,
        "featured" -> featured.getResults)
    }

    result.map(QueryResult(_))

  }

  override def findSubscriptionApps(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._
    future {
      eventLog(correlationId, eventSource.FIND_SUBSCRIPTION_APPS, s"start = $start, limit = $limit")
      val subscriptionApps = appRepo.findSubscriptionApps(start, limit).map(asAppObject(_))
      eventLog(correlationId, eventSource.FIND_SUBSCRIPTION_APPS, "SUCCESS")
      QueryResults(subscriptionApps)
    }
  }

  override def findDownloadableApps(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._
    future {
      eventLog(correlationId, eventSource.FIND_DOWNLOADABLE_APPS, s"start = $start, limit = $limit")
      val downloadableApps = appRepo.findDownloadableApps(start, limit).map(asAppObject(_))
      eventLog(correlationId, eventSource.FIND_DOWNLOADABLE_APPS, "SUCCESS")
      QueryResults(downloadableApps)
    }
  }

  override def findAppsByDeveloper(developer: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    val eventMsg = s"start = $start, limit = $limit, developer = $developer"
    eventLog(correlationId, eventSource.FIND_APPS_BY_DEVELOPER, eventMsg)

    val queryMap = Map("developer" -> developer).collect {
      case (k, v) => (k, v)
    }
    val results = doFind(start, limit, queryMap)

    eventLog(correlationId, eventSource.FIND_APPS_BY_DEVELOPER, "Asynchronusly computed!")
    results
  }

  override def findSimilarApps(appId: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {
      val eventMsg = s"start = $start, limit = $limit, app-id = $appId"
      eventLog(correlationId, eventSource.FIND_SIMILAR_APPS, eventMsg)

      val similarApps = appRepo.findSimilarApps(appId, start, limit).map(asAppObject(_))
      eventLog(correlationId, eventSource.FIND_SIMILAR_APPS, "Asynchronusly computed!")
      QueryResults(similarApps)
    }
  }

  def findCategoryApps(category:String, start: Int=0, limit: Int=Int.MaxValue)
                      (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {
      val eventMsg = s"start = $start, limit = $limit, category = $category"
      eventLog(correlationId, eventSource.FIND_CATEGORY_APPS_WITHOUT_SORTING, eventMsg)

      val categoryApps = appRepo.findCategoryApps(category, start, limit).map(asAppObject(_))
      eventLog(correlationId, eventSource.FIND_CATEGORY_APPS_WITHOUT_SORTING, "Asynchronusly computed!")
      QueryResults(categoryApps)
    }
  }
}
