package hms.appstore.api.service

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.repo.{AppRatingRepo, SalatMapper}
import hms.appstore.api.json.{StatusCode}
import scala.concurrent.Future
import hms.appstore.api.service.Futures._
import hms.appstore.api.json.AppRatingAddReq

/**
 * Created by kasun on 4/25/18.
 */
class AppRatingServiceImpl(implicit val bindingModule: BindingModule) extends AppRatingService
with Injectable
with SalatMapper
with EventLogging {

  private val appRatingRepo = inject[AppRatingRepo]

  override def create(req: AppRatingAddReq)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.ADD_RATING, "adding rating to the app: " + req.appId)
      appRatingRepo.create(req)
      eventLog(correlationId, EventSource.CORE.ADD_RATING, "added rating to the app: " + req.appId)
      S1000
    }
  }
}
