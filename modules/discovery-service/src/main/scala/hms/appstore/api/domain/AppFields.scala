package hms.appstore.api.domain

import com.novus.salat.annotations.Key
import org.joda.time.DateTime

case class AppScreenShot(url: Option[String], caption: Option[String]){
  def asMap: Map[String, String] = (url, caption) match {
    case (Some(u), Some(c)) => Map("url" -> u, "caption" -> c)
    case _ => Map.empty
  }
}

case class AppBanner(url: Option[String], caption: Option[String]){
  def asMap: Map[String, String] = (url, caption) match {
    case (Some(u), Some(c)) => Map("url" -> u, "caption" -> c)
    case _ => Map.empty
  }
}

case class AppFields(_id: String,
                     name: String,
                     @Key("publish-name") displayName: String,
                     category: List[String],
                     app_icon: String,
                     app_banners: List[AppBanner] = List.empty,
                     usage: Long,
                     currency: String,
                     developer: String,
                     description: String,
                     short_description: String,
                     labels: List[String],
                     app_type: List[String],
                     instructions: Map[String, String],
                     rating: Number,
                     rate_count: Number,
                     password: String,
                     status: String,
                     remarks: String,
                     date_added: Long = System.currentTimeMillis(),
                     modified_date: Long = System.currentTimeMillis(),
                     app_screenshots: List[AppScreenShot] = List.empty)

