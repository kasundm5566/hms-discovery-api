package hms.appstore.api.service

import com.typesafe.scalalogging.slf4j.Logging

trait UsernameDisplayFormatter {
  def formatUsername(username: String): String
}


class DefaultUsernameDisplayFormatter extends UsernameDisplayFormatter {
  def formatUsername(username: String): String = username
}

class MobileNumberMaskUsernameDisplayFormatter extends UsernameDisplayFormatter with Logging{
  
  val dialogNumPrefix = List("9476", "9477", "076", "077", "76", "77")

  def formatUsername(username: String): String = {
//    logger.debug("masking the username [{}]",username)

    if (dialogNumPrefix.count(x => username.startsWith(x)) > 0) {
      val maskingLen = username.length - 4
      val mask = ("X" * maskingLen).toList
      username.reverse.patch(4, mask, maskingLen).reverse
    } else {
      username
    }
  }
}