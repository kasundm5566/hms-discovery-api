package hms.appstore.api.service.admin

import hms.appstore.api.json.Application

/**
 * Application Life Cycle Events
 */
sealed trait AppStateEvent {
  def eventId: Long
  def userId: String
  def app: Application
}

case class AppPublished(eventId: Long, userId: String, app: Application) extends AppStateEvent

case class AppRejected(eventId: Long, userId: String, app: Application) extends AppStateEvent

case class AppUnPublished(eventId: Long, userId: String, app: Application) extends AppStateEvent
