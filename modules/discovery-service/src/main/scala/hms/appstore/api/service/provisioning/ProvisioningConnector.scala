package hms.appstore.api.service.provisioning

import hms.appstore.api.service.ServiceContext
import scala.concurrent.Future

trait ProvisioningConnector {
   def findAppDetails(appId: String)(implicit sc: ServiceContext): Future[String]
}
