package hms.appstore.api.service

import admin.AdminServiceModule
import cms.CmsServiceModule
import governance.GovernanceServiceModule
import query.QueryServiceModule
import subscription.SubscriptionServiceModule

import com.escalatesoft.subcut.inject.NewBindingModule
import concurrent.ExecutionContext
import java.util.concurrent.Executors
import hms.appstore.api.service.caas.CaasServiceModule
import hms.appstore.api.service.notification.{NotificationServiceImpl, NotificationService, NotificationServiceModule}

object BaseServiceModule extends NewBindingModule({
  implicit module =>
    module.bind[ExecutionContext] toSingle ExecutionContext.
      fromExecutor(Executors.newFixedThreadPool(Runtime.getRuntime.availableProcessors() * 2))

    module.bind[SysConfigService] toSingle new DefaultSysConfigService
    module.bind[StatusCodeMapper] toSingle new StatusCodeMapperImpl
}
)

object ServiceModule extends NewBindingModule({
  implicit module =>

    module <~ BaseServiceModule
    module <~ CmsServiceModule
    module <~ CaasServiceModule
    module <~ GovernanceServiceModule
    module <~ SubscriptionServiceModule
    module <~ QueryServiceModule
    module <~ AdminServiceModule
    module <~ BannerServiceModule
    module <~ NotificationServiceModule

    module.bind[AppDetailsService] toSingle new AppDetailsServiceImpl

    module.bind[AppCommentService] toSingle new AppCommentServiceImpl

    module.bind[AppRatingService] toSingle new AppRatingServiceImpl

    module.bind[BannerService] toSingle new BannerServiceImpl

    module.bind[NotificationService] toSingle new NotificationServiceImpl

    module.bind[ClientPlatformService] toSingle new ClientPlatformServiceImpl

    module.bind[UsernameDisplayFormatter] toSingle {
      val sysConfig = BaseServiceModule.inject[SysConfigService](None)
      sysConfig.usernameDisplayFormatter match {
        case "default" => new DefaultUsernameDisplayFormatter
        case "mobile.number.mask" => new MobileNumberMaskUsernameDisplayFormatter
      }
    }
})
