/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.governance

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.json.{MsgJsonProtocol, AbuseReportResp, AbuseReport}
import hms.appstore.api.service.{EventSource, ServiceContext, ServiceFutureFilter}
import hms.scala.http.{HttpConnectorConfig, DefaultHttpClient}
import hms.scala.http.marshalling.HttpMarshalling


class GovernanceConnectorImp(implicit val bindingModule: BindingModule) extends GovernanceConnector
  with ServiceFutureFilter
  with Injectable {

  import MsgJsonProtocol._
  import HttpMarshalling._

  protected val eventSource =  EventSource.GOVERNANCE

  private val connector = new DefaultHttpClient(
    HttpConnectorConfig("governanceConnector", "http", inject[String]("governance.host"), inject[Int]("governance.port"))
  )

  def report(of: AbuseReport)(implicit sc: ServiceContext) = {
    val path = "/governance/report"
    val result = connector.doPost[AbuseReport, AbuseReportResp](path, of)
    filterWithServiceSuccess(eventSource.REPORT, path, result)
  }
}
