/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.casbah.query.Imports._

import hms.appstore.api.json.{SearchQuery, AppStatus, QueryResults}
import hms.appstore.api.repo._
import hms.appstore.api.service.{EventSource, EventLogging, ServiceContext}
import hms.appstore.api.service.Futures._

import scala.concurrent.Future
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.repo.AppQueryCmd
import scala.Some
import scala.util.matching.Regex


class AppSearchServiceImpl(implicit val bindingModule: BindingModule) extends AppSearchService
with Injectable
with SalatMapper
with EventLogging {

  private val appRepo = inject[AppRepo]

  protected val eventSource = EventSource.CORE

  private val regexChars = List('?', '(', ')', '*', '+', '\\', '`', '{', '}', '[', ']', '/')

  private def searchTxtAsRegex(txt: String) = {
    val filteredTxt = txt.filter(!regexChars.contains(_))
    s".*$filteredTxt.*(?i)".r
  }

  /**
   * search by query
   *
   * @param searchQuery
   * @param sc
   * @return
   */
  def search(searchQuery: SearchQuery)
            (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {

      eventLog(correlationId, eventSource.SEARCH_APPS, s"searchQuery = $searchQuery")

      val queryArgsOpt = searchQuery.searchText.map {
        txt =>
          Map(
            "$or" -> List(
              Map("publish-name" -> searchTxtAsRegex(txt)),
              Map("description" -> searchTxtAsRegex(txt))
            )
          )
      }

      val queryArgs = queryArgsOpt.getOrElse(Map.empty[String, List[Map[String, Regex]]])

      val query = AppQueryCmd(
        queryArgs = queryArgs,
        includeOnly = Map.empty[String, String],
        sortOrder = NoSorting,
        appStatus = searchQuery.status,
        appCategory = searchQuery.category,
        appId = searchQuery.appId,
        sp = searchQuery.spName,
        appType = searchQuery.appType,
        start = searchQuery.start,
        limit = searchQuery.limit
      )

      val apps = appRepo.find(query).map(asAppObject(_))
      val results = QueryResults(apps)

      eventLog(correlationId, eventSource.SEARCH_APPS, "Success")
      results
    }
  }


  /**
   * search by query
   *
   * @param searchQuery
   * @param sc
   * @return
   */
  def searchByName(searchQuery: SearchQuery)
                  (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {

      eventLog(correlationId, eventSource.SEARCH_APPS, s"searchQuery = $searchQuery")

      val queryArgsOpt = searchQuery.searchText.map {
        txt => Map("publish-name" -> searchTxtAsRegex(txt))
      }

      val queryArgs = queryArgsOpt.getOrElse(Map.empty[String, List[Map[String, Regex]]])

      val query = AppQueryCmd(
        queryArgs = queryArgs,
        includeOnly = Map.empty[String, String],
        sortOrder = NoSorting,
        appStatus = searchQuery.status,
        appCategory = searchQuery.category,
        appId = searchQuery.appId,
        sp = searchQuery.spName,
        appType = searchQuery.appType,
        start = searchQuery.start,
        limit = searchQuery.limit
      )

      val apps = appRepo.find(query).map(asAppObject(_))
      val results = QueryResults(apps)

      eventLog(correlationId, eventSource.SEARCH_APPS, "Success")
      results
    }
  }

  /**
   * searchRelatedApps
   *
   * calls the find repo method
   * @param appId (String) application id
   * @param start (Int) number of elements to skip
   * @param limit (Int) number of elements to return
   * @return QueryResult[AppStoreApp]
   */
  def searchRelated(appId: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._

    future {
      eventLog(correlationId, eventSource.SEARCH_RELATED_APPS, s"appId = $appId")

      val apps = appRepo.findOne(Some(appId)).map(asAppObject(_)).map {
        app => appRepo.find(
          AppQueryCmd(
            Map(
              "_id" -> Map("$ne" -> appId),
              "$or" -> List(
                Map("developer" -> app.developer),
                Map("category" -> Map("$in" -> app.category.split(", ")))
              )
            ), Map.empty[String, String], DescDateAdded, Some(AppStatus.Published), None, None, None, None, None, start, limit
          )
        ).map(asAppObject(_))
      }.getOrElse(List.empty)

      val results = QueryResults(apps)

      eventLog(correlationId, eventSource.SEARCH_RELATED_APPS, s"Success")
      results
    }
  }

  def searchExcludingCategory(searchText: String, excludingCategory: String, start: Int, limit: Int)
                             (implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]] = {
    import sc._
    eventLog(correlationId, eventSource.SEARCH_APPS_EXCLUDING_CATEGORY, s"category = $excludingCategory")
    future{
      eventLog(correlationId, eventSource.SEARCH_APPS_EXCLUDING_CATEGORY, s"Success")
      QueryResults(appRepo.findExcludingCategory(searchText, excludingCategory, start, limit).map(asAppObject(_)))
    }
  }

}
