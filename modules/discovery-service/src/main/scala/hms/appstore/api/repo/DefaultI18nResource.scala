package hms.appstore.api.repo

import java.util.{Locale, ResourceBundle}

class DefaultI18nResource extends I18nResource {
  private val resourceBundle = ResourceBundle.getBundle("messages")

  def getString(key: String, locale: Locale = Locale.ENGLISH): String = resourceBundle.getString(key)
}
