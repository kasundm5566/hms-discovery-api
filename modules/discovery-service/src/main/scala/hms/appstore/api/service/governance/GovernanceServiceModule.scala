package hms.appstore.api.service.governance

import com.escalatesoft.subcut.inject.NewBindingModule
import hms.scala.http.util.ConfigUtils
import com.typesafe.config.ConfigFactory
import hms.appstore.api.service.auth.AuthServiceModule


object GovernanceServiceModule extends NewBindingModule({
  implicit module =>
    module <~ AuthServiceModule

    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "nbl.connector.governance")

    module.bind [String] idBy "governance.host" toSingle conf.getString("host")
    module.bind [Int] idBy "governance.port" toSingle conf.getInt("port")

    module.bind[GovernanceConnector] toSingle new GovernanceConnectorImp
    module.bind[ReportAbuseService] toSingle new ReportAbuseServiceImpl
})
