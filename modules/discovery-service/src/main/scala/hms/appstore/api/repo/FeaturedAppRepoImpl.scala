/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.DBObject
import com.mongodb.casbah.commons.conversions.scala.{RegisterJodaTimeConversionHelpers, RegisterConversionHelpers}
import com.mongodb.casbah.commons.MongoDBObject


class FeaturedAppRepoImpl(implicit val bindingModule: BindingModule) extends FeaturedAppRepo with Injectable {
  RegisterConversionHelpers()
  RegisterJodaTimeConversionHelpers()

  private val db: Db = inject[Db]

  def findIds(start: Int, limit: Int): List[DBObject] = {
    db.featuredApps.find().sort(
      MongoDBObject("$natural" -> -1)
    ).skip(start).
      limit(limit).toList
  }
}
