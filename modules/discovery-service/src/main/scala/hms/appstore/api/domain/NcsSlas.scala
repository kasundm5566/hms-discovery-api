package hms.appstore.api.domain

import com.mongodb.casbah.query.Imports._
import hms.appstore.api.repo.BaseSalatMapper

class NcsSlas(source: DBObject) extends NcsSla with BaseSalatMapper {
  private lazy val ncsSlas = (
    asObject[CommonNcsSla](source),
    asObject[VodafoneNcsSla](source),
    asObject[DialogNcsSla](source)) // add new ncs slas if needed

  lazy val ncses = ncsSlas._1.ncses ++ ncsSlas._2.ncses ++ ncsSlas._3.ncses

  lazy val chargingSlas = ncsSlas._1.chargingSlas

  lazy val messagingSlas = ncsSlas._2.messagingSlas ++ ncsSlas._3.messagingSlas
}
