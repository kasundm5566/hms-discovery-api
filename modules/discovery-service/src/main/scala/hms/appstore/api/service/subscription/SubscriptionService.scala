/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.subscription

import hms.appstore.api.json._
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.service.ServiceContext

import scala.concurrent.Future

trait SubscriptionService {

  def mySubscriptions(sessionId: String, start: Int, limit: Int)(implicit sc: ServiceContext):  Future[QueryResults[AppStoreApp]]

  def subscribe(sessionId: String, appId: String)(implicit sc: ServiceContext): Future[SubscriptionActionResp]

  def unSubscribe(sessionId: String, appId: String)(implicit sc: ServiceContext): Future[SubscriptionActionResp]

  def subscriptionCount(appId: String)(implicit sc: ServiceContext): Future[SubscriptionCountResp]
}
