package hms.appstore.api.repo

import com.mongodb.casbah.query.Imports._
import hms.appstore.api.json.{CategoryEditReq, CategoryAddReq, AddOrRemoveAppsOfCategoryReq}
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.AppStatus._
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryReq
import hms.appstore.api.json.CategoryEditReq
import hms.appstore.api.service.ServiceContext

trait CategoryRepo {

  def createOrUpdateCategory(id: String, c: DBObject)

  def createCategory(c:DBObject)

  def editCategory(id: String, req: CategoryEditReq): Int

  def removeCategory(id: String): Int

  def addOrRemoveAppsOfCategory(id: String, req: AddOrRemoveAppsOfCategoryReq)(implicit sc: ServiceContext)

  def createOrUpdateParentCategory(id: String, sc: DBObject)

  def removeParentCategory(id: String)

  def findCategories(start: Int = 0, limit: Int = 10): List[DBObject]

  def findParentCategories(start: Int = 0, limit: Int= 10, filter: Boolean = true): List[DBObject]

  def editCategoriesOfApp(appStatus: Option[AppStatus], appId: String, categories: List[String]): Int
}
