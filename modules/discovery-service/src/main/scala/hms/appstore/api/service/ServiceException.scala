/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service

import hms.appstore.api.json.StatusCode

/**
 * Thrown to indicate that, service has encountered an error.
 * This will be converted to HTTP 200  with the application/json content of the [[hms.appstore.api.json.StatusCode]]
 */
class ServiceException(correlationId: CorrelationId,
                       statusCode: StatusCode,
                       thrownFrom: EventSource,
                       rootCause: Option[Throwable] = None) extends Exception(statusCode.description, rootCause.getOrElse(null)) {

  def getCorrelationId = correlationId

  def getStatusCode = statusCode

  def getThrownFrom = thrownFrom

  def getRootCause = rootCause
}