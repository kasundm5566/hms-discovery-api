package hms.appstore.api.repo

import com.mongodb.casbah.Imports._
import hms.appstore.api.domain.AppStoreApp
import com.escalatesoft.subcut.inject.Injectable

/**
 * Helper trait to map case class objects to db objects and vice versa.
 * This trait helps to avoid unnecessary imports.
 */
trait SalatMapper extends BaseSalatMapper with Injectable{

  lazy val i18nResource  = inject[I18nResource]

  protected def asAppObject(source: DBObject): AppStoreApp = AppStoreApp(new NcsSlaExtractor(i18nResource)(_), source)

}
