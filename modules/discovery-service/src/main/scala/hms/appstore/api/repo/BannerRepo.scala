package hms.appstore.api.repo

import com.mongodb.casbah.query.Imports._
import hms.appstore.api.json.{BannerLocation, BannersCount, BannerLocations, BannerAddReq}
import scala.collection.mutable.ListBuffer
import hms.appstore.api.service.ServiceContext

/**
 * Created by kasun on 4/2/18.
 */
trait BannerRepo {

  def getBanners(start: Int = 0, limit: Int = 10): List[DBObject]

  def getBannersByLocation(location: String, start: Int = 0, limit: Int = 10): List[DBObject]

  def addBanner(b: DBObject)

  def removeBanner(id: Int)

  def editBanner(id: Int, req: BannerAddReq)

  def getBannerLocations (): Set[BannerLocation]

  def getBannersCount (): BannersCount

  def getBannersCountByLocation (location: String): BannersCount
}
