package hms.appstore.api.domain

case class ChargingLabel(label: String, amount: Option[String])

object ChargingLabel extends {

  def apply(ncsSla: NcsSla): ChargingLabel = {

    val services =  ncsSla.chargingSlas.filter(_.charging.chargingType != "free").map(_.charging)
    val messaging = ncsSla.messagingSlas.map(s => s.mo.map(_.charging) ++ s.mt.map(_.charging)).flatten.filter(_.chargingType != "free")

    (services ++ messaging) match {
      case Nil => ChargingLabel("FREE", None)
      case head :: Nil => head match {
        case Charging("flat", _, amount, Some("daily")) => ChargingLabel("DAILY", amount)
        case Charging("flat", _, amount, Some("weekly")) => ChargingLabel("WEEKLY", amount)
        case Charging("flat", _, amount, Some("monthly")) => ChargingLabel("MONTHLY", amount)
        case Charging("flat", _, amount,  None) => if (services.exists(_.chargingType == "downloadable")) {
          ChargingLabel("ACTIVATION", amount)
        } else  ChargingLabel("PER USAGE", amount)
      }
      case head :: tail => ChargingLabel("MULTIPLE", None)
    }
  }
}
