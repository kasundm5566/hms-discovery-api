/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.auth

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}

import hms.appstore.api.json._
import hms.appstore.api.service._
import hms.appstore.api.service.Futures._

import scala.concurrent.{Await, Future}
import hms.appstore.api.json.UserDetailsByUsernameReq
import hms.appstore.api.json.AuthenticateByMPinReq
import hms.appstore.api.json.AuthenticateResp
import hms.appstore.api.json.SessionValidationReq
import hms.appstore.api.json.MPinChangeReq
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.AuthenticateByUsernameReq
import hms.appstore.api.json.UserDetailsResp
import hms.appstore.api.json.UserDetailsByMsisdnReq
import hms.appstore.api.json.AuthenticateByPasswordReq
import hms.appstore.api.json.MPinChangeResp
import net.liftweb.common.Empty
import spray.http.MultipartFormData
import hms.appstore.api.service.admin.IOUtil
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit


class AuthServiceImpl(implicit val bindingModule: BindingModule) extends AuthService with Injectable with EventLogging {
  private val commonRegConnector = inject[CommonRegConnector]
  private val sessionTokenService = inject[SessionTokenService]
  private val sysConfigService = inject[SysConfigService]
  val defaultDateOfBirth = "01/01/1970"

  val listOfAuthProviders = Map(
    "android" -> List("auth-by-msisdn", "auth-by-password"),
    "web" -> List("auth-by-password", "auth-by-username"),
    "wap" -> List("auth-by-password", "auth-by-msisdn"),
    "ussd" -> List("auth-by-msisdn", "auth-by-mpin")
  )

  val failure = AuthenticateResp("E5200", Some("Authentication failure, your credentials are incorrect!"), None, None)

  def authProviders(platform: String, msisdn: Option[String], host: Option[String])(implicit sc: ServiceContext): Future[AuthProviders] = {
    import sc._
    future {
      val availableProviders = listOfAuthProviders.get(platform).getOrElse(List.empty)
      val preferredProvider = {
        platform match {
          case "android" | "wap" if msisdn.isDefined & host.isDefined => "auth-by-msisdn"
          case "android" | "wap" if !host.isDefined | !msisdn.isDefined => "auth-by-password"
          case "web" => "auth-by-username"
          case "ussd" => "auth-by-msisdn"
        }
      }
      AuthProviders(availableProviders, preferredProvider)
    }
  }

  def authenticate(msisdn: Option[String], host: Option[String])(implicit sc: ServiceContext): Future[AuthenticateResp] = {
    import sc._

    val allowedHosts = sysConfigService.allowedHosts

    val filteredHostIp = host.map(ips => {
      if (ips.contains(",")) {
        val commaSeparated = ips.split(",")
        commaSeparated.last.trim
      } else {
        ips.trim
      }
    })

    (msisdn, filteredHostIp) match {
      case (Some(m), Some(h)) if allowedHosts.collectFirst {
        case r if r.findFirstIn(h).nonEmpty => true
      }.getOrElse(false) => {
        commonRegConnector.userDetailsByMsisdn(UserDetailsByMsisdnReq(m)).flatMap {
          resp => resp.statusCode match {
            case "S1000" => {
              val sessionToken = sessionTokenService.create(resp.getMsisdn.get, resp.getUserName.get)
              future {
                AuthenticateResp("S1000", resp.statusDescription, Some(sessionToken.id), resp.getMsisdn, resp.additionalParams)
              }
            }
            case "E5201" => {
              commonRegConnector.createAutoLoginConsumerUser(AutoLoginUserRegistrationReq(m)).map {
                userRegistrationResp => userRegistrationResp.statusCode match {
                  case "S1000" =>
                    val sessionToken = sessionTokenService.create(msisdn.get, m)
                    AuthenticateResp("S1003", Some("You have successfully logged in to the system."), Some(sessionToken.id), msisdn, resp.additionalParams)
                  case _ =>
                    failure.copyFrom(E5000.status, E5000.description)
                }
              }
            }
            case _ => {
              future {
                failure.copyFrom(E5000.status, E5000.description)
              }
            }
          }
        }
      }
      case _ => future(failure)
    }
  }

  def authenticate(req: AuthenticateByUsernameReq)(implicit sc: ServiceContext): Future[AuthenticateResp] = {
    import sc._
    commonRegConnector.userDetailsByUsername(UserDetailsByUsernameReq(req.username)).map {
      resp => {
        val sessionToken = sessionTokenService.create(resp.msisdn, req.username)
        AuthenticateResp("S1000", Some("Success"), Some(sessionToken.id), Some(resp.msisdn))
      }
    }
  }

  def authenticate(req: AuthenticateByMsisdnReq)(implicit sc: ServiceContext): Future[AuthenticateResp] = {
    import sc._
    commonRegConnector.userDetailsByMsisdn(UserDetailsByMsisdnReq(req.msisdn)).map {
      resp => {
        val sessionToken = sessionTokenService.create(req.msisdn, resp.getUserName.getOrElse("anonymous"))
        AuthenticateResp("S1000", Some("Success"), Some(sessionToken.id), Some(resp.getMsisdn.getOrElse("anonymous")))
      }
    }
  }

  def authenticate(req: AuthenticateByMPinReq)(implicit sc: ServiceContext): Future[AuthenticateResp] = {
    import sc._
    commonRegConnector.authenticateByMPin(req).map {
      resp => {
        val sessionToken = sessionTokenService.create(req.msisdn, "anonymous") //TODO: Fix this Mayooran
        AuthenticateResp("S1000", Some("Success"), Some(sessionToken.id), resp.mobileNo)
      }
    }
  }

  def authenticate(req: AuthenticateByPasswordReq)(implicit sc: ServiceContext): Future[AuthenticateResp] = {
    import sc._
    commonRegConnector.authenticateByPassword(req).map {
      resp => {
        resp.statusCode match {
          case "S1000" => {
            val sessionToken = sessionTokenService.create(resp.mobileNo.get, req.username)
            AuthenticateResp("S1000", Some("Success"), Some(sessionToken.id), resp.mobileNo)
          }
          case _ => {
            AuthenticateResp(resp.statusCode, resp.statusDescription, None, resp.mobileNo)
          }
        }
      }
    }
  }

  def validate(req: SessionValidationReq)(implicit sc: ServiceContext): Future[SessionValidationResp] = {
    import sc._
    //if session token is created by external auth provider delegate the validation
    if (SessionToken.isExternal(req.sessionId)) {
      commonRegConnector.validate(req)
    } else {
      future {
        sessionTokenService.validate(req.sessionId) match {
          case Some(token) => SessionValidationResp.build(S1000, Some(token.id), Some(token.msisdn), Some(token.username))
          case _ => throw new ServiceException(correlationId, E5105, EventSource.CORE.SESSION_VALIDATION)
        }
      }
    }
  }

  def changeMPin(req: MPinChangeReq)(implicit sc: ServiceContext): Future[MPinChangeResp] = {
    commonRegConnector.changeMPin(req)
  }

  def userDetails(req: UserDetailsByMsisdnReq)(implicit sc: ServiceContext): Future[UserDetailsResp] = {
    commonRegConnector.userDetailsByMsisdn(req)
  }

  def userDetailsByName(req: UserDetailsByUsernameReq)(implicit sc: ServiceContext): Future[UserDetailByUserNameResp] = {
    commonRegConnector.userDetailsByUsername(req)
  }

  override def userProfileDetails(sessionId: String)(implicit sc: ServiceContext): Future[UserProfileDetailsResp] = {
    import sc._
    val req = SessionValidationReq(sessionId)
    validate(req).flatMap {
      case SessionValidationResp(_, _, _, Some(msisdn), Some(name)) => {
        commonRegConnector.userProfileDetails(UserDetailsByUsernameReq(name))
      }
    }
  }

  def userProfileUpdate(sessionId: String, req: MultipartFormData)(implicit sc: ServiceContext): Future[UserProfileUpdateResp] = {
    import sc._

    validate(SessionValidationReq(sessionId)).flatMap {
      case SessionValidationResp(_, _, _, Some(msisdn), Some(name)) => {

        val firstName = req.get("firstName").get.entity.asString
        val lastName = req.get("lastName").get.entity.asString
        val username = name
        val email = req.get("email").get.entity.asString
        var image = ""
        var fileName = ""
        if (!req.get("image").isEmpty) {
          image = Base64String.apply(req.get("image").get.entity.data.toByteArray).data
          fileName = req.get("image").get.filename.get
        }
        val userUpdateReq = UserProfileUpdateReq(firstName, lastName, username, email, image, fileName)
        commonRegConnector.updateUserProfile(userUpdateReq)
      }
    }
  }

  override def developerProfileDetails(req: UserDetailsByUsernameReq)(implicit sc: ServiceContext): Future[DeveloperProfileDetailsResp] = {
    import sc._
    future {
      val profileDetails = Await.result(commonRegConnector.userProfileDetails(req), Duration(2000, TimeUnit.MILLISECONDS))
      val developerProfile: DeveloperProfileDetailsResp = DeveloperProfileDetailsResp(profileDetails.image,
        profileDetails.lastName.getOrElse(""), profileDetails.username.getOrElse(""), profileDetails.firstName.getOrElse(""),
        profileDetails.email.getOrElse(""), profileDetails.statusCode, profileDetails.statusDescription)
      developerProfile
    }
  }

  override def resetUserPassword(req: UserPasswordResetReq)(implicit sc: ServiceContext): Future[UserPasswordResetResp] = {
    import sc._
    commonRegConnector.resetUserPassword(req)
  }
}
