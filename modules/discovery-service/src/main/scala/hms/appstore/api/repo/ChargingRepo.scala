package hms.appstore.api.repo

import hms.appstore.api.json.{ChargingDescriptionMessage, ChargingData, ChargingEditReq}
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.AppStatus._
import hms.appstore.api.json.ChargingEditReq
import hms.appstore.api.json.ChargingData
import hms.appstore.api.json.ChargingDescriptionMessage

/**
 * Created by kasun on 5/18/18.
 */
trait ChargingRepo {

  def editChargingAmount(appStatus: Option[AppStatus], req: ChargingEditReq)

  def findChargingDetails(appId: String): Set[ChargingData]

  def findChargingDescriptionMessageFormat(appId: String): Set[ChargingDescriptionMessage]
}
