package hms.appstore.api.service

import hms.appstore.api.json.{MobileScreenShot, ScreenShot, Icon, UploadType}
import hms.scala.http.util.ConfigUtils
import com.typesafe.config.{Config, ConfigFactory}
import scala.collection.JavaConverters._
import scala.util.matching.Regex

case class ImageValidationConfig(minWidth: Int, minHeight: Int, maxSizeInBytes: Int) {
  def aspectRatio: Float = minWidth.toFloat / minHeight.toFloat
}

trait SysConfigService {

  def config: Config

  def operators: Set[String]

  def currency : String

  def authProvider : String

  def ncsTypes: Set[String]

  def imagePathPrefix: String

  def imageDirectoryPath: String

  def imageValidationConfig(ut: UploadType): ImageValidationConfig

  def accessControlAllowedHosts: List[String]

  def snmpStartupTrap: String

  def snmpShutdownTrap: String

  def statusCodeConfig : Config

  def allowedHosts : List[Regex]

  def encryptionKey : String

  def rngAlgorithm : String

  def paymentInstrumentType: String

  def usernameDisplayFormatter: String
}

class DefaultSysConfigService extends SysConfigService {

  private val _config = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.discovery.api")

  private val _defaultImgConfig = ImageValidationConfig(minWidth = 240, minHeight = 360, maxSizeInBytes = 50 * 1024)

  private val _imageValConfig = Map[UploadType, ImageValidationConfig](
    Icon -> ImageValidationConfig(minWidth = 120, minHeight = 120, maxSizeInBytes = 50 * 1024),
    ScreenShot -> ImageValidationConfig(minWidth = 240, minHeight = 360, maxSizeInBytes = 50 * 1024),
    MobileScreenShot -> ImageValidationConfig(minWidth = 360, minHeight = 600, maxSizeInBytes = 40 * 1024)
  )

  val config = _config

  val operators: Set[String] = _config.getStringList("operator.ids").asScala.toSet

  val currency: String = _config.getString("currency.code")

  val authProvider: String = _config.getString("auth.provider")

  val ncsTypes: Set[String] =
    Set(
      "subscription",
      "downloadable"
    ) ++ Set(
    "sms.mo",
    "sms.mt",
    "ussd.mo",
    "ussd.mt",
    "wap-push.mo",
    "wap-push.mt"
  ).map(sla => operators.map(op => s"$op-$sla")).flatten


  val imagePathPrefix: String = "images/applications/icons"

  val imageDirectoryPath: String = "/hms/data/discovery-api/images"

  val accessControlAllowedHosts: List[String] = List("*")

  lazy val snmpStartupTrap: String = _config.getString("snmp.startup.trap")

  lazy val snmpShutdownTrap: String = _config.getString("snmp.shutdown.trap")

  def imageValidationConfig(ut: UploadType): ImageValidationConfig = _imageValConfig.getOrElse(ut, _defaultImgConfig)

  val statusCodeConfig = ConfigFactory.load("status-codes.conf")

  val allowedHosts: List[Regex] = _config.getStringList("allowed.hosts").asScala.toList.map(_.r)

  val encryptionKey: String = _config.getString("encryption.key")

  val rngAlgorithm: String = _config.getString("rng.algorithm")

  val paymentInstrumentType: String = _config.getString("discovery.pi.type")

  val usernameDisplayFormatter: String = _config.getString("discovery.uname.fmt")
}
