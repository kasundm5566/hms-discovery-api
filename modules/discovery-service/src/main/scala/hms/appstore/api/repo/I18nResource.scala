package hms.appstore.api.repo

import java.util.{ResourceBundle, Locale}

trait I18nResource {
  def getString(key: String, locale: Locale = Locale.ENGLISH): String
}


