/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.auth

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.service._
import javax.crypto.Cipher
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}
import org.parboiled.common.Base64
import java.security.SecureRandom

class SessionTokenServiceImpl(implicit val bindingModule: BindingModule) extends SessionTokenService
  with Injectable
  with EventLogging {

  private val sysConfigService = inject[SysConfigService]
  val KEY = sysConfigService.encryptionKey
  val instance: Cipher = Cipher.getInstance("AES/CTR/NoPadding")
  val IV = new Array[Byte](instance.getBlockSize)
//  SecureRandom.getInstance(sysConfigService.initializationVector).nextBytes(IV)

  def create(msisdn: String, username: String)(implicit sc: ServiceContext): SessionToken = {

    val plainToken = s"&|$msisdn|$username|" + System.currentTimeMillis
    val encryptedToken = encrypt(plainToken, KEY)
    val encryptedSessionId = Base64.custom().encodeToString(encryptedToken, false)
    new SessionToken(s"${SessionToken.DsPrefix}$encryptedSessionId", msisdn, username)

  }

  def validate(id: String)(implicit sc: ServiceContext): Option[SessionToken] = {
    val sessionIdWithoutPrefix = id.substring(3)
    val decryptedBase64 = Base64.custom().decode(sessionIdWithoutPrefix)

    if(decryptedBase64 != null){
      val decryptedToken = decrypt(decryptedBase64, KEY)
      if(decryptedToken.split('|')(0) == "&" ){
        val msisdn = decryptedToken.split('|')(1)
        val username = decryptedToken.split('|')(2)
        Some(new SessionToken(id, msisdn, username))
      } else None
    } else None
  }

  private def encrypt(plainText: String, encryptionKey: String)(implicit sc: ServiceContext): Array[Byte] = {
    val cipher = Cipher.getInstance("AES/CTR/NoPadding")
    val key: SecretKeySpec = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES")

    cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(IV))
    cipher.doFinal(plainText.getBytes("UTF-8"))
  }

  private def decrypt(cipherText: Array[Byte], encryptionKey: String)(implicit sc: ServiceContext): String = {
    val cipher: Cipher = Cipher.getInstance("AES/CTR/NoPadding")
    val key: SecretKeySpec = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES")

    cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(IV))
    new String(cipher.doFinal(cipherText), "UTF-8")
  }
}
