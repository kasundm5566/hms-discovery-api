package hms.appstore.api.repo

import com.mongodb.casbah.query.Imports._
import com.mongodb.casbah.commons.conversions.scala._
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}


class DownloadReqRepoImpl(implicit val bindingModule: BindingModule) extends DownloadReqRepo with Injectable {
  RegisterConversionHelpers()
  RegisterJodaTimeConversionHelpers()

  private val db: Db = inject[Db]

  def findByMsisdn(msisdn: String, start: Int = 0, limit: Int = Int.MaxValue): List[DBObject] = {
    db.downloadRequests.find(
      MongoDBObject("address" -> s"tel:$msisdn"),
      MongoDBObject("applicationId" -> 1, "status" -> 1, "requestedDate" -> 1, "contentId" -> 1)
    ).skip(start).limit(limit).toList
  }

  def findByApplicationId(applicationId: String, msisdn: String): List[DBObject] = {
    db.downloadRequests.find(
      MongoDBObject("address" -> s"tel:$msisdn", "applicationId" -> applicationId),
      MongoDBObject("applicationId" -> 1, "status" -> 1, "requestedDate" -> 1, "contentId" -> 1)
    ).toList
  }

}
