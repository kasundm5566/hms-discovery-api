package hms.appstore.api.service

import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.slf4j.Logging

trait EventLogging extends Logging{
  private lazy val eventLogger = LoggerFactory.getLogger("info")

  def eventLog(id: CorrelationId,  source: EventSource, out: AnyRef) {
    eventLogger.info("{}|{}|{}|{}", id, source.name, source.action, out)
  }
}
