package hms.appstore.api.service


import scala.concurrent.Future
import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.service.Futures._
import hms.appstore.api.json.ClientVersion

class ClientPlatformServiceImpl (implicit val bindingModule: BindingModule) extends ClientPlatformService with Injectable {

  private val sysConfig = inject[SysConfigService]

  def findLatestVersion(platform: String, osVersion: String)(implicit sc: ServiceContext): Future[ClientVersion] = {
    future {
        if(sysConfig.config.hasPath(s"client.$platform.$osVersion")){
          val latestVersion = sysConfig.config.getString(s"client.$platform.$osVersion.version")
          val clientLocation = sysConfig.config.getString(s"client.$platform.$osVersion.url")
          ClientVersion("S1000", Some("Success"), Some(platform), Some(latestVersion), Some(clientLocation))
        }
      else{
          ClientVersion("E5301", Some("Currently new version of appstore is not available"), None, None, None)
        }

    }
  }
}
