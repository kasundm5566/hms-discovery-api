/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import hms.appstore.api.json._
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.service.ServiceContext

import scala.concurrent.Future
import AppStatus._
import hms.appstore.api.domain.AppStoreApp
import scala.Some
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.AppStatus

trait AppFindService {

  def findOne(appId: String, appStatus: Option[AppStatus] = Some(AppStatus.Published))(implicit sc: ServiceContext): Future[QueryResult[AppStoreApp]]

  def findAppOrPendingApp(appId: String)(implicit sc: ServiceContext): Future[QueryResult[AppStoreApp]]

  def findApps(appIds: List[String])(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findAll(status: Option[AppStatus] = Some(Published), category: Option[String] = None, start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findPending(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findByCategory(start: Int, limit: Int, category: String, platform: Option[String] = None)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findMostlyUsed(start: Int, limit: Int, category: Option[String] = None, platform: Option[String] = None)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findNewlyAdded(start: Int, limit: Int, category: Option[String] = None, platform: Option[String] = None)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findTopRated(start: Int, limit: Int, category: Option[String] = None, platform: Option[String] = None)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findFreeApps(start: Int, limit: Int, category: Option[String] = None, platform: Option[String] = None)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findAllAppIds(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[String]]

  def findAppsFromEachCategory(start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResult[Map[String, List[AppStoreApp]]]]

  def findSubscriptionApps(start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findDownloadableApps(start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findAppsByDeveloper(developer: String, start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findSimilarApps(appId: String, start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

  def findCategoryApps(category:String, start: Int=0, limit: Int=Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[AppStoreApp]]

}
