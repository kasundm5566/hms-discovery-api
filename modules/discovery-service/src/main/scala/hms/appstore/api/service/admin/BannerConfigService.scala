package hms.appstore.api.service.admin

import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext
import scala.concurrent.Future
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.BannerAddReq
import hms.appstore.api.service.ServiceContext
import com.mongodb.casbah.commons.MongoDBList
import scala.collection.mutable.ListBuffer

/**
 * Created by kasun on 4/2/18.
 */
trait BannerConfigService {

  def addBanner(req: BannerAddReq)(implicit sc: ServiceContext): Future[StatusCode]

  def removeBanner(id: Int)(implicit sc: ServiceContext): Future[StatusCode]

  def editBanner(id: Int, req: BannerAddReq)(implicit sc: ServiceContext): Future[StatusCode]

  def getBanners(start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[Banner]]

  def getBannerLocations()(implicit sc: ServiceContext): Future[QueryResults[BannerLocation]]

  def getBannersCount()(implicit sc: ServiceContext): Future[QueryResult[BannersCount]]

  def getBannersCountByLocation(location: String)(implicit sc: ServiceContext): Future[QueryResult[BannersCount]]

  def getBannersByLocation(location: String, start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[Banner]]
}
