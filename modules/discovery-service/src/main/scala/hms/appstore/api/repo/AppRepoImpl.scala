/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import com.escalatesoft.subcut.inject._

import com.mongodb.casbah.query.Imports._
import com.mongodb.casbah.commons.conversions.scala._
import hms.appstore.api.json.AppStatus
import com.mongodb.casbah.query.Imports
import hms.appstore.api.service.{EventSource, ServiceContext, EventLogging}
import scala.collection.mutable.ListBuffer

class AppRepoImpl(implicit val bindingModule: BindingModule) extends AppRepo with Injectable with EventLogging {

  import AppSortingImplicits._

  RegisterConversionHelpers()
  RegisterJodaTimeConversionHelpers()

  private val db: Db = inject[Db]


  def findOne(id: Option[String] = None): Option[DBObject] = {
    db.apps.findOne(id.fold(AppQueryCmd().queryDbObj)(v => AppQueryCmd(MongoDBObject("_id" -> v)).queryDbObj))
  }

  def findAppIds(args: AppQueryCmd): List[String] = {

    import args._

    db.apps.find(queryDbObj, includeOnly.asDBObject).
      sort(sortOrder).
      skip(start).
      limit(limit).
      toList.map(_.getAsOrElse[String]("_id", ""))
  }

  def findByIds(ids: List[String]): List[DBObject] = {
    if (!ids.isEmpty) {
      db.apps.find(AppQueryCmd("_id".$in(ids)).queryDbObj).toList
    } else List.empty
  }

  def findByIdsUnsorted(ids: List[String]): List[DBObject] = {
    var buf: ListBuffer[DBObject] = new ListBuffer[DBObject]()
    if (!ids.isEmpty) {
      for (id <- ids) {
        val query = MongoDBObject("$and" -> MongoDBList(MongoDBObject("_id" -> id), MongoDBObject("status" -> "Publish")))
        db.apps.find(query).foreach(u => {
          buf += u
        })
      }
    }
    buf.toList
  }

  def findByIdInOrder(ids: List[String]): List[DBObject] = {
    if (!ids.isEmpty) {
      val orIds = ids.map(i => MongoDBObject("_id" -> i))
      db.apps.find(AppQueryCmd($or(orIds)).queryDbObj).toList
    } else List.empty
  }

  def findDownloadableByIdInOrder(ids: List[String]): List[Imports.DBObject] = {
    if (!ids.isEmpty) {
      val orIds = ids.map(i => MongoDBObject("_id" -> i))
      db.apps.find(AppQueryCmd($or(orIds), ncsType = Option("downloadable")).queryDbObj).toList
    } else List.empty
  }

  /**
   * Find applications from apps + pending_apps collection by the query
   *
   * Queries for an object in apps collection.
   *
   * @param args (AppQueryCmd) contains complex query map, start, limit
   * @return list of DBObjects
   */
  def findAppOrPendingApp(args: AppQueryCmd): List[DBObject] = {

    import args._

    val apps = db.apps.find(queryDbObj, includeOnly).
      skip(start).
      limit(limit).
      toList

    val pendingApps = db.pendingApps.find(queryDbObj.filterKeys(_ != "status"), includeOnly).
      skip(start).
      limit(limit).
      toList

    apps ++ pendingApps
  }


  /**
   * find
   *
   * Queries for an object in apps collection. The results can be sorted, limited
   * with respect to the options present in the args
   *
   * @param args (AppQueryCmd) contains complex query map, start, limit
   * @return list of DBObjects
   */
  def find(args: AppQueryCmd): List[DBObject] = {

    import args._

    if (!args.appStatus.exists(_ == AppStatus.New)) {
      db.apps.find(queryDbObj, includeOnly).
        sort(sortOrder).
        skip(start).
        limit(limit).
        toList

    } else {
      db.pendingApps.find(queryDbObj.filterKeys(_ != "status"), includeOnly).
        sort(sortOrder).
        skip(start).
        limit(limit).
        toList
    }
  }

  def countWithQuery(args: AppQueryCmd): Long = {
    import args._

    if (!args.appStatus.exists(_ == AppStatus.New)) {
      db.apps.count(queryDbObj)
    } else {
      db.pendingApps.count(queryDbObj.filterKeys(_ != "status"))
    }
  }

  def update(args: AppUpdateCmd) {
    import args._

    if (!appStatus.exists(_ == AppStatus.New)) {
      db.apps.update(queryArgs, updateArgs)
    } else {
      db.pendingApps.update(queryArgs, updateArgs)
    }
  }

  override def findSimilarApps(appId: String, start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject] = {
    import sc._
    val queryForAppId = MongoDBObject("appIds" -> appId)
    val dbObjects = db.categories.find(queryForAppId).toList

    var appIds: MongoDBList = MongoDBList.empty
    var appIdList: List[String] = List.empty[String]

    for (dbObject <- dbObjects) {
      db.categories.findOne(MongoDBObject("name" -> dbObject.get("name"))).foreach(x => appIds = x.as[MongoDBList]("appIds"))
      for (applicationId <- appIds) {
        if (applicationId.toString != appId) {
          appIdList ::= applicationId.toString
        }
      }
    }
    findByIds(appIdList.distinct.slice(start, start+limit))
  }

  override def findDownloadableApps(start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject] = {
    var apps: ListBuffer[DBObject] = new ListBuffer[DBObject]()
    db.apps.find().foreach(x => {
      x.as[MongoDBList]("ncses").foreach(u => {
        if (u.asInstanceOf[BasicDBObject].get("ncs-type") == "downloadable" && x.get("status").toString == "Publish") {
          apps += x
        }
      })
    })
    apps.toList.distinct.slice(start, start+limit)
  }

  override def findSubscriptionApps(start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject] = {
    import sc._
    var apps: ListBuffer[DBObject] = new ListBuffer[DBObject]()

    db.apps.find().foreach(x => {
      x.as[MongoDBList]("ncses").foreach(u => {
        if (u.asInstanceOf[BasicDBObject].get("ncs-type") == "subscription" && x.get("status").toString == "Publish") {
          apps += x
        }
      })
    })
    apps.toList.distinct.slice(start, start+limit)
  }

  override def findCategoryApps(category: String, start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject] = {
    import sc._
    val queryForCategoryId = MongoDBObject("_id" -> category)
    var appIds: MongoDBList = MongoDBList.empty
    if (db.categories.find(queryForCategoryId).count == 1) {
      db.categories.findOne(queryForCategoryId).foreach(x => appIds = x.as[MongoDBList]("appIds"))
    }
    val appIdList = appIds.toList.collect {
      case s: String => s
    }
    findByIdsUnsorted(appIdList.slice(start, start+limit))
  }

  override def findExcludingCategory(searchText: String, category: String, start: Int, limit: Int)(implicit sc: ServiceContext): List[Imports.DBObject] = {
    val queryForExcludingCategory = MongoDBObject("$and" -> MongoDBList(MongoDBObject("publish-name" -> MongoDBObject("$regex" -> searchText, "$options" -> "i")), MongoDBObject("category" -> MongoDBObject("$ne" -> category)), MongoDBObject("status" -> "Publish")))
    db.apps.find(queryForExcludingCategory).skip(start).limit(limit).toList
  }
}
