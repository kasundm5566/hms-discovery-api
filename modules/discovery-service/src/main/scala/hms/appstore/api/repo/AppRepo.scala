/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import hms.appstore.api.json.AppStatus._
import com.mongodb.casbah.query.Imports._
import hms.appstore.api.service.ServiceContext

sealed trait AppSorting

case object NoSorting extends AppSorting

case object DescDateAdded extends AppSorting

case object DescUsage extends AppSorting

case object DescRating extends AppSorting

case object DescRequestedDate extends AppSorting

case object DescModifiedDated extends AppSorting

/**
 * AppQueryCmd
 *
 * @param queryArgs query input as map
 * @param sortOrder (AppSorting) NoSorting, DescDateAdded, DescUsage, DescRating
 * @param start number of elements to skip
 * @param limit number of elements to return
 */
case class AppQueryCmd(queryArgs: DBObject = MongoDBObject.empty,
                       includeOnly: DBObject = MongoDBObject.empty,
                       sortOrder: AppSorting = DescDateAdded,
                       appStatus: Option[AppStatus] = Some(Published),
                       appCategory: Option[String] = None,
                       appId: Option[String] = None,
                       appType: Option[String] = None,
                       sp: Option[String] = None,
                       ncsType: Option[String] = None,
                       start: Int = 0,
                       limit: Int = Int.MaxValue) {
  lazy val queryDbObj = {
    queryArgs ++
      appStatus.fold(MongoDBObject.empty)(s => MongoDBObject("status" -> s.toString)) ++
      appCategory.fold(MongoDBObject.empty)(c => MongoDBObject("category" -> c)) ++
      appId.fold(MongoDBObject.empty)(i => MongoDBObject("app-id" -> i)) ++
      appType.fold(MongoDBObject.empty)(t => ("app_type" $in List(t))) ++
      ncsType.fold(MongoDBObject.empty)(n => MongoDBObject("ncs-slas.downloadable.ncs-type" -> n)) ++
      sp.fold(MongoDBObject.empty)(sp => MongoDBObject("created-by" -> (".*" + sp + ".*").r))
  }

}

case class AppUpdateCmd(queryArgs: DBObject = MongoDBObject.empty,
                        updateArgs: DBObject = MongoDBObject.empty,
                        appStatus: Option[AppStatus] = None)

trait AppRepo {
  def findOne(id: Option[String] = None): Option[DBObject]

  def findAppIds(query: AppQueryCmd): List[String]

  def findAppOrPendingApp(query: AppQueryCmd): List[DBObject]

  def find(query: AppQueryCmd): List[DBObject]

  def findByIds(ids: List[String]): List[DBObject]

  def findByIdsUnsorted(ids: List[String]): List[DBObject]

  def findByIdInOrder(ids: List[String]): List[DBObject]

  def findDownloadableByIdInOrder(ids: List[String]): List[DBObject]

  def countWithQuery(query: AppQueryCmd): Long

  def update(query: AppUpdateCmd)

  def findSimilarApps(appId: String, start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject]

  def findDownloadableApps(start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject]

  def findSubscriptionApps(start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject]

  def findCategoryApps(category: String, start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject]

  def findExcludingCategory(searchText: String, category: String, start: Int, limit: Int)(implicit sc: ServiceContext): List[DBObject]
}
