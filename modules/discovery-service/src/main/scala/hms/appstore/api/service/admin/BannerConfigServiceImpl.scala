package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}

import hms.appstore.api.json._
import hms.appstore.api.repo._
import hms.appstore.api.service._
import hms.appstore.api.service.Futures._

import concurrent.Future

/**
 * Created by kasun on 4/2/18.
 */
class BannerConfigServiceImpl(implicit val bindingModule: BindingModule) extends BannerConfigService
with Injectable
with BaseSalatMapper
with EventLogging {

  private val bannerRepo = inject[BannerRepo]

  override def addBanner(req: BannerAddReq)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.ADD_BANNER, req)
      bannerRepo.addBanner(asDBObject[BannerAddReq](req));
      val resp = S1000
      eventLog(correlationId, EventSource.CORE.ADD_BANNER, resp)
      resp
    }
  }

  def removeBanner(id: Int)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      bannerRepo.removeBanner(id);
      val resp = S1000
      eventLog(correlationId, EventSource.CORE.REMOVE_BANNER, resp)
      resp
    }
  }

  override def editBanner(id: Int, req: BannerAddReq)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.CREATE_CATEGORY, req)
      bannerRepo.editBanner(id, req);
      val resp = S1000
      eventLog(correlationId, EventSource.CORE.EDIT_BANNER, resp)
      resp
    }
  }


  override def getBanners(start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[Banner]] = {
    future {
      val banners = bannerRepo.getBanners(start = start, limit = limit).map(asObject[Banner](_))
      val results = QueryResults(banners)
      results
    }
  }

  override def getBannerLocations()(implicit sc: ServiceContext): Future[QueryResults[BannerLocation]] = {
    future {
      val bannerLocations = bannerRepo.getBannerLocations()
      QueryResults(bannerLocations.toList)
    }
  }

  override def getBannersCount()(implicit sc: ServiceContext): Future[QueryResult[BannersCount]] = {
    future {
      val bannersCount = bannerRepo.getBannersCount()
      QueryResult(bannersCount)
    }
  }

  override def getBannersByLocation(location: String, start: Int, limit: Int)(implicit sc: ServiceContext): Future[QueryResults[Banner]] = {
    future {
      val banners = bannerRepo.getBannersByLocation(location, start, limit).map(asObject[Banner](_))
      QueryResults(banners)
    }
  }

  override def getBannersCountByLocation(location: String)(implicit sc: ServiceContext): Future[QueryResult[BannersCount]] = {
    future {
      val bannersCount = bannerRepo.getBannersCountByLocation(location)
      QueryResult(bannersCount)
    }
  }
}
