package hms.appstore.api.repo

import com.mongodb.casbah.Imports._
import com.novus.salat._

trait BaseSalatMapper {

  implicit val ctx = new Context {
    val name = "when-necessary"
    override val typeHintStrategy = StringTypeHintStrategy(when = TypeHintFrequency.WhenNecessary, typeHint = TypeHint)
  }

  protected def asObject[Y <: AnyRef](source: DBObject)(implicit m : Manifest[Y]): Y = grater[Y].asObject(source)

  protected def asDBObject[Y <: AnyRef](source: Y)(implicit m : Manifest[Y]): DBObject = grater[Y].asDBObject(source)

  protected def asObjects[Y <: AnyRef](source: List[DBObject])(implicit m : Manifest[Y]): List[Y] = source.map(asObject(_))

  protected def asDBObjects[Y <: AnyRef](source: List[Y])(implicit m : Manifest[Y]): List[DBObject] = source.map(asDBObject(_))
}
