package hms.appstore.api.domain

import com.novus.salat.annotations._
import hms.appstore.api.json.UserComment
import org.bson.types.ObjectId

case class AppUserComment(@Key("_id") id: ObjectId = new ObjectId,
                          username: Option[String],
                          @Key("app_id") appId: String,
                          @Key("time_in_mili") dateTime: Long,
                          comments: String,
                          @Ignore abuse: Option[Boolean] = Some(false)) extends UserComment {
  def commentId: Option[String] = Some(id.toString)

  override def image: Option[String] = Some("")
}
