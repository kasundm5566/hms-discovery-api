package hms.appstore.api.service.caas

import hms.appstore.api.json.{PaymentInstrumentsResp, PaymentInstrumentsReq}
import hms.appstore.api.service.ServiceContext
import scala.concurrent.Future


trait CaasConnector {

  def findPaymentInstruments(req: PaymentInstrumentsReq)(implicit sc: ServiceContext): Future[PaymentInstrumentsResp]

}
