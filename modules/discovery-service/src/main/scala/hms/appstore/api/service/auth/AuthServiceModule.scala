/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.auth

import com.escalatesoft.subcut.inject.NewBindingModule
import com.typesafe.config.ConfigFactory
import hms.appstore.api.service.BaseServiceModule
import hms.scala.http.util.ConfigUtils

object AuthServiceModule extends NewBindingModule({
  implicit module =>
    module <~ BaseServiceModule

    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "nbl.connector.common.reg")

    module.bind[String] idBy "common.reg.host" toSingle conf.getString("host")
    module.bind[Int] idBy "common.reg.port" toSingle conf.getInt("port")

    module.bind[SessionTokenService] toSingle new SessionTokenServiceImpl

    module.bind[CommonRegConnector] toSingle new CommonRegConnectorImpl

    module.bind[UserRegService] toSingle new UserRegServiceImpl

    module.bind[AuthService] toSingle new AuthServiceImpl
})

