package hms.appstore.api.service.admin

import hms.appstore.api.json.{AppStatus, StatusCode}
import hms.appstore.api.repo.Db
import hms.appstore.api.service._
import hms.appstore.api.service.Futures._
import hms.appstore.api.service.provisioning.ProvisioningConnector

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.casbah.commons.Imports._
import com.mongodb.util.JSON
import com.typesafe.config.ConfigFactory

import scala.concurrent.Future
import scala.collection.JavaConverters._
import hms.scala.http.util.ConfigUtils
import hms.appstore.api.service.ServiceContext
import scala.Some
import com.mongodb.casbah.commons.Imports
import hms.appstore.api.service.ServiceContext
import scala.Some


class AppStateMgmtServiceImpl(implicit val bindingModule: BindingModule) extends AppStateMgmtService
with Injectable
with EventLogging {

  private val apps = inject[Db].apps

  private val categories = inject[Db].categories

  private val pendingApps = inject[Db].pendingApps

  private val provConnector = inject[ProvisioningConnector]

  private val sysConfig = inject[SysConfigService]

  /**
   * Create the initial app and move the status to pending status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def request(appId: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    val instructions = sysConfig.operators.map(k => (k, "")).toMap
    provConnector.findAppDetails(appId).map {
      js => {
        val query = MongoDBObject("_id" -> appId)
        val initialData: MongoDBObject = JSON.parse(js).asInstanceOf[com.mongodb.DBObject]
        val app = initialData ++ MongoDBObject(
          "app_icon" -> "",
          "app_screen_shots" -> MongoDBList.empty,
          "app_type" -> MongoDBList(initialData.getAsOrElse[String]("category", "")),
          "category" -> MongoDBList(initialData.getAsOrElse[String]("category", "")),
          "labels" -> MongoDBList.empty,
          "instructions" -> instructions,
          "publish-name" -> initialData.getAsOrElse[String]("name", ""),
          "developer" -> initialData.getAsOrElse[String]("created-by", ""),
          "rating" -> 0L,
          "rate_count" -> 0L,
          "short_description" -> "",
          "short_description_constraint" -> "",
          "usage" -> 0L,
          "currency" -> sysConfig.currency,
          "remarks" -> remarks,
          "date_added" -> System.currentTimeMillis(),
          "modified_date" -> System.currentTimeMillis()
        )

        apps.findOne(query).fold[StatusCode] {
          pendingApps.update(query, app, upsert = true);
          S1000
        } {
          app => if (app.get("status") == AppStatus.UnPublished) {
            pendingApps.insert(app);
            apps.remove(query);
            S1000
          } else E5106
        }
      }
    }
  }

  /**
   * Move the application from pending status to publish status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def approve(appId: String, publishName: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._
    future {
      val query = MongoDBObject("_id" -> appId)

      pendingApps.findOne(query) match {
        case Some(app) => {
          app.put("publish-name", publishName)
          app.put("status", AppStatus.Published.toString)
          app.put("remarks", remarks)
          app.put("date_added", System.currentTimeMillis())
          app.put("modified_date", System.currentTimeMillis())
          apps.insert(app)
          val addAppToCategory = MongoDBObject(
            "$addToSet" -> MongoDBObject("appIds" -> appId)
          )
          var categoriesList: MongoDBList = MongoDBList.empty
          apps.findOne(query).foreach(x => categoriesList = x.as[MongoDBList]("category"))

          for (category <- categoriesList) {
            val appCategory = category.toString
            val queryForCategoryName = MongoDBObject("_id" -> appCategory)
            categories.findAndModify(queryForCategoryName, addAppToCategory)
          }

          pendingApps.remove(query)
          S1000
        }
        case None => throw new ServiceException(correlationId, E5101, EventSource.CORE.ERROR_HANDLING)
      }
    }
  }

  /**
   * Move the the application from pending status to rejected status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def reject(appId: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      val query = MongoDBObject("_id" -> appId)

      pendingApps.findOne(query) match {
        case Some(app) => pendingApps.remove(query); S1000
        case None => throw new ServiceException(correlationId, E5101, EventSource.CORE.ERROR_HANDLING)
      }
    }
  }

  /**
   * Move the application from publish status to un-publish status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def unpublish(appId: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      val query = MongoDBObject(
        "_id" -> appId,
        "status" -> AppStatus.Published.toString
      )

      apps.findOne(query) match {
        case Some(app) => {
          app.put("remarks", remarks)
          app.put("status", AppStatus.UnPublished.toString)
          app.put("modified_date", System.currentTimeMillis())

          apps.update(query, app)
          S1000
        }
        case None => throw new ServiceException(correlationId, E5101, EventSource.CORE.ERROR_HANDLING)
      }
    }
  }

  /**
   * Move the application from un-publish status to publish status
   * @param appId  appId
   * @param remarks  notes
   * @return
   */
  def publish(appId: String, publishName: String, remarks: String)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      val query = MongoDBObject(
        "_id" -> appId,
        "status" -> AppStatus.UnPublished.toString
      )

      apps.findOne(query) match {
        case Some(app) => {
          app.put("publish-name", publishName)
          app.put("status", AppStatus.Published.toString)
          app.put("remarks", remarks)
          app.put("date_added", System.currentTimeMillis())
          app.put("modified_date", System.currentTimeMillis())

          apps.update(query, app)
          S1000
        }
        case None => throw new ServiceException(correlationId, E5101, EventSource.CORE.ERROR_HANDLING)
      }
    }
  }
}
