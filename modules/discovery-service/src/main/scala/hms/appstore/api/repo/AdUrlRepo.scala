package hms.appstore.api.repo

import com.mongodb.casbah.query.Imports._

trait AdUrlRepo {

  def create(dbo: DBObject)

  def createOrUpdate(id: String, dbo: DBObject)

  def remove(id: String)

  def findAll(): List[DBObject]
}
