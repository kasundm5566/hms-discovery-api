package hms.appstore.api.service

import scala.concurrent.Future
import hms.appstore.api.json.{AppRatingAddReq, StatusCode}

/**
 * Created by kasun on 4/25/18.
 */
trait AppRatingService {
  def create(req: AppRatingAddReq)(implicit sc: ServiceContext): Future[StatusCode]
}
