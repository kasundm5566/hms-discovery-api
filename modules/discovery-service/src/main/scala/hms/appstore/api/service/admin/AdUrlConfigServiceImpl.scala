package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.json.{QueryResults, StatusCode, AdUrl}
import hms.appstore.api.repo.{AdUrlRepo, SalatMapper}
import hms.appstore.api.service._
import hms.appstore.api.service.Futures._
import concurrent.Future
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.AdUrl


class AdUrlConfigServiceImpl(implicit val bindingModule: BindingModule) extends AdUrlConfigService
  with Injectable
  with EventLogging
  with SalatMapper{

  private val adUrlRepo = inject[AdUrlRepo]

  def findAll(implicit sc: ServiceContext): Future[QueryResults[AdUrl]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.FIND_ALL_AD_URLS, "")

      val adUrls = adUrlRepo.findAll().map(asObject[AdUrl](_))

      val resp = QueryResults(adUrls)

      eventLog(correlationId, EventSource.CORE.FIND_ALL_AD_URLS, resp)

      resp
    }
  }

  def createOrUpdate(urls: List[AdUrl])(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.CREATE_UPDATE_AD_URLS, urls)

      urls.foreach{
        url => adUrlRepo.createOrUpdate(url.id, asDBObject[AdUrl](url))
      }

      val resp = S1000

      eventLog(correlationId, EventSource.CORE.CREATE_UPDATE_AD_URLS, resp)

      resp
    }
  }
}
