package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.mongodb.casbah.query.Imports._

import hms.appstore.api.json._
import hms.appstore.api.repo.{AppRepo, Db, SalatMapper}
import hms.appstore.api.service._
import hms.appstore.api.service.Futures._
import hms.appstore.api.service.ServiceContext
import concurrent.Future


class FeaturedAppMgmtServiceImpl(implicit val bindingModule: BindingModule) extends FeaturedAppMgmtService
  with Injectable
  with SalatMapper
  with EventLogging {

  private val featuredApps = inject[Db].featuredApps
  private val appRepo = inject[AppRepo]

  def update(positions: Map[String, Int])(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    def doFindApp(id: String): Application = {
      val appOpt = appRepo.findOne(Some(id)).map(asAppObject(_))
      appOpt.getOrElse(throw new ServiceException(correlationId, E5101, EventSource.CORE.ERROR_HANDLING))
    }

    future {

      val newFeaturedApps = for {
        (key, pos) <- positions
      } yield {
        (key -> Map("_id" -> key, s"featured_app_$pos" -> doFindApp(key).displayName))
      }

      featuredApps.remove(Map.empty[String, AnyRef])

      newFeaturedApps.foreach {
        case (id, app) => featuredApps.insert(app)
      }

      S1000
    }
  }
}
