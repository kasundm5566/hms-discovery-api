/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.query

import hms.appstore.api.json._
import hms.appstore.api.service.ServiceContext

import scala.concurrent.Future

trait CategoryService {

  def findCategories(start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[Category]]

  def findParentCategories(start: Int = 0, limit: Int = Int.MaxValue)(implicit sc: ServiceContext): Future[QueryResults[ParentCategory]]
}
