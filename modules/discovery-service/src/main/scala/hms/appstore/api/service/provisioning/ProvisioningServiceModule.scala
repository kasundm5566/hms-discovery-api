package hms.appstore.api.service.provisioning

import com.escalatesoft.subcut.inject.NewBindingModule
import com.typesafe.config.ConfigFactory
import hms.appstore.api.service.BaseServiceModule
import hms.scala.http.util.ConfigUtils

/**
 * Created by nirojan on 12/4/13.
 */
object ProvisioningServiceModule extends NewBindingModule({
  implicit module =>

    module <~ BaseServiceModule

    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "nbl.connector.provisioning")

    module.bind [String] idBy "provisioning.host" toSingle conf.getString("host")
    module.bind [Int] idBy "provisioning.port" toSingle conf.getInt("port")

    module.bind [ProvisioningConnector] toSingle new ProvisioningConnectorImpl()
})
