package hms.appstore.api.service.notification

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.service.{ServiceContext, EventSource, SysConfigService, ServiceFutureFilter}
import hms.appstore.api.json._
import hms.scala.http.marshalling.HttpMarshalling
import hms.scala.http.{HttpConnectorConfig, DefaultHttpClient}
import scala.concurrent.Future
import hms.appstore.api.json.AppUpdateNotificationReq
import hms.appstore.api.json.NotifyResp
import hms.scala.http.HttpConnectorConfig
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.NotifyReq

/**
 * Created by kasun on 6/20/18.
 */
class NotificationAdminConnectorImpl(implicit val bindingModule: BindingModule) extends NotificationAdminConnector
with ServiceFutureFilter
with Injectable{

  import MsgJsonProtocol._
  import HttpMarshalling._

  private val sysConfig = inject[SysConfigService]
  protected val ec = EventSource.NOTIFICATION_ADMIN
  private val connector = new DefaultHttpClient(
    HttpConnectorConfig("notificationAdminConnector", "http", inject[String]("notification.admin.host"), inject[Int]("notification.admin.port"))
  )

  override def notifyAll(req: NotifyReq)(implicit sc: ServiceContext): Future[NotifyResp] = {
    val path = "/appstore/admin/notification/v1/notify"
    val result = connector.doPost[NotifyReq, NotifyResp](path, req)
    result
  }

  override def notifyAppSpecific(req: NotifyReq)(implicit sc: ServiceContext): Future[NotifyResp] = {
    val path = "/appstore/admin/notification/v1/notify"
    val result = connector.doPost[NotifyReq, NotifyResp](path, req)
    result
  }

  override def notifyAppUpdate(req: AppUpdateNotificationReq)(implicit sc: ServiceContext): Future[AppUpdateNotificationResp] = {
    val path = "/appstore/admin/notification/v1/buildFile"
    val result = connector.doPost[AppUpdateNotificationReq, AppUpdateNotificationResp](path, req)
    result
  }
}
