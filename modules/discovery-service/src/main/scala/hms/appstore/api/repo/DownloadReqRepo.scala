package hms.appstore.api.repo

import com.mongodb.casbah.query.Imports._

trait DownloadReqRepo {
  def findByMsisdn(msisdn: String, start: Int = 0, limit: Int = Int.MaxValue): List[DBObject]

  def findByApplicationId(applicationId: String, msisdn: String): List[DBObject]
}
