package hms.appstore.api.repo

import com.mongodb.casbah.query.Imports._
import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.json._
import hms.appstore.api.service.{EventSource, EventLogging, ServiceContext}
import java.net.URLDecoder
import hms.appstore.api.json.AppStatus._
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryReq
import hms.appstore.api.json.CategoryEditReq
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryReq
import hms.appstore.api.json.CategoryEditReq

class CategoryRepoImpl(implicit val bindingModule: BindingModule) extends CategoryRepo with Injectable {
  private val db: Db = inject[Db]
  private val imagePath: String = "images/applications/icons/category/"

  def createOrUpdateCategory(id: String, c: DBObject) {
    db.categories.update(MongoDBObject("_id" -> id), c)
  }

  def createCategory(c: DBObject) {

    val nextID = findNextIdForCategory()
    c.put("position", nextID)

    c.put("_id", c.get("name").toString)

    val query = MongoDBObject("name" -> c.get("name"))
    c.put("appIds", List[String]())
    c.put("image_url", imagePath + c.get("image_url").toString)
    if (db.categories.find(query).count == 0) {
      db.categories.insert(c)
    }
  }

  private def findNextIdForCategory(): Int = {
    val query = MongoDBObject() // All documents
    val fields = MongoDBObject("position" -> 1) // Only return `_id`
    val orderBy = MongoDBObject("position" -> -1) // Order by _id descending

    val ids = db.categories.find(query, fields).sort(orderBy).limit(1).toVector

    var nextId = 1
    if (ids.size > 0) {
      ids.foreach(i => nextId = i.get("position").asInstanceOf[Int])
      nextId + 1
    } else {
      nextId
    }
  }

  def editCategory(id: String, req: CategoryEditReq): Int = {
    val queryForCategory = MongoDBObject("_id" -> id)

    if (db.categories.find(queryForCategory).count == 1) {

      val obj = db.categories.findOne(queryForCategory).get

      val categoryName = req.name
      val oldCategoryName = obj.get("name").toString
      val oldCategoryPosition = obj.get("position").toString.toInt
      val categoryDesc = req.description.getOrElse(obj.get("description").toString)
      var categoryImage = ""

      if (req.image_url.isEmpty) {
        categoryImage = obj.get("image_url").toString
      } else {
        categoryImage = imagePath + req.image_url.get
      }

      var oldAppIds: MongoDBList = MongoDBList.empty
      db.categories.findOne(queryForCategory).foreach(x => oldAppIds = x.as[MongoDBList]("appIds"))

      val category = MongoDBObject("_id" -> categoryName, "name" -> categoryName,
        "description" -> categoryDesc, "image_url" -> categoryImage, "appIds" -> oldAppIds, "position" -> oldCategoryPosition)

      db.categories.remove(MongoDBObject("_id" -> id))
      db.categories.insert(category)
      for (appId <- oldAppIds) {
        val queryToCheckAppAvailability = MongoDBObject("_id" -> appId)
        if (db.apps.find(queryToCheckAppAvailability).count > 0) {
          val removeCategoriesOfApp = MongoDBObject(
            "$pull" -> MongoDBObject("category" -> oldCategoryName)
          )
          val addCategoriesOfApp = MongoDBObject(
            "$addToSet" -> MongoDBObject("category" -> categoryName)
          )
          db.apps.findAndModify(queryToCheckAppAvailability, removeCategoriesOfApp)
          db.apps.findAndModify(queryToCheckAppAvailability, addCategoriesOfApp)
        }
      }
      1
    } else {
      0
    }


    /*if (db.categories.find(queryForCategory).count == 1) {
      val update = MongoDBObject(
        "$set" -> MongoDBObject("name" -> req.name)
      )
      db.categories.findAndModify(queryForCategory, update)

      if (!req.description.isEmpty) {
        val updateDescription = MongoDBObject(
          "$set" -> MongoDBObject("description" -> req.description)
        )
        db.categories.findAndModify(queryForCategory, updateDescription)
      }

      if (!req.image_url.isEmpty) {
        val updateImageUrl = MongoDBObject(
          "$set" -> MongoDBObject("image_url" -> (imagePath + req.image_url.get))
        )
        db.categories.findAndModify(queryForCategory, updateImageUrl)
      }
      val updateId = MongoDBObject(
        "$set" -> MongoDBObject("_id" -> req.name)
      )
      db.categories.findAndModify(queryForCategory, updateId)
      1
    } else {
      0
    }*/
  }

  def removeCategory(id: String): Int = {
    val queryForCategory = MongoDBObject("_id" -> id)

    var oldAppIds: MongoDBList = MongoDBList.empty
    db.categories.findOne(queryForCategory).foreach(x => oldAppIds = x.as[MongoDBList]("appIds"))

    for (appId <- oldAppIds) {
      val queryToCheckAppAvailability = MongoDBObject("_id" -> appId)
      if (db.apps.find(queryToCheckAppAvailability).count > 0) {
        val removeCategoriesOfApp = MongoDBObject(
          "$pull" -> MongoDBObject("category" -> id)
        )
        db.apps.findAndModify(queryToCheckAppAvailability, removeCategoriesOfApp)
      }
    }

    db.categories.remove(MongoDBObject("_id" -> id)).getN()
  }

  def addOrRemoveAppsOfCategory(id: String, req: AddOrRemoveAppsOfCategoryReq)(implicit sc: ServiceContext) {

    val queryForCategoryName = MongoDBObject("_id" -> id)
    if (db.categories.find(queryForCategoryName).count == 1) {
      val categoryName = db.categories.findOne(queryForCategoryName).get.get("name").toString

      val newAppIds = req.appIds
      var oldAppIds: MongoDBList = MongoDBList.empty
      db.categories.findOne(queryForCategoryName).foreach(x => oldAppIds = x.as[MongoDBList]("appIds"))

      val removedAppIds = oldAppIds diff newAppIds
      val newlyInsertedAppIds = newAppIds diff oldAppIds

      // Remove categories from apps
      for (removedAppId <- removedAppIds) {
        val queryToCheckAppAvailability = MongoDBObject("_id" -> removedAppId)
        if (db.apps.find(queryToCheckAppAvailability).count > 0) {
          val updateCategoriesOfApp = MongoDBObject(
            "$pull" -> MongoDBObject("category" -> categoryName)
          )
          db.apps.findAndModify(queryToCheckAppAvailability, updateCategoriesOfApp)
        }
      }

      // Add categories to apps
      for (addedAppId <- newlyInsertedAppIds) {
        val queryToCheckAppAvailability = MongoDBObject("_id" -> addedAppId)
        if (db.apps.find(queryToCheckAppAvailability).count > 0) {
          val updateCategoriesOfApp = MongoDBObject(
            "$addToSet" -> MongoDBObject("category" -> categoryName)
          )
          db.apps.findAndModify(queryToCheckAppAvailability, updateCategoriesOfApp)
        }
      }

      // Add appIds of the categories collection.
      val queryToCheckCategoryAvailability = MongoDBObject("_id" -> id)
      if (db.categories.find(queryToCheckCategoryAvailability).count > 0) {
        val updateAppIdsOfCategory = MongoDBObject(
          "$set" -> MongoDBObject("appIds" -> req.appIds)
        )
        db.categories.findAndModify(queryToCheckCategoryAvailability, updateAppIdsOfCategory)
      }
    }
  }

  def createOrUpdateParentCategory(id: String, sc: DBObject) {
    db.superCategories.update(MongoDBObject("_id" -> id), sc)
  }

  def removeParentCategory(id: String) {
    db.superCategories.remove(MongoDBObject("_id" -> id))
  }

  def findCategories(start: Int = 0, limit: Int = 10): List[DBObject] = {
    db.categories.find().
      sort(MongoDBObject("position" -> 1)).
      skip(start).
      limit(limit).
      toList
  }

  def findParentCategories(start: Int = 0, limit: Int = 10, filter: Boolean = true): List[DBObject] = {
    val query = if (filter) Map("value" -> filter) else Map.empty[String, AnyRef]

    db.superCategories.find(query).
      skip(start).
      limit(limit).
      toList
  }

  override def editCategoriesOfApp(appStatus: Option[AppStatus], appId: String, categories: List[String]): Int = {
    if (!appStatus.exists(_ == AppStatus.New)) {
      val addAppToCategory = MongoDBObject(
        "$addToSet" -> MongoDBObject("appIds" -> appId)
      )
      val removeAppFromCategory = MongoDBObject(
        "$pull" -> MongoDBObject("appIds" -> appId)
      )
      val findCategoriesOfTheApp = MongoDBObject("appIds" -> appId)

      // Remove app from all categories
      db.categories.find(findCategoriesOfTheApp).foreach(category => {
        val categoryId = category.get("_id").toString
        db.categories.findAndModify(MongoDBObject("_id" -> categoryId), removeAppFromCategory)
      })

      // Add app to all the newly assigned categories
      categories.foreach(category => {
        val queryForCategoryName = MongoDBObject("_id" -> category)
        db.categories.findAndModify(queryForCategoryName, addAppToCategory)
      })
      1
    } else {
      0
    }
  }
}
