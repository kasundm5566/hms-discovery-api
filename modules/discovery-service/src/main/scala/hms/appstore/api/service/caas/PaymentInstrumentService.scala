package hms.appstore.api.service.caas

import hms.appstore.api.service.ServiceContext
import scala.concurrent.Future
import hms.appstore.api.json.PaymentInstrumentsResp


trait PaymentInstrumentService {

  def findPaymentInstruments(sessionId: String, applicationId: String)(implicit sc: ServiceContext):Future[PaymentInstrumentsResp]

}
