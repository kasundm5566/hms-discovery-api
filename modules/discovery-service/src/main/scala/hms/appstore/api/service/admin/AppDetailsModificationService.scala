package hms.appstore.api.service.admin

import hms.appstore.api.json._
import concurrent.Future
import hms.appstore.api.service.ServiceContext


trait AppDetailsModificationService {

  /**
   * Cache the ImageUploadReq in memory, so it can be process later by [[hms.appstore.api.service.admin.AppDetailsModificationService.updateDetails]]
   * @param req  image upload request
   * @return
   */
  def cacheImageUpload(req: ImageUploadReq)(implicit sc: ServiceContext): QueryResult[ImageUploadResp]


  /**
   * Process an app update details request, this might be called after [[hms.appstore.api.service.admin.AppDetailsModificationService.cacheImageUpload]] call.
   *
   * action:
   * 1. collect all icon, screen-shot images from image upload caches if available
   *    and delete old files if needed and recreate new files
   *    (this has to be done inside $TOMCAT_HOME/webapps/appstore-web/WEB-INF/images/applications/icons/<app-name>.jpg)
   *    Note: Have soft links /hms/data/discovery-api/images ..>   $TOMCAT_HOME/webapps/appstore-web/WEB-INF/images
   *
   * 2. and update the modified app fields in the db
   * @param req  app update request
   * @return
   */
  @deprecated
  def updateDetails(req: UpdateReq)(implicit sc: ServiceContext): Future[StatusCode]

  def updateDetailsAndCharging(req: UpdateAppAndChargingReq)(implicit sc: ServiceContext): Future[StatusCode]
}
