package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.repo.{ChargingRepo, BaseSalatMapper}
import hms.appstore.api.service.{S1000, EventSource, ServiceContext, EventLogging}
import hms.appstore.api.json._
import scala.concurrent.Future
import hms.appstore.api.service.Futures._
import hms.appstore.api.json.ChargingData
import hms.appstore.api.json.ChargingEditReq
import hms.appstore.api.service.ServiceContext

/**
 * Created by kasun on 5/18/18.
 */
class ChargingConfigServiceImpl(implicit val bindingModule: BindingModule) extends ChargingConfigService
with Injectable
with BaseSalatMapper
with EventLogging {

  private val chargingRepo = inject[ChargingRepo]

  override def editCharging(req: ChargingEditReq)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.EDIT_CHARGING_AMOUNT, "")
      chargingRepo.editChargingAmount(Some(AppStatus.New) ,req)
      val resp = S1000
      eventLog(correlationId, EventSource.CORE.EDIT_CHARGING_AMOUNT, resp)
      resp
    }
  }

  override def findChargingDetails(appId: String)(implicit sc: ServiceContext): Future[QueryResults[ChargingData]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.FIND_CHARGING_DETAILS, "")
      val chargings = chargingRepo.findChargingDetails(appId)
      val resp = S1000
      eventLog(correlationId, EventSource.CORE.FIND_CHARGING_DETAILS, resp)
      QueryResults(chargings.toList)
    }
  }

  override def findChargingDescriptionMessageFormat(appId: String)(implicit sc: ServiceContext): Future[QueryResults[ChargingDescriptionMessage]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.FIND_CHARGING_DESCRIPTION_MESSAGE_FORMAT, "")
      val messageFormats = chargingRepo.findChargingDescriptionMessageFormat(appId)
      val resp = S1000
      eventLog(correlationId, EventSource.CORE.FIND_CHARGING_DESCRIPTION_MESSAGE_FORMAT, resp)
      QueryResults(messageFormats.toList)
    }
  }
}
