package hms.appstore.api.repo

import hms.appstore.api.json.AppRatingAddReq
import hms.appstore.api.service.ServiceContext

/**
 * Created by kasun on 4/27/18.
 */
trait AppRatingRepo {
  def create(req: AppRatingAddReq)
}
