package hms.appstore.api.service.admin

import hms.appstore.api.json._
import scala.concurrent.Future
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.ChargingEditReq
import hms.appstore.api.json.ChargingData
import hms.appstore.api.service.ServiceContext

/**
 * Created by kasun on 5/18/18.
 */
trait ChargingConfigService {

  def editCharging(req: ChargingEditReq)(implicit sc: ServiceContext): Future[StatusCode]

  def findChargingDetails(appId: String)(implicit sc: ServiceContext): Future[QueryResults[ChargingData]]

  def findChargingDescriptionMessageFormat(appId: String)(implicit sc: ServiceContext): Future[QueryResults[ChargingDescriptionMessage]]
}
