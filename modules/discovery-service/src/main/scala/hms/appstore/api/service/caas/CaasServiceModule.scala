package hms.appstore.api.service.caas

import com.escalatesoft.subcut.inject.NewBindingModule
import hms.scala.http.util.ConfigUtils
import com.typesafe.config.ConfigFactory

import hms.appstore.api.repo.RepoModule
import hms.appstore.api.service.auth.AuthServiceModule
import hms.appstore.api.service.query.QueryServiceModule

object CaasServiceModule extends NewBindingModule({
  implicit module =>

    module <~ RepoModule
    module <~ AuthServiceModule

    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "nbl.connector.caas")

    module.bind [String] idBy "caas.host" toSingle conf.getString("host")
    module.bind [Int] idBy "caas.port" toSingle conf.getInt("port")


    module.bind [CaasConnector] toSingle new CaasConnectorImpl
    module.bind [PaymentInstrumentService] toSingle new PaymentInstrumentServiceImpl

})