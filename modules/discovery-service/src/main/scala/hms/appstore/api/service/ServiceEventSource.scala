package hms.appstore.api.service

case class EventSource(name: String, action: String)

object EventSource {
  case object CORE {
    val COUNT_APPS_BY_QUERY = EventSource("core", "count-apps-by-query")
    val COUNT_APPS_BY_CATEGORY = EventSource("core", "count-apps-by-category")
    val COUNT_FREE_APPS = EventSource("core","count-free-apps")
    val COUNT_DOWNLOADABLE_FREE_APPS = EventSource("core","count-downloadable-free-apps")
    val COUNT_DOWNLOADABLE_APPS = EventSource("core","count-downloadable-apps")
    val COUNT_PUBLISHED_APPS = EventSource("core","count-published-apps")
    val COUNT_APPS_BY_STATUS = EventSource("core","count-apps-by-status")
    val FIND_APP_BY_ID_QUERY = EventSource("core","find-app-by-id-query")
    val FIND_APP_ONE_PENDING_APPS = EventSource("core","find-one-app-from-apps-or-pending-apps")
    val FIND_APPS_BY_IDS = EventSource("core","find-apps-by-ids")
    val FIND_ALL_APPS = EventSource("core","find-all-apps")
    val FIND_APPS_FROM_EACH_CATEGORY = EventSource("core","find-apps-from-each-category")
    val FIND_PENDING_APPS = EventSource("core","find-pending-apps")
    val FIND_APPS_BY_CATEGORY = EventSource("core","find-apps-by-category")
    val FIND_APPS_BY_DEVELOPER = EventSource("core","find-apps-by-developer")
    val FIND_MOSTLY_USED_APPS = EventSource("core","find-mostly-used-apps")
    val FIND_SIMILAR_APPS = EventSource("core", "find-similar-apps")
    val FIND_CATEGORY_APPS_WITHOUT_SORTING = EventSource("core", "find-category-apps-without-sorting")
    val FIND_NEWLY_ADD_APPS = EventSource("core","find-newly-added-apps")
    val FIND_TOP_RATED_APPS = EventSource("core","find-top-rated-apps")
    val FIND_FREE_APPS = EventSource("core","find-free-apps")
    val FIND_ALL_APP_IDS = EventSource("core","find-all-app-ids")
    val FIND_SUBSCRIPTION_APPS = EventSource("core","find-subscription-apps")
    val FIND_DOWNLOADABLE_APPS = EventSource("core","find-downloadable-apps")
    val FIND_CATEGORIES = EventSource("core","find-categories")
    val FIND_PARENT_CATEGORIES = EventSource("core","find-parent-categories")
    val FIND_FEATURED_APPS = EventSource("core","find-featured-apps")
    val SESSION_TIMEOUT = EventSource("core","session-timeout")
    val FIND_ALL_AD_URLS = EventSource("core","find-all-adurls")
    val CREATE_UPDATE_AD_URLS = EventSource("core","create-or-update-adurls")
    val CACHE_IMAGE_UPLOAD = EventSource("core","cache-image-upload")
    val CREATE_APP_ICON = EventSource("core","create-or-recreate-application-icon")
    val CREATE_SCREEN_SHOT = EventSource("core","create-or-recreate-screenshot")
    val UPDATE_APP_DETAILS = EventSource("core","update-app-details")
    val UPDATE_APP_DETAILS_AND_CHARGING = EventSource("core","update-app-details-and-charging")
    val FIND_ALL_PARENT_CATEGORIES = EventSource("core","find-all-parent-categories")
    val CREATE_UPDATE_CATEGORIES = EventSource("core","create-or-update-categories")
    val CREATE_CATEGORIES = EventSource("core","create-categories")
    val CREATE_CATEGORY = EventSource("core","create-category")
    val EDIT_CATEGORY= EventSource("core","edit-category-name")
    val REMOVE_CATEGORY = EventSource("core","remove-category")
    val ADD_BANNER = EventSource("core","add-banner")
    val EDIT_BANNER = EventSource("core","edit-banner")
    val GET_BANNERS = EventSource("core","get-banners")
    val GET_BANNER_LOCATIONS = EventSource("core","get-banner-locations")
    val GET_BANNERS_COUNT = EventSource("core","get-banners-count")
    val REMOVE_BANNER = EventSource("core","remove-banner")
    val ADD_APPS_TO_CATEGORY = EventSource("core","add-apps-to-category")
    val REMOVE_APPS_FROM_CATEGORY = EventSource("core","remove-apps-from-category")
    val CREATE_UPDATE_PARENT_CATEGORIES = EventSource("core","create-or-update-parent-categories")
    val FIND_COMMENTS = EventSource("core","find-comments")
    val FIND_COMMENTS_WITH_USER_IMAGE = EventSource("core","find-comments-with-user-image")
    val UPDATE_COMMENTS = EventSource("core","update-comments")
    val ADD_RATING = EventSource("core", "add-rating")
    val APP_DETAILS = EventSource("core","app-details")
    val ERROR_HANDLING = EventSource("core","error-handling")
    val SESSION_VALIDATION = EventSource("core","session-validation")
    val SEARCH_APPS = EventSource("core","search-apps")
    val SEARCH_APPS_EXCLUDING_CATEGORY = EventSource("core","search-apps-excluding-category")
    val SEARCH_RELATED_APPS = EventSource("core","search-related-apps")
    val EDIT_CHARGING_AMOUNT = EventSource("core","edit-charging-amount")
    val FIND_CHARGING_DETAILS = EventSource("core","find-charging-details")
    val FIND_CHARGING_DESCRIPTION_MESSAGE_FORMAT = EventSource("core","find-charging-details")
  }

  case object SUBSCRIPTION{
    val REG = EventSource("subscription-connector", "reg")
    val UN_REG = EventSource("subscription-connector","un-reg")
    val APP_LIST = EventSource("subscription-connector","app-list")
    val SUBSCRIPTION_STATUS = EventSource("subscription-connector","subscription-status")
    val SUBSCRIPTION_COUNT = EventSource("subscription-connector","subscription-count")
  }

  case object CMS{
    val CMS_SERVICE_DOWNLOAD = EventSource("cms-connector", "cms-service-download")
    val CMS_SERVICE_DOWNLOAD_CHARGE = EventSource("cms-connector", "cms-service-download-charge")
    val QUERY_CONTENT_DETAILS = EventSource("cms-connector","query-content-details")
  }

  case object GOVERNANCE{
    val REPORT = EventSource("governance-connector", "report")
  }

  case object CAAS{
    val PAYMENT_INSTRUMENTS = EventSource("caas-connector", "payment-instruments")
  }

  case object COMMON_REG{
    val AUTHENTICATE_BY_PASSWORD = EventSource("common-reg-connector", "authenticate-by-password")
    val AUTHENTICATE_BY_MPIN = EventSource("common-reg-connector", "authenticate-by-mpin")
    val AUTHENTICATE_BY_MSISDN = EventSource("common-reg-connector", "authenticate-by-msisdn")
    val USER_DETAILS_BY_USERNAME = EventSource("common-reg-connector", "user-details-by-username")
    val USER_PROFILE_UPDATE = EventSource("common-reg-connector", "user-profile-update")
    val USER_SESSION_VALIDATE = EventSource("common-reg-connector", "user-session-validate")
    val CHANGE_MPIN_BY_MSISDN = EventSource("common-reg-connector", "change-mpin-by-msisdn")
    val USER_DETAILS_BY_MSISDN = EventSource("common-reg-connector", "user-details-by-msisdn")
    val CREATE_CONSUMER_USER = EventSource("common-reg-connector", "create-consumer-user")
    val VERIFY_MSISDN = EventSource("common-reg-connector","verify-msisdn")
    val REQUEST_NEW_VERIFICATION_CODE = EventSource("common-reg-connector","request-new-verification-code")
    val RECOVER_USER_ACCOUNT = EventSource("common-reg-connector","recover-user-account")
    val CREATE_AUTO_LOGIN_CONSUMER_USER = EventSource("common-reg-connector", "create-auto-login-consumer-user")
    val RESET_USER_PASSWORD = EventSource("common-reg-connector", "reset-user-password")
  }

  case object NOTIFICATION_SERVER{
    val DEVICE_REGISTER = EventSource("notification-server", "register-device")
    val FIND_MESSAGES = EventSource("notification-server", "find-messages")
  }

  case object NOTIFICATION_ADMIN{
    val NOTIFY_ALL = EventSource("notification-admin", "notify-all")
    val NOTIFY_APP_SPECIFIC = EventSource("notification-admin", "notify-app-specific")
    val NOTIFY_APP_UPDATE = EventSource("notification-admin", "notify-app-update")
  }
}