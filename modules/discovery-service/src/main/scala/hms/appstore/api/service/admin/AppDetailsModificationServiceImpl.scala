package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import com.googlecode.concurrentlinkedhashmap.ConcurrentLinkedHashMap
import hms.appstore.api.json._
import hms.appstore.api.service._
import hms.appstore.api.service.Futures._

import query.AppFindService
import scala.concurrent.Future
import hms.appstore.api.repo._
import hms.appstore.api.json.ImageUploadReq
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.UpdateReq
import java.io.File
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.json.AppStatus.AppStatus
import hms.appstore.api.json.ImageUploadReq
import hms.appstore.api.json.UpdateAppAndChargingReq
import hms.appstore.api.domain.AppStoreApp
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.UpdateReq
import scala.Some
import hms.appstore.api.repo.AppUpdateCmd

class AppDetailsModificationServiceImpl(implicit val bindingModule: BindingModule) extends AppDetailsModificationService
with Injectable
with EventLogging {


  private val cache = new ConcurrentLinkedHashMap.Builder[String, ImageUploadReq]().initialCapacity(10)
    .maximumWeightedCapacity(1000)
    .build()


  private val appRepo = inject[AppRepo]
  private val chargingRepo = inject[ChargingRepo]
  private val categoryRepo = inject[CategoryRepo]
  private val appFindService = inject[AppFindService]
  private val sysConfigService = inject[SysConfigService]
  private val validation = inject[ImageValidationService]

  /**
   * Cache the ImageUploadReq in memory, so it can be process later by [[hms.appstore.api.service.admin.AppDetailsModificationService.updateDetails]]
   * @param req  image upload request
   * @return
   */
  def cacheImageUpload(req: ImageUploadReq)(implicit sc: ServiceContext): QueryResult[ImageUploadResp] = {
    import sc._

    eventLog(correlationId, EventSource.CORE.CACHE_IMAGE_UPLOAD, req)

    val status = validation.validate(req)

    val resp = if (status.isSuccess) {
      val success = ImageUploadResp.success()
      cache.put(success.uid, req)
      QueryResult(success)
    } else {
      QueryResult(status, ImageUploadResp.failure())
    }

    eventLog(correlationId, EventSource.CORE.CACHE_IMAGE_UPLOAD, resp)

    resp
  }

  /**
   * Process an app update details request, this might be called after [[hms.appstore.api.service.admin.AppDetailsModificationService.cacheImageUpload]] call.
   *
   * action:
   * 1. collect all icon, screen-shot images from image upload caches if available
   * and delete old files if needed and recreate new files
   * (this has to be done inside $TOMCAT_HOME/webapps/appstore-web/WEB-INF/images/applications/icons/<app-name>.jpg)
   * Note: Have soft links /hms/data/discovery-api/images ..>   $TOMCAT_HOME/webapps/appstore-web/WEB-INF/images
   *
   * 2. and update the modified app fields in the db
   * @param req  app update request
   * @return
   */
  @deprecated
  def updateDetails(req: UpdateReq)(implicit sc: ServiceContext): Future[StatusCode] = {

    import sc._
    import com.mongodb.casbah.query.Imports._


    def mkCaption(filePath: String): String = {
      filePath.split("/").toList.last.split("_").toList.reverse match {
        case suffix :: "screenshot" :: "mobile" :: tail => "Mobile Screen Shot " + suffix.charAt(0)
        case suffix :: "screenshot" :: tail => "Screen Shot " + suffix.charAt(0)
        case _  => "Screen Shot"
      }
    }


    def createImageOnDisk(appName: String, fileName: String, data: Array[Byte]) {
      IOUtil.createFile(sysConfigService.imageDirectoryPath, appName, fileName, data)
    }

    def updateIcon(app: QueryResult[AppStoreApp]) {
      req.appIcon.foreach {
        uid => Option(cache.get(uid)).foreach {
          upload => {
            val appName = app.getResult.name
            val fileName = s"$appName.${upload.fileType.toString}"
            createImageOnDisk(appName, fileName, upload.data.toBytes)
            eventLog(correlationId, EventSource.CORE.CREATE_APP_ICON, fileName)
          }
        }
      }
    }

    def updateScreenShots(app: QueryResult[AppStoreApp]) {
      req.screenShots.foreach {
        case (imageId, uid) => {
          Option(cache.get(uid)).foreach {
            upload => {
              val appName = app.getResult.name
              val fileName = s"${appName}_${imageId}.${upload.fileType.toString}"
              createImageOnDisk(appName, fileName, upload.data.toBytes)
              eventLog(correlationId, EventSource.CORE.CREATE_SCREEN_SHOT, fileName)
            }
          }
        }
      }
    }

    eventLog(correlationId, EventSource.CORE.UPDATE_APP_DETAILS, req)

    appFindService.findAppOrPendingApp(req.id).map {
      app => {

        updateIcon(app)

        updateScreenShots(app)

        val newScreenShotUrls = req.screenShots.map {
          case (imageId, uid) => {
            Option(cache.get(uid)).map {
              upload => {
                val appName = app.getResult.name
                val fileName = s"${appName}_${imageId}.${upload.fileType.toString}"
                sysConfigService.imagePathPrefix + "/" + appName + "/" + fileName
              }
            }
          }
        }.filter(_.isDefined).flatten.toSet


        val oldScreenShotUrls = app.getResult.screenShots.map(_.get("url")).filter(_.isDefined).flatten.toSet
        val appScreenShots = (oldScreenShotUrls ++ newScreenShotUrls).map(url => Map("url" -> url, "caption" -> mkCaption(url))).toList

        val labelUpdate = req.labels match {
          case Nil => MongoDBObject.empty
          case ls => MongoDBObject("labels" -> ls)
        }

        val instructionUpdate = req.instructions match {
          case m if m.isEmpty => MongoDBObject.empty
          case _ => MongoDBObject("instructions" -> req.instructions)
        }

        val appScreenShotUpdate = appScreenShots match {
          case Nil => MongoDBObject.empty
          case _ => MongoDBObject("app_screenshots" -> appScreenShots)
        }

        val appIconUpdate = req.appIcon.map(uid => cache.get(uid)).map {
          upload => {
            val appName = app.getResult.name
            val fileName = s"$appName.${upload.fileType.toString}"
            sysConfigService.imagePathPrefix + "/" + appName + "/" + fileName
          }
        } match {
          case Some(p) => MongoDBObject("app_icon" -> p)
          case None => MongoDBObject.empty
        }

        val lastModifiedDateUpdate = MongoDBObject("modified_date" -> System.currentTimeMillis())

        //do we need to update app.name?
        val updatedFields = req.category.fold(MongoDBObject.empty)(s => MongoDBObject("category" -> MongoDBList(s))) ++
                            req.shortDesc.fold(MongoDBObject.empty)(s => MongoDBObject("short_description" -> s)) ++
                            req.description.fold(MongoDBObject.empty)(s => MongoDBObject("description" -> s)) ++
                            req.publishName.fold(MongoDBObject.empty)(s => MongoDBObject("publish-name" -> s)) ++
                            appIconUpdate ++ appScreenShotUpdate ++ labelUpdate ++ instructionUpdate ++ lastModifiedDateUpdate


        appRepo.update(
            AppUpdateCmd(
                Map("_id" -> req.id),
                Map("$set" -> updatedFields),
                appStatus = Some(app.getResult.status)
            )
        )

        val resp = S1000

        eventLog(correlationId, EventSource.CORE.UPDATE_APP_DETAILS, resp)

        resp
      }
    }
  }

  override def updateDetailsAndCharging(req: UpdateAppAndChargingReq)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._
    import com.mongodb.casbah.query.Imports._


    def mkCaption(filePath: String): String = {
      filePath.split("/").toList.last.split("_").toList.reverse match {
        case suffix :: "screenshot" :: "mobile" :: tail => "Mobile Screen Shot " + suffix.charAt(0)
        case suffix :: "screenshot" :: tail => "Screen Shot " + suffix.charAt(0)
        case _ => "Screen Shot"
      }
    }


    def createImageOnDisk(appName: String, fileName: String, data: Array[Byte]) {
      IOUtil.createFile(sysConfigService.imageDirectoryPath, appName, fileName, data)
    }

    def updateIcon(app: QueryResult[AppStoreApp]) {
      req.appIcon.foreach {
        uid => Option(cache.get(uid)).foreach {
          upload => {
            val appName = app.getResult.name
            val fileName = s"$appName.${upload.fileType.toString}"
            createImageOnDisk(appName, fileName, upload.data.toBytes)
            eventLog(correlationId, EventSource.CORE.CREATE_APP_ICON, fileName)
          }
        }
      }
    }

    def updateScreenShots(app: QueryResult[AppStoreApp]) {
      req.screenShots.foreach {
        case (imageId, uid) => {
          Option(cache.get(uid)).foreach {
            upload => {
              val appName = app.getResult.name
              val fileName = s"${appName}_${imageId}.${upload.fileType.toString}"
              createImageOnDisk(appName, fileName, upload.data.toBytes)
              eventLog(correlationId, EventSource.CORE.CREATE_SCREEN_SHOT, fileName)
            }
          }
        }
      }
    }

    eventLog(correlationId, EventSource.CORE.UPDATE_APP_DETAILS, req)

    appFindService.findAppOrPendingApp(req.id).map {
      app => {

        updateIcon(app)

        updateScreenShots(app)

        chargingRepo.editChargingAmount(Some(app.getResult.status), req.chargingDescription)

        val newScreenShotUrls = req.screenShots.map {
          case (imageId, uid) => {
            Option(cache.get(uid)).map {
              upload => {
                val appName = app.getResult.name
                val fileName = s"${appName}_${imageId}.${upload.fileType.toString}"
                sysConfigService.imagePathPrefix + "/" + appName + "/" + fileName
              }
            }
          }
        }.filter(_.isDefined).flatten.toSet


        val oldScreenShotUrls = app.getResult.screenShots.map(_.get("url")).filter(_.isDefined).flatten.toSet
        val appScreenShots = (oldScreenShotUrls ++ newScreenShotUrls).map(url => Map("url" -> url, "caption" -> mkCaption(url))).toList

        val labelUpdate = req.labels match {
          case Nil => MongoDBObject.empty
          case ls => MongoDBObject("labels" -> ls)
        }

        val instructionUpdate = req.instructions match {
          case m if m.isEmpty => MongoDBObject.empty
          case _ => MongoDBObject("instructions" -> req.instructions)
        }

        val appScreenShotUpdate = appScreenShots match {
          case Nil => MongoDBObject.empty
          case _ => MongoDBObject("app_screenshots" -> appScreenShots)
        }

        val appIconUpdate = req.appIcon.map(uid => cache.get(uid)).map {
          upload => {
            val appName = app.getResult.name
            val fileName = s"$appName.${upload.fileType.toString}"
            sysConfigService.imagePathPrefix + "/" + appName + "/" + fileName
          }
        } match {
          case Some(p) => MongoDBObject("app_icon" -> p)
          case None => MongoDBObject.empty
        }

        val lastModifiedDateUpdate = MongoDBObject("modified_date" -> System.currentTimeMillis())

        var categoriesList: List[String] = List.empty
        if(!req.category.isEmpty){
          categoriesList = req.category.get.split(",").toList
        }
        categoryRepo.editCategoriesOfApp(Some(app.getResult.status), req.id, categoriesList)

        //do we need to update app.name?
        val updatedFields = req.category.fold(MongoDBObject.empty)(s => MongoDBObject("category" -> s.split(",").toList)) ++
          req.shortDesc.fold(MongoDBObject.empty)(s => MongoDBObject("short_description" -> s)) ++
          req.description.fold(MongoDBObject.empty)(s => MongoDBObject("description" -> s)) ++
          req.publishName.fold(MongoDBObject.empty)(s => MongoDBObject("publish-name" -> s)) ++
          appIconUpdate ++ appScreenShotUpdate ++ labelUpdate ++ instructionUpdate ++ lastModifiedDateUpdate

        appRepo.update(
          AppUpdateCmd(
            Map("_id" -> req.id),
            Map("$set" -> updatedFields),
            appStatus = Some(app.getResult.status)
          )
        )

        val resp = S1000

        eventLog(correlationId, EventSource.CORE.UPDATE_APP_DETAILS, resp)

        resp
      }
    }
  }
}
