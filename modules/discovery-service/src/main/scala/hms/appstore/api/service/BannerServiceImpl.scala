package hms.appstore.api.service

import com.escalatesoft.subcut.inject._


import hms.appstore.api.json._
import hms.appstore.api.repo._
import hms.appstore.api.service.Futures._

import scala.concurrent.Future

/**
 * Created by kasun on 3/20/18.
 */

class BannerServiceImpl(implicit val bindingModule: BindingModule) extends BannerService
with Injectable
with SalatMapper
with EventLogging {

  private val bannerRepo = inject[BannerRepo]

  def find(bannerLocation: String)(implicit sc: ServiceContext): Future[QueryResults[Banner]] = {
    future {
      val banners = bannerRepo.getBannersByLocation(bannerLocation).map(asObject[Banner](_))
      QueryResults(banners)
    }
  }
}
