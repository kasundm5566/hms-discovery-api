package hms.appstore.api.service.admin

import concurrent.Future
import hms.appstore.api.json.StatusCode
import hms.appstore.api.service.ServiceContext


trait FeaturedAppMgmtService {
  def update(positions: Map[String, Int])(implicit sc: ServiceContext): Future[StatusCode]
}
