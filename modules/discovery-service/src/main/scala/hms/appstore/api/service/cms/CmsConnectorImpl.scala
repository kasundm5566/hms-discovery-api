/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.service.cms

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}
import hms.appstore.api.json._
import hms.appstore.api.service.{EventSource, ServiceContext, ServiceFutureFilter}
import hms.scala.http.{HttpConnectorConfig, DefaultHttpClient}
import hms.scala.http.marshalling.HttpMarshalling
import scala.concurrent.Future

class CmsConnectorImpl(implicit val bindingModule: BindingModule) extends CmsConnector
  with ServiceFutureFilter
  with Injectable {

  import MsgJsonProtocol._
  import HttpMarshalling._

  protected val eventSource = EventSource.CMS

  private val connector = new DefaultHttpClient(
    HttpConnectorConfig("cmsConnector", "http", inject[String]("cms.host"), inject[Int]("cms.port"))
  )

  def findPlatforms(query: PlatformQuery)(implicit sc: ServiceContext) = {
    val path = "/cms/cms-service/query/content-details"
    val result = connector.doPost[PlatformQuery, PlatformResults](path, query)
    filterWithIgnoreErrors(eventSource.QUERY_CONTENT_DETAILS, path, result, PlatformResults("S1000", Some("Success"), Some(Map.empty)))
  }

  def downloadApp(req: DownloadReq)(implicit sc: ServiceContext) = {
    val path = "/cms/cms-service/download"
    val result = connector.doPost[DownloadReq, DownloadResp](path, req.copy(address = "tel:" + req.address))
    filterWithHttpSuccess(eventSource.CMS_SERVICE_DOWNLOAD, path, result)
  }

  def downloadAppWithCharge(req: DownloadChargeReq)(implicit sc: ServiceContext): Future[DownloadChargeResp] = {
    val path = "/cms/cms-service/download/charge"
    val result = connector.doPost[DownloadChargeReq, DownloadChargeResp](path, req)
    filterWithHttpSuccess(eventSource.CMS_SERVICE_DOWNLOAD_CHARGE, path, result)
  }

  def appDownloadsCount(req: AppDownloadsCountReq)(implicit sc: ServiceContext): Future[AppDownloadsCountResp]={
    val path = "/cms/cms-service/app/downloads-count"
    val result=connector.doPost[AppDownloadsCountReq, AppDownloadsCountResp](path,req)
    result
  }
}
