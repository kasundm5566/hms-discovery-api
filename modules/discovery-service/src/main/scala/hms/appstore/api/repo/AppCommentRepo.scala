/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.api.repo

import com.mongodb.casbah.query.Imports._

trait AppCommentRepo {

  def create(dbObject: DBObject)

  def find(appId: String, start: Int, limit: Int): List[DBObject]

  def remove(commentId : String)

}
