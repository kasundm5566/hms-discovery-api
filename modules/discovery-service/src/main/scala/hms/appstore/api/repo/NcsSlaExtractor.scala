package hms.appstore.api.repo

import com.mongodb.casbah.query.Imports._
import hms.appstore.api.domain._

class NcsSlaExtractor(i18nResource: I18nResource)(current: DBObject) {
  private lazy val ncsSla = new NcsSlas(current.getAsOrElse[DBObject]("ncs-slas", DBObject.empty))
  private lazy val lbl = ChargingLabel(ncsSla)

  private val isSub = (c: ChargingSla) => c.ncsType == "subscription"
  private val isDwn = (c: ChargingSla) => c.ncsType == "downloadable"
  private val isFree = (c: Charging) => c.chargingType == "free"

  lazy val ncs: Set[String] = ncsSla.ncses

  lazy val subscription: Boolean = ncsSla.chargingSlas.exists(isSub)

  lazy val downloadable: Boolean = ncsSla.chargingSlas.exists(isDwn)

  def chargingLabel(cur: String): String =  {
    List(
      lbl.label,
      " " ,
      lbl.amount.map(a => cur + " %.2f".format(a.toFloat) + i18nResource.getString("charging.details.plus.taxes")).getOrElse("")
    ).mkString
  }

  def chargingDetails(curr: String): String = {


    def buildMsgChargingDetails(ms: List[MessagingSla]): String = {
      def smsChargingDetails: List[String] = {
        ms.collect{case m if m.ncsType == "sms"=>
          m.mo.map(s => i18nResource.getString("charging.details.sms.mo").format(curr, s.charging.amount.getOrElse("0").toFloat)) ++
          m.mt.map(s => i18nResource.getString("charging.details.sms.mt").format(curr, s.charging.amount.getOrElse("0").toFloat))
        }.flatten.toList
      }

      def wapPushChargingDetails: List[String] = {
        ms.collect{case m if m.ncsType == "wap-push" =>
          m.mo.map(s => i18nResource.getString("charging.details.wap.push.mo").format(curr, s.charging.amount.getOrElse("0").toFloat)) ++
            m.mt.map(s => i18nResource.getString("charging.details.wap.push.mt").format(curr, s.charging.amount.getOrElse("0").toFloat))
        }.flatten.toList
      }

      def ussdChargingDetails: List[String] = {
        ms.find(_.ncsType == "ussd").flatMap{m =>
          m.mo.map(s => i18nResource.getString("charging.details.ussd.menu").format(curr, s.charging.amount.getOrElse("0").toFloat))
        }.toList
      }

      (smsChargingDetails ++ wapPushChargingDetails ++ ussdChargingDetails).mkString("\n")
    }

    def buildSubChargingDetails(sub: ChargingSla): String = {
      i18nResource.getString("charging.details.subscription").format(sub.charging.frequency.getOrElse("default").capitalize, curr, sub.charging.amount.getOrElse("0").toFloat)
    }

    def buildDwnChargingDetails(dwn: ChargingSla): String = {
      i18nResource.getString("charging.details.download").format(curr, dwn.charging.amount.getOrElse("0").toFloat)
    }

    val chargingSlas = ncsSla.chargingSlas.filter(s => !isFree(s.charging))
    val messagingSlas = ncsSla.messagingSlas.map{
      m => m.copy(
        mo = m.mo.filter(s1 => !isFree(s1.charging)),
        mt = m.mt.filter(s2 => !isFree(s2.charging))
      )
    }.filter(s => s.mo.isDefined || s.mt.isDefined)

    val downloadable = chargingSlas.filter(isDwn).headOption
    val subscription = chargingSlas.filter(isSub).headOption

    (messagingSlas , subscription, downloadable) match {
      case (Nil, None, None) => i18nResource.getString("charging.details.free")
      case (Nil, Some(sub), None) => buildSubChargingDetails(sub)
      case (Nil, None, Some(dwn)) => buildDwnChargingDetails(dwn)
      case (Nil, Some(sub), Some(dwn)) => List(buildDwnChargingDetails(dwn), buildSubChargingDetails(sub)).mkString("\n")
      case (ms, None, None) => buildMsgChargingDetails(ms)
      case (ms, Some(sub), None) => List(buildSubChargingDetails(sub), buildMsgChargingDetails(ms)).mkString("\n")
      case (ms, None, Some(dwn)) => List(buildDwnChargingDetails(dwn), buildMsgChargingDetails(ms)).mkString("\n")
      case (ms, Some(sub), Some(dwn)) => List(buildDwnChargingDetails(dwn), buildSubChargingDetails(sub), buildMsgChargingDetails(ms)).mkString("\n") // this is wired?
    }
  }

}