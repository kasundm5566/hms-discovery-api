package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.NewBindingModule

import hms.appstore.api.repo.RepoModule
import hms.appstore.api.service._
import query.QueryServiceModule
import hms.appstore.api.service.provisioning.ProvisioningServiceModule

object AdminServiceModule extends NewBindingModule({
  implicit module =>
    module <~ BaseServiceModule
    module <~ RepoModule
    module <~ QueryServiceModule
    module <~ ProvisioningServiceModule

    module.bind[ImageValidationService] toSingle new ImageValidationServiceImpl
    module.bind[AppDetailsModificationService] toSingle new AppDetailsModificationServiceImpl
    module.bind[AppStateMgmtService] toSingle new AppStateMgmtServiceImpl
    module.bind[FeaturedAppMgmtService] toSingle new FeaturedAppMgmtServiceImpl
    module.bind[CategoryConfigService] toSingle new CategoryConfigServiceImpl
    module.bind[BannerConfigService] toSingle new BannerConfigServiceImpl
    module.bind[ChargingConfigService] toSingle new ChargingConfigServiceImpl
    module.bind[AdUrlConfigService] toSingle new AdUrlConfigServiceImpl
})
