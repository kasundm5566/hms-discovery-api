package hms.appstore.api.service

import util.{Failure, Success}
import util.control.NonFatal
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.util.Try

object Futures {

  class PromiseCompletingRunnable[T](correlationId: CorrelationId, body: => T) extends Runnable {
    val promise = Promise[T]()

    override def run() {
      promise complete {
        try Success(body) catch {
          case NonFatal(e) => e match {
            case e: ServiceException => Failure(e)
            case _ => Failure(new ServiceException(correlationId, E5000, EventSource.CORE.ERROR_HANDLING, Some(e)))
          }
        }
      }
    }
  }

  def apply[T](body: => T)(implicit exec: ServiceContext): Future[T] = {
    val runnable = new PromiseCompletingRunnable(exec.correlationId, body)
    exec.executionContext.execute(runnable)
    runnable.promise.future
  }

  def future[T](body: T)(implicit exec: ServiceContext): Future[T] =  Futures(body)

  def futureToFutureTry[T](f: Future[T])(implicit xc: ExecutionContext = ExecutionContext.global): Future[Try[T]] =
    f.map(Success(_)).recover({case e => Failure(e)})
}
