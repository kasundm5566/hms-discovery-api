package hms.appstore.api.service.admin

import com.escalatesoft.subcut.inject.{Injectable, BindingModule}

import hms.appstore.api.json._
import hms.appstore.api.repo._
import hms.appstore.api.service._
import hms.appstore.api.service.Futures._

import concurrent.Future
import hms.appstore.api.service.ServiceContext
import hms.appstore.api.json.ParentCategory
import hms.appstore.api.json.Category


class CategoryConfigServiceImpl(implicit val bindingModule: BindingModule) extends CategoryConfigService
with Injectable
with BaseSalatMapper
with EventLogging {

  private val categoryRepo = inject[CategoryRepo]


  def findAllParentCategories(implicit sc: ServiceContext): Future[QueryResults[ParentCategory]] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.FIND_ALL_PARENT_CATEGORIES, "")


      val parentCategories = categoryRepo.findParentCategories(filter = false).map(c => asObject[ParentCategory](c))

      val resp = QueryResults(parentCategories)

      eventLog(correlationId, EventSource.CORE.FIND_ALL_PARENT_CATEGORIES, resp)

      resp
    }
  }


  def createOrUpdateCategories(cs: List[Category])(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.CREATE_UPDATE_CATEGORIES, cs)

      cs.foreach(c => categoryRepo.createOrUpdateCategory(c.name, asDBObject[Category](c)))

      val resp = S1000
      eventLog(correlationId, EventSource.CORE.CREATE_UPDATE_CATEGORIES, resp)

      resp
    }
  }

  def createCategory(cs: CategoryAddReq)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.CREATE_CATEGORY, cs)

      categoryRepo.createCategory(asDBObject[CategoryAddReq](cs))

      val resp = S1000
      eventLog(correlationId, EventSource.CORE.CREATE_CATEGORY, resp)

      resp
    }
  }

  def editCategory(id: String, req: CategoryEditReq)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.EDIT_CATEGORY, req)

      val status = categoryRepo.editCategory(id, req)
      val resp = S1000

      if (status == 0) {
        eventLog(correlationId, EventSource.CORE.EDIT_CATEGORY, "cannot find id [" + id + "] in categories.")
      } else {
        eventLog(correlationId, EventSource.CORE.EDIT_CATEGORY, resp)
      }
      resp
    }
  }

  def removeCategory(id: String)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._
    future {
      eventLog(correlationId, EventSource.CORE.REMOVE_CATEGORY, "id")
      val resp = S1000
      val status = categoryRepo.removeCategory(id)
      if (status == 0) {
        eventLog(correlationId, EventSource.CORE.REMOVE_CATEGORY, "cannot find category id [" + id + "].")
      } else {
        eventLog(correlationId, EventSource.CORE.REMOVE_CATEGORY, "category id [" + id + "] removed.")
        eventLog(correlationId, EventSource.CORE.REMOVE_CATEGORY, resp)
      }
      resp
    }
  }

  def addOrRemoveAppsOfCategory(id: String, req: AddOrRemoveAppsOfCategoryReq)(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._
    future {
      eventLog(correlationId, EventSource.CORE.ADD_APPS_TO_CATEGORY, "Updating apps of the category [" + id + "]")
      eventLog(correlationId, EventSource.CORE.ADD_APPS_TO_CATEGORY, "Updating apps of the category request " + req)
      val resp = S1000
      categoryRepo.addOrRemoveAppsOfCategory(id, req)

      eventLog(correlationId, EventSource.CORE.ADD_APPS_TO_CATEGORY, resp)
      resp
    }
  }

  def createOrUpdateParentCategories(pcs: List[ParentCategory])(implicit sc: ServiceContext): Future[StatusCode] = {
    import sc._

    future {
      eventLog(correlationId, EventSource.CORE.CREATE_UPDATE_PARENT_CATEGORIES, pcs)

      pcs.foreach {
        pc => categoryRepo.createOrUpdateParentCategory(pc.id, asDBObject[ParentCategory](pc))
      }

      val resp = S1000
      eventLog(correlationId, EventSource.CORE.CREATE_UPDATE_PARENT_CATEGORIES, pcs)

      resp
    }

  }
}
