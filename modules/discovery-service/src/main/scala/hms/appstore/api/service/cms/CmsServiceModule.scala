package hms.appstore.api.service.cms

import com.escalatesoft.subcut.inject.NewBindingModule
import com.typesafe.config.ConfigFactory

import hms.scala.http.util.ConfigUtils
import hms.appstore.api.service.auth.AuthServiceModule
import hms.appstore.api.repo.RepoModule

object CmsServiceModule extends NewBindingModule({
  implicit module =>

    module <~ RepoModule
    module <~ AuthServiceModule

    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "nbl.connector.cms")

    module.bind [String] idBy "cms.host" toSingle conf.getString("host")
    module.bind [Int] idBy "cms.port" toSingle conf.getInt("port")

    module.bind [CmsConnector] toSingle new CmsConnectorImpl
    module.bind [DownloadService] toSingle new DownloadServiceImpl
})
