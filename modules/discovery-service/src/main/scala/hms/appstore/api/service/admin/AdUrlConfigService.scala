package hms.appstore.api.service.admin

import hms.appstore.api.json.{QueryResults, StatusCode, AdUrl}
import hms.appstore.api.service.ServiceContext
import concurrent.Future

trait AdUrlConfigService {

  def findAll(implicit sc: ServiceContext): Future[QueryResults[AdUrl]]

  def createOrUpdate(urls: List[AdUrl])(implicit sc: ServiceContext): Future[StatusCode]
}
