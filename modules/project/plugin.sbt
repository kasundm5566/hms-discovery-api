import sbt._
import Keys._

resolvers += "hms releases" at "http://archiva.hsenidmobile.com/repository/internal"

addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.3.0")

addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.7.0")

addSbtPlugin("hms.sbt.plugins" % "sbt-jsw-plugin" % "0.2.3")

addSbtPlugin("hms.sbt.plugin" % "sbt-profile-plugin" % "0.1.3")
