import sbt._
import Keys._
import SbtJswPlugin._
import scala.Some
import SbtProfilePlugin._
import SbtProfilePlugin.SbtProfileKeys._

object DiscoveryApiBuild extends Build {
  val ProjectVersion = "vdf-1.0.65-SNAPSHOT"
  val Organization   = "hms.appstore.api"

  val ScalaVersion = "2.10.0"
  val ScalaBinaryVersion = "2.10"
  val AkkaVersion  = "2.2.3"
  val SprayVersion = "1.2.0"
  val LiftVersion  = "2.5"
  val SalatVersion = "1.9.2"

  val Akka            = "com.typesafe.akka" %% "akka-actor" % AkkaVersion
  val AkkaProvided    = "com.typesafe.akka" %% "akka-actor" % AkkaVersion %  "provided->default"
  val AkkaSlf4j       = "com.typesafe.akka" %% "akka-slf4j" % AkkaVersion

  val JodaTime        = "joda-time" % "joda-time"    % "2.1"
  val JodaConvert     = "org.joda"  % "joda-convert" % "1.2"

  val Subcut          = "com.escalatesoft.subcut" %% "subcut" % "2.0"

  val TypesafeConfig  = "com.typesafe" % "config"              % "1.0.0"
  val TypesafeLogging = "com.typesafe" %% "scalalogging-slf4j" % "1.0.1"

  val Salat           = "com.novus" %% "salat-core" % SalatVersion
  val SalatUtil       = "com.novus" %% "salat-util" % SalatVersion

  val HmsScalaUtil    = "hms.scala.util" %% "hms-scala-util"     % "1.0.0"
  val AsynchHtpClient = "hms.scala.http" %% "asynch-http-client" % "1.0.7"
  val HmsCommonUtil    = "hms.common"     % "hms-common-util"     % "1.0.6"

  val LiftJson        = "net.liftweb" %% "lift-json"    % LiftVersion % "test->default" // to test spray json serialization using different library
  val LiftMongo       = "net.liftweb" %% "lift-mongodb" % LiftVersion // to have xpath like querying on SDP SLA nested maps

  val SprayServlet    = "io.spray" % "spray-servlet" % SprayVersion
  val SprayRouting    = "io.spray" % "spray-routing" % SprayVersion
  val SprayCaching    = "io.spray" % "spray-caching" % SprayVersion
  val SprayTestKit    = "io.spray" % "spray-testkit" % SprayVersion % "test->default"

  val SprayJetty      = "org.eclipse.jetty.orbit" % "javax.servlet" % "3.0.0.v201112011016" artifacts Artifact("javax.servlet", "jar", "jar")
  val SprayJettyWeb   = "org.eclipse.jetty"       % "jetty-webapp"  % "8.1.7.v20120910"

  val ServletApi      = "javax.servlet" % "servlet-api"   % "2.5"   % "provided->default"
  val Slf4jLog4j      = "org.slf4j"     % "slf4j-log4j12" % "1.7.2"


  val jsonDependencies = Seq(
    AkkaProvided,
    JodaTime,
    JodaConvert,
    SalatUtil,
    AsynchHtpClient,
    LiftJson
  )

  val serviceDependencies = Seq(
    Akka,
    Slf4jLog4j,
    Subcut,
    Salat,
    LiftMongo,
    TypesafeConfig,
    TypesafeLogging,
    HmsScalaUtil,
    SprayCaching
  )

  val serverDependencies = Seq(
    AkkaSlf4j,
    SprayServlet,
    SprayRouting,
    SprayTestKit,
    SprayJetty,
    SprayJettyWeb,
    HmsCommonUtil
  )

  val clientDependencies = Seq(
    Akka,
    Slf4jLog4j,
    Subcut,
    TypesafeConfig,
    TypesafeLogging
  )


  val testDependencies = Seq(
    "org.scalamock" %% "scalamock-specs2-support" % "3.0.1" % "test",
    "hms.specs" %% "specs-matchers" % "0.1.0" % "test"
  )


  val dependencyGraphSettings = net.virtualvoid.sbt.graph.Plugin.graphSettings

  val AkkaReleases = "akka releases" at "http://repo.akka.io/releases"
  val AkkaSnapshots = "akka releases" at "http://repo.akka.io/snapshots"

  val HMSReleases =  "hms archiva internal" at "http://archiva.hsenidmobile.com/repository/internal"
  val HMSSnapshots = "hms archiva snapshots" at "http://archiva.hsenidmobile.com/repository/snapshots"

  val SprayReleases = "spray releases" at "http://repo.spray.io/"
  val SprayNightlies = "spray repo" at "http://nightlies.spray.io/"

  val SonatypeReleases = "sonatype releases" at "https://oss.sonatype.org/content/groups/public"
  val SonatypeSnapshots = "sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

  val UseOnlyStdRepositories = false

  val IvyCredentialFile = if (ProjectVersion.endsWith("SNAPSHOT")) {
    Path.userHome / ".ivy2" / ".credentials-snapshot"
  } else {
    Path.userHome / ".ivy2" / ".credentials-release"
  }

  val moduleLookupConfigurations = if (!UseOnlyStdRepositories) {
    Seq(
      ModuleConfiguration("com.escalatesoft.subcut", SonatypeReleases),
      ModuleConfiguration("io.spray", SprayReleases),
      ModuleConfiguration("com.novus", SonatypeReleases),
      ModuleConfiguration("hms.specs", HMSReleases),
      ModuleConfiguration("hms.common", HMSReleases),
      ModuleConfiguration("hms.scala.util", HMSReleases),
      ModuleConfiguration("hms.scala.http", HMSReleases)
    )
  } else Seq.empty

  val excludedFilesInJar: NameFilter = (s: String) => """(.*?)\.(properties|props|conf|dsl|txt|xml)$""".r.pattern.matcher(s).matches

  lazy val baseSettings = Defaults.defaultSettings ++ profileSettings ++ dependencyGraphSettings ++ Seq(
    buildProfile := "vodafone",
    buildProfiles := Seq("dialog", "vodafone", "robi-bd"),
    version := ProjectVersion,
    organization := Organization,
    scalaVersion := ScalaVersion,
    scalaBinaryVersion := ScalaBinaryVersion,
    scalacOptions += "-deprecation",
    scalacOptions += "-unchecked",
    moduleConfigurations ++= moduleLookupConfigurations,
    initialize ~= { _ => sys.props("scalac.patmat.analysisBudget") = "1024" },
    publishTo <<= (version) { version: String =>
      val repo = "http://archiva.hsenidmobile.com/repository/"
      if (version.trim.endsWith("SNAPSHOT"))
        Some("Archiva Managed snapshots Repository" at repo + "snapshots/")
      else
        Some("Archiva Managed internal Repository" at repo + "internal/")
    },
    credentials += Credentials(IvyCredentialFile),
    parallelExecution in Test := false
  )

  lazy val modules = Project(id = "discovery-api", base = file("."),
    settings = baseSettings ++ Seq(
      name := "discovery-api"
    )
  ) aggregate(json, service, server, client)

  lazy val json = Project(id = "discovery-json", base = file("discovery-json"),
    settings = baseSettings ++ Seq(
      name := "discovery-json",
      libraryDependencies ++= jsonDependencies,
      libraryDependencies ++= testDependencies
    )
  )

  lazy val service = Project(id = "discovery-service", base = file("discovery-service"),
    settings = baseSettings ++ Seq(
      name := "discovery-service",
      libraryDependencies ++= serviceDependencies,
      libraryDependencies ++= testDependencies
    )
  ) dependsOn(json)

  lazy val server = Project(id = "discovery-server", base = file("discovery-server"),
    settings = baseSettings ++  jswPluginSettings ++ Seq(
      name := "discovery-server",
      libraryDependencies ++= serverDependencies,
      libraryDependencies ++= testDependencies,
      resourceDirectories in Compile <<= (resourceDirectories in Compile, baseDirectory, buildProfile) {
        (rd, bd, p) => {
          val resourceDir = bd / "src" / "main" / "profile" / p / "resources"
          if (resourceDir.exists) rd ++ Seq(resourceDir)
          else rd
        }
      },
      excludeFilter := excludedFilesInJar,
      jswMainClass := "hms.appstore.api.ServerMain",
      jswInitialHeapSizeInMB := 512,
      jswMaxHeapSizeInMB := 1536,
      jswJvmOptions :=
          "-XX:+UseParNewGC;" +
          "-XX:+UseConcMarkSweepGC;" +
          "-XX:MaxPermSize=256m;" +
          "-XX:+CMSClassUnloadingEnabled;" +
          "-verbose:gc;" +
          "-Xloggc:logs/gclog;" +
          "-DMONGO.POOLSIZE=40"
      ,
      jswSupportedPlatforms := Seq(
        "linux-x86-32", "linux-x86-64", "windows-x86-32"
      )
    )
  ) dependsOn(service)

  lazy val client = Project(id = "discovery-client", base = file("discovery-client"),
    settings = baseSettings ++ Seq(
      name := "discovery-client",
      libraryDependencies ++= clientDependencies,
      libraryDependencies ++= testDependencies
    )
  ) dependsOn(json)
}

