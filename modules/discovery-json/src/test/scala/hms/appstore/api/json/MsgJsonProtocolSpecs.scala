package hms.appstore.api.json

import org.specs2.mutable.Specification
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat

class MsgJsonProtocolSpecs extends Specification {
  "MsgJsonProtocol" should {
    import spray.json._
    import MsgJsonProtocol._

    "serialize an app" in {
      val app: Application = new ApplicationJson(id = "11",
        displayName = "funapp",
        category = "games",
        appIcon = "images/funapp.png",
        appBanners = None,
        usage = 1000,
        currency = "LKR",
        developer = "neo corp",
        rating = 4.5,
        rate_count = 0,
        labels = List.empty,
        description = "fun app",
        shortDesc = "fun",
        appTypes = List.empty,
        screenShots = List.empty,
        instructions = Map.empty,
        ncs = Set("sms", "mms"),
        subscription = false,
        downloadable = false,
        subscriptionStatus = SubscriptionStatus.NotAvailable,
        downloadStatus = Some(DownloadStatusJson("url-expired", DateTime.now(), "")),
        chargingLabel = "FREE",
        chargingDetails = "This is a free application",
        userComments = List(UserCommentReq(
          commentId=Some("123"),
          username=Some("hsenid2"),
          appId = "APP_000059",
          comments="application comment",
          dateTime=System.currentTimeMillis(),
          abuse=Some(false)
        )),
        status = AppStatus.Published,
        requestedDate = DateTime.now(),
        modifiedDate = DateTime.now(),
        remarks = "Published!",
        downloadableBinaries = Map.empty,
        downloadsCount = 0,
        subscriptionsCount = 0
      )

      val jsonStr = app.toJson.prettyPrint

      val jValue = JsonExtractor(jsonStr)

      jValue.extractOpt[String]("id") must beSome(app.id)

      jValue.extractOpt[String]("password") must beNone
      jValue.extractOpt[String]("ncs-sla") must beNone

      jValue.extractOpt[String]("name") must beSome(app.displayName)
      jValue.extractOpt[String]("app-icon") must beSome(app.appIcon)

      jValue.extractOpt[Long]("usage") must beSome(app.usage)

      jValue.extractOpt[String]("currency") must beSome(app.currency)
      jValue.extractOpt[String]("developer") must beSome(app.developer)


      jValue.extractOpt[Double]("rating") must beSome(app.rating)

      jValue.extractOpt[List[String]]("labels") must beSome(app.labels)

      jValue.extractOpt[String]("description") must beSome(app.description)
      jValue.extractOpt[String]("short-description") must beSome(app.shortDesc)

      jValue.extractOpt[List[Map[String, String]]]("app-screenshots") must beSome(app.screenShots)
      jValue.extractOpt[Map[String, String]]("instructions") must beSome(app.instructions)

      jValue.extractOpt[Set[String]]("ncs") must beSome(app.ncs)

      jValue.extractOpt[Boolean]("subscription") must beSome(app.subscription)
      jValue.extractOpt[Boolean]("downloadable") must beSome(app.downloadable)

      jValue.extractOpt[String]("subscription-status") must beSome(app.subscriptionStatus.toString)
      jValue.extractOpt[Map[String, String]]("download-status").get must haveKeys("download-status", "requested-date")
      jValue.extractOpt[String]("charging-label") must beSome(app.chargingLabel)
      jValue.extractOpt[String]("charging-details") must beSome(app.chargingDetails)
      jValue.extractOpt[String]("status") must beSome(app.status.toString)
      jValue.extractOpt[String]("requested-date") must beSome(ISODateTimeFormat.dateTime().print(app.requestedDate))
      jValue.extractOpt[String]("modified-date") must beSome(ISODateTimeFormat.dateTime().print(app.modifiedDate))

      jValue.extractOpt[List[UserCommentReq]]("user-comments") mustNotEqual(List.empty[UserCommentReq])

      jValue.extractOpt[Application#DnBinaries]("downloadable-binaries") must beSome(app.downloadableBinaries)

      println(jsonStr)

      success
    }

    "parse cms application/json" in {
      import spray.json._
      import MsgJsonProtocol._

      val source = """
                     |{
                     |   "statusCode":"S1000",
                     |   "result":{
                     |      "APP_000850":{
                     |         "devices":{
                     |            "HTC":[
                     |               {
                     |                  "model":"My Touch 3G",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               },
                     |               {
                     |                  "model":"My Touch 4G",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               }
                     |            ],
                     |            "Samsung":[
                     |               {
                     |                  "model":"Galaxy Nexus",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               },
                     |               {
                     |                  "model":"Galaxy S",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               },
                     |               {
                     |                  "model":"Galaxy S II",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               },
                     |               {
                     |                  "model":"Galaxy Y",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               }
                     |            ]
                     |         },
                     |         "platforms":{
                     |            "Android":[
                     |               {
                     |                  "version":"2.1",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               },
                     |               {
                     |                  "version":"3.0",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               },
                     |               {
                     |                  "version":"4.0",
                     |                  "content":[
                     |                     {
                     |                        "content-id":"1312100410310001",
                     |                        "version":"1"
                     |                     }
                     |                  ]
                     |               }
                     |            ]
                     |         }
                     |      }
                     |   },
                     |   "statusDescription":"Success"
                     |}
                   """.stripMargin

      val resp = source.asJson.convertTo[PlatformResults]

      resp.platformsByAppId("APP_000850") must not beEmpty

      success
    }


    "parse cms error application/json" in {
      import spray.json._
      import MsgJsonProtocol._

      val source = """
                     |{
                     |   "statusCode":"E1829",
                     |   "statusDescription" : "There are no approved downloadable files for search criteria"
                     |}
                   """.stripMargin

      val resp = source.asJson.convertTo[PlatformResults]
      resp.statusCode must beEqualTo("E1829")
      resp.statusDescription must beEqualTo(Some("There are no approved downloadable files for search criteria"))
      success
    }

    //TODO add test case to test application comments marshalling /unmarshalling
    "parse app details application/json" in {

      import spray.json._
      import MsgJsonProtocol._
      import hms.appstore.api.json.UserCommentReq

      val source =
        """
          |{
          |  "id": "11",
          |  "name": "funapp",
          |  "category": "games",
          |  "app-icon": "images/funapp.png",
          |  "usage": 1000,
          |  "currency": "LKR",
          |  "developer": "neo corp",
          |  "rating": 4.5,
          |  "labels": [],
          |  "description": "fun app",
          |  "short-description": "fun",
          |  "app-types": [],
          |  "app-screenshots": [],
          |  "instructions": {
          |
          |  },
          |  "ncs": ["sms", "mms"],
          |  "subscription": false,
          |  "downloadable": false,
          |  "subscription-status": "NOT_AVAILABLE",
          |  "download-status": {
          |    "download-status": "url-expired",
          |    "requested-date": "2014-01-13T14:11:27.558+05:30"
          |  },
          |  "charging-label": "FREE",
          |  "charging-details": "This is a free application",
          |  "user-comments": [{
          |    "username": "some user",
          |    "app-id": "APP_000059",
          |    "comments": "nice app",
          |    "date-time": 1389602487681
          |  }],
          |  "status": "Publish",
          |  "requested-date": "2014-01-13T14:11:27.682+05:30",
          |  "modified-date": "2014-01-13T14:11:27.682+05:30",
          |  "remarks": "Published!",
          |  "downloadable-binaries": {
          |
          |  }
          |}
          |
        """.stripMargin

      val resp = source.asJson.convertTo[Application]
      val comments = resp.userComments
      comments mustNotEqual(List.empty[UserCommentReq])

      comments.foreach(a =>
      {
        println(
          s"| appId : ${a.appId} " +
          s"| comment : ${a.comments} " +
          s"| abused : ${a.abuse.getOrElse(false)} " +
          s"| user : ${a.username.getOrElse("user")}"
        )
      })
    }

    "serialize an image upload" in {
      val upload = ImageUploadReq("screenshot_1", FileType.PNG, Icon, Base64String(Array[Byte](0x00, 0x00)))
      val uploadExtracted = upload.toJson.convertTo[ImageUploadReq]
      println(uploadExtracted)
      uploadExtracted must beEqualTo(upload)
      success
    }


    "serialize an user details by username req" in {
      val userDetailsByNameReq = UserDetailsByUsernameReq("hsenid2")
      println(userDetailsByNameReq.toJson.prettyPrint)
      success
    }
  }

}

case class JsonExtractor(source: String) {

  import net.liftweb.json._

  private implicit val formats = DefaultFormats

  private lazy val jValue = parse(source)

  def extractOpt[T: Manifest](path: String): Option[T] = {
    (jValue \ path).extractOpt[T]
  }
}
