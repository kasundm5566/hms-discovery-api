package hms.appstore.api.json

import org.specs2.mutable.Specification


class SubscriptionStatusResponseSpecs extends Specification{

  "Subscription Status " should {
    "Subscription status REGISTERED mapped to REGISTERED" in {
      val status = SubscriptionStatusResp("S1000", None, Some(SubscriptionStatus.Registered))
      status.getSubscriptionStatus must beEqualTo(SubscriptionStatus.Registered)
    }

    "Subscription status TRIAL mapped to REGISTERED" in {
      val status = SubscriptionStatusResp("S1000", None, Some(SubscriptionStatus.Trial))
      status.getSubscriptionStatus must beEqualTo(SubscriptionStatus.Registered)
    }

    "Subscription status UNREGISTERED mapped to UNREGISTERED" in {
      val status = SubscriptionStatusResp("S1000", None, Some(SubscriptionStatus.UnRegistered))
      status.getSubscriptionStatus must beEqualTo(SubscriptionStatus.UnRegistered)
    }

    "Subscription status BLOCKED mapped to UNREGISTERED" in {
      val status = SubscriptionStatusResp("S1000", None, Some(SubscriptionStatus.Blocked))
      status.getSubscriptionStatus must beEqualTo(SubscriptionStatus.UnRegistered)
    }

    "Subscription status PENDING CHARGE mapped to PENDING CHARGE" in {
      val status = SubscriptionStatusResp("S1000", None, Some(SubscriptionStatus.PendingCharge))
      status.getSubscriptionStatus must beEqualTo(SubscriptionStatus.PendingCharge)
    }

    "Subscription status REG_PENDING mapped to PENDING CHARGE" in {
      val status = SubscriptionStatusResp("S1000", None, Some(SubscriptionStatus.RegPending))
      status.getSubscriptionStatus must beEqualTo(SubscriptionStatus.PendingCharge)
    }

    "Subscription status TEMPORARY_BLOCKED mapped to PENDING CHARGE" in {
      val status = SubscriptionStatusResp("S1000", None, Some(SubscriptionStatus.TemporaryBlocked))
      status.getSubscriptionStatus must beEqualTo(SubscriptionStatus.PendingCharge)
    }

    "Subscription empty status mapped to NOT_AVAILABLE" in {
      val status = SubscriptionStatusResp("S1000", None, None)
      status.getSubscriptionStatus must beEqualTo(SubscriptionStatus.NotAvailable)
    }
  }
}
