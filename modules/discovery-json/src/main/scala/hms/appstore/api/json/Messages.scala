package hms.appstore.api.json

import com.novus.salat.annotations._
import org.joda.time.DateTime

import spray.json._
import MsgJsonProtocol._
import org.parboiled.common.Base64
import java.util.UUID
import spray.http.MultipartFormData

/**
 * Status message of the Discovery API
 */
trait StatusCode {
  def status: String

  def description: String

  /**
   * Extract status code form SXXX, EXXXX case objects!
   * @return
   */
  def asStatusCode = this

  def isSuccess: Boolean = status.trim match {
    case s if s.startsWith("S") => true
    case _ => false
  }
}

private[json] case class StatusCodeJson(status: String, description: String) extends StatusCode {
  override def toString: String = asStatusCode.toJson.prettyPrint
}


/**
 * ParentCategory
 *
 * represent the parent category (i.e: based on the applications' rating, created date, usage
 * the super category holds the application
 *
 * eg:
 * {
 * "id" : "panelRight",
 * "category" : "mostlyUsed",
 * }
 */
case class ParentCategory(@Key("_id") id: String, category: String) {
  override def toString: String = this.toJson.prettyPrint

  /**
   * Translate parent category id to name, since the name is not saved in the db.
   * @return
   */
  def name: String = id.split("\\.").map(_.capitalize).mkString(" ")
}

/**
 * Category
 *
 * represent the application category
 *
 * eg:
 * {
 * "id" : "1",
 * "name" : "Alert",
 * "description" : "Alert Applications",
 * "image_url":"/images/applications/icons/category/business-icon.jpg",
 * appIds: ["APP_0001", "APP_0002"]
 * }
 */
case class Category(@Key("_id") id: String, name: String, description: String, image_url: String, appIds: List[String], position: Int) {
  override def toString: String = this.toJson.prettyPrint
}

case class Banner(@Key("_id") id: Int, display_location: String, link: String, description: String, image_url: String) {
  override def toString: String = this.toJson.prettyPrint
}

trait UserComment {
  def commentId: Option[String]

  def username: Option[String]

  def image: Option[String]

  def appId: String

  def comments: String

  def dateTime: Long

  def abuse: Option[Boolean]
}


case class UserCommentReq(commentId: Option[String],
                          username: Option[String],
                          image: Option[String] = Option(""),
                          appId: String,
                          comments: String,
                          dateTime: Long,
                          abuse: Option[Boolean]) extends UserComment


/**
 * Application
 * represents the appstore application
 *
 * eg:
 * {
 * "id" : "APP_000018",
 * "labels" : [
 * "Business",
 * "Request",
 * ""
 * ],
 * "app-type" : [
 * "cas",
 * "sms",
 * "downloadable"
 * ],
 * .
 * .
 * .
 * "created-date" : "Oct 19, 2011 12:33:40 PM",
 * "category" : "Alert",
 * }
 *
 *
 */
trait Application {
  type DnBinaries = Map[String, List[Map[String, List[Map[String, String]]]]]

  def id: String

  def displayName: String

  def category: String

  def appIcon: String

  def appBanners: Option[List[Map[String, String]]]

  def usage: Long

  def currency: String

  def developer: String

  def rating: Double

  def rate_count: Int

  def labels: List[String]

  def description: String

  def shortDesc: String

  def appTypes: List[String]

  def screenShots: List[Map[String, String]]

  def instructions: Map[String, String]

  def ncs: Set[String]

  def subscription: Boolean

  def downloadable: Boolean

  def subscriptionStatus: SubscriptionStatus.SubscriptionStatus

  def downloadStatus: Option[DownloadStatus]

  def chargingLabel: String

  def chargingDetails: String

  def downloadableBinaries: DnBinaries

  def userComments: List[UserComment]

  def status: AppStatus.AppStatus

  def requestedDate: DateTime

  def modifiedDate: DateTime

  def remarks: String

  def downloadsCount: Int

  def subscriptionsCount: Int
}

private[json] class ApplicationJson(override val id: String,
                                    override val displayName: String,
                                    override val category: String,
                                    override val appIcon: String,
                                    override val appBanners: Option[List[Map[String, String]]],
                                    override val usage: Long,
                                    override val currency: String,
                                    override val developer: String,
                                    override val rating: Double,
                                    override val rate_count: Int,
                                    override val labels: List[String],
                                    override val description: String,
                                    override val shortDesc: String,
                                    override val appTypes: List[String],
                                    override val screenShots: List[Map[String, String]],
                                    override val instructions: Map[String, String],
                                    override val ncs: Set[String],
                                    override val subscription: Boolean,
                                    override val downloadable: Boolean,
                                    override val subscriptionStatus: SubscriptionStatus.SubscriptionStatus,
                                    override val downloadStatus: Option[DownloadStatus],
                                    override val chargingLabel: String,
                                    override val chargingDetails: String,
                                    override val userComments: List[UserComment],
                                    override val status: AppStatus.AppStatus,
                                    override val requestedDate: DateTime,
                                    override val modifiedDate: DateTime,
                                    override val remarks: String,
                                    override val downloadableBinaries: Application#DnBinaries,
                                    override val downloadsCount: Int,
                                    override val subscriptionsCount: Int) extends Application {
  override def toString: String = {
    val app: Application = this
    app.toJson.prettyPrint
  }
}

case class AppCount(count: Long) {
  override def toString: String = this.toJson.prettyPrint
}

case class BannerLocations(displayLocations: Set[String]) {
  override def toString: String = this.toJson.prettyPrint
}

case class BannersCount(count: Long) {
  override def toString: String = this.toJson.prettyPrint
}

case class QueryResults[+R](status: String, description: String, results: Option[List[R]]) extends StatusCode {
  def getResults: List[R] = results.getOrElse(List.empty)
}

object QueryResults {
  def apply[T](ls: List[T]): QueryResults[T] = {
    QueryResults(status = "S1000", description = "Success", results = Option(ls))
  }

  def apply[T](elem: T): QueryResults[T] = {
    QueryResults(status = "S1000", description = "Success", results = Option(List(elem)))
  }

  def empty[T] = QueryResults(status = "S1000", description = "Success", results = Option(List.empty[T]))
}

case class QueryResult[R](status: String, description: String, result: Option[R]) extends StatusCode {
  def getResult = result.get
}

object QueryResult {
  def apply[T](elem: T): QueryResult[T] = {
    QueryResult(status = "S1000", description = "Success", result = Some(elem))
  }

  def apply[T](statusCode: StatusCode, elem: T): QueryResult[T] = {
    QueryResult(statusCode.status, statusCode.description, Some(elem))
  }
}


/**
 * SDP NBL API response format of Registration,CMS,Governance,Subscription services.
 */

trait ServiceResp[T] extends StatusCode {

  def statusCode: String

  def statusDescription: Option[String]

  def status = statusCode

  def description = statusDescription.getOrElse("No description found")

  def copyFrom[S <: ServiceResp[T]](status: String, description: String): T
}

case class AuthProviders(availableProviders: List[String], preferredProvider: String) {
  override def toString: String = this.toJson.prettyPrint
}

case class ClientVersion(statusCode: String,
                         statusDescription: Option[String],
                         platform: Option[String],
                         newClientVersion: Option[String],
                         clientLocation: Option[String]) {
  override def toString: String = this.toJson.prettyPrint
}

case class AuthenticateByMPinReq(msisdn: String, mpin: String) {
  override def toString: String =
    s""" { "msisdn" : "$msisdn" , "mpin" : "*********" }"""
}

case class AuthenticateByMPinResp(statusCode: String, statusDescription: Option[String]) extends ServiceResp[AuthenticateByMPinResp] {
  override def copyFrom[S <: ServiceResp[AuthenticateByMPinResp]](status: String, description: String): AuthenticateByMPinResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class AuthenticateByPasswordReq(username: String, password: String) {
  override def toString: String =
    s""" { "username" : "$username" , "password" : "*********" }"""
}

case class AuthenticateByPasswordResp(statusCode: String, statusDescription: Option[String]) extends ServiceResp[AuthenticateByPasswordResp] {
  override def copyFrom[S <: ServiceResp[AuthenticateByPasswordResp]](status: String, description: String): AuthenticateByPasswordResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class AuthenticateByUsernameReq(username: String)

case class AuthenticateByMsisdnReq(msisdn: String)

case class AuthenticateResp(statusCode: String,
                            statusDescription: Option[String],
                            sessionId: Option[String],
                            mobileNo: Option[String],
                            additionalParameters: Option[Map[String, String]] = Some(Map.empty)
                             ) extends ServiceResp[AuthenticateResp] {
  override def toString: String = this.toJson.prettyPrint

  private def fromAdditionalParameters = additionalParameters.getOrElse(Map.empty[String, String])

  def getRemainingTime = fromAdditionalParameters.get("remainingTime")

  def msisdnVerified = if (fromAdditionalParameters.contains("msisdnVerified")) {
    fromAdditionalParameters.get("msisdnVerified").map(_.toString.toBoolean)
  } else if (fromAdditionalParameters.contains("msisdn-verified")) {
    fromAdditionalParameters.get("msisdn-verified").map(_.toString.toBoolean)
  } else {
    Some(true)
  }

  override def copyFrom[S <: ServiceResp[AuthenticateResp]](status: String, description: String): AuthenticateResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class AppBannersResp(statusCode: String,
                          statusDescription: Option[String],
                          banners: List[Map[String, String]]
                           ) extends ServiceResp[AppBannersResp] {
  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[AppBannersResp]](status: String, description: String): AppBannersResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class SessionValidationReq(sessionId: String) {
  override def toString: String = this.toJson.prettyPrint
}

case class SessionValidationResp(statusCode: String,
                                 statusDescription: Option[String],
                                 sessionId: Option[String],
                                 mobileNo: Option[String],
                                 username: Option[String]) extends ServiceResp[SessionValidationResp] {
  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[SessionValidationResp]](status: String, description: String): SessionValidationResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

object SessionValidationResp {
  def build(statusCode: StatusCode,
            sessionId: Option[String] = None,
            mobileNo: Option[String] = None,
            username: Option[String] = None): SessionValidationResp = {
    SessionValidationResp(statusCode.status, Some(statusCode.description), sessionId, mobileNo, username)
  }
}

case class PlatformContent(version: String, content: List[Map[String, String]] = List.empty) {
  override def toString: String = this.toJson.prettyPrint
}

case class DeviceContent(model: String, content: List[Map[String, String]] = List.empty) {
  override def toString: String = this.toJson.prettyPrint
}

case class PlatformsAndDevices(devices: Map[String, List[DeviceContent]] = Map.empty, platforms: Map[String, List[PlatformContent]] = Map.empty) {
  override def toString: String = this.toJson.prettyPrint
}

case class PlatformQuery(applicationIds: List[String], platform: Option[String] = None, version: Option[String] = None) {
  override def toString: String = this.toJson.prettyPrint
}


case class PlatformResults(statusCode: String, statusDescription: Option[String], result: Option[Map[String, PlatformsAndDevices]] = None) extends ServiceResp[PlatformResults] {

  /**
   * Retrieve list of supported downloadable content ids along with platform details for an application.
   * @param id application's id
   * @return downloadable content ids with platform
   */
  def platformsByAppId(id: String) = {
    getResult.getOrElse(id, PlatformsAndDevices()).
      platforms.map {
      case (pl, bin) => pl -> bin.map {
        c => Map(c.version -> c.content)
      }
    }
  }

  override def copyFrom[S <: ServiceResp[PlatformResults]](status: String, description: String): PlatformResults =
    this.copy(statusCode = status, statusDescription = Some(description))

  def getResult = result.getOrElse(Map.empty)

  override def toString: String = this.toJson.prettyPrint
}

case class PaymentInstrumentsReq(subscriberId: String, applicationId: String, instrumentType: String) {
  override def toString: String = this.toJson.prettyPrint
}

case class PaymentInstrumentsResp(statusCode: String, statusDescription: Option[String], paymentInstrumentList: Option[List[Map[String, String]]] = None)
  extends ServiceResp[PaymentInstrumentsResp] {
  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[PaymentInstrumentsResp]](status: String, description: String): PaymentInstrumentsResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class DownloadReq(applicationId: String, contentId: String, address: String, channel: Option[String]) {
  override def toString: String = this.toJson.prettyPrint
}

case class DownloadResp(statusCode: String,
                        statusDescription: Option[String],
                        wapUrl: Option[String],
                        paymentInstrumentList: Option[List[Map[String, String]]],
                        downloadRequestId: Option[String]) extends ServiceResp[DownloadResp] {
  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[DownloadResp]](status: String, description: String): DownloadResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class UpdateCountResp(count: Int) {
  override def toString: String = this.toJson.prettyPrint
}

case class DownloadChargeReq(downloadRequestId: String,
                             paymentInstrumentName: String,
                             additionalParams: Option[Map[String, String]] = None) {
  override def toString: String = this.toJson.prettyPrint
}

case class DownloadChargeResp(statusCode: String,
                              statusDescription: Option[String],
                              wapUrl: Option[String],
                              shortDescription: Option[String],
                              longDescription: Option[String]) extends ServiceResp[DownloadChargeResp] {
  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[DownloadChargeResp]](status: String, description: String): DownloadChargeResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

trait DownloadStatus {
  def status: String

  def requestedDate: DateTime

  def contentId: String
}

private[json] case class DownloadStatusJson(status: String, requestedDate: DateTime, contentId: String) extends DownloadStatus

case class AbuseReport(applicationID: String, senderAddress: String, message: String, medium: String) {
  override def toString: String = this.toJson.prettyPrint
}

case class AbuseReportResp(statusCode: String, statusDescription: Option[String]) extends ServiceResp[AbuseReportResp] {
  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[AbuseReportResp]](status: String, description: String): AbuseReportResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class SubscriptionAction(subscriberId: String, applicationId: String, password: String, action: Int) {
  override def toString: String = this.toJson.prettyPrint
}

case class SubscriptionActionResp(statusCode: String, statusDescription: Option[String]) extends ServiceResp[SubscriptionActionResp] {
  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[SubscriptionActionResp]](status: String, description: String): SubscriptionActionResp =
    this.copy(statusCode = status, statusDescription = Some(description))

}

/**
 * Subscription Status
 * Check hms.subscription.domain.SubscriptionStatus for more details
 */
object SubscriptionStatus extends Enumeration {
  type SubscriptionStatus = Value

  //New Subscription API Status
  val Registered = Value("REGISTERED")
  val UnRegistered = Value("UNREGISTERED")
  val PendingCharge = Value("PENDING CHARGE")

  //Old Subscription API Status
  val Initial = Value("INITIAL")
  val RegPending = Value("REG_PENDING")
  val TemporaryBlocked = Value("TEMPORARY_BLOCKED")
  val Blocked = Value("BLOCKED")
  val Trial = Value("TRIAL")

  //this is not a state in subscription, this is only used if user is not logged in or app is not a subscription app
  val NotAvailable = Value("NOT_AVAILABLE")
}

case class SubscriptionCountResp(statusCode: String, statusDescription: Option[String], baseSize: String) extends ServiceResp[SubscriptionCountResp] {

  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[SubscriptionCountResp]](status: String, description: String): SubscriptionCountResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class SubscriptionStatusResp(statusCode: String, statusDescription: Option[String], subscriptionStatus: Option[SubscriptionStatus.SubscriptionStatus]) extends ServiceResp[SubscriptionStatusResp] {

  import SubscriptionStatus._

  override def toString: String = this.toJson.prettyPrint

  def getSubscriptionStatus = subscriptionStatus match {
    case Some(Registered) | Some(Trial) => Registered
    case Some(UnRegistered) | Some(Blocked) => UnRegistered
    case Some(PendingCharge) | Some(RegPending) | Some(TemporaryBlocked) | Some(Initial) => PendingCharge
    case _ => NotAvailable
  }

  override def copyFrom[S <: ServiceResp[SubscriptionStatusResp]](status: String, description: String): SubscriptionStatusResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class SubscribedApps(statusCode: String, statusDescription: Option[String], appIds: List[String] = List.empty) extends ServiceResp[SubscribedApps] {
  override def toString: String = this.toJson.prettyPrint

  override def copyFrom[S <: ServiceResp[SubscribedApps]](status: String, description: String): SubscribedApps =
    this.copy(statusCode = status, statusDescription = Some(description))
}


object AppStatus extends Enumeration {
  type AppStatus = Value

  //appstore.apps collection status
  val Published = Value("Publish")
  val UnPublished = Value("Unpublish")

  // appstore.pending_approval_apps status
  val New = Value("New")

  lazy val valuesAsSet = values.map(_.toString).toSet
}

//Internal API messages

case class SearchQuery(searchText: Option[String] = None,
                       category: Option[String] = None,
                       status: Option[AppStatus.AppStatus] = None,
                       appId: Option[String] = None,
                       appType: Option[String] = None,
                       spName: Option[String] = None,
                       start: Int = 0,
                       limit: Int = 10) {
  override def toString: String = this.toJson.prettyPrint
}


case class UpdateReq(id: String,
                     category: Option[String] = None,
                     appIcon: Option[String] = None,
                     description: Option[String] = None,
                     publishName: Option[String] = None,
                     shortDesc: Option[String] = None,
                     labels: List[String] = List.empty,
                     screenShots: Map[String, String] = Map.empty,
                     instructions: Map[String, String] = Map.empty) {
  override def toString: String = this.toJson.prettyPrint
}

case class UpdateAppAndChargingReq(id: String,
                                   category: Option[String] = None,
                                   appIcon: Option[String] = None,
                                   description: Option[String] = None,
                                   publishName: Option[String] = None,
                                   shortDesc: Option[String] = None,
                                   labels: List[String] = List.empty,
                                   screenShots: Map[String, String] = Map.empty,
                                   instructions: Map[String, String] = Map.empty,
                                   chargingDescription: ChargingEditReq) {
  override def toString: String = this.toJson.prettyPrint
}


sealed trait UploadType {
  def name: String
}

case object Icon extends UploadType {
  val name = "Icon"
}

case object ScreenShot extends UploadType {
  val name = "ScreenShot"
}

case object MobileScreenShot extends UploadType {
  val name = "MobileScreenShot"
}

object UploadType {
  def withName(name: String): UploadType = name match {
    case Icon.name => Icon
    case ScreenShot.name => ScreenShot
    case MobileScreenShot.name => MobileScreenShot
    case _ => ScreenShot
  }
}

object FileType extends Enumeration {
  type FileType = Value
  val PNG = Value("png")
  val JPG = Value("jpg")

  lazy val valuesAsSet = values.map(_.toString).toSet
}

case class Base64String(data: String) {
  def toBytes: Array[Byte] = Base64.rfc2045().decodeFast(data)
}

object Base64String {
  def apply(bytes: Array[Byte]): Base64String = Base64String(Base64.rfc2045().encodeToString(bytes, false))
}

case class ImageUploadReq(fileNamePrefix: String,
                          fileType: FileType.FileType,
                          uploadType: UploadType,
                          data: Base64String) {
  s""" { "fileNamePrefix" : "$fileNamePrefix" , "fileType" : "$fileType" , "uploadType" : "$uploadType", data : "truncated ..." }"""
}

case class ImageUploadResp(uid: String) {
  override def toString: String = this.toJson.prettyPrint
}

object ImageUploadResp {
  def success(): ImageUploadResp = ImageUploadResp(UUID.randomUUID().toString)

  def failure(): ImageUploadResp = ImageUploadResp("-1")
}

case class AdUrl(@Key("_id") id: String, name: String, @Key("image-url") imageUrl: String, @Key("target-url") targetUrl: String)


case class UserDetailsByMsisdnReq(msisdn: String)

case class UserDetailsByUsernameReq(username: String)

case class UserDetailsResp(statusCode: String,
                           statusDescription: Option[String],
                           userStatus: Option[String],
                           userType: Option[String],
                           userRoles: Option[Set[String]],
                           additionalParams: Option[Map[String, String]]) extends ServiceResp[UserDetailsResp] {

  private def fromAdditionalParams = additionalParams.getOrElse(Map.empty)

  def getMsisdn = fromAdditionalParams.get("msisdn")

  def getMPin = fromAdditionalParams.get("mpin")

  def getEmail = fromAdditionalParams.get("email")

  def getUserName = fromAdditionalParams.get("username")

  override def copyFrom[S <: ServiceResp[UserDetailsResp]](status: String, description: String): UserDetailsResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class MPinChangeReq(msisdn: String, newPin: String, oldPin: Option[String])

case class MPinChangeResp(statusCode: String,
                          statusDescription: Option[String]) extends ServiceResp[MPinChangeResp] {
  override def copyFrom[S <: ServiceResp[MPinChangeResp]](status: String, description: String): MPinChangeResp =
    this.copy(statusCode = status, statusDescription = Some(description))
}

case class UserRegistrationReq(msisdn: String,
                               userName: String,
                               operator: String,
                               password: String,
                               birthDate: String,
                               firstName: Option[String],
                               lastName: Option[String],
                               verifyMsisdn: Boolean,
                               email: Option[String],
                               mpin: Option[String],
                               domain: String)

case class UserRegistrationResp(statusCode: String,
                                statusDescription: Option[String]) extends ServiceResp[UserRegistrationResp] {
  def copyFrom[S <: ServiceResp[UserRegistrationResp]](status: String, description: String): UserRegistrationResp =
    this.copy(status, Some(description))
}

case class MsisdnVerificationReq(
                                  username: String,
                                  verificationCode: String)

case class MsisdnVerificationResp(statusCode: String,
                                  statusDescription: Option[String]) extends ServiceResp[MsisdnVerificationResp] {
  def copyFrom[S <: ServiceResp[MsisdnVerificationResp]](status: String, description: String): MsisdnVerificationResp =
    this.copy(status, Some(description))
}

case class MsisdnVerificationCodeReq(username: String, msisdn: String)


case class UserDetailByUserNameResp(msisdn: String,
                                    lastName: Option[String],
                                    username: Option[String],
                                    birthDate: Option[String],
                                    firstName: Option[String],
                                    statusCode: String,
                                    displayName: Option[String],
                                    statusDescription: Option[String]) extends ServiceResp[UserDetailByUserNameResp] {

  def copyFrom[S <: ServiceResp[UserDetailByUserNameResp]](status: String, description: String): UserDetailByUserNameResp = {
    this.copy(statusCode = status, statusDescription = Some(description))
  }
}

case class UserProfileDetailsResp(msisdn: String,
                                  image: String,
                                  lastName: Option[String],
                                  username: Option[String],
                                  birthDate: Option[String],
                                  firstName: Option[String],
                                  email: Option[String],
                                  userType: String,
                                  statusCode: String,
                                  displayName: Option[String],
                                  statusDescription: Option[String]) extends ServiceResp[UserProfileDetailsResp] {

  def copyFrom[S <: ServiceResp[UserProfileDetailsResp]](status: String, description: String): UserProfileDetailsResp = {
    this.copy(statusCode = status, statusDescription = Some(description))
  }
}

case class DeveloperProfileDetailsResp(image: String,
                                       lastName: String,
                                       username: String,
                                       firstName: String,
                                       email: String,
                                       statusCode: String,
                                       statusDescription: Option[String]) extends ServiceResp[DeveloperProfileDetailsResp] {

  def copyFrom[S <: ServiceResp[DeveloperProfileDetailsResp]](status: String, description: String): DeveloperProfileDetailsResp = {
    this.copy(statusCode = status, statusDescription = Some(description))
  }
}

case class PushNotification(dispatchedTime: String,
                            requestId: String,
                            messageType: String,
                            messageContent: String,
                            activity: String,
                            data: JsValue
                             ) {
  override def toString: String = this.toJson.prettyPrint
}

case class PushNotificationsResp(
                                  status: String,
                                  messages: List[PushNotification]
                                  ) {
  override def toString: String = this.toJson.prettyPrint
}

case class UserProfileUpdateReq(firstName: String,
                                lastName: String,
                                username: String,
                                email: String,
                                image: String,
                                fileName: String)

case class UserProfileUpdateResp(statusCode: String,
                                 statusDescription: Option[String]) extends ServiceResp[UserProfileUpdateResp] {
  def copyFrom[S <: ServiceResp[UserProfileUpdateResp]](status: String, description: String): UserProfileUpdateResp =
    this.copy(status, Some(description))
}

case class TestResp(statusCode: String,
                    statusDescription: Option[String]) extends ServiceResp[TestResp] {
  def copyFrom[S <: ServiceResp[TestResp]](status: String, description: String): TestResp =
    this.copy(status, Some(description))
}


case class AppRatingAddReq(appId: String, rating: Int)

case class AppRatingAddResp(statusCode: String,
                            statusDescription: Option[String]) extends ServiceResp[AppRatingAddResp] {
  def copyFrom[S <: ServiceResp[AppRatingAddResp]](status: String, description: String): AppRatingAddResp =
    this.copy(status, Some(description))
}

case class AddOrRemoveAppsOfCategoryReq(appIds: List[String])

case class AddOrRemoveAppsOfCategoryResp(statusCode: String,
                                         statusDescription: Option[String]) extends ServiceResp[AddOrRemoveAppsOfCategoryResp] {
  def copyFrom[S <: ServiceResp[AddOrRemoveAppsOfCategoryResp]](status: String, description: String): AddOrRemoveAppsOfCategoryResp =
    this.copy(status, Some(description))
}

case class CategoryAddReq(name: String,
                          description: String,
                          image_url: String)

case class CategoryAddResp(statusCode: String,
                           statusDescription: Option[String]) extends ServiceResp[CategoryAddResp] {
  def copyFrom[S <: ServiceResp[CategoryAddResp]](status: String, description: String): CategoryAddResp =
    this.copy(status, Some(description))
}

case class CategoryEditReq(name: String,
                           description: Option[String],
                           image_url: Option[String])

case class CategoryEditResp(statusCode: String,
                            statusDescription: Option[String]) extends ServiceResp[CategoryEditResp] {
  def copyFrom[S <: ServiceResp[CategoryEditResp]](status: String, description: String): CategoryEditResp =
    this.copy(status, Some(description))
}

case class BannerAddReq(display_location: String,
                        link: String,
                        description: String,
                        image_url: String)

case class BannerAddResp(statusCode: String,
                         statusDescription: Option[String]) extends ServiceResp[BannerAddResp] {
  def copyFrom[S <: ServiceResp[BannerAddResp]](status: String, description: String): BannerAddResp =
    this.copy(status, Some(description))
}

case class UserAccountRecoveryReq(recoverText: String)

case class UserAccountRecoveryResp(statusCode: String,
                                   statusDescription: Option[String]) extends ServiceResp[UserAccountRecoveryResp] {
  def copyFrom[S <: ServiceResp[UserAccountRecoveryResp]](status: String, description: String): UserAccountRecoveryResp =
    this.copy(status, Some(description))
}

case class AppDownloadsCountReq(applicationId: String) {
  override def toString: String = this.toJson.prettyPrint
}

case class AppDownloadsCountResp(downloadsCount: Int) {
  override def toString: String = this.toJson.prettyPrint
}

case class ChargingData(ncsType: String, chargingType: String, amount: Option[String], direction: Option[String], frequency: Option[String]) {
  override def toString: String = this.toJson.prettyPrint
}

case class BannerLocation(display_location: String, name: String, min_width: Int, max_width: Int, min_height: Int, max_height: Int, size: Int) {
  override def toString: String = this.toJson.prettyPrint
}

case class ChargingDescriptionMessage(ncsType: String, messageFormat: String) {
  override def toString: String = this.toJson.prettyPrint
}

case class ChargingEditReq(appId: String, chargingData: List[ChargingData])

case class ChargingEditResp(statusCode: String, statusDescription: Option[String]) extends ServiceResp[ChargingEditResp] {
  def copyFrom[S <: ServiceResp[ChargingEditResp]](status: String, description: String): ChargingEditResp =
    this.copy(status, Some(description))
}

case class AutoLoginUserRegistrationReq(msisdn: String)

case class AutoLoginUserRegistrationResp(statusCode: String,
                                         statusDescription: Option[String]) extends ServiceResp[AutoLoginUserRegistrationResp] {
  def copyFrom[S <: ServiceResp[AutoLoginUserRegistrationResp]](status: String, description: String): AutoLoginUserRegistrationResp =
    this.copy(status, Some(description))
}

case class DeviceRegistrationReq(deviceId: String,
                                 userId: Option[String],
                                 platform: String,
                                 platformVersion: String,
                                 notificationToken: String)

case class NotificationResp(statusCode: String,
                            statusDescription: Option[String],
                            additionalParameters: Option[Map[String, String]] = Some(Map.empty)) extends ServiceResp[NotificationResp] {
  def copyFrom[S <: ServiceResp[NotificationResp]](status: String, description: String): NotificationResp =
    this.copy(status, Some(description))
}

case class DeviceRegistrationServerResp(deviceId: String,
                                        userId: String) {
}

case class NotifyReq(recipient: Recipient,
                     messageTitle: String,
                     messageContent: String,
                     requestId: String,
                     createdBy: String,
                     dispatchType: String)

case class Recipient(recipientType: String,
                     appId: Option[String]) {
  override def toString: String = this.toJson.prettyPrint
}

case class NotifyResp(requestId: String)

case class AppUpdateNotificationReq(requestId: String,
                                    appId: String,
                                    appName: String,
                                    buildVersion: String,
                                    status: String,
                                    dispatchType: String)

case class AppUpdateNotificationResp(status: String,
                                     taskId: String)

case class UserPasswordResetReq(msisdn: String)

case class UserPasswordResetResp(statusCode: String,
                                 statusDescription: Option[String]) extends ServiceResp[UserPasswordResetResp] {
  def copyFrom[S <: ServiceResp[UserPasswordResetResp]](status: String, description: String): UserPasswordResetResp =
    this.copy(status, Some(description))
}

