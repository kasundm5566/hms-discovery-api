package hms.appstore.api.json

import spray.json._
import reflect.ClassTag
import hms.scala.http.json.JsObjectReader
import org.joda.time.DateTime
import org.joda.time.format.ISODateTimeFormat
import spray.http.MultipartFormData
import scala.util.parsing.json.JSONArray
import hms.appstore.api.json.ImageUploadReq
import hms.appstore.api.json.ClientVersion
import hms.appstore.api.json.DownloadChargeResp
import hms.appstore.api.json.CategoryAddResp
import hms.appstore.api.json.PlatformResults
import hms.appstore.api.json.BannerAddReq
import hms.appstore.api.json.UserDetailByUserNameResp
import hms.appstore.api.json.StatusCodeJson
import hms.appstore.api.json.DownloadReq
import hms.appstore.api.json.CategoryEditReq
import hms.appstore.api.json.AppRatingAddResp
import hms.appstore.api.json.UserCommentReq
import hms.appstore.api.json.SubscriptionActionResp
import hms.appstore.api.json.AbuseReportResp
import scala.Some
import hms.appstore.api.json.Category
import hms.appstore.api.json.AppBannersResp
import hms.appstore.api.json.SubscribedApps
import hms.appstore.api.json.PaymentInstrumentsResp
import hms.appstore.api.json.PlatformQuery
import hms.appstore.api.json.PlatformsAndDevices
import hms.appstore.api.json.UserAccountRecoveryResp
import hms.appstore.api.json.MsisdnVerificationReq
import hms.appstore.api.json.AuthenticateResp
import hms.appstore.api.json.SessionValidationReq
import hms.appstore.api.json.AppCount
import hms.appstore.api.json.MsisdnVerificationCodeReq
import hms.appstore.api.json.AdUrl
import hms.appstore.api.json.AuthenticateByPasswordReq
import hms.appstore.api.json.CategoryEditResp
import hms.appstore.api.json.UserProfileUpdateReq
import hms.appstore.api.json.CategoryAddReq
import hms.appstore.api.json.SubscriptionStatusResp
import hms.appstore.api.json.UserRegistrationReq
import hms.appstore.api.json.AuthenticateByMPinReq
import hms.appstore.api.json.MPinChangeReq
import hms.appstore.api.json.BannerLocations
import hms.appstore.api.json.UserProfileDetailsResp
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryReq
import hms.appstore.api.json.DeviceContent
import hms.appstore.api.json.BannersCount
import hms.appstore.api.json.AbuseReport
import hms.appstore.api.json.UpdateReq
import hms.appstore.api.json.ParentCategory
import hms.appstore.api.json.PlatformContent
import hms.appstore.api.json.MsisdnVerificationResp
import hms.appstore.api.json.UserDetailsByMsisdnReq
import hms.appstore.api.json.UpdateCountResp
import hms.appstore.api.json.BannerAddResp
import hms.appstore.api.json.AppDownloadsCountReq
import hms.appstore.api.json.MPinChangeResp
import hms.appstore.api.json.AuthProviders
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryResp
import hms.appstore.api.json.UserAccountRecoveryReq
import hms.appstore.api.json.PaymentInstrumentsReq
import hms.appstore.api.json.UserRegistrationResp
import hms.appstore.api.json.AppDownloadsCountResp
import hms.appstore.api.json.UserDetailsByUsernameReq
import hms.appstore.api.json.Banner
import hms.appstore.api.json.SearchQuery
import hms.appstore.api.json.SubscriptionAction
import hms.appstore.api.json.AppRatingAddReq
import hms.appstore.api.json.UserDetailsResp
import hms.appstore.api.json.DownloadResp
import hms.appstore.api.json.UserProfileUpdateResp
import hms.appstore.api.json.DownloadStatusJson
import hms.appstore.api.json.DownloadChargeReq
import hms.appstore.api.json.AuthenticateByMsisdnReq
import hms.appstore.api.json.ImageUploadReq
import hms.appstore.api.json.ClientVersion
import hms.appstore.api.json.UpdateAppAndChargingReq
import hms.appstore.api.json.DownloadChargeResp
import hms.appstore.api.json.CategoryAddResp
import hms.appstore.api.json.PlatformResults
import hms.appstore.api.json.BannerAddReq
import hms.appstore.api.json.UserDetailByUserNameResp
import hms.appstore.api.json.StatusCodeJson
import hms.appstore.api.json.DownloadReq
import hms.appstore.api.json.CategoryEditReq
import hms.appstore.api.json.AppRatingAddResp
import hms.appstore.api.json.UserCommentReq
import hms.appstore.api.json.SubscriptionActionResp
import hms.appstore.api.json.AbuseReportResp
import scala.Some
import hms.appstore.api.json.Category
import hms.appstore.api.json.AppBannersResp
import hms.appstore.api.json.SubscribedApps
import hms.appstore.api.json.PaymentInstrumentsResp
import hms.appstore.api.json.PlatformQuery
import hms.appstore.api.json.PlatformsAndDevices
import hms.appstore.api.json.UserAccountRecoveryResp
import hms.appstore.api.json.MsisdnVerificationReq
import hms.appstore.api.json.AuthenticateResp
import hms.appstore.api.json.SessionValidationReq
import hms.appstore.api.json.AppCount
import hms.appstore.api.json.MsisdnVerificationCodeReq
import hms.appstore.api.json.AdUrl
import hms.appstore.api.json.AuthenticateByPasswordReq
import hms.appstore.api.json.CategoryEditResp
import hms.appstore.api.json.UserProfileUpdateReq
import hms.appstore.api.json.CategoryAddReq
import hms.appstore.api.json.ChargingData
import hms.appstore.api.json.SubscriptionStatusResp
import hms.appstore.api.json.ChargingEditReq
import hms.appstore.api.json.UserRegistrationReq
import hms.appstore.api.json.AuthenticateByMPinReq
import hms.appstore.api.json.MPinChangeReq
import hms.appstore.api.json.BannerLocations
import hms.appstore.api.json.UserProfileDetailsResp
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryReq
import hms.appstore.api.json.DeviceContent
import hms.appstore.api.json.BannersCount
import hms.appstore.api.json.AbuseReport
import hms.appstore.api.json.UpdateReq
import hms.appstore.api.json.ParentCategory
import hms.appstore.api.json.PlatformContent
import hms.appstore.api.json.MsisdnVerificationResp
import hms.appstore.api.json.UserDetailsByMsisdnReq
import hms.appstore.api.json.UpdateCountResp
import hms.appstore.api.json.ChargingEditResp
import hms.appstore.api.json.BannerAddResp
import hms.appstore.api.json.AppDownloadsCountReq
import hms.appstore.api.json.MPinChangeResp
import hms.appstore.api.json.AuthProviders
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryResp
import hms.appstore.api.json.UserAccountRecoveryReq
import hms.appstore.api.json.PaymentInstrumentsReq
import hms.appstore.api.json.UserRegistrationResp
import hms.appstore.api.json.AppDownloadsCountResp
import hms.appstore.api.json.UserDetailsByUsernameReq
import hms.appstore.api.json.Banner
import hms.appstore.api.json.SearchQuery
import hms.appstore.api.json.SubscriptionAction
import hms.appstore.api.json.AppRatingAddReq
import hms.appstore.api.json.UserDetailsResp
import hms.appstore.api.json.DownloadResp
import hms.appstore.api.json.UserProfileUpdateResp
import hms.appstore.api.json.DownloadStatusJson
import hms.appstore.api.json.DownloadChargeReq
import hms.appstore.api.json.AuthenticateByMsisdnReq
import hms.appstore.api.json.UpdateAppAndChargingReq
import hms.appstore.api.json.AutoLoginUserRegistrationResp
import hms.appstore.api.json.AppUpdateNotificationResp
import hms.appstore.api.json.NotifyResp
import hms.appstore.api.json.BannerAddReq
import hms.appstore.api.json.UserDetailByUserNameResp
import hms.appstore.api.json.StatusCodeJson
import hms.appstore.api.json.CategoryEditReq
import hms.appstore.api.json.PushNotificationsResp
import hms.appstore.api.json.AppRatingAddResp
import hms.appstore.api.json.UserCommentReq
import hms.appstore.api.json.SubscriptionActionResp
import hms.appstore.api.json.AbuseReportResp
import scala.Some
import hms.appstore.api.json.AppBannersResp
import hms.appstore.api.json.PaymentInstrumentsResp
import hms.appstore.api.json.SubscriptionCountResp
import hms.appstore.api.json.AppCount
import hms.appstore.api.json.AutoLoginUserRegistrationReq
import hms.appstore.api.json.CategoryEditResp
import hms.appstore.api.json.UserProfileUpdateReq
import hms.appstore.api.json.DeviceRegistrationReq
import hms.appstore.api.json.ChargingData
import hms.appstore.api.json.SubscriptionStatusResp
import hms.appstore.api.json.ChargingEditReq
import hms.appstore.api.json.UserRegistrationReq
import hms.appstore.api.json.AuthenticateByMPinReq
import hms.appstore.api.json.MPinChangeReq
import hms.appstore.api.json.BannerLocations
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryReq
import hms.appstore.api.json.BannersCount
import hms.appstore.api.json.AbuseReport
import hms.appstore.api.json.UpdateReq
import hms.appstore.api.json.UserDetailsByMsisdnReq
import hms.appstore.api.json.UpdateCountResp
import hms.appstore.api.json.ChargingEditResp
import hms.appstore.api.json.BannerAddResp
import hms.appstore.api.json.AuthProviders
import hms.appstore.api.json.UserAccountRecoveryReq
import hms.appstore.api.json.PaymentInstrumentsReq
import hms.appstore.api.json.UserDetailsByUsernameReq
import hms.appstore.api.json.Recipient
import hms.appstore.api.json.AppRatingAddReq
import hms.appstore.api.json.UserDetailsResp
import hms.appstore.api.json.ChargingDescriptionMessage
import hms.appstore.api.json.DownloadStatusJson
import hms.appstore.api.json.UserPasswordResetReq
import hms.appstore.api.json.DownloadChargeReq
import hms.appstore.api.json.AuthenticateByMsisdnReq
import hms.appstore.api.json.ImageUploadReq
import hms.appstore.api.json.ClientVersion
import hms.appstore.api.json.DownloadChargeResp
import hms.appstore.api.json.CategoryAddResp
import hms.appstore.api.json.PlatformResults
import hms.appstore.api.json.DownloadReq
import hms.appstore.api.json.BannerLocation
import hms.appstore.api.json.NotificationResp
import hms.appstore.api.json.Category
import hms.appstore.api.json.SubscribedApps
import hms.appstore.api.json.PlatformQuery
import hms.appstore.api.json.PlatformsAndDevices
import hms.appstore.api.json.UserPasswordResetResp
import hms.appstore.api.json.UserAccountRecoveryResp
import hms.appstore.api.json.MsisdnVerificationReq
import hms.appstore.api.json.SessionValidationReq
import hms.appstore.api.json.AuthenticateResp
import hms.appstore.api.json.DeviceRegistrationServerResp
import hms.appstore.api.json.MsisdnVerificationCodeReq
import hms.appstore.api.json.AdUrl
import hms.appstore.api.json.DeveloperProfileDetailsResp
import hms.appstore.api.json.AuthenticateByPasswordReq
import hms.appstore.api.json.CategoryAddReq
import hms.appstore.api.json.UserProfileDetailsResp
import hms.appstore.api.json.NotifyReq
import hms.appstore.api.json.DeviceContent
import hms.appstore.api.json.ParentCategory
import hms.appstore.api.json.PlatformContent
import hms.appstore.api.json.MsisdnVerificationResp
import hms.appstore.api.json.AppDownloadsCountReq
import hms.appstore.api.json.MPinChangeResp
import hms.appstore.api.json.AddOrRemoveAppsOfCategoryResp
import hms.appstore.api.json.AppDownloadsCountResp
import hms.appstore.api.json.UserRegistrationResp
import hms.appstore.api.json.AppUpdateNotificationReq
import hms.appstore.api.json.Banner
import hms.appstore.api.json.SearchQuery
import hms.appstore.api.json.SubscriptionAction
import hms.appstore.api.json.DownloadResp
import hms.appstore.api.json.UserProfileUpdateResp
import hms.appstore.api.json.PushNotification

/**
 * Discovery API message serialization formats
 */
trait MsgJsonProtocol extends DefaultJsonProtocol with JsObjectReader {

  def fromStatusDetailOrDescription(value: JsValue): Option[String] = {

    val statusDetail = fromJsValue[Option[String]](value, "statusDetail")
    val statusDescription = fromJsValue[Option[String]](value, "statusDescription")
    statusDescription.fold(statusDetail)(Some.apply)
  }

  implicit val statusCodeFmt = new RootJsonFormat[StatusCode] {
    def write(statusCode: StatusCode): JsValue = {
      JsObject(
        "statusCode" -> JsString(statusCode.status),
        "statusDescription" -> JsString(statusCode.description)
      )
    }

    def read(value: JsValue): StatusCode = {
      StatusCodeJson(
        status = fromJsValue[String](value, "statusCode"),
        description = fromJsValue[String](value, "statusDescription")
      )
    }
  }


  implicit val categoryFmt = new RootJsonFormat[Category] {
    def write(c: Category): JsValue = {
      JsObject(
        "id" -> JsString(c.id),
        "name" -> JsString(c.name),
        "description" -> JsString(c.description),
        "image_url" -> JsString(c.image_url),
        "appIds" -> JsArray(c.appIds.map(_.toJson).toList),
        "position" -> JsNumber(c.position)
      )
    }

    def read(value: JsValue): Category = {
      Category(
        id = fromJsValue[String](value, "id"),
        name = fromJsValue[String](value, "name"),
        description = fromJsValue[String](value, "description"),
        image_url = fromJsValue[String](value, "image_url"),
        appIds = fromJsValue[List[String]](value, "appIds"),
        position = fromJsValue[Int](value, "position")
      )
    }
  }

  implicit val bannerFmt = new RootJsonFormat[Banner] {
    def write(b: Banner): JsValue = {
      JsObject(
        "id" -> JsNumber(b.id),
        "display_location" -> JsString(b.display_location),
        "link" -> JsString(b.link),
        "description" -> JsString(b.description),
        "image_url" -> JsString(b.image_url)
      )
    }

    def read(value: JsValue): Banner = {
      Banner(
        id = fromJsValue[Int](value, "id"),
        display_location = fromJsValue[String](value, "display_location"),
        link = fromJsValue[String](value, "link"),
        description = fromJsValue[String](value, "description"),
        image_url = fromJsValue[String](value, "image_url")
      )
    }
  }

  implicit val superCategoryFmt = new RootJsonFormat[ParentCategory] {
    def write(s: ParentCategory): JsValue = {
      JsObject(
        "id" -> JsString(s.id),
        "category" -> JsString(s.category)
      )
    }

    def read(value: JsValue): ParentCategory = {
      ParentCategory(
        id = fromJsValue[String](value, "id"),
        category = fromJsValue[String](value, "category")
      )
    }
  }


  implicit val userCommentFmt = new RootJsonFormat[UserComment] {
    def write(uc: UserComment): JsValue = {
      JsObject(
        "comment-id" -> uc.commentId.toJson,
        "username" -> uc.username.map(formatUsername).toJson,
        "image" -> JsString(uc.image.getOrElse("")),
        "app-id" -> JsString(uc.appId),
        "comments" -> JsString(uc.comments),
        "date-time" -> JsNumber(uc.dateTime)
      )
    }

    def read(value: JsValue): UserComment = {
      UserCommentReq(
        commentId = fromJsValue[Option[String]](value, "comment-id"),
        username = fromJsValue[Option[String]](value, "username"),
        image = fromJsValue[Option[String]](value, "image"),
        appId = fromJsValue[String](value, "app-id"),
        comments = fromJsValue[String](value, "comments"),
        dateTime = fromJsValue[Long](value, "date-time"),
        abuse = fromJsValue[Option[Boolean]](value, "abuse")
      )
    }
  }

  implicit val authProviderFmt = jsonFormat2(AuthProviders)

  implicit val clientPlatformFmt = jsonFormat5(ClientVersion)

  implicit val appCountFmt = jsonFormat1(AppCount)

  implicit val bannerLocationsFmt = jsonFormat1(BannerLocations)

  implicit val bannersCountFmt = jsonFormat1(BannersCount)

  implicit object SubscriptionStatusFormat extends RootJsonFormat[SubscriptionStatus.SubscriptionStatus] {
    def write(obj: SubscriptionStatus.SubscriptionStatus): JsValue = JsString(obj.toString)

    def read(json: JsValue): SubscriptionStatus.SubscriptionStatus = json match {
      case JsString(a) => SubscriptionStatus.withName(a)
      case _ => deserializationError(s"JsString expected, but found $json")
    }
  }

  implicit object AppStatusFormat extends RootJsonFormat[AppStatus.AppStatus] {
    def write(obj: AppStatus.AppStatus): JsValue = JsString(obj.toString)

    def read(json: JsValue): AppStatus.AppStatus = json match {
      case JsString(a) => AppStatus.withName(a)
      case _ => deserializationError(s"JsString expected, but found $json")
    }
  }

  implicit val dateTimeFmt = new RootJsonFormat[DateTime] {
    def write(obj: DateTime): JsValue = JsString(ISODateTimeFormat.dateTime().print(obj))

    def read(json: JsValue): DateTime = json match {
      case JsString(dt) => ISODateTimeFormat.dateTime().parseDateTime(dt)
      case _@value => deserializationError(s"String expected, but found $value")
    }
  }

  implicit val downloadStatusFmt = new RootJsonFormat[DownloadStatus] {
    def write(ds: DownloadStatus): JsValue = {
      JsObject(
        "download-status" -> JsString(ds.status),
        "requested-date" -> ds.requestedDate.toJson,
        "content-id" -> JsString(ds.contentId)
      )
    }

    def read(value: JsValue): DownloadStatus = {
      DownloadStatusJson(
        status = fromJsValue[String](value, "download-status"),
        requestedDate = fromJsValue[DateTime](value, "requested-date"),
        contentId = fromJsValue[String](value, "content-id")
      )
    }
  }


  def formatUsername(username: String): String = username

  implicit val appFmt = new RootJsonFormat[Application] {
    def write(app: Application): JsValue = {
      JsObject(
        "id" -> JsString(app.id),
        "name" -> JsString(app.displayName),
        "category" -> app.category.toJson,
        "app-icon" -> JsString(app.appIcon),
        "app-banners" -> app.appBanners.toJson,
        "usage" -> JsNumber(app.usage),
        "currency" -> JsString(app.currency),
        "developer" -> JsString(formatUsername(app.developer)),
        "rating" -> JsNumber(app.rating),
        "rate_count" -> JsNumber(app.rate_count),
        "labels" -> app.labels.toJson,
        "description" -> JsString(app.description),
        "short-description" -> JsString(app.shortDesc),
        "app-types" -> app.appTypes.toJson,
        "app-screenshots" -> app.screenShots.toJson,
        "instructions" -> app.instructions.toJson,
        "ncs" -> app.ncs.toJson,
        "subscription" -> JsBoolean(app.subscription),
        "downloadable" -> JsBoolean(app.downloadable),
        "subscription-status" -> app.subscriptionStatus.toJson,
        "download-status" -> app.downloadStatus.toJson,
        "charging-label" -> JsString(app.chargingLabel),
        "charging-details" -> JsString(app.chargingDetails),
        "user-comments" -> app.userComments.toJson,
        "status" -> app.status.toJson,
        "requested-date" -> app.requestedDate.toJson,
        "modified-date" -> app.modifiedDate.toJson,
        "remarks" -> app.remarks.toJson,
        "downloadable-binaries" -> app.downloadableBinaries.toJson,
        "downloadsCount" -> app.downloadsCount.toJson,
        "subscriptionsCount" -> app.subscriptionsCount.toJson
      )
    }

    def read(value: JsValue) = {
      new ApplicationJson(
        id = fromJsValue[String](value, "id"),
        displayName = fromJsValue[String](value, "name"),
        category = fromJsValue[String](value, "category"),
        appIcon = fromJsValue[String](value, "app-icon"),
        appBanners = fromJsValue[Option[List[Map[String, String]]]](value, "app-banners"),
        usage = fromJsValue[Long](value, "usage"),
        currency = fromJsValue[String](value, "currency"),
        developer = fromJsValue[String](value, "developer"),
        rating = fromJsValue[Double](value, "rating"),
        rate_count = fromJsValue[Int](value, "rate_count"),
        labels = fromJsValue[List[String]](value, "labels"),
        description = fromJsValue[String](value, "description"),
        shortDesc = fromJsValue[String](value, "short-description"),
        appTypes = fromJsValue[List[String]](value, "app-types"),
        screenShots = fromJsValue[List[Map[String, String]]](value, "app-screenshots"),
        instructions = fromJsValue[Map[String, String]](value, "instructions"),
        ncs = fromJsValue[Set[String]](value, "ncs"),
        subscription = fromJsValue[Boolean](value, "subscription"),
        downloadable = fromJsValue[Boolean](value, "downloadable"),
        subscriptionStatus = fromJsValue[SubscriptionStatus.SubscriptionStatus](value, "subscription-status"),
        downloadStatus = fromJsValue[Option[DownloadStatus]](value, "download-status"),
        chargingLabel = fromJsValue[String](value, "charging-label"),
        chargingDetails = fromJsValue[String](value, "charging-details"),
        userComments = fromJsValue[List[UserComment]](value, "user-comments"),
        status = fromJsValue[AppStatus.AppStatus](value, "status"),
        requestedDate = fromJsValue[DateTime](value, "requested-date"),
        modifiedDate = fromJsValue[DateTime](value, "modified-date"),
        remarks = fromJsValue[String](value, "remarks"),
        downloadableBinaries = fromJsValue[Application#DnBinaries](value, "downloadable-binaries"),
        downloadsCount = fromJsValue[Int](value, "downloadsCount"),
        subscriptionsCount = fromJsValue[Int](value, "subscriptionsCount")
      )
    }
  }

  implicit def queryResultsFmt[T: JsonFormat : ClassTag] = new RootJsonFormat[QueryResults[T]] {
    def write(obj: QueryResults[T]): JsValue = {
      JsObject(
        "statusCode" -> JsString(obj.status),
        "statusDescription" -> JsString(obj.description),
        "results" -> obj.results.toJson
      )
    }

    def read(value: JsValue): QueryResults[T] = {
      QueryResults(
        status = fromJsValue[String](value, "statusCode"),
        description = fromJsValue[String](value, "statusDescription"),
        results = fromJsValue[Option[List[T]]](value, "results")
      )
    }
  }

  implicit def queryResultFmt[T: JsonFormat : ClassTag] = new RootJsonFormat[QueryResult[T]] {
    def write(obj: QueryResult[T]): JsValue = {
      JsObject(
        "statusCode" -> JsString(obj.status),
        "statusDescription" -> JsString(obj.description),
        "result" -> obj.result.toJson
      )
    }

    def read(value: JsValue): QueryResult[T] = {
      QueryResult(
        status = fromJsValue[String](value, "statusCode"),
        description = fromJsValue[String](value, "statusDescription"),
        result = fromJsValue[Option[T]](value, "result")
      )
    }
  }

  implicit val authenticateByPasswordReqFmt = jsonFormat2(AuthenticateByPasswordReq)
  implicit val authenticateByMPinReqFmt = jsonFormat2(AuthenticateByMPinReq)
  implicit val authenticateByMsisdnReqFmt = jsonFormat1(AuthenticateByMsisdnReq)

  implicit val authenticateRespFmt = jsonFormat5(AuthenticateResp)

  implicit val sessionReqFmt = jsonFormat1(SessionValidationReq)
  implicit val sessionRespFmt = jsonFormat5(SessionValidationResp.apply)

  implicit val deviceContentFmt = jsonFormat2(DeviceContent)
  implicit val platformContentFmt = jsonFormat2(PlatformContent)
  implicit val platformsAndDevicesFmt = jsonFormat2(PlatformsAndDevices)

  implicit val platformQueryFmt = jsonFormat3(PlatformQuery)
  implicit val platformRespFmt = jsonFormat3(PlatformResults)

  implicit val piRespFmt = jsonFormat3(PaymentInstrumentsResp)

  implicit val piReqFmt = new RootJsonFormat[PaymentInstrumentsReq] {
    def write(pi: PaymentInstrumentsReq): JsValue = {
      JsObject(
        "applicationId" -> JsString(pi.applicationId),
        "subscriberId" -> JsString(pi.subscriberId),
        "type" -> JsString(pi.instrumentType)
      )
    }

    def read(value: JsValue): PaymentInstrumentsReq = {
      PaymentInstrumentsReq(
        applicationId = fromJsValue[String](value, "applicationId"),
        subscriberId = fromJsValue[String](value, "subscriberId"),
        instrumentType = fromJsValue[String](value, "type")
      )
    }
  }

  implicit val downloadReqFmt = jsonFormat4(DownloadReq)

  implicit val downloadRespFmt = new RootJsonFormat[DownloadResp] {
    def write(obj: DownloadResp): JsValue = {
      JsObject(
        "statusCode" -> JsString(obj.statusCode),
        "statusDescription" -> obj.statusDescription.toJson,
        "wapUrl" -> obj.wapUrl.toJson,
        "paymentInstrumentList" -> obj.paymentInstrumentList.toJson,
        "downloadRequestId" -> obj.downloadRequestId.toJson
      )
    }

    def read(value: JsValue): DownloadResp = {
      DownloadResp(
        statusCode = fromJsValue[String](value, "statusCode"),
        statusDescription = fromStatusDetailOrDescription(value),
        wapUrl = fromJsValue[Option[String]](value, "wapUrl"),
        paymentInstrumentList = fromJsValue[Option[List[Map[String, String]]]](value, "paymentInstrumentList"),
        downloadRequestId = fromJsValue[Option[String]](value, "downloadRequestId")
      )
    }
  }

  implicit val appBannersRespFmt = new RootJsonFormat[AppBannersResp] {
    def write(obj: AppBannersResp): JsValue = {
      JsObject(
        "statusCode" -> JsString(obj.statusCode),
        "statusDescription" -> obj.statusDescription.toJson,
        "banners" -> obj.banners.toJson
      )
    }

    def read(value: JsValue): AppBannersResp = {
      AppBannersResp(
        statusCode = fromJsValue[String](value, "statusCode"),
        statusDescription = Option(fromJsValue[String](value, "statusDescription")),
        banners = fromJsValue[List[Map[String, String]]](value, "banners")
      )
    }
  }

  implicit val downloadChargeReqFmt = jsonFormat3(DownloadChargeReq)

  implicit val downloadChargeRespFmt = new RootJsonFormat[DownloadChargeResp] {
    def write(obj: DownloadChargeResp): JsValue = {
      JsObject(
        "statusCode" -> JsString(obj.statusCode),
        "statusDescription" -> obj.statusDescription.toJson,
        "wapUrl" -> obj.wapUrl.toJson,
        "shortDescription" -> obj.shortDescription.toJson,
        "longDescription" -> obj.longDescription.toJson
      )
    }

    def read(value: JsValue): DownloadChargeResp = {
      DownloadChargeResp(
        statusCode = fromJsValue[String](value, "statusCode"),
        statusDescription = fromStatusDetailOrDescription(value),
        wapUrl = fromJsValue[Option[String]](value, "wapUrl"),
        shortDescription = fromJsValue[Option[String]](value, "shortDescription"),
        longDescription = fromJsValue[Option[String]](value, "longDescription")
      )
    }
  }

  implicit val updateCountRespFmt = jsonFormat1(UpdateCountResp)

  implicit val abuseReportFmt = jsonFormat4(AbuseReport)
  implicit val abuseReportRespFmt = jsonFormat2(AbuseReportResp)

  implicit object SubscribedAppsFormat extends RootJsonFormat[SubscribedApps] {
    def write(a: SubscribedApps) = JsObject(
      "statusCode" -> JsString(a.statusCode),
      "statusDescription" -> a.statusDescription.toJson,
      "subscribed-apps" -> JsArray(a.appIds.map(JsString(_)))
    )

    def read(value: JsValue) = {
      SubscribedApps(
        statusCode = fromJsValue[String](value, "statusCode"),
        statusDescription = fromStatusDetailOrDescription(value),
        appIds = fromJsValue[List[String]](value, "subscribed-apps")
      )
    }
  }

  implicit val subReqFmt = jsonFormat4(SubscriptionAction)

  implicit val subRespFmt = new RootJsonFormat[SubscriptionActionResp] {
    def write(a: SubscriptionActionResp) = JsObject(
      "statusCode" -> JsString(a.statusCode),
      "statusDescription" -> a.statusDescription.toJson
    )

    def read(value: JsValue) = {
      SubscriptionActionResp(
        statusCode = fromJsValue[String](value, "statusCode"),
        statusDescription = fromStatusDetailOrDescription(value)
      )
    }
  }

  implicit val subStatusFmt = jsonFormat3(SubscriptionStatusResp)
  implicit val subCountStatusFmt = jsonFormat3(SubscriptionCountResp)


  //Internal API Messages formats

  implicit object UploadTypeFormat extends RootJsonFormat[UploadType] {
    def write(obj: UploadType): JsValue = JsString(obj.name)

    def read(json: JsValue): UploadType = json match {
      case JsString(a) => UploadType.withName(a)
      case _ => deserializationError(s"JsString expected, but found $json")
    }
  }

  implicit object FileTypeFormat extends RootJsonFormat[FileType.FileType] {
    def write(obj: FileType.FileType): JsValue = JsString(obj.toString)

    def read(json: JsValue): FileType.FileType = json match {
      case JsString(a) => FileType.withName(a)
      case _ => deserializationError(s"JsString expected, but found $json")
    }
  }

  implicit object Base64StringFormat extends RootJsonFormat[Base64String] {
    def write(obj: Base64String): JsValue = JsString(obj.data)

    def read(json: JsValue): Base64String = json match {
      case JsString(a) => Base64String(a)
      case _ => deserializationError(s"JsString expected, but found $json")
    }
  }


  implicit object CreateUserReqFormat extends RootJsonFormat[UserRegistrationReq] {
    def write(obj: UserRegistrationReq) = JsObject(
      "domain" -> JsString(obj.domain),
      "msisdn" -> JsString(obj.msisdn),
      "operator" -> JsString(obj.operator),
      "username" -> JsString(obj.userName),
      "password" -> JsString(obj.password),
      "last-name" -> JsString(obj.lastName.getOrElse("")),
      "first-name" -> JsString(obj.firstName.getOrElse("")),
      "birth-date" -> JsString(obj.birthDate),
      "verify-msisdn" -> JsBoolean(obj.verifyMsisdn),
      "email" -> JsString(obj.email.getOrElse("")),
      "mpin" -> JsString(obj.mpin.getOrElse(""))
    )

    def read(json: JsValue): UserRegistrationReq = {
      UserRegistrationReq(
        domain = fromJsValue[String](json, "domain"),
        msisdn = fromJsValue[String](json, "msisdn"),
        operator = fromJsValue[String](json, "operator"),
        userName = fromJsValue[String](json, "username"),
        password = fromJsValue[String](json, "password"),
        lastName = fromJsValue[Option[String]](json, "last-name"),
        firstName = fromJsValue[Option[String]](json, "first-name"),
        birthDate = fromJsValue[String](json, "birth-date"),
        verifyMsisdn = fromJsValue[Boolean](json, "verify-msisdn"),
        email = fromJsValue[Option[String]](json, "email"),
        mpin = fromJsValue[Option[String]](json, "mpin")
      )
    }
  }

  implicit object ChargingEditReqFormat extends RootJsonFormat[ChargingEditReq] {
    def write(obj: ChargingEditReq) = JsObject(
      "appId" -> JsString(obj.appId),
      "chargingData" -> JsArray(obj.chargingData.map(_.toJson).toList)
    )

    def read(json: JsValue): ChargingEditReq = {
      ChargingEditReq(
        appId = fromJsValue[String](json, "appId"),
        chargingData = fromJsValue[List[ChargingData]](json, "chargingData")
      )
    }
  }

  implicit object CreateConsumerUserResFormat extends RootJsonFormat[UserRegistrationResp] {
    def write(obj: UserRegistrationResp): JsValue = {
      JsObject(
        "statusCode" -> JsString(obj.statusCode),
        "statusDescription" -> obj.statusDescription.toJson
      )
    }

    def read(json: JsValue): UserRegistrationResp = {
      UserRegistrationResp(
        statusCode = fromJsValue[String](json, "statusCode"),
        statusDescription = fromJsValue[Option[String]](json, "statusDescription")
      )
    }
  }

  implicit object MsisdnVerificationReqFormat extends RootJsonFormat[MsisdnVerificationReq] {
    def write(obj: MsisdnVerificationReq): JsValue = {
      JsObject(
        "username" -> JsString(obj.username),
        "msisdn-verification-code" -> JsString(obj.verificationCode)
      )
    }

    def read(json: JsValue): MsisdnVerificationReq = {
      MsisdnVerificationReq(
        username = fromJsValue[String](json, "username"),
        verificationCode = fromJsValue[String](json, "msisdn-verification-code")
      )
    }
  }

  implicit object MsisdnVerificationRespFormat extends RootJsonFormat[MsisdnVerificationResp] {

    def write(obj: MsisdnVerificationResp): JsObject = JsObject(
      "statusCode" -> JsString(obj.statusCode),
      "statusDescription" -> obj.statusDescription.toJson
    )

    def read(json: JsValue): MsisdnVerificationResp = {
      MsisdnVerificationResp(
        statusCode = fromJsValue[String](json, "statusCode"),
        statusDescription = fromJsValue[Option[String]](json, "statusDescription")
      )
    }
  }

  implicit object MsisdnVerificationCodeReqFormat extends RootJsonFormat[MsisdnVerificationCodeReq] {
    def write(obj: MsisdnVerificationCodeReq): JsObject = JsObject(
      "username" -> JsString(obj.username),
      "msisdn" -> JsString(obj.msisdn)
    )

    def read(json: JsValue): MsisdnVerificationCodeReq = {
      MsisdnVerificationCodeReq(
        username = fromJsValue[String](json, "username"),
        msisdn = fromJsValue[String](json, "msisdn")
      )
    }
  }

  /*  implicit object userProfileUpdateReqFormat extends RootJsonFormat[UserProfileUpdateReq]{
      def write(obj: UserProfileUpdateReq): JsObject = JsObject(
        "msisdn" -> JsString(obj.msisdn),
        "image" -> JsString(obj.image),
        "lastName" -> JsString(obj.lastName),
        "firstName" -> JsString(obj.firstName),
        "email" -> JsString(obj.email)
      )

      def read(json: JsValue): UserProfileUpdateReq = {
        UserProfileUpdateReq(
          msisdn = fromJsValue[String](json, "msisdn"),
          image = fromJsValue[String](json, "image"),
          lastName = fromJsValue[String](json, "lastName"),
          firstName = fromJsValue[String](json, "firstName"),
          email = fromJsValue[String](json, "email")
        )
      }
    }*/

  implicit object userAccountRecoveryReqFormat extends RootJsonFormat[UserAccountRecoveryReq] {
    def write(obj: UserAccountRecoveryReq): JsObject = JsObject(
      "recover-text" -> JsString(obj.recoverText)
    )

    def read(json: JsValue): UserAccountRecoveryReq = UserAccountRecoveryReq(
      recoverText = fromJsValue[String](json, "recover-text")
    )
  }

  /*  implicit object userProfileUpdateReqFormat extends RootJsonFormat[UserProfileUpdateReq] {
      def write(obj: UserProfileUpdateReq): JsObject = JsObject(
        "msisdn" -> JsString(obj.msisdn),
        "lastName" -> JsString(obj.lastName),
        "firstName" -> JsString(obj.firstName),
        "email" -> JsString(obj.email),
        "image" -> JsString(obj.image.toString)
      )

      def read(json: JsValue): UserProfileUpdateReq = UserProfileUpdateReq(
        msisdn = fromJsValue[String](json, "msisdn"),
        lastName = fromJsValue[String](json, "lastName"),
        firstName = fromJsValue[String](json, "firstName"),
        email = fromJsValue[String](json, "email"),
        image = fromJsValue[String](json, "image")
      )
    }*/

  implicit object userAccountRecoveryRespFormat extends RootJsonFormat[UserAccountRecoveryResp] {

    def write(obj: UserAccountRecoveryResp): JsObject = JsObject(
      "statusCode" -> JsString(obj.statusCode),
      "statusDescription" -> obj.statusDescription.toJson
    )

    def read(json: JsValue): UserAccountRecoveryResp = UserAccountRecoveryResp(
      statusCode = fromJsValue[String](json, "statusCode"),
      statusDescription = fromJsValue[Option[String]](json, "statusDescription")
    )
  }

  implicit object appDownloadsCountReqFormat extends RootJsonFormat[AppDownloadsCountReq] {
    def write(obj: AppDownloadsCountReq): JsObject = JsObject(
      "applicationId" -> JsString(obj.applicationId)
    )

    def read(json: JsValue): AppDownloadsCountReq = AppDownloadsCountReq(
      applicationId = fromJsValue[String](json, "applicationId")
    )
  }

  implicit object appDownloadsCountRespFormat extends RootJsonFormat[AppDownloadsCountResp] {

    def write(obj: AppDownloadsCountResp): JsObject = JsObject(
      "downloadsCount" -> JsNumber(obj.downloadsCount)
    )

    def read(json: JsValue): AppDownloadsCountResp = AppDownloadsCountResp(
      downloadsCount = fromJsValue[Int](json, "downloadsCount")
    )
  }

  implicit object CreateAutoLoginUserReqFormat extends RootJsonFormat[AutoLoginUserRegistrationReq] {
    def write(obj: AutoLoginUserRegistrationReq) = JsObject(
      "msisdn" -> JsString(obj.msisdn)
    )

    def read(json: JsValue): AutoLoginUserRegistrationReq = {
      AutoLoginUserRegistrationReq(
        msisdn = fromJsValue[String](json, "msisdn")
      )
    }
  }

  implicit object PushNotificationsRespFormat extends RootJsonFormat[PushNotificationsResp] {
    def write(obj: PushNotificationsResp) = JsObject(
      "statusCode" -> JsString(obj.status),
      "messages" -> JsArray(obj.messages.map(_.toJson).toList)
    )

    def read(json: JsValue): PushNotificationsResp = {
      PushNotificationsResp(
        status = fromJsValue[String](json, "status"),
        messages = fromJsValue[List[PushNotification]](json,"messages")
      )
    }
  }

  implicit object PushNotificationFormat extends RootJsonFormat[PushNotification] {
    def write(obj: PushNotification) = JsObject(
      "dispatchedTime" -> JsString(obj.dispatchedTime),
      "requestId" -> JsString(obj.requestId),
      "messageType" -> JsString(obj.messageType),
      "messageContent" -> JsString(obj.messageContent),
      "activity" -> JsString(obj.activity),
      "data" -> JsonParser(obj.data.toString())
    )

    def read(json: JsValue): PushNotification = {
      PushNotification(
        dispatchedTime = fromJsValue[String](json, "dispatchedTime"),
        requestId = fromJsValue[String](json, "requestId"),
        messageType = fromJsValue[String](json, "messageType"),
        messageContent = fromJsValue[String](json, "messageContent"),
        activity = fromJsValue[String](json, "activity"),
        data = if (fromJsValue[String](json, "data").isEmpty) JsonParser("{}") else JsonParser(fromJsValue[String](json, "data"))
      )
    }
  }

  implicit object CreateAutoLoginUserResFormat extends RootJsonFormat[AutoLoginUserRegistrationResp] {
    def write(obj: AutoLoginUserRegistrationResp): JsValue = {
      JsObject(
        "statusCode" -> JsString(obj.statusCode),
        "statusDescription" -> obj.statusDescription.toJson
      )
    }

    def read(json: JsValue): AutoLoginUserRegistrationResp = {
      AutoLoginUserRegistrationResp(
        statusCode = fromJsValue[String](json, "statusCode"),
        statusDescription = fromJsValue[Option[String]](json, "statusDescription")
      )
    }
  }

  implicit object NotifyAllReqFormat extends RootJsonFormat[NotifyReq] {
    def write(obj: NotifyReq) = JsObject(
      "recipient" -> obj.recipient.toJson,
      "messageTitle" -> JsString(obj.messageTitle),
      "messageContent" -> JsString(obj.messageContent),
      "requestId" -> JsString(obj.requestId),
      "createdBy" -> JsString(obj.createdBy),
      "dispatchType" -> JsString(obj.dispatchType)
    )

    def read(json: JsValue): NotifyReq = {
      NotifyReq(
        recipient = fromJsValue[Recipient](json, "recipient"),
        messageTitle = fromJsValue[String](json, "messageTitle"),
        messageContent = fromJsValue[String](json, "messageContent"),
        requestId = fromJsValue[String](json, "requestId"),
        createdBy = fromJsValue[String](json, "createdBy"),
        dispatchType = fromJsValue[String](json, "dispatchType")
      )
    }
  }

  implicit object UserPasswordResetReqFormat extends RootJsonFormat[UserPasswordResetReq] {
    def write(obj: UserPasswordResetReq) = JsObject(
      "msisdn" -> JsString(obj.msisdn)
    )

    def read(json: JsValue): UserPasswordResetReq = {
      UserPasswordResetReq(
        msisdn = fromJsValue[String](json, "msisdn")
      )
    }
  }

  implicit object UserPasswordResetResFormat extends RootJsonFormat[UserPasswordResetResp] {
    def write(obj: UserPasswordResetResp): JsValue = {
      JsObject(
        "statusCode" -> JsString(obj.statusCode),
        "statusDescription" -> obj.statusDescription.toJson
      )
    }

    def read(json: JsValue): UserPasswordResetResp = {
      UserPasswordResetResp(
        statusCode = fromJsValue[String](json, "statusCode"),
        statusDescription = fromJsValue[Option[String]](json, "statusDescription")
      )
    }
  }

  implicit val searchQueryFmt = jsonFormat8(SearchQuery)
  implicit val updateAppReqFmt = jsonFormat9(UpdateReq)
  implicit val updateAppAndChargingReqFmt = jsonFormat10(UpdateAppAndChargingReq)
  implicit val imageUploadReqFmt = jsonFormat4(ImageUploadReq)
  implicit val imageUploadRespFmt = jsonFormat1(ImageUploadResp.apply)
  implicit val adUrlFmt = jsonFormat4(AdUrl)

  implicit val userDetailsByMsisdnReqFmt = jsonFormat1(UserDetailsByMsisdnReq)
  implicit val userDetailsByUsernameReqFmt = jsonFormat1(UserDetailsByUsernameReq)

  implicit val userDetailsRespFmt = jsonFormat6(UserDetailsResp)

  implicit val mPinChangeReqFmt = jsonFormat3(MPinChangeReq)
  implicit val mPinChangeRespFmt = jsonFormat2(MPinChangeResp)

  implicit val userDetailsByUsernameRespFmt = jsonFormat8(UserDetailByUserNameResp)

  implicit val userProfileDetailsRespFmt = jsonFormat11(UserProfileDetailsResp)

  implicit val userProfileUpdateReqFmt = jsonFormat6(UserProfileUpdateReq)
  implicit val userProfileUpdateRespFmt = jsonFormat2(UserProfileUpdateResp)

  implicit val categoryEditReqFmt = jsonFormat3(CategoryEditReq)
  implicit val categoryEditRespFmt = jsonFormat2(CategoryEditResp)

  implicit val categoryAddReqFmt = jsonFormat3(CategoryAddReq)
  implicit val categoryAddRespFmt = jsonFormat2(CategoryAddResp)

  implicit val addAppsToCategoryReqFmt = jsonFormat1(AddOrRemoveAppsOfCategoryReq)
  implicit val addAppsToCategoryRespFmt = jsonFormat2(AddOrRemoveAppsOfCategoryResp)

  implicit val bannerAddReqFmt = jsonFormat4(BannerAddReq)
  implicit val bannerAddRespFmt = jsonFormat2(BannerAddResp)

  implicit val appRatingAddReqFmt = jsonFormat2(AppRatingAddReq)
  implicit val appRatingAddRespFmt = jsonFormat2(AppRatingAddResp)

  implicit val chargingDataFmt = jsonFormat5(ChargingData)

  implicit val chargingDescriptionMessageFmt = jsonFormat2(ChargingDescriptionMessage)

  implicit val chargingEditReqFmt = jsonFormat2(ChargingEditReq)
  implicit val chargingEditRespFmt = jsonFormat2(ChargingEditResp)

  implicit val bannerLocationFmt = jsonFormat7(BannerLocation)

  implicit val developerProfileDetailsRespFmt = jsonFormat7(DeveloperProfileDetailsResp)

  implicit val deviceRegistrationReqFmt = jsonFormat5(DeviceRegistrationReq)
  implicit val deviceRegistrationRespFmt = jsonFormat3(NotificationResp)

  implicit val deviceRegistrationServerRespFmt = jsonFormat2(DeviceRegistrationServerResp)

  implicit val recipientFmt = jsonFormat2(Recipient)
  implicit val notifyReqFmt = jsonFormat6(NotifyReq)
  implicit val notifyAllRespFmt = jsonFormat1(NotifyResp)

  implicit val appUpdateNotificationReqFmt = jsonFormat6(AppUpdateNotificationReq)
  implicit val appUpdateNotificationRespFmt = jsonFormat2(AppUpdateNotificationResp)

}

object MsgJsonProtocol extends MsgJsonProtocol
